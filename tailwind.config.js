import typography from '@tailwindcss/typography';

/** @type {import('tailwindcss').Config} */
export default {
    future: {
      hoverOnlyWhenSupported: true,
    },
    content: [
      './resources/**/*.antlers.html',
      './resources/**/*.blade.php',
      './resources/**/*.vue',
      './resources/js/*.js',
      './content/**/*.md'
    ],
    theme: {
      extend: {
        fontFamily: {
          primary: 'poppins',
        },
        colors: {
          'c1' : '#0A0745',
          'c2' : '#4BB061',
          'c3' : '#69C1DB'
        },
        width: {
          'wrapper' : '1200px',
          'main' : '768px',
          'side' : '384px'
        },
        maxWidth:{
          'wrapper' : '1200px'
        },
        animation: {
          'row-add': 'row_add 0.6s linear',
          'htmx': 'htmx 1s ease infinite',
          'loader': 'loader 0.2s ease',
          'boop': 'boop 0.4s ease',
  
        },
        keyframes: {
          row_add: {
            '0%'  : { transform: 'scale(0)' , height: '0px', border: '0'  },
            '60%' : { transform: 'scale(1)',  height: '48px', border: '0'   },
            '80%' : {  border: '4px solid #4BB061'   },
            '100%' : { border: '0'  },
          },
          htmx: {
            '0%'  : { left: '0%' , right: '100%'},
            '100%'  : { left: '100%' , right: '-90%'},
          },
          loader: {
            '0%'  : { transform: 'scale(0)'},
            '100%' : { transform: 'scale(1)'},
          },
          boop: {
            '0%'  : { transform: 'scale(1)'},
            '50%'  : { transform: 'scale(1.2)'},
            '100%' : { transform: 'scale(1)'},
          },
        }
      },
    },
    plugins: [
      typography,
    ],
  }