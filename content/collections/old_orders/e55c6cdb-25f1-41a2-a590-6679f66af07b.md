---
id: e55c6cdb-25f1-41a2-a590-6679f66af07b
blueprint: old_orders
order_number: 8
order_date: '2023-06-12'
company: f29dee5f-1953-487d-b766-b1d3baafb2f0
location: 16741830-9ac1-40cf-8b25-ea9ba2f30305
user: 'Lisa Hofman'
total: 189.5
products:
  -
    product: f6a02908-bf2b-415a-a473-7faa835c59d6
    product_number: null
    product_title: '54010 Air-O-Kit vulling LEMON'
    price: 5.5
    amount: 3
  -
    product: efd00f14-9977-4fe6-b178-328c8854790d
    product_number: 74416
    product_title: 'Oppervlakte alcohol 80%'
    price: 17.3
    amount: '10'
title: '8'
---
