---
id: d11dc577-0009-4307-a114-432147f40d13
blueprint: old_orders
order_number: 11
order_date: '2023-10-23'
company: 4e00303b-45d0-49d8-9aad-8a8ef0e22948
location: e6713451-367d-43ef-91ec-87d84a4c609f
user: Bijdehand
total: 468.26
products:
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Handzeep Eco 500ml eurofles vierkant'
    price: 2.015
    amount: '84'
  -
    product: ef5f9bb6-c599-418d-a6a7-eb614a440256
    product_number: CART00914
    product_title: 'Handschoen Nitril Blauw maat M'
    price: 2.99
    amount: '100'
title: '11'
---
