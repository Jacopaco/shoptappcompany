---
id: 4b26e9a1-0ee2-44b5-af76-03e0b1a055b0
blueprint: old_orders
order_number: 1
order_date: '2022-11-09'
company: d7654c12-3145-4478-8fa3-970c4b764ae1
location: 9841050c-5fbe-43be-96c8-a2d6a5435499
user: 'Barbara Schokkenbroek'
total: 76.75
products:
  -
    product: 52e9b7f6-aa99-4d4b-92b4-569c40c19698
    product_number: 2181
    product_title: 'Poetsrol mini coreless cellulose 1laags'
    price: 25.25
    amount: 1
  -
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    product_number: null
    product_title: 'Handschoen Nitril ongepoederd maat M'
    price: 5.15
    amount: 10
title: '1'
---
