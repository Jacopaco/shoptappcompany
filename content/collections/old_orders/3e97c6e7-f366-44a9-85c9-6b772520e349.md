---
id: 3e97c6e7-f366-44a9-85c9-6b772520e349
blueprint: old_orders
order_number: 4
order_date: '2023-10-24'
company: a933caf6-a138-462a-a9ad-76b9c7e0344b
location: 52571907-9205-4d01-962e-3ba319c62abe
user: 'Moniek van den Berkmortel'
total: 23.32
products:
  -
    product: 669dcce7-2e55-49a8-9845-acd2bbe5bbb8
    product_number: 98918
    product_title: 'TAPP desinfectie 1000 ml Untouchable OS'
    price: 11.66
    amount: 2
title: '4'
---
