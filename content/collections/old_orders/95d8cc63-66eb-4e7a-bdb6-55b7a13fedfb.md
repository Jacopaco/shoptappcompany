---
id: 95d8cc63-66eb-4e7a-bdb6-55b7a13fedfb
blueprint: old_orders
order_number: 2
order_date: '2022-08-01'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 663.24
products:
  -
    product: f6481173-6519-4ed1-a2f4-af29dfccee0a
    product_number: 1881
    product_title: 'Onderzoeksbankrollen 185x39cm (6rl)'
    price: 89.2
    amount: 2
  -
    product: 4d0ac429-a708-4e16-b626-95c8a1a1de7c
    product_number: 188
    product_title: 'Onderzoeksbankrollen 185x59cm (4rl)'
    price: 87.46
    amount: 2
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken 60x80 zwart (500st)'
    price: 27.2
    amount: 1
  -
    product: 5149804c-a262-4eec-a9ed-8adfb5ccc2d1
    product_number: 6533
    product_title: 'Afvalzakken 70x110cm  (250st)'
    price: 33.2
    amount: 1
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 3
  -
    product: b593cd3e-3366-4451-805a-56d449c9c305
    product_number: 2155
    product_title: 'Servetten cellulose 1laags'
    price: 23.6
    amount: 1
  -
    product: 18624551-ee5b-4bd7-91aa-105e1d7441de
    product_number: 2170
    product_title: 'Facial tissue cellulose 2laags'
    price: 29.95
    amount: 1
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 4
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Navulling handzeep Eurofles (12x500ml)'
    price: 26.28
    amount: 2
title: '2'
---
