---
id: a9dc18cc-a0bd-4f2b-b1f0-1aef0ed0157f
blueprint: old_orders
order_number: 93
order_date: '2024-03-26'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 307.761
products:
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken T50 60 x 80 | 50 liter grijs'
    price: 27.2
    amount: 1
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 2
  -
    product: b593cd3e-3366-4451-805a-56d449c9c305
    product_number: 2155
    product_title: 'Servetten cellulose 1laags'
    price: 23.6
    amount: 1
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 3
  -
    product: 7839e7f7-98f6-438e-b33d-ad6e586c2a4c
    product_number: '151057'
    product_title: 'Afvalzak Blauw 80x110cm 10x20 stuks T60'
    price: 59.211
    amount: 1
  -
    product: 6e4ee919-9b3c-4d37-b977-83e89f57c55f
    product_number: '6540  1881'
    product_title: 'Onderzoeksbankrollen cellulose 2 lgs 39 cm x 150 m 6st'
    price: 95.71
    amount: 1
title: '93'
---
