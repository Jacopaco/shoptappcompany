---
id: db851fa2-7c4c-460e-a561-afe76a34dace
blueprint: old_orders
order_number: 17
order_date: '2024-02-12'
company: b0c021bd-503f-4f45-84de-dcc0dfc662ac
location: c4053601-ae30-4783-9fdc-a26afacce843
user: 'Barbara van der Kuil'
total: 328.5
products:
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.5
    amount: 11
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 19
    amount: '6'
title: '17'
---
