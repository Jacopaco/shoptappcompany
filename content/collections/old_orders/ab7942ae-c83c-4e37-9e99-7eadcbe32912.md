---
id: ab7942ae-c83c-4e37-9e99-7eadcbe32912
blueprint: old_orders
order_number: 12
order_date: '2023-06-21'
company: 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
location: 6463e748-7dae-432d-ad50-a774b95850ef
user: 'Huize Winterdijk'
total: 2019.78
products:
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Navulling handzeep Eurofles (12x500ml)'
    price: 26.28
    amount: '1'
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.95
    amount: '24'
  -
    product: d55bb5e5-9756-403e-8e61-c4c8af703dde
    product_number: 2100
    product_title: 'Toiletpapier cellulose 3laags/250 vel'
    price: 39.22
    amount: '6'
  -
    product: 67cb64a7-f786-4077-a3d5-de7a3bcf4c8d
    product_number: null
    product_title: 'Sterilium 500ml'
    price: 4.25
    amount: '10'
  -
    product: 03691d31-8783-43bc-8c61-078d324b8f09
    product_number: null
    product_title: '74416-74106 10 flessen oppervlakte desinfectie 80%'
    price: 60
    amount: '1'
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 44.61
    amount: '5'
  -
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    product_number: null
    product_title: 'Handschoen Nitril ongepoederd blauw maat M'
    price: 2.89
    amount: '40'
  -
    product: b3ac5eff-8aa3-4430-a86d-43f7d85384d7
    product_number: null
    product_title: 'Handschoen Nitril ongepoederd blauw maat L'
    price: 2.89
    amount: '40'
  -
    product: 602bac8b-0e0e-40f8-99db-8655f138c363
    product_number: null
    product_title: '2255 Afvalzakken 45 x 50 / 8 liter transparant'
    price: 29.3
    amount: '1'
  -
    product: 8361caa4-24a2-4976-b700-e83d0e3cd2ae
    product_number: null
    product_title: 'Clax Revoflow Enzi 4ltr. (3st.)'
    price: 399.87
    amount: '1'
  -
    product: cdf92aab-be2e-4297-8464-130588757243
    product_number: null
    product_title: '080553 Ecolab MAXX Windus C2'
    price: 6.78
    amount: '5'
  -
    product: 358981c6-0252-4787-b9b5-796446b7984e
    product_number: null
    product_title: '080554 Ecolab MAXX Magic2'
    price: 8.91
    amount: '10'
  -
    product: 11e0b489-419b-4de8-8c00-fce73263e29e
    product_number: null
    product_title: '117456 Ecolab MAXX Into Active'
    price: 8.42
    amount: '15'
  -
    product: d15d8fff-b9ab-4bf2-b4c5-f7c28279d800
    product_number: null
    product_title: 'Vochtige washandjes'
    price: 22.08
    amount: '2'
title: '12'
---
