---
id: b1760600-8847-42cb-9b0b-42e030417cd9
blueprint: old_orders
order_number: 90
order_date: '2024-03-07'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 244.25
products:
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken T50 60 x 80 | 50 liter grijs'
    price: 27.2
    amount: 1
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 2
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 4
  -
    product: 6e4ee919-9b3c-4d37-b977-83e89f57c55f
    product_number: '6540  1881'
    product_title: 'Onderzoeksbankrollen cellulose 2 lgs 39 cm x 150 m 6st'
    price: 95.71
    amount: 1
title: '90'
---
