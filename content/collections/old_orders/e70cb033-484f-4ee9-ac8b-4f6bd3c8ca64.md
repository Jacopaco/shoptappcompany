---
id: e70cb033-484f-4ee9-ac8b-4f6bd3c8ca64
blueprint: old_orders
order_number: 4
order_date: '2023-01-10'
company: 4e00303b-45d0-49d8-9aad-8a8ef0e22948
location: e6713451-367d-43ef-91ec-87d84a4c609f
user: Bijdehand
total: 954.91
products:
  -
    product: 07cc6d9b-3615-4155-aea6-e02ae993ab3c
    product_number: 2142
    product_title: 'Handdoekjes interfold cellulose 3laags'
    price: 43.06
    amount: 5
  -
    product: 0d5889b2-0f69-458d-809e-86a318357384
    product_number: 2186
    product_title: 'Poetsrol midi cellulose coreless 1laags'
    price: 27.07
    amount: 8
  -
    product: 03691d31-8783-43bc-8c61-078d324b8f09
    product_number: null
    product_title: '74416-74106 10 flessen oppervlakte desinfectie 80%'
    price: 60
    amount: 5
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 44.61
    amount: 5
title: '4'
---
