---
id: 478539ac-703d-4eeb-b3b8-c4c631d33730
blueprint: old_orders
order_number: 6
order_date: '2023-09-25'
company: 342ca54a-98c1-451c-8ea8-f377ef5519e9
location: 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
user: 'Facilitair Lagarde Groep'
total: 78
products:
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.5
    amount: 4
title: '6'
---
