---
id: 6e25a1af-e0ae-4d7f-8a2a-06aa40ae4ad1
blueprint: old_orders
order_number: 20
order_date: '2024-01-05'
company: 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
location: 6463e748-7dae-432d-ad50-a774b95850ef
user: 'Huize Winterdijk'
total: 1129.9556
products:
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.95
    amount: 10
  -
    product: 5eb9747d-85f6-48fb-8c86-513aaa7cba6a
    product_number: 84600100KF
    product_title: 'RAINBOW sanitairreiniger 6x1 liter'
    price: 15.762
    amount: 1
  -
    product: 89cd32f0-b85a-4daa-8f0f-ccbfc5a67afd
    product_number: '072399'
    product_title: 'Ecolab Kristalin Bio'
    price: 29.6638
    amount: 1
  -
    product: 358981c6-0252-4787-b9b5-796446b7984e
    product_number: '080554'
    product_title: 'Ecolab MAXX Magic2 - 1 L'
    price: 11.4878
    amount: 5
  -
    product: 452106cb-b413-4c63-a906-dfed04ccad9a
    product_number: '080555'
    product_title: 'Ecolab MAXX Magic2 - 2 x 5 L'
    price: 108.8714
    amount: 2
  -
    product: 11e0b489-419b-4de8-8c00-fce73263e29e
    product_number: '117456'
    product_title: 'Ecolab MAXX Into Active '
    price: 10.863
    amount: 5
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Handzeep Eco 500ml eurofles vierkant'
    price: 2.015
    amount: 5
  -
    product: e39d2cd3-371d-4c36-868d-bac06041db6d
    product_number: CART00915
    product_title: 'Vochtige washandjes 8st'
    price: 1.17
    amount: '120'
  -
    product: 76a530ba-ee5d-4ee2-905b-4c7f36b68d9b
    product_number: '080554'
    product_title: 'Ecolab MAXX Magic2 - 1 L'
    price: 11.4878
    amount: 10
  -
    product: c36ca4d2-5eee-4561-8d75-6eecef5404e9
    product_number: '83990010'
    product_title: 'Bio afvalzak 50x60cm 15 my 40x10st'
    price: 71.91
    amount: 1
  -
    product: ef5f9bb6-c599-418d-a6a7-eb614a440256
    product_number: CART00914
    product_title: 'Handschoen Nitril Blauw maat M'
    price: 2.99
    amount: 70
  -
    product: e7c40665-d355-4737-a5ad-e00e3eeeee99
    product_number: CART00913
    product_title: 'Handschoen Nitril Blauw maat L'
    price: 2.99
    amount: 3
title: '20'
---
