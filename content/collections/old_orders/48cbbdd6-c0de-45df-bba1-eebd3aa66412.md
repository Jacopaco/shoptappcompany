---
id: 48cbbdd6-c0de-45df-bba1-eebd3aa66412
blueprint: old_orders
order_number: 35
order_date: '2023-01-30'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 200.04
products:
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken 60x80 zwart (500st)'
    price: 27.2
    amount: 1
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 2
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 4
  -
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    product_number: null
    product_title: 'Handschoen Nitril ongepoederd blauw maat M'
    price: 5.15
    amount: 5
  -
    product: 9a204dcc-30dc-467a-afd6-f4c4b917bca7
    product_number: null
    product_title: 'Handschoenen nitril maat XL'
    price: 5.15
    amount: 5
title: '35'
---
