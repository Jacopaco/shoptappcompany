---
id: cd6ce6c6-ddbe-47a8-ae31-2fa4da09327e
blueprint: old_orders
order_number: 13
order_date: '2023-08-29'
company: 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
location: 6463e748-7dae-432d-ad50-a774b95850ef
user: 'Huize Winterdijk'
total: 788.18
products:
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken T50 60 x 80 | 50 liter grijs'
    price: 31.915
    amount: '1'
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.95
    amount: '15'
  -
    product: 5149804c-a262-4eec-a9ed-8adfb5ccc2d1
    product_number: 6533
    product_title: 'Afvalzakken T50 - 70 x 110 cm blauw'
    price: 25.545
    amount: '3'
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 54.34
    amount: '7'
title: '13'
---
