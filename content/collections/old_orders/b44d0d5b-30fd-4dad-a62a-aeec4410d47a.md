---
id: b44d0d5b-30fd-4dad-a62a-aeec4410d47a
blueprint: old_orders
order_number: 2
order_date: '2023-01-11'
company: 8ed96843-92e3-4f5e-bf96-e054d740cea6
location: 175073cd-07d9-40df-9e81-927b690b393b
user: 'Hetty Hartman'
total: 432
products:
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 43.2
    amount: 10
title: '2'
---
