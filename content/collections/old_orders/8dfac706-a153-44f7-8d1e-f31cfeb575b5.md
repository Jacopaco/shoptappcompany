---
id: 8dfac706-a153-44f7-8d1e-f31cfeb575b5
blueprint: old_orders
order_number: 72
order_date: '2023-09-07'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 31ab6798-7e55-4a96-8c41-c9d35bf02694
user: 'Radiotherapiegroep Locatie Deventer'
total: 838.42
products:
  -
    product: 30fdc1fb-a49e-4204-abe8-4f6a244e6d16
    product_number: 2002130
    product_title: 'Zakken zwart  58x100'
    price: 37.2
    amount: 1
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 6
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 6
  -
    product: fd618e8a-780d-41c0-9307-0403bebf9d88
    product_number: '6549'
    product_title: 'Onderzoeksbankrollen 9x50M cellulose 2laags niet geperforeerd'
    price: 79
    amount: 7
title: '72'
---
