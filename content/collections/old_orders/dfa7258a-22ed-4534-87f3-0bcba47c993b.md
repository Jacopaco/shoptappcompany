---
id: dfa7258a-22ed-4534-87f3-0bcba47c993b
blueprint: old_orders
order_number: 7
order_date: '2022-09-06'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 213.8
products:
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken 60x80 zwart (500st)'
    price: 27.2
    amount: 4
  -
    product: 5149804c-a262-4eec-a9ed-8adfb5ccc2d1
    product_number: 6533
    product_title: 'Afvalzakken 70x110cm  (250st)'
    price: 33.2
    amount: 2
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 2
title: '7'
---
