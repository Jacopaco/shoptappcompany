---
id: 212cd3be-bbe2-4f2e-adbf-20fce2ffe878
blueprint: old_orders
order_number: 4
order_date: '2023-04-04'
company: d267d33a-5684-4194-856e-727b2f4b79ba
location: 4e8302b0-f18d-40a8-865e-7040d27f3085
user: 'CBS de Ontdekking'
total: 69.5
products:
  -
    product: 3ad3e27b-5377-4466-a94f-0cfae74f7ea8
    product_number: 98914-TAPP
    product_title: 'TAPP Foamzeep 2,5 liter'
    price: 6.95
    amount: 10
title: '4'
---
