---
id: 9cef2c0d-da62-40eb-a536-bb9f6ec1dbd5
blueprint: old_orders
order_number: 64
order_date: '2023-06-13'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 216.99
products:
  -
    product: f6481173-6519-4ed1-a2f4-af29dfccee0a
    product_number: 1881
    product_title: 'Onderzoeksbankrollen 185x39cm (6rl)'
    price: 89.2
    amount: 1
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 2
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 3
  -
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    product_number: null
    product_title: 'Handschoen Nitril ongepoederd blauw maat M'
    price: 5.15
    amount: 5
title: '64'
---
