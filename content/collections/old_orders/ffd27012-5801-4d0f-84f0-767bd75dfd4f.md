---
id: ffd27012-5801-4d0f-84f0-767bd75dfd4f
blueprint: old_orders
order_number: 9
order_date: '2022-09-13'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 31ab6798-7e55-4a96-8c41-c9d35bf02694
user: 'Radiotherapiegroep Locatie Deventer'
total: 410.39
products:
  -
    product: 8caa6467-0748-4c0d-8e61-194f03e93178
    product_number: 2302070
    product_title: 'HDPE pedaalemmerzakken (50cm x 55cm)  Transparant T15'
    price: 23.2
    amount: 2
  -
    product: 30fdc1fb-a49e-4204-abe8-4f6a244e6d16
    product_number: 2002130
    product_title: 'Zakken wit 58x100'
    price: 37.2
    amount: 2
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 7
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 7
title: '9'
---
