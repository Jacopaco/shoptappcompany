---
id: 3a3df785-1abd-40ad-aecc-efafad0cbbb2
blueprint: old_orders
order_number: 8
order_date: '2023-11-20'
company: 342ca54a-98c1-451c-8ea8-f377ef5519e9
location: 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
user: 'Facilitair Lagarde Groep'
total: 126.32
products:
  -
    product: 0e5c32ce-c8c3-41f6-a2f4-f657698be113
    product_number: 2200
    product_title: 'Toiletpapier bulkpack cellulose 2laags'
    price: 31.58
    amount: 4
title: '8'
---
