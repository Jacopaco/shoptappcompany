---
id: e5f0361b-9beb-4ed1-8eaa-7b23aefe9dd6
blueprint: old_orders
order_number: 5
order_date: '2023-06-09'
company: 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
location: e6090873-ae14-4d62-b52a-da6089045f90
user: Fikret
total: 173.5
products:
  -
    product: 2899e303-20e4-4b77-8510-4ef285aa9dc1
    product_number: 2161
    product_title: 'Handdoekrol Matic cellulose 2laags'
    price: 34.7
    amount: 5
title: '5'
---
