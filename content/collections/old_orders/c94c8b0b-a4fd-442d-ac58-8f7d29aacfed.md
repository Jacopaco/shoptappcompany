---
id: c94c8b0b-a4fd-442d-ac58-8f7d29aacfed
blueprint: old_orders
order_number: 54
order_date: '2023-04-20'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 31ab6798-7e55-4a96-8c41-c9d35bf02694
user: 'Radiotherapiegroep Locatie Deventer'
total: 172.84
products:
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 2
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 4
  -
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    product_number: null
    product_title: 'Handschoen Nitril ongepoederd blauw maat M'
    price: 5.15
    amount: 10
title: '54'
---
