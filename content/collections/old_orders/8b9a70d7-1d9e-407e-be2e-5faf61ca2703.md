---
id: 8b9a70d7-1d9e-407e-be2e-5faf61ca2703
blueprint: old_orders
order_number: 92
order_date: '2023-12-18'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 641.08
products:
  -
    product: 5149804c-a262-4eec-a9ed-8adfb5ccc2d1
    product_number: 6533
    product_title: 'Afvalzakken T50 - 70 x 110 cm blauw '
    price: 33.2
    amount: 3
  -
    product: a9845e3d-b168-4a57-9ea8-943a6aa45c09
    product_number: 2105
    product_title: 'Toiletpapier cellulose 2laags/400 vel'
    price: 22.07
    amount: 4
  -
    product: b593cd3e-3366-4451-805a-56d449c9c305
    product_number: 2155
    product_title: 'Servetten cellulose 1laags'
    price: 23.6
    amount: 1
  -
    product: 18624551-ee5b-4bd7-91aa-105e1d7441de
    product_number: 2170
    product_title: 'Facial tissue cellulose 2laags'
    price: 29.95
    amount: 1
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 4
  -
    product: 6e4ee919-9b3c-4d37-b977-83e89f57c55f
    product_number: '6540  1881'
    product_title: 'Onderzoeksbankrollen cellulose 2 lgs 39 cm x 150 m 6st'
    price: 95.71
    amount: 2
  -
    product: 9442651c-7418-4214-bb06-8f6f5ba4f1ae
    product_number: '6543 - 188'
    product_title: 'Onderzoeksbankrollen cellulose 2 lgs 59 cm x 150 m 4st'
    price: 88.91
    amount: 1
  -
    product: ef5f9bb6-c599-418d-a6a7-eb614a440256
    product_number: CART00914
    product_title: 'Handschoen Nitril Blauw maat M'
    price: 2.99
    amount: 3
  -
    product: e7c40665-d355-4737-a5ad-e00e3eeeee99
    product_number: CART00913
    product_title: 'Handschoen Nitril Blauw maat L'
    price: 2.99
    amount: 3
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Handzeep Eco 500ml eurofles vierkant'
    price: 2.015
    amount: 12
title: '92'
---
