---
id: 370b69c4-eddc-4121-bfab-be3e18b8f1cb
blueprint: old_orders
order_number: 1
order_date: '2024-02-15'
company: 50f975cc-aa02-494f-81aa-fbbd2580a184
location: 9e892bb6-3012-4729-9233-8cf35bd7901d
user: 'Corieke Klaassen'
total: 132.69
products:
  -
    product: 98734a4a-2cee-475b-ab3a-a0f06c67fcfd
    product_number: 2141
    product_title: 'Handdoekjes interfold cellulose 2laags'
    price: 44.23
    amount: 3
title: '1'
---
