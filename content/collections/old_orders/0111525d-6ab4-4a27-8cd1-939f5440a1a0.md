---
id: 0111525d-6ab4-4a27-8cd1-939f5440a1a0
blueprint: old_orders
order_number: 74
order_date: '2023-09-18'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 4cef8fa7-db4a-4e92-acb7-b845094905f8
user: 'Radiotherapiegroep locatie Arnhem'
total: 106.26
products:
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.3
    amount: 3
  -
    product: 6e4ee919-9b3c-4d37-b977-83e89f57c55f
    product_number: '6540  1881'
    product_title: 'Onderzoeksbankrollen cellulose 2 lgs 39 cm x 150 m 6st'
    price: 0
    amount: 1
  -
    product: f654be00-85dc-4050-8cb8-decdaf8e4f11
    product_number: 98995
    product_title: 'Handalcohol 500ml 70%'
    price: 4.03
    amount: 12
title: '74'
---
