---
id: f5614f89-cc13-4220-8176-691311468944
blueprint: old_orders
order_number: 6
order_date: '2023-07-13'
company: b0c021bd-503f-4f45-84de-dcc0dfc662ac
location: 2ff26ae1-499b-4b86-bdef-7f9af142e76f
user: 'Barbara van der Kuil'
total: 476.61
products:
  -
    product: 3c5cf221-9708-4f0e-b693-e76a597f175d
    product_number: 2130
    product_title: 'Handdoekjes multifold cellulose 2laags'
    price: 29.52
    amount: 8
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 51
    amount: 4
  -
    product: 9565eab3-b690-4dbb-864d-3a08b424e26f
    product_number: '2921033'
    product_title: 'Wisdoeken geimpregneerd wit, 80cm'
    price: 36.45
    amount: 1
title: '6'
---
