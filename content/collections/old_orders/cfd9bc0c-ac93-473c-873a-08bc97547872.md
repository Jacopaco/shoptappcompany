---
id: cfd9bc0c-ac93-473c-873a-08bc97547872
blueprint: old_orders
order_number: 16
order_date: '2023-10-18'
company: 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
location: 6463e748-7dae-432d-ad50-a774b95850ef
user: 'Huize Winterdijk'
total: 517.1674
products:
  -
    product: 068bc6e0-c1c9-42b6-b43e-6516f8d3a440
    product_number: 2250
    product_title: 'Afvalzakken T50 60 x 80 | 50 liter grijs'
    price: 37.5615
    amount: '1'
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.95
    amount: '3'
  -
    product: 5149804c-a262-4eec-a9ed-8adfb5ccc2d1
    product_number: 6533
    product_title: 'Afvalzakken T50 - 70 x 110 cm blauw '
    price: 30.0645
    amount: '1'
  -
    product: 602bac8b-0e0e-40f8-99db-8655f138c363
    product_number: '2255'
    product_title: 'Afvalzakken T10 45 x 50 | 16 liter transparant '
    price: 26.01
    amount: '2'
  -
    product: 452106cb-b413-4c63-a906-dfed04ccad9a
    product_number: '080555'
    product_title: 'Ecolab MAXX Magic2 '
    price: 108.8714
    amount: '1'
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Handzeep Eco 500ml eurofles vierkant'
    price: 2.015
    amount: '20'
  -
    product: 7430ab87-00da-4ae1-95ee-0dc2b84c5786
    product_number: '54046'
    product_title: 'BATTERIJ MICROB. PROCEL  LR14'
    price: 2.275
    amount: '4'
  -
    product: ef5f9bb6-c599-418d-a6a7-eb614a440256
    product_number: CART00914
    product_title: 'Handschoen Nitril Blauw maat M'
    price: 2.99
    amount: '40'
  -
    product: e7c40665-d355-4737-a5ad-e00e3eeeee99
    product_number: CART00913
    product_title: 'Handschoen Nitril Blauw maat L'
    price: 2.99
    amount: '20'
title: '16'
---
