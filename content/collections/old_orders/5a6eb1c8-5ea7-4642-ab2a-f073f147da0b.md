---
id: 5a6eb1c8-5ea7-4642-ab2a-f073f147da0b
blueprint: old_orders
order_number: 22
order_date: '2024-03-06'
company: b0c021bd-503f-4f45-84de-dcc0dfc662ac
location: 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
user: 'Barbara van der Kuil'
total: 571.67
products:
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 19.5
    amount: 7
  -
    product: 0d5889b2-0f69-458d-809e-86a318357384
    product_number: 2186
    product_title: 'Poetsrol midi cellulose coreless 1laags'
    price: 23
    amount: 3
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 49
    amount: 6
  -
    product: 3ad3e27b-5377-4466-a94f-0cfae74f7ea8
    product_number: 98914-TAPP
    product_title: 'Foamzeep rood 2,5 liter'
    price: 6.5
    amount: 4
  -
    product: 7ec2e97c-d2f2-4284-8896-3038b1413db7
    product_number: '5541'
    product_title: 'Handdoekdispenser mini kunststof wit, PQMiniH'
    price: 23.085
    amount: 2
title: '22'
---
