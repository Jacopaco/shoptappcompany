---
id: 11feff1c-40b6-4062-aad2-33819316eb74
blueprint: old_orders
order_number: 10
order_date: '2023-10-19'
company: 4e00303b-45d0-49d8-9aad-8a8ef0e22948
location: e6713451-367d-43ef-91ec-87d84a4c609f
user: Bijdehand
total: 1009.41
products:
  -
    product: f654be00-85dc-4050-8cb8-decdaf8e4f11
    product_number: 98995
    product_title: 'Handalcohol 500ml 70%'
    price: 5.611
    amount: '16'
  -
    product: a74efe92-26d7-48f6-bf70-5057e3a5cdfc
    product_number: 98990
    product_title: 'Handzeep Eco 500ml eurofles vierkant'
    price: 2.015
    amount: '16'
  -
    product: bacb6508-dd52-421e-bb8e-fab43f72259f
    product_number: 2145
    product_title: 'Handdoekjes interfold cellulose 3laags'
    price: 51.103
    amount: '10'
  -
    product: 31ae31c1-3feb-493e-a922-622e1e1384d1
    product_number: 2110
    product_title: 'Toiletpapier compact cellulose 2laags'
    price: 56.43
    amount: '4'
  -
    product: 1e4c931d-24af-464a-90c8-6a63d236b29f
    product_number: 2189
    product_title: 'Poetsrol midi coreless cellulose 1laags'
    price: 37.661
    amount: '4'
title: '10'
---
