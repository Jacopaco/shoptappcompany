---
id: 93883317-4719-419c-bda1-6e646b97c94f
blueprint: old_orders
order_number: 3
order_date: '2023-01-17'
company: d267d33a-5684-4194-856e-727b2f4b79ba
location: 4e8302b0-f18d-40a8-865e-7040d27f3085
user: 'CBS de Ontdekking'
total: 168
products:
  -
    product: b968562f-645b-47b3-8320-ff6b790f9349
    product_number: 2135
    product_title: 'Handdoekjes Z-vouw cellulose 2laags'
    price: 21
    amount: 8
title: '3'
---
