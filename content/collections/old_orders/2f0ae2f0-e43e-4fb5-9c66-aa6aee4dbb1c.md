---
id: 2f0ae2f0-e43e-4fb5-9c66-aa6aee4dbb1c
blueprint: old_orders
order_number: 85
order_date: '2024-02-19'
company: bebc5c46-0553-4d62-b6ff-de33648493c4
location: 31ab6798-7e55-4a96-8c41-c9d35bf02694
user: 'Radiotherapiegroep Locatie Deventer'
total: 451.2
products:
  -
    product: 30fdc1fb-a49e-4204-abe8-4f6a244e6d16
    product_number: 2002130
    product_title: 'Zakken zwart  58x100'
    price: 37.2
    amount: 1
  -
    product: fd618e8a-780d-41c0-9307-0403bebf9d88
    product_number: '6549'
    product_title: 'Onderzoeksbankrollen 9x50M cellulose 2laags niet geperforeerd'
    price: 69
    amount: 6
title: '85'
---
