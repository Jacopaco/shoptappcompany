---
id: fce0d245-d608-46d9-b6f3-fe83ec2ec059
blueprint: order
order_number: 1244
order_status: placed
payment_status: paid
items:
  -
    id: 5111091d-d22d-477e-83f1-973acf4d1e01
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax: null
    metadata: {  }
  -
    id: 6d5fd6dc-bd38-4135-88de-ead8efc2fd11
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: 75637c1a-7563-4172-ab11-3c6a56e6f559
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax: null
    metadata: {  }
  -
    id: e32a041f-ae56-40b7-a7d5-0fed3d1adddc
    product: '2135'
    variant: null
    quantity: 1
    total: 1930
    tax: null
    metadata: {  }
grand_total: '123.14'
items_total: '101.77'
tax_total: '21.37'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1244'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1706486400
    data: {  }
  -
    status: paid
    timestamp: 1711715919
    data: {  }
---
