---
id: df8c15db-a6d5-4538-aafa-664c8d42091b
blueprint: order
order_number: 1089
order_status: placed
payment_status: paid
items:
  -
    id: 980edecf-fa99-4eb8-9165-0eb15903defe
    product: '2302070'
    variant: null
    quantity: 2
    total: 4640
    tax: null
    metadata: {  }
  -
    id: 9a25d3d8-fa41-42f6-8136-3d4ad21c13a1
    product: '2002130'
    variant: null
    quantity: 3
    total: 11160
    tax: null
    metadata: {  }
  -
    id: 6b73d913-5633-47aa-b58c-78f1f9e46b6d
    product: '2105'
    variant: null
    quantity: 3
    total: 6621
    tax: null
    metadata: {  }
  -
    id: 7f0efb23-eaef-4297-8995-89c9ee4826ba
    product: '2135'
    variant: null
    quantity: 1
    total: 1930
    tax: null
    metadata: {  }
grand_total: '294.65'
items_total: '243.51'
tax_total: '51.14'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1089'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1681171200
    data: {  }
  -
    status: paid
    timestamp: 1711715715
    data: {  }
---
