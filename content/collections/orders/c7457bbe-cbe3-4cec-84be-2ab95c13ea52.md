---
id: c7457bbe-cbe3-4cec-84be-2ab95c13ea52
blueprint: order
order_number: 1403
title: '#1403'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: Ordentall
shipping_street: Westblaak
shipping_number: '113'
shipping_city: Rotterdam
shipping_postal_code: 3012KH
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1718118473
    data: {  }
  -
    status: paid
    timestamp: 1718118473
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: fe982a7d-fa32-47fc-8064-294b281aa5f8
    product: '2250'
    variant: null
    quantity: 4
    total: 10704
    tax:
      amount: 2248
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 90f07a81-6f80-4203-8b60-08c3cbf13150
    product: '54010'
    variant: null
    quantity: 10
    total: 5500
    tax:
      amount: 1155
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: c5c3581a-2b03-4e54-aa1f-f477924990ba
    product: '2302070'
    variant: null
    quantity: 2
    total: 4660
    tax:
      amount: 979
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 25996
items_total: 20864
tax_total: 4382
shipping_total: 750
coupon_total: 0
customer: 8abbdec0-9318-4ac2-a73d-6b36d7253a65
---
