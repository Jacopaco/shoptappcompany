---
id: f1f7a204-4c4e-4aab-badf-f36eae0aefaa
blueprint: order
order_number: 1264
order_status: placed
payment_status: paid
items:
  -
    id: ee431ce7-5471-4001-aae8-eaaea5628a0a
    product: '2105'
    variant: null
    quantity: 16
    total: 30400
    tax: null
    metadata: {  }
  -
    id: 188cc5f8-d6e5-4158-94b9-020b6cdf603f
    product: '2135'
    variant: null
    quantity: '24'
    total: 46800
    tax: null
    metadata: {  }
  -
    id: 03f08bc9-0045-42b5-9b85-39fcd9b76188
    product: '2110'
    variant: null
    quantity: 6
    total: 29400
    tax: null
    metadata: {  }
  -
    id: c643a646-c530-455c-8312-93208b4cebf3
    product: cart00641
    variant: null
    quantity: 4
    total: 1196
    tax: null
    metadata: {  }
  -
    id: 044eee43-be05-495a-9c03-ab922af48533
    product: 5070002002-1
    variant: null
    quantity: 10
    total: 3840
    tax: null
    metadata: {  }
grand_total: '1,350.80'
items_total: '1,116.36'
tax_total: '234.44'
shipping_total: 0
coupon_total: 0
customer: 96aa3564-af32-4dd6-8e8d-b8d4a5035671
title: '#1264'
org_id: '41186583'
use_shipping_for_billing: true
shipping_name: 'ZW OBL Van Schendelstraat 9 Dagbesteding, 3261 SJ (kostenplaats 97037)'
shipping_street: 'Van Schendelstraat'
shipping_city: 'Oud Beijerland'
shipping_postal_code: '3261 SJ'
status_log:
  -
    status: placed
    timestamp: 1708992000
    data: {  }
  -
    status: paid
    timestamp: 1711715947
    data: {  }
---
