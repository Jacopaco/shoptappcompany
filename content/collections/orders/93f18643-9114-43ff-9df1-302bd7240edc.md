---
id: 93f18643-9114-43ff-9df1-302bd7240edc
blueprint: order
order_number: 1499
title: '#1499'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: d709a259-f683-4bbf-877a-bbc43c186d3e
    product: '98917'
    variant: null
    quantity: 2
    total: 956
    tax:
      amount: 201
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 2065
items_total: 956
tax_total: 359
shipping_total: 750
coupon_total: 0
---
