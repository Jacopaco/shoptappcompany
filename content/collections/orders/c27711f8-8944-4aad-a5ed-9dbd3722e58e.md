---
id: c27711f8-8944-4aad-a5ed-9dbd3722e58e
blueprint: order
order_number: 1234
order_status: placed
payment_status: paid
items:
  -
    id: 403ae862-57ed-47a5-b6ff-962183508e32
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax: null
    metadata: {  }
  -
    id: 05fe5c32-d790-4391-9886-cacea9301300
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax: null
    metadata: {  }
  -
    id: 9faebdd3-252e-4c7f-9219-4775c99be7b1
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
  -
    id: 27f03481-45b2-4399-bf6b-370ff5f9a870
    product: 6540-1881
    variant: null
    quantity: 1
    total: 9571
    tax: null
    metadata: {  }
  -
    id: 10be83bb-3d22-4a65-ae6f-eeb190c6f8df
    product: '98990'
    variant: null
    quantity: 12
    total: 2418
    tax: null
    metadata: {  }
grand_total: '274.74'
items_total: '227.06'
tax_total: '47.68'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1234'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1704931200
    data: {  }
  -
    status: paid
    timestamp: 1711715905
    data: {  }
---
