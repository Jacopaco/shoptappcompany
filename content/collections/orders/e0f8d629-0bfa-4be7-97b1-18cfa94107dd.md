---
id: e0f8d629-0bfa-4be7-97b1-18cfa94107dd
blueprint: order
order_number: 1346
title: '#1346'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 229d12a2-347e-4bff-98c4-557cc3d791a6
    product: '54010'
    variant: null
    quantity: 1
    total: 450
    tax:
      amount: 95
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5ca8db76-8117-4c05-96bb-e240b9fa471f
    product: '74417'
    variant: null
    quantity: 1
    total: 3565
    tax:
      amount: 749
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 5609
items_total: 4015
tax_total: 844
shipping_total: 750
coupon_total: 0
customer: b94aecbf-d569-4e0e-9483-8c8be947ec81
---
