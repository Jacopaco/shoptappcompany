---
id: 711e6b9c-2af3-43de-b08e-3fefcf790dde
blueprint: order
order_number: 1489
title: '#1489'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'ZW Zevenbergen Langeweg 16, 4762 RB (kostenplaats 97033)'
shipping_street: Langeweg
shipping_number: '14 boerderij'
shipping_city: Zevenbergen
shipping_postal_code: '4762 RB'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1726553116
    data: {  }
  -
    status: paid
    timestamp: 1726553116
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 3df130f9-041a-41e2-8670-1e755c1cdf54
    product: '2135'
    variant: null
    quantity: 6
    total: 11700
    tax:
      amount: 2457
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 88394177-00a5-4801-ba0d-38cb88d686c7
    product: 98914-tapp
    variant: null
    quantity: 6
    total: 3900
    tax:
      amount: 819
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 19784
items_total: 15600
tax_total: 3434
shipping_total: 750
coupon_total: 0
customer: 0e9640f9-a2e4-4e0d-a0da-3f6ef3659d51
---
