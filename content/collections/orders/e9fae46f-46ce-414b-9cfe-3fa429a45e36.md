---
id: e9fae46f-46ce-414b-9cfe-3fa429a45e36
blueprint: order
order_number: 1221
order_status: placed
payment_status: paid
items:
  -
    id: 6346acbf-8b1a-494e-afe4-311aa2106c15
    product: '2110'
    variant: null
    quantity: '3'
    total: 12071
    tax: null
    metadata: {  }
  -
    id: fc09cc6c-9cd7-49c4-a1c0-8346452d4d74
    product: cart00914
    variant: null
    quantity: '70'
    total: 20930
    tax: null
    metadata: {  }
  -
    id: 7735122e-4f1c-433c-a8d5-99200a75be84
    product: cart00913
    variant: null
    quantity: '30'
    total: 8970
    tax: null
    metadata: {  }
grand_total: '507.84'
items_total: '419.71'
tax_total: '88.14'
shipping_total: 0
coupon_total: 0
customer: af849acf-89b2-4eee-9641-de33b297a24c
title: '#1221'
use_shipping_for_billing: true
shipping_name: 'Huize Winterdijk'
status_log:
  -
    status: placed
    timestamp: 1702339200
    data: {  }
  -
    status: paid
    timestamp: 1711715883
    data: {  }
---
