---
id: 71ed2b88-0399-4db7-b748-7cfaf74a83b8
blueprint: order
order_number: 1450
title: '#1450'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Zuidwester Spijkenisse'
shipping_street: 'P.J. Bliekstraat'
shipping_number: 1-3
shipping_city: Spijkenisse
shipping_postal_code: '3201 PL'
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1722925602
    data: {  }
  -
    status: paid
    timestamp: 1722925602
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 2378238e-45f4-4488-b29b-a49726a5d8e6
    product: '2135'
    variant: null
    quantity: 20
    total: 39000
    tax:
      amount: 8190
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 0f78b6a6-c932-44f3-8803-83f0b3c4dd9d
    product: '2186'
    variant: null
    quantity: 8
    total: 18400
    tax:
      amount: 3864
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 67313423-28a7-4cde-8048-b88a59ad6927
    product: '2250'
    variant: null
    quantity: 2
    total: 6540
    tax:
      amount: 1373
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: e77b858f-44d9-4e7a-9680-414d938a2e51
    product: '2105'
    variant: null
    quantity: 20
    total: 38000
    tax:
      amount: 7980
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6b1e6b4c-a403-40a6-8cd3-00895d682001
    product: 5070002002-1
    variant: null
    quantity: 24
    total: 9216
    tax:
      amount: 1935
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 1fcf6f2a-01f3-4be8-890c-5049ad81a3f8
    product: 6970002002-1
    variant: null
    quantity: 10
    total: 3000
    tax:
      amount: 630
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 138128
items_total: 114156
tax_total: 23972
shipping_total: 0
coupon_total: 0
customer: 96aa3564-af32-4dd6-8e8d-b8d4a5035671
---
