---
id: 0ca66a65-d359-4732-934f-af61f408ac52
blueprint: order
order_number: 1188
order_status: placed
payment_status: paid
items:
  -
    id: cd2ee7fd-ec35-4140-a582-9292cf0a544b
    product: '98918'
    variant: null
    quantity: 2
    total: 2332
    tax: null
    metadata: {  }
grand_total: '28.22'
items_total: '23.32'
tax_total: '4.90'
shipping_total: 0
coupon_total: 0
customer: 9ebdbaf5-9136-4cf4-a16c-fcb124618821
title: '#1188'
use_shipping_for_billing: true
shipping_name: 'Tandartspraktijk SaChaLo'
shipping_street: Kerkveld
shipping_city: Meijel
status_log:
  -
    status: placed
    timestamp: 1698105600
    data: {  }
  -
    status: paid
    timestamp: 1711715837
    data: {  }
---
