---
id: d34f4d49-fdee-4b3a-a87d-ddaa12bdf5e4
blueprint: order
order_number: 1620
title: '#1620'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'Radiotherapiegroep, locatie Arnhem, Ede'
shipping_street: Wagnerlaan
shipping_number: '47'
shipping_city: Arnhem
shipping_postal_code: 6815AD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1737456428
    data: {  }
  -
    status: paid
    timestamp: 1737456429
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 9bac6876-8cd0-4303-a7c0-ece75c840907
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax:
      amount: 811
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 0069b261-46ed-4b45-881b-09dec2221b91
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax:
      amount: 927
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: d4ae67cc-cde1-4cbc-baf3-3f8924c84ef9
    product: cart00960
    variant: null
    quantity: 1
    total: 3004
    tax:
      amount: 631
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: a5163d7c-1078-42a1-b8ef-66566d553ea9
    product: '98995'
    variant: null
    quantity: 24
    total: 11880
    tax:
      amount: 2495
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 28930
items_total: 23158
tax_total: 5022
shipping_total: 750
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
---
