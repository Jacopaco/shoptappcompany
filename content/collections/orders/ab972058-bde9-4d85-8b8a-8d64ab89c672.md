---
id: ab972058-bde9-4d85-8b8a-8d64ab89c672
blueprint: order
order_number: 1111
order_status: placed
payment_status: paid
items:
  -
    id: e4b12530-3238-410c-9fff-e1e71f965792
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: b068f386-1102-4d89-996e-ef25c788b438
    product: '2302070'
    variant: null
    quantity: 1
    total: 2320
    tax: null
    metadata: {  }
  -
    id: d066d7b0-1263-4fe7-a3a0-d1cd8f007f8f
    product: '2002130'
    variant: null
    quantity: 2
    total: 7440
    tax: null
    metadata: {  }
  -
    id: 7e64f2c0-f884-4c4c-b333-89585dc0a315
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 9a806a25-6d79-4815-8f9f-75efa9f44f31
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
grand_total: '281.74'
items_total: '232.84'
tax_total: '48.90'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1111'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1683763200
    data: {  }
  -
    status: paid
    timestamp: 1711715737
    data: {  }
---
