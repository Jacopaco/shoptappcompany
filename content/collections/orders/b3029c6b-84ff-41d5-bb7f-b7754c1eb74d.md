---
id: b3029c6b-84ff-41d5-bb7f-b7754c1eb74d
blueprint: order
order_number: 1622
title: '#1622'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: Veldheim
shipping_street: Bovenbuurtweg
shipping_number: '33'
shipping_city: Ede
shipping_postal_code: '6717 XA'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1737639819
    data: {  }
  -
    status: paid
    timestamp: 1737639819
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: d4ce44d7-c0f9-42bb-a8b2-83a6fd525bed
    product: 84600202f
    variant: null
    quantity: 1
    total: 5680
    tax:
      amount: 1193
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 3675266a-ed02-448f-bff8-d13c5e4ee995
    product: '3826120630'
    variant: null
    quantity: 10
    total: 550
    tax:
      amount: 116
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 1766a378-5e6f-49f0-8142-8effa2fb5404
    product: cart00927
    variant: null
    quantity: 1
    total: 374
    tax:
      amount: 79
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 9c6312b3-f671-4aa5-b48d-f1232d3ac805
    product: cart00926
    variant: null
    quantity: 10
    total: 3740
    tax:
      amount: 785
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 4389d871-9f82-4e30-ad1f-d5448ea110d0
    product: cart00956
    variant: null
    quantity: 10
    total: 3750
    tax:
      amount: 788
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6db49670-0ab0-4a4d-b9ab-543953f537b3
    product: '180205100'
    variant: null
    quantity: 1
    total: 3685
    tax:
      amount: 774
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 22422
items_total: 17779
tax_total: 3893
shipping_total: 750
coupon_total: 0
customer: ff9c6ae5-8b31-4b1e-ada1-5f32db8b66b9
---
