---
id: 45c5a531-bde0-4aed-8aad-8aa2facc32a2
blueprint: order
order_number: 1418
title: '#1418'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Praktijk voor Tandheelkunde Op Dubbeldam'
shipping_street: Vijverplantsoen
shipping_number: 1A
shipping_city: Dordrecht
shipping_postal_code: '3319 SW'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1719847830
    data: {  }
  -
    status: paid
    timestamp: 1719847830
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 9928c6d5-eb0b-4221-9337-ce257fdfe9a7
    product: cart00647
    variant: null
    quantity: 50
    total: 14950
    tax:
      amount: 3140
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 18840
items_total: 14950
tax_total: 3140
shipping_total: 750
coupon_total: 0
customer: ff0f0fb5-c98f-4caa-86f0-89b5b9c857b0
---
