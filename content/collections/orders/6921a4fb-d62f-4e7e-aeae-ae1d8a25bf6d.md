---
id: 6921a4fb-d62f-4e7e-aeae-ae1d8a25bf6d
blueprint: order
order_number: 1326
title: '#1326'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 1982d2be-2e1f-40f9-9962-7db9863fe021
    product: '2135'
    variant: null
    quantity: 24
    total: 42120
    tax:
      amount: 8845
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 9916ea8e-1913-401e-9bb4-1046d9a3e589
    product: '2110'
    variant: null
    quantity: 10
    total: 40230
    tax:
      amount: 8448
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 99643
items_total: 82350
tax_total: 17293
shipping_total: 0
coupon_total: 0
customer: b94aecbf-d569-4e0e-9483-8c8be947ec81
---
