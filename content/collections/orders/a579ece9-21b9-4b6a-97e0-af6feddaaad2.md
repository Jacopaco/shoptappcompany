---
id: a579ece9-21b9-4b6a-97e0-af6feddaaad2
blueprint: order
order_number: 1001
order_status: placed
payment_status: paid
items:
  -
    id: 9b796312-4723-44bb-968f-4fdc20a1dfb6
    product: '1881'
    variant: null
    quantity: 2
    total: 17840
    tax: null
    metadata: {  }
  -
    id: 8ce95bae-5dec-4e1f-a49e-aaa9783f12ff
    product: '188'
    variant: null
    quantity: 2
    total: 17492
    tax: null
    metadata: {  }
  -
    id: f7933915-2a0e-43b5-bfa1-a568b4881bd4
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax: null
    metadata: {  }
  -
    id: 2a1d4eb0-0586-4fb9-8444-e970a9ba7835
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: a1fefd3e-81a9-4dcc-8541-ce7b861bb42e
    product: '2105'
    variant: null
    quantity: 3
    total: 6621
    tax: null
    metadata: {  }
  -
    id: 4a25b8c6-c8b2-4c7d-9da8-872f093f46a4
    product: '2155'
    variant: null
    quantity: 1
    total: 2360
    tax: null
    metadata: {  }
  -
    id: ca8a5dbe-b125-4678-a5e2-cd621c108104
    product: '2170'
    variant: null
    quantity: 1
    total: 2995
    tax: null
    metadata: {  }
  -
    id: 4957ed84-ef17-4995-8b8c-1d7a565aaf79
    product: '2135'
    variant: null
    quantity: 4
    total: 7720
    tax: null
    metadata: {  }
  -
    id: 2895d1e5-42cd-4316-a74d-f19e48216331
    product: '98990'
    variant: null
    quantity: 2
    total: 5256
    tax: null
    metadata: {  }
grand_total: '802.52'
items_total: '663.24'
tax_total: '139.28'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1001'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1659312000
    data: {  }
  -
    status: paid
    timestamp: 1711715643
    data: {  }
---
