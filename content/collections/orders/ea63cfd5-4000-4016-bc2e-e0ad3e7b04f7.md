---
id: ea63cfd5-4000-4016-bc2e-e0ad3e7b04f7
blueprint: order
order_number: 1011
order_status: placed
payment_status: paid
items:
  -
    id: 150ea1b6-def4-4fb8-b50d-54e352fab021
    product: '1881'
    variant: null
    quantity: 2
    total: 17840
    tax: null
    metadata: {  }
  -
    id: cc04b0f1-3a02-4b2e-84c1-60fcfbd4428b
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 81787336-fa22-4274-920b-d1f422675d5e
    product: '2170'
    variant: null
    quantity: 1
    total: 2995
    tax: null
    metadata: {  }
  -
    id: 433cdaee-2cd2-4f8c-97eb-1915d1fcfa86
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
grand_total: '375.57'
items_total: '310.39'
tax_total: '65.18'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1011'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1662940800
    data: {  }
  -
    status: paid
    timestamp: 1711715651
    data: {  }
---
