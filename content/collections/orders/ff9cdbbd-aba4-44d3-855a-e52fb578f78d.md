---
id: ff9cdbbd-aba4-44d3-855a-e52fb578f78d
blueprint: order
order_number: 1414
title: '#1414'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Stichting Zideris'
shipping_street: Cuneraweg
shipping_number: '12'
shipping_city: Rhenen
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1719409080
    data: {  }
  -
    status: paid
    timestamp: 1719409080
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: a7306562-dd0c-4db8-aebc-eae70a8f0e82
    product: '98990'
    variant: null
    quantity: 12
    total: 3660
    tax:
      amount: 769
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 19bc68c5-ecca-4a7d-8e5e-7764dafa2770
    product: '2145'
    variant: null
    quantity: 10
    total: 51100
    tax:
      amount: 10731
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 278ffb42-bfae-4fb8-8f74-2e9ad88ff33a
    product: '2189'
    variant: null
    quantity: 5
    total: 18830
    tax:
      amount: 3954
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 40c05ad7-b8e6-4160-aa01-6581549e2e23
    product: cart00914
    variant: null
    quantity: 10
    total: 2990
    tax:
      amount: 628
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 9f6bff00-138c-4c8f-8a2b-7b56a2ec53b6
    product: cart00913
    variant: null
    quantity: 50
    total: 14950
    tax:
      amount: 3140
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 110752
items_total: 91530
tax_total: 19222
shipping_total: 0
coupon_total: 0
customer: d67cc734-8a62-4a1c-9400-e9f1ffb70939
---
