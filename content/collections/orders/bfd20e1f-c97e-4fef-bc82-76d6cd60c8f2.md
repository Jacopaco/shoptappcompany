---
id: bfd20e1f-c97e-4fef-bc82-76d6cd60c8f2
blueprint: order
order_number: 1406
title: '#1406'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Huize Winterdijk'
shipping_street: Winterdijk
shipping_number: '8'
shipping_city: Gouda
shipping_postal_code: 2801SJ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1718619224
    data: {  }
  -
    status: paid
    timestamp: 1718619224
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: d55485ee-0f67-4897-b23b-f9696ae82eec
    product: cart00914
    variant: null
    quantity: 140
    total: 41860
    tax:
      amount: 8791
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: dea713e8-ba69-44dd-8a6d-d283528e63ce
    product: cart00913
    variant: null
    quantity: 50
    total: 14950
    tax:
      amount: 3140
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 056d67c7-02a0-4f2f-9937-deeb50cb1bf8
    product: cart00641
    variant: null
    quantity: 30
    total: 8970
    tax:
      amount: 1884
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6d42a7d9-d3aa-4042-a846-58be13318000
    product: cart00915
    variant: null
    quantity: 48
    total: 5616
    tax:
      amount: 1179
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 7ffbfba1-3865-46bd-be1c-c6ad3d62ad2a
    product: '14258'
    variant: null
    quantity: 1
    total: 600
    tax:
      amount: 126
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 87116
items_total: 71996
tax_total: 15120
shipping_total: 0
coupon_total: 0
customer: af849acf-89b2-4eee-9641-de33b297a24c
---
