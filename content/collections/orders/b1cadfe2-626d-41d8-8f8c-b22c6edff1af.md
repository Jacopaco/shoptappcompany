---
id: b1cadfe2-626d-41d8-8f8c-b22c6edff1af
blueprint: order
order_number: 1167
order_status: placed
payment_status: paid
items:
  -
    id: 17857f10-5cc3-498b-aada-5b940eb1a83f
    product: '2115'
    variant: null
    quantity: 6
    total: 31170
    tax: null
    metadata: {  }
  -
    id: de159a7a-ef74-492a-9506-a4017f83feb5
    product: '2250'
    variant: null
    quantity: 1
    total: 3192
    tax: null
    metadata: {  }
  -
    id: 1eb5a8cd-5676-49d9-91ef-b0651cded82e
    product: '98915'
    variant: null
    quantity: 1
    total: 980
    tax: null
    metadata: {  }
grand_total: '427.63'
items_total: '353.42'
tax_total: '74.22'
shipping_total: 0
coupon_total: 0
customer: c48526ff-8e7d-4837-9760-efab06ff8f98
title: '#1167'
use_shipping_for_billing: true
shipping_name: 'Eben Haëzer School Barneveld'
shipping_street: Schoutenstraat
shipping_city: Barneveld
status_log:
  -
    status: placed
    timestamp: 1694476800
    data: {  }
  -
    status: paid
    timestamp: 1711715810
    data: {  }
---
