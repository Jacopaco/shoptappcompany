---
id: 82ecd9fe-aee8-4c41-b03f-2027c72d7690
blueprint: order
order_number: 1214
order_status: placed
payment_status: paid
items:
  -
    id: 8aff941c-9325-4c6d-b6c9-afd1663dbf84
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: 04644db8-11c1-4a8f-bf3b-8432b79c4cb2
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax: null
    metadata: {  }
  -
    id: 3cedc701-cdef-4342-b04f-b3cea70d72ce
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax: null
    metadata: {  }
  -
    id: 556d621d-c0f4-429e-9f01-0b7d69443382
    product: 6540-1881
    variant: null
    quantity: 2
    total: 19142
    tax: null
    metadata: {  }
  -
    id: c21cc2ac-5b34-4171-8ace-aaae46756d29
    product: '98990'
    variant: null
    quantity: 12
    total: 2418
    tax: null
    metadata: {  }
grand_total: '374.46'
items_total: '309.47'
tax_total: '64.99'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1214'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1701734400
    data: {  }
  -
    status: paid
    timestamp: 1711715874
    data: {  }
---
