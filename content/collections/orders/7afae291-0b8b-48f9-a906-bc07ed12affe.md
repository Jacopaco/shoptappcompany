---
id: 7afae291-0b8b-48f9-a906-bc07ed12affe
blueprint: order
order_number: 1091
order_status: placed
payment_status: paid
items:
  -
    id: 32b25e82-8b43-48ae-bf24-6a8f78960583
    product: b3ac5eff-8aa3-4430-a86d-43f7d85384d7
    variant: null
    quantity: 15
    total: 4485
    tax: null
    metadata: {  }
  -
    id: 5ea332da-85b1-469d-b5ce-b03e1060b41c
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    variant: null
    quantity: 10
    total: 2990
    tax: null
    metadata: {  }
grand_total: '90.45'
items_total: '74.75'
tax_total: '15.70'
shipping_total: 0
coupon_total: 0
customer: bf46aa1e-9b87-40fb-9876-b60c1f133550
title: '#1091'
use_shipping_for_billing: true
shipping_name: Beekheim
shipping_street: 'Ds. E. Fransenlaan'
shipping_city: Barneveld
shipping_postal_code: '3772 TX'
status_log:
  -
    status: placed
    timestamp: 1681257600
    data: {  }
  -
    status: paid
    timestamp: 1711715717
    data: {  }
---
