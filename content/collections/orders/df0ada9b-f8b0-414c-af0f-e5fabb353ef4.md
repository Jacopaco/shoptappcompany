---
id: df0ada9b-f8b0-414c-af0f-e5fabb353ef4
blueprint: order
order_number: 1317
title: '#1317'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Dental Expo'
shipping_street: Europaplein
shipping_number: '24'
shipping_city: Amsterdam
shipping_postal_code: 1234DB
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1712221512
    data: {  }
  -
    status: paid
    timestamp: 1712221512
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 39123896-7760-4aca-a7f5-441251e97a55
    product: '74415'
    variant: null
    quantity: 1
    total: 2823
    tax:
      amount: 593
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 4cd3e877-8bc1-48b5-9e0e-acc37cec4a0b
    product: '2250'
    variant: null
    quantity: 1
    total: 3756
    tax:
      amount: 789
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 91719c8d-904a-4043-86d4-4cf1619e3e0e
    product: '2170'
    variant: null
    quantity: 1
    total: 3387
    tax:
      amount: 711
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 12809
items_total: 9966
tax_total: 2093
shipping_total: 750
coupon_total: 0
customer: b94aecbf-d569-4e0e-9483-8c8be947ec81
---
