---
id: 9b64bd81-5d07-4eb5-8e7d-dc301f5ce472
blueprint: order
order_number: 1394
title: '#1394'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
vat_id: NL861983506B01
org_id: '81199074'
shipping_name: Geboortezorgwinkel
shipping_street: Banneweg
shipping_number: '6'
shipping_city: Gorinchem
shipping_postal_code: '4205 DG'
use_shipping_address_for_billing: true
status_log:
  -
    status: placed
    timestamp: 1717505008
    data: {  }
  -
    status: paid
    timestamp: 1717505008
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 0e1e2b18-9287-43d4-887c-3ac75ffb4d1d
    product: '2135'
    variant: null
    quantity: 1
    total: 2106
    tax:
      amount: 442
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 8d2bdf0e-49bb-447a-9fa4-4bb484006ea7
    product: '98990'
    variant: null
    quantity: 3
    total: 723
    tax:
      amount: 152
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 4173
items_total: 2829
tax_total: 594
shipping_total: 750
coupon_total: 0
customer: 1014372a-dd9a-40b5-988a-5a3c9aa0d1b5
---
