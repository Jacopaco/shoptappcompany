---
id: a0cd1e73-e52e-448f-a0f8-60bb6df9e9a2
blueprint: order
order_number: 1473
title: '#1473'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 2e3340e9-9606-43d6-88ec-72c6b2b10c9c
    product: '568974'
    variant: null
    quantity: 1
    total: 4750
    tax:
      amount: 998
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 6656
items_total: 4750
tax_total: 1156
shipping_total: 750
coupon_total: 0
---
