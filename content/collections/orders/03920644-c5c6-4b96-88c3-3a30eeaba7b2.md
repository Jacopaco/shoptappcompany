---
id: 03920644-c5c6-4b96-88c3-3a30eeaba7b2
blueprint: order
order_number: 1564
title: '#1564'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'H2 Tandartsen'
shipping_street: Haterseveldweg
shipping_number: '285'
shipping_city: Nijmegen
shipping_postal_code: 6532XR
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1732710665
    data: {  }
  -
    status: paid
    timestamp: 1732710665
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 1fef2525-27b1-4bba-a7d7-1678bea26b6e
    product: cart00637
    variant: null
    quantity: 15
    total: 4485
    tax:
      amount: 942
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: e9d71109-9c64-47d0-828c-9312c3945625
    product: cart00647
    variant: null
    quantity: 30
    total: 8970
    tax:
      amount: 1884
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: b7c2ba0f-f155-47a2-ba5e-fc7ef6cfc56d
    product: '2135'
    variant: null
    quantity: 1
    total: 1830
    tax:
      amount: 384
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 96d4902f-fd47-4edc-b258-7b54d2d70ac4
    product: '2125'
    variant: null
    quantity: 1
    total: 2593
    tax:
      amount: 545
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 22541
items_total: 17878
tax_total: 3913
shipping_total: 750
coupon_total: 0
customer: 0a35b985-6a6c-4724-93aa-3fb58907a34e
---
