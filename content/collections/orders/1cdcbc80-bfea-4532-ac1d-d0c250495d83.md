---
id: 1cdcbc80-bfea-4532-ac1d-d0c250495d83
blueprint: order
order_number: 1246
order_status: placed
payment_status: paid
items:
  -
    id: 9e97f824-8cb7-4a91-a2de-5336f165a935
    product: '2105'
    variant: null
    quantity: 5
    total: 11035
    tax: null
    metadata: {  }
  -
    id: 2a62e78e-b1a8-4442-b885-30d7d52d6876
    product: '2135'
    variant: null
    quantity: 4
    total: 7720
    tax: null
    metadata: {  }
  -
    id: 247a345f-5df7-4497-94d7-5326ebd61e12
    product: '6549'
    variant: null
    quantity: 5
    total: 16405
    tax: null
    metadata: {  }
  -
    id: 26c71c9d-975c-4e1a-81c8-6aa767c3065c
    product: '98995'
    variant: null
    quantity: 1
    total: 561
    tax: null
    metadata: {  }
  -
    id: 2ab7e863-ce20-46cd-959f-320583814d7c
    product: '98990'
    variant: null
    quantity: 1
    total: 202
    tax: null
    metadata: {  }
grand_total: '434.66'
items_total: '359.23'
tax_total: '75.44'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1246'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1706832000
    data: {  }
  -
    status: paid
    timestamp: 1711715921
    data: {  }
---
