---
id: b133242b-3030-4bba-831f-f5ef315e4826
blueprint: order
order_number: 1527
title: '#1527'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 9b43a9a5-70fc-4ba3-a060-c32418326d6f
    product: '98920'
    variant: null
    quantity: 10
    total: 11660
    tax:
      amount: 2449
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 15017
items_total: 11660
tax_total: 2607
shipping_total: 750
coupon_total: 0
customer: 1306296e-af27-4a67-9166-cebf43cfa895
---
