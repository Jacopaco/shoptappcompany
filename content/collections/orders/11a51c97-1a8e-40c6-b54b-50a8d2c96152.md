---
id: 11a51c97-1a8e-40c6-b54b-50a8d2c96152
blueprint: order
order_number: 1359
title: '#1359'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_number: '47'
shipping_city: Arnhem
shipping_postal_code: 6815AD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1715164255
    data: {  }
  -
    status: paid
    timestamp: 1715164255
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 6c43aa99-df2e-40e6-8cfd-bb6f1556c46d
    product: 6540-1881
    variant: null
    quantity: 2
    total: 19142
    tax:
      amount: 4020
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: b496313e-09c5-4192-91db-2c9180e4bae0
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax:
      amount: 1216
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 45b930d8-182a-4976-9649-78dad300af06
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax:
      amount: 927
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 289b3e11-b991-4c82-ba58-8873a66ab783
    product: '98990'
    variant: null
    quantity: 12
    total: 2580
    tax:
      amount: 542
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: ef82ee67-e2cb-4a14-964a-27516be8f58c
    product: cart00914
    variant: null
    quantity: 4
    total: 1196
    tax:
      amount: 251
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 07b645fd-706f-40c8-a09a-c254a422d122
    product: cart00913
    variant: null
    quantity: 1
    total: 299
    tax:
      amount: 63
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 40440
items_total: 33421
tax_total: 7019
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
---
