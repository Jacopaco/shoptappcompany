---
id: 65c11ca4-fdbb-4f4a-ba57-5c55c28bab8c
blueprint: order
order_number: 1130
order_status: placed
payment_status: paid
items:
  -
    id: 6d2bb0dc-5a2e-43ba-8172-e9789b788892
    product: '1881'
    variant: null
    quantity: 1
    total: 8920
    tax: null
    metadata: {  }
  -
    id: e0905cee-d00f-43be-bfae-3e74fbba8834
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: dd3e1d56-0acc-4c36-b2fb-79d63f552db2
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
  -
    id: cbf4a8ed-fce8-4b81-a678-e52b7ed817ef
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    variant: null
    quantity: 5
    total: 2575
    tax: null
    metadata: {  }
grand_total: '262.56'
items_total: '216.99'
tax_total: '45.57'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1130'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1686614400
    data: {  }
  -
    status: paid
    timestamp: 1711715760
    data: {  }
---
