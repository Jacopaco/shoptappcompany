---
id: 49d6c865-a10e-421b-a32c-b1658f6b8354
blueprint: order
order_number: 1211
order_status: placed
payment_status: paid
items:
  -
    id: d904f205-aa3c-4e73-af72-33e43936d51d
    product: '2135'
    variant: null
    quantity: 5
    total: 9750
    tax: null
    metadata: {  }
  -
    id: 6cdd7e49-0ad7-462d-9ff4-85dfa459d13e
    product: '2110'
    variant: null
    quantity: 1
    total: 4900
    tax: null
    metadata: {  }
grand_total: '177.27'
items_total: '146.50'
tax_total: '30.77'
shipping_total: 0
coupon_total: 0
customer: 00c75da6-78cd-44e8-b49d-98f371c2352f
title: '#1211'
use_shipping_for_billing: true
shipping_name: 'Adullam Centraal Bureau'
shipping_street: Postbus
shipping_city: Barneveld
shipping_postal_code: '3770 AA'
status_log:
  -
    status: placed
    timestamp: 1701388800
    data: {  }
  -
    status: paid
    timestamp: 1711715870
    data: {  }
---
