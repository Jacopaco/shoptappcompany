---
id: 9c9b00f1-1f7b-4abf-b331-ca270c36ccd3
blueprint: order
order_number: 1138
order_status: placed
payment_status: paid
items:
  -
    id: 0a93bc02-440b-440d-84e4-872cd41d9446
    product: '1881'
    variant: null
    quantity: 1
    total: 8920
    tax: null
    metadata: {  }
  -
    id: 4770fa5a-09ae-46ae-b7c0-45d126a503af
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: 304d7a44-7701-4aae-936c-47c738139d03
    product: '2105'
    variant: null
    quantity: 3
    total: 6621
    tax: null
    metadata: {  }
  -
    id: 49a38b82-f288-4823-b7a9-000704f4adc8
    product: '2155'
    variant: null
    quantity: 1
    total: 2360
    tax: null
    metadata: {  }
  -
    id: 11e6cf1c-2f19-4bec-9d2a-f09cbec00698
    product: '2135'
    variant: null
    quantity: 4
    total: 7720
    tax: null
    metadata: {  }
  -
    id: 62568200-c2cd-4896-a155-9bed286321af
    product: '98990'
    variant: null
    quantity: 1
    total: 2628
    tax: null
    metadata: {  }
grand_total: '381.98'
items_total: '315.69'
tax_total: '66.29'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1138'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1687737600
    data: {  }
  -
    status: paid
    timestamp: 1711715773
    data: {  }
---
