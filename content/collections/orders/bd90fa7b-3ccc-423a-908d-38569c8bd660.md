---
id: bd90fa7b-3ccc-423a-908d-38569c8bd660
blueprint: order
order_number: 1652
title: '#1652'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Tandartspraktijk Mheer'
shipping_street: 'Burgemeester Beckersweg'
shipping_number: '43'
shipping_city: Mheer
shipping_postal_code: '6261 NZ'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1740390789
    data: {  }
  -
    status: paid
    timestamp: 1740390789
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 3f3903ed-85a9-46e7-a77d-a778a855e730
    product: cart00913
    variant: null
    quantity: 10
    total: 3490
    tax:
      amount: 733
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 9f3ce14f-b4e2-457b-8a44-6525077e5650
    product: cart00647
    variant: null
    quantity: 50
    total: 17450
    tax:
      amount: 3665
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 7e90c2e7-cbd2-47c2-9d3e-c5e267ca801f
    product: '2142'
    variant: null
    quantity: 1
    total: 4733
    tax:
      amount: 994
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 31065
items_total: 25673
tax_total: 5392
shipping_total: 0
coupon_total: 0
customer: 3bf820a2-2219-4328-9dee-d38f26f3454c
---
