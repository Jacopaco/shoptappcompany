---
id: b6bf6d9a-4290-4bcd-a43c-8b226a05f5f9
blueprint: order
order_number: 1573
title: '#1573'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'Lagarde Groep'
shipping_street: Mercuriusweg
shipping_number: '45'
shipping_city: Barneveld
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1733392938
    data: {  }
  -
    status: paid
    timestamp: 1733392938
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 92a3adcb-1dc6-4fb3-abf5-3203a805055f
    product: '2135'
    variant: null
    quantity: 3
    total: 5850
    tax:
      amount: 1229
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 7b2e10f6-82d4-4362-83d5-1945924e4345
    product: '2200'
    variant: null
    quantity: 3
    total: 9474
    tax:
      amount: 1990
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 19451
items_total: 15324
tax_total: 3377
shipping_total: 750
coupon_total: 0
customer: 7f976cba-5263-43e6-9a27-729a06c3551a
---
