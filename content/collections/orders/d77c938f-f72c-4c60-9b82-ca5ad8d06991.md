---
id: d77c938f-f72c-4c60-9b82-ca5ad8d06991
blueprint: order
order_number: 1339
title: '#1339'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Christelijke Basisschool (CBS) Het Kompas'
shipping_street: Gregoriuslaan
shipping_number: '40'
shipping_city: Lexmond
shipping_postal_code: 4148SZ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1712750160
    data: {  }
  -
    status: paid
    timestamp: 1712750160
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: db1e8063-091c-4bac-a552-cdc3fe4103ea
    product: '98916'
    variant: null
    quantity: 7
    total: 6958
    tax:
      amount: 1461
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 9169
items_total: 6958
tax_total: 1461
shipping_total: 750
coupon_total: 0
customer: 9eddba44-5334-4fe9-9383-20ecc48cc44d
---
