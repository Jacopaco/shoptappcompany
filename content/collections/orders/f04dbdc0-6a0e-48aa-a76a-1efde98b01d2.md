---
id: f04dbdc0-6a0e-48aa-a76a-1efde98b01d2
blueprint: order
order_number: 1353
title: '#1353'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_number: '47'
shipping_city: Arnhem
shipping_postal_code: 6815AD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1714551435
    data: {  }
  -
    status: paid
    timestamp: 1714551436
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 8dae50a2-9861-420c-b336-f5a90e687040
    product: 6540-1881
    variant: null
    quantity: 1
    total: 9571
    tax:
      amount: 2010
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: cb5f99cd-e450-47f2-b5ab-d4f71b86be67
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax:
      amount: 463
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 636103d9-99e6-4273-81cf-9e02081d14e5
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax:
      amount: 811
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 4e3b3ee6-11c4-4807-abec-90997577ee08
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax:
      amount: 571
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: ee25bf67-406b-4b3a-ac6e-b4c369250fad
    product: '2155'
    variant: null
    quantity: 1
    total: 2360
    tax:
      amount: 496
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6ab040f0-8c85-4aa0-b767-ff789bfa1fae
    product: cart00914
    variant: null
    quantity: 2
    total: 598
    tax:
      amount: 126
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 72f83a06-b0ae-4681-bda4-925b1541ba67
    product: '98995'
    variant: null
    quantity: 12
    total: 5940
    tax:
      amount: 1247
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 32980
items_total: 27256
tax_total: 5724
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
---
