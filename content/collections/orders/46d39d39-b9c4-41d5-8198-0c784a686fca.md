---
id: 46d39d39-b9c4-41d5-8198-0c784a686fca
blueprint: order
order_number: 1643
title: '#1643'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Huize Winterdijk'
shipping_street: Winterdijk
shipping_number: '8'
shipping_city: Gouda
shipping_postal_code: 2801SJ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1739798801
    data: {  }
  -
    status: paid
    timestamp: 1739798802
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 14457cb5-f1d6-43b6-87fc-1571b132ae4a
    product: '2135'
    variant: null
    quantity: 12
    total: 23940
    tax:
      amount: 5027
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 98770bb9-6488-48db-be9d-161bcf4e5a51
    product: '2100'
    variant: null
    quantity: 1
    total: 2925
    tax:
      amount: 614
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 1cb81bfd-597c-4d31-b454-2c602ac1a3c0
    product: '2110'
    variant: null
    quantity: 1
    total: 4900
    tax:
      amount: 1029
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 01bd3282-be69-427d-bc52-d99d7f751eec
    product: cart00960
    variant: null
    quantity: 3
    total: 9012
    tax:
      amount: 1893
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: a81cc344-5692-4e87-8d55-e3e9fd3655ee
    product: '150203000'
    variant: null
    quantity: 2
    total: 5076
    tax:
      amount: 1066
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 16f04a61-419d-40b0-9981-ac8236415b36
    product: '150106'
    variant: null
    quantity: 1
    total: 11101
    tax:
      amount: 2331
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5dc75fb3-55db-458d-a20e-fddeeaf9519b
    product: cart00914
    variant: null
    quantity: 140
    total: 48860
    tax:
      amount: 10261
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: f69eb821-c408-49f2-a1f0-727f2f50bd2e
    product: cart00913
    variant: null
    quantity: 10
    total: 3490
    tax:
      amount: 733
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: a0ba94ab-6ee3-4dbc-8676-0054d23db8b3
    product: '98990'
    variant: null
    quantity: 12
    total: 2412
    tax:
      amount: 507
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5c731d68-ae2b-4610-a835-a3739f15248f
    product: cart00915
    variant: null
    quantity: 240
    total: 28080
    tax:
      amount: 5897
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 169154
items_total: 139796
tax_total: 29358
shipping_total: 0
coupon_total: 0
customer: af849acf-89b2-4eee-9641-de33b297a24c
---
