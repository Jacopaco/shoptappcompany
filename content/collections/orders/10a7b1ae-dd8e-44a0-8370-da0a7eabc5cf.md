---
id: 10a7b1ae-dd8e-44a0-8370-da0a7eabc5cf
blueprint: order
order_number: 1014
order_status: placed
payment_status: paid
items:
  -
    id: d4d98523-bfda-404f-aa64-b9144b183c91
    product: '1881'
    variant: null
    quantity: '1'
    total: 8920
    tax: null
    metadata: {  }
  -
    id: 9303c53f-7d02-4f0e-af48-a1117cb0f9df
    product: '2105'
    variant: null
    quantity: '1'
    total: 2207
    tax: null
    metadata: {  }
  -
    id: ac75bb0d-30f1-4d1e-b194-ac23e3fbb458
    product: '2155'
    variant: null
    quantity: 1
    total: 2360
    tax: null
    metadata: {  }
  -
    id: 5464a42d-6747-46bd-80ab-605e678fe9e0
    product: '2135'
    variant: null
    quantity: '2'
    total: 3860
    tax: null
    metadata: {  }
grand_total: '209.90'
items_total: '173.47'
tax_total: '36.43'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1014'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1664323200
    data: {  }
  -
    status: paid
    timestamp: 1711715654
    data: {  }
---
