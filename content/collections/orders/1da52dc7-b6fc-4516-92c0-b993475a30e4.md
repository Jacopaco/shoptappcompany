---
id: 1da52dc7-b6fc-4516-92c0-b993475a30e4
blueprint: order
order_number: 1051
order_status: placed
payment_status: paid
items:
  -
    id: f17be667-ce96-4808-bc31-d33e8984198b
    product: '1881'
    variant: null
    quantity: 1
    total: 8920
    tax: null
    metadata: {  }
  -
    id: 0709552b-a076-4501-bfe5-aeb4ce118fd1
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: d87db930-f096-4cf7-ab9a-a12d31e5dee6
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 1348548c-7cc0-4dbf-a532-060d005245d1
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax: null
    metadata: {  }
grand_total: '248.22'
items_total: '205.14'
tax_total: '43.08'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1051'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1674432000
    data: {  }
  -
    status: paid
    timestamp: 1711715681
    data: {  }
---
