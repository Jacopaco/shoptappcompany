---
id: 58a1d6f7-cd2e-4ce7-9b67-5e1ea2f904e6
blueprint: order
order_number: 1587
title: '#1587'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Huize Winterdijk'
shipping_street: Winterdijk
shipping_number: '8'
shipping_city: Gouda
shipping_postal_code: 2801SJ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1734449254
    data: {  }
  -
    status: paid
    timestamp: 1734449254
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 5125a63b-4495-49f3-960f-3a7a1a6a398d
    product: '2135'
    variant: null
    quantity: 10
    total: 19950
    tax:
      amount: 4190
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: d2bdd249-905a-42b8-993a-9f66429fbf68
    product: '2110'
    variant: null
    quantity: 2
    total: 9800
    tax:
      amount: 2058
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: e683a326-fd4c-4e31-b8e4-8a2ecba48ca5
    product: '98990'
    variant: null
    quantity: 10
    total: 2010
    tax:
      amount: 422
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 25d1002a-2f13-4bd1-8c14-26141e41a565
    product: cart00914
    variant: null
    quantity: 100
    total: 34900
    tax:
      amount: 7329
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 35d2f3d7-523d-497d-b29b-533a4a62407f
    product: cart00913
    variant: null
    quantity: 50
    total: 17450
    tax:
      amount: 3665
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: fbc876d2-fd15-4b2c-a7f5-93431ef6abd7
    product: '98995'
    variant: null
    quantity: 5
    total: 2805
    tax:
      amount: 589
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 206574f1-c2b6-4c06-b87e-876474c05d3f
    product: cart00960
    variant: null
    quantity: 1
    total: 3004
    tax:
      amount: 631
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: dbcc1da4-7a48-423b-8ac6-cf2e1b2df95a
    product: cart00915
    variant: null
    quantity: 168
    total: 19656
    tax:
      amount: 4128
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 733b769d-0a2a-4811-9fdb-a1a723d8f416
    product: '14258'
    variant: null
    quantity: 1
    total: 600
    tax:
      amount: 126
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: f990cccf-c85b-45b8-8e89-1948c1fd839d
    product: '7317'
    variant: null
    quantity: 5
    total: 1845
    tax:
      amount: 387
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 135545
items_total: 112020
tax_total: 23525
shipping_total: 0
coupon_total: 0
customer: af849acf-89b2-4eee-9641-de33b297a24c
---
