---
id: 0e6e6b4b-cd25-4b43-a14d-677a3a0d1482
blueprint: order
order_number: 1532
title: '#1532'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'ZW Oud Beijerland Beneden Oostdijk 22 BGG, 3261 KX  Nederland (kostenplaats 97037)'
shipping_street: 'Beneden Oostdijk BGG'
shipping_city: 'Oud Beijerland'
shipping_postal_code: '3261 KX'
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1729836164
    data: {  }
  -
    status: paid
    timestamp: 1729836164
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 0eddec5c-1ffb-4a94-b5af-f90ebb4ebab5
    product: '2135'
    variant: null
    quantity: 10
    total: 19500
    tax:
      amount: 4095
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 24503
items_total: 19500
tax_total: 4253
shipping_total: 750
coupon_total: 0
customer: 96aa3564-af32-4dd6-8e8d-b8d4a5035671
---
