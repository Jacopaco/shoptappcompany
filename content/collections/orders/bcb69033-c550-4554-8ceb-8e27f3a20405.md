---
id: bcb69033-c550-4554-8ceb-8e27f3a20405
blueprint: order
order_number: 1079
order_status: placed
payment_status: paid
items:
  -
    id: cc9fe2ef-96f4-45c1-b4a0-a058f2f15b57
    product: '1881'
    variant: null
    quantity: 1
    total: 8920
    tax: null
    metadata: {  }
  -
    id: 6291ce73-7af9-40cc-b132-a596a13768d6
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax: null
    metadata: {  }
  -
    id: 03681ec1-da05-4cb6-a781-5095d0ae43d2
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
  -
    id: d938a54c-f3e7-4743-9a3d-cbae7d30a489
    product: 6990e935-fd66-468f-8c3a-1df9cf44dfd2
    variant: null
    quantity: 5
    total: 2575
    tax: null
    metadata: {  }
grand_total: '235.85'
items_total: '194.92'
tax_total: '40.93'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1079'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1679356800
    data: {  }
  -
    status: paid
    timestamp: 1711715705
    data: {  }
---
