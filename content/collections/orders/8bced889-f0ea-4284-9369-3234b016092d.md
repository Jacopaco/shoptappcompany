---
id: 8bced889-f0ea-4284-9369-3234b016092d
blueprint: order
order_number: 1646
title: '#1646'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'KL00270 CBS Het Kompas'
shipping_street: Gregoriuslaan
shipping_number: '40'
shipping_city: Lexmond
shipping_postal_code: 4148SZ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1739871742
    data: {  }
  -
    status: paid
    timestamp: 1739871743
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 543f3553-8843-4cfa-bcb2-c4533fb54556
    product: '2110'
    variant: null
    quantity: 8
    total: 45144
    tax:
      amount: 9480
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 0ead216b-9a37-427c-b343-c099f3c3513d
    product: '2135'
    variant: null
    quantity: 12
    total: 25200
    tax:
      amount: 5292
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 85116
items_total: 70344
tax_total: 14772
shipping_total: 0
coupon_total: 0
customer: 9eddba44-5334-4fe9-9383-20ecc48cc44d
---
