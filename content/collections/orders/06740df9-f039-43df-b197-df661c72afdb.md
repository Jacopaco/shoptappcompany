---
id: 06740df9-f039-43df-b197-df661c72afdb
blueprint: order
order_number: 1094
order_status: placed
payment_status: paid
items:
  -
    id: 07a5848f-7159-471b-9252-27b34941e474
    product: '2135'
    variant: null
    quantity: '30'
    total: 58500
    tax: null
    metadata: {  }
grand_total: '707.85'
items_total: '585.00'
tax_total: '122.85'
shipping_total: 0
coupon_total: 0
customer: 24dd550d-4eb7-4333-9c6b-5d25ac8f4750
title: '#1094'
org_id: '41186583'
use_shipping_for_billing: true
shipping_name: 'Zuidwester Goes'
shipping_street: Rommerswalestraat
shipping_city: Goes
shipping_postal_code: '4461 ET'
status_log:
  -
    status: placed
    timestamp: 1681776000
    data: {  }
  -
    status: paid
    timestamp: 1711715720
    data: {  }
---
