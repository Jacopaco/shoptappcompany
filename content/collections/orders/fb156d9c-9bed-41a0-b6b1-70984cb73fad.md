---
id: fb156d9c-9bed-41a0-b6b1-70984cb73fad
blueprint: order
order_number: 1555
title: '#1555'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: be52c23e-3711-426c-9166-b00ad7794231
    product: '2170'
    variant: null
    quantity: 1
    total: 3692
    tax:
      amount: 775
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5dfdd953-97c6-4fbc-a198-bb4845b8a2dc
    product: '2141'
    variant: null
    quantity: 3
    total: 13800
    tax:
      amount: 2898
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 8ddc9295-b2d4-4369-b329-c30c73f36859
    product: '2110'
    variant: null
    quantity: 2
    total: 9800
    tax:
      amount: 2058
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 33023
items_total: 27292
tax_total: 5731
shipping_total: 0
coupon_total: 0
customer: a52ebbe2-8945-47f4-94f8-73bedbc856df
---
