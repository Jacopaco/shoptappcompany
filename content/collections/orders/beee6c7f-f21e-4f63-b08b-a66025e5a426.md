---
id: beee6c7f-f21e-4f63-b08b-a66025e5a426
blueprint: order
order_number: 1004
order_status: placed
payment_status: paid
items:
  -
    id: 92afa76c-21d9-4706-a1e5-4f58f2316b7c
    product: '1881'
    variant: null
    quantity: 3
    total: 26760
    tax: null
    metadata: {  }
  -
    id: bfe2d6df-7dd8-408c-86ba-96d739c5d837
    product: '2105'
    variant: null
    quantity: 3
    total: 6621
    tax: null
    metadata: {  }
  -
    id: 0e4beabb-e24c-49f8-a517-10de80fe8135
    product: '2135'
    variant: null
    quantity: 5
    total: 9650
    tax: null
    metadata: {  }
  -
    id: 4ac1fd08-d40f-411a-ad62-e0d14679cf54
    product: '98990'
    variant: null
    quantity: 2
    total: 5256
    tax: null
    metadata: {  }
grand_total: '584.27'
items_total: '482.87'
tax_total: '101.40'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1004'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1660608000
    data: {  }
  -
    status: paid
    timestamp: 1711715646
    data: {  }
---
