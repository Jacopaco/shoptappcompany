---
id: ccb71ec6-1397-446e-abe6-22c2f71e8377
blueprint: order
order_number: 1305
title: '#1305'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 98239e62-1831-4eee-9431-47420a0bc54e
    product: '8315'
    variant: null
    quantity: 4
    total: 132540
    tax:
      amount: 27833
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 160373
items_total: 132540
tax_total: 27833
shipping_total: 0
coupon_total: 0
---
