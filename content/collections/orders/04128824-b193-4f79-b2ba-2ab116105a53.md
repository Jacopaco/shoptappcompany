---
id: 04128824-b193-4f79-b2ba-2ab116105a53
blueprint: order
order_number: 1361
title: '#1361'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Zuidwester Goes'
shipping_street: Rommerswalestraat
shipping_number: '1'
shipping_city: Goes
shipping_postal_code: '4461 ET'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1715168814
    data: {  }
  -
    status: paid
    timestamp: 1715168814
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 8b2e1545-d20b-4223-8be1-cb18fcfd75e6
    product: '2135'
    variant: null
    quantity: 2
    total: 3900
    tax:
      amount: 819
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 5469
items_total: 3900
tax_total: 819
shipping_total: 750
coupon_total: 0
customer: 56c1b994-6eff-4f75-a45c-485118c3ab3e
---
