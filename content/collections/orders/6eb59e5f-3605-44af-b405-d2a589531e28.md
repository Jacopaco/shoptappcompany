---
id: 6eb59e5f-3605-44af-b405-d2a589531e28
blueprint: order
order_number: 1032
order_status: placed
payment_status: paid
items:
  -
    id: ad7a6328-b07f-433f-8adb-4da2feaa44fd
    product: '2200'
    variant: null
    quantity: 3
    total: 10230
    tax: null
    metadata: {  }
  -
    id: 13c609c1-c518-4ae6-9179-337ab4d49000
    product: '2161'
    variant: null
    quantity: 6
    total: 20820
    tax: null
    metadata: {  }
  -
    id: ee2e3e55-310d-4286-a93c-145698eb2756
    product: '14257'
    variant: null
    quantity: 1
    total: 6000
    tax: null
    metadata: {  }
grand_total: '448.31'
items_total: '370.50'
tax_total: '77.81'
shipping_total: 0
coupon_total: 0
customer: bc28621c-a733-4d4f-ac05-47217c1c76a7
title: '#1032'
use_shipping_for_billing: true
shipping_name: 'Istanbul Steakhouse'
shipping_street: Slotermeerlaan
shipping_city: A
shipping_postal_code: '1063 JN'
status_log:
  -
    status: placed
    timestamp: 1669248000
    data: {  }
  -
    status: paid
    timestamp: 1711715667
    data: {  }
---
