---
id: f0780de8-575e-4104-a40f-dfa97f3cdb28
blueprint: order
order_number: 1519
title: '#1519'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Christelijke Basisschool (CBS) Het Kompas'
shipping_street: Gregoriuslaan
shipping_number: '40'
shipping_city: Lexmond
shipping_postal_code: 4148SZ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1729152961
    data: {  }
  -
    status: paid
    timestamp: 1729152961
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 95c83dc8-5004-4a48-ad72-b80357d576ae
    product: '2135'
    variant: null
    quantity: 12
    total: 25200
    tax:
      amount: 5292
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 30492
items_total: 25200
tax_total: 5292
shipping_total: 0
coupon_total: 0
customer: 9eddba44-5334-4fe9-9383-20ecc48cc44d
---
