---
id: 5268f7ce-0da5-49d4-b4ae-71b9ab1204c7
blueprint: order
order_number: 1150
order_status: placed
payment_status: paid
items:
  -
    id: e9263eb9-7943-4868-8847-4b1b3bfab737
    product: '2302070'
    variant: null
    quantity: 1
    total: 2320
    tax: null
    metadata: {  }
  -
    id: fee35976-1580-408c-a7cb-984fa417e7f9
    product: '2002130'
    variant: null
    quantity: 1
    total: 3720
    tax: null
    metadata: {  }
  -
    id: 66384397-e334-4880-866a-9ae5c311617e
    product: '2105'
    variant: null
    quantity: 4
    total: 8828
    tax: null
    metadata: {  }
  -
    id: 2e2a1aa7-7985-4768-8427-fb27c9a98858
    product: '2135'
    variant: null
    quantity: 10
    total: 19300
    tax: null
    metadata: {  }
  -
    id: 1b9f83b4-2e37-4e6e-a73c-31c9ab8930a5
    product: '6549'
    variant: null
    quantity: 8
    total: 63200
    tax: null
    metadata: {  }
grand_total: '1,178.15'
items_total: '973.68'
tax_total: '204.47'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1150'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1690416000
    data: {  }
  -
    status: paid
    timestamp: 1711715788
    data: {  }
---
