---
id: c474a824-4696-416c-8997-fd7744273f8f
blueprint: order
order_number: 1524
title: '#1524'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'ZW Hellevoetsluis  Schoolstraat 14 Dagbesteding, 3224 AM Kostenplaats 97035'
shipping_street: Schoolstraat
shipping_number: '14'
shipping_city: Hellevoetsluis
shipping_postal_code: '3224 AM'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1729587741
    data: {  }
  -
    status: paid
    timestamp: 1729587741
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 42ab475c-a2d6-4544-9396-49cc77167391
    product: 98914-tapp
    variant: null
    quantity: 6
    total: 3900
    tax:
      amount: 819
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 47a14f9c-e38e-4e58-b708-71ae9b57b011
    product: '2110'
    variant: null
    quantity: 5
    total: 24500
    tax:
      amount: 5145
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 8a8a6bbe-ceb0-43e5-b1f4-21bbc67f2a46
    product: '2135'
    variant: null
    quantity: 6
    total: 11700
    tax:
      amount: 2457
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: d1f7287a-e98d-433e-9b63-9c989776ed77
    product: '2186'
    variant: null
    quantity: 3
    total: 6900
    tax:
      amount: 1449
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: cc9cc67d-4ac7-4542-a274-c4f607b00e0d
    product: '98910'
    variant: null
    quantity: 12
    total: 4380
    tax:
      amount: 920
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 62170
items_total: 51380
tax_total: 10790
shipping_total: 0
coupon_total: 0
customer: 96aa3564-af32-4dd6-8e8d-b8d4a5035671
---
