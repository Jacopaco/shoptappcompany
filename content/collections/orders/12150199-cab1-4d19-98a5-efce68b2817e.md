---
id: 12150199-cab1-4d19-98a5-efce68b2817e
blueprint: order
order_number: 1147
order_status: placed
payment_status: paid
items:
  -
    id: 76942164-ef10-4f98-a427-1ac9fc41c02b
    product: '2130'
    variant: null
    quantity: 8
    total: 23616
    tax: null
    metadata: {  }
  -
    id: 385ab8ac-0771-4041-b9bf-6966f30aa5ca
    product: '2110'
    variant: null
    quantity: 4
    total: 20400
    tax: null
    metadata: {  }
  -
    id: 5d459e4d-7fce-46ef-a768-a2aa971ff91f
    product: '2921033'
    variant: null
    quantity: 1
    total: 3645
    tax: null
    metadata: {  }
grand_total: '576.70'
items_total: '476.61'
tax_total: '100.09'
shipping_total: 0
coupon_total: 0
customer: 96aa3564-af32-4dd6-8e8d-b8d4a5035671
title: '#1147'
org_id: '41186583'
use_shipping_for_billing: true
shipping_name: 'ZW OBL Van Schendelstraat 9 Dagbesteding, 3261 SJ (kostenplaats 97037)'
shipping_street: 'Van Schendelstraat'
shipping_city: 'Oud Beijerland'
shipping_postal_code: '3261 SJ'
status_log:
  -
    status: placed
    timestamp: 1689206400
    data: {  }
  -
    status: paid
    timestamp: 1711715784
    data: {  }
---
