---
id: 69cd6d91-7fe9-433a-83c2-0d5315dddd07
blueprint: order
order_number: 1292
title: '#1292'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 2cd6d631-b3d9-4b55-9713-4aa4256219ab
    product: '21419953'
    variant: null
    quantity: 1
    total: 27300
    tax:
      amount: 5733
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 33033
items_total: 27300
tax_total: 5733
shipping_total: 0
coupon_total: 0
---
