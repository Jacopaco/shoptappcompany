---
id: 3cc8fa38-8c1b-434c-b705-834b14c5e1ab
blueprint: order
order_number: 1471
title: '#1471'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'Tandartsenpraktijk Curfs'
shipping_street: Rijksweg
shipping_number: '101'
shipping_city: Gulpen
shipping_postal_code: '6271 AD'
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1725444004
    data: {  }
  -
    status: paid
    timestamp: 1725444004
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: c28d15c7-7b25-44d9-8429-31b9b9ea36f8
    product: '98920'
    variant: null
    quantity: 6
    total: 6996
    tax:
      amount: 1469
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: bd0f4735-25bc-4b03-a7aa-4165ae6894b7
    product: '98921'
    variant: null
    quantity: 6
    total: 5400
    tax:
      amount: 1134
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 15907
items_total: 12396
tax_total: 2761
shipping_total: 750
coupon_total: 0
customer: a399bf7a-1e5f-4080-a0da-02cc21ab3143
---
