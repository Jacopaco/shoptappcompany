---
id: f2a94b6f-5d66-4eb6-ae3e-e615ffd3f0cf
blueprint: order
order_number: 1405
title: '#1405'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_number: '47'
shipping_city: Arnhem
shipping_postal_code: 6815AD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1718616885
    data: {  }
  -
    status: paid
    timestamp: 1718616885
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 1de979f7-1b80-431d-bfce-2357909124c3
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax:
      amount: 571
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5b6f6043-e8e5-4c3b-bd41-eb6d655ed011
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax:
      amount: 463
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: c7aab89e-29ad-4bb8-961a-9122db0a3bca
    product: 6540-1881
    variant: null
    quantity: 2
    total: 19142
    tax:
      amount: 4020
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 71eb704f-24c7-41a8-bd35-930d4d5a4d87
    product: '98995'
    variant: null
    quantity: 12
    total: 5940
    tax:
      amount: 1247
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: fd148c95-6a87-42d6-9bad-272e405eb48c
    product: '2135'
    variant: null
    quantity: 1
    total: 1930
    tax:
      amount: 405
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 38645
items_total: 31939
tax_total: 6706
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
---
