---
id: 8ebe4daf-5ecb-475d-a6ab-b3d1a8f9222e
blueprint: order
order_number: 1410
title: '#1410'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: Hollywoud
shipping_street: Sportlaan
shipping_number: '59'
shipping_city: Almkerk
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1718970210
    data: {  }
  -
    status: paid
    timestamp: 1718970210
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 7e0b656e-6091-488a-add5-e4a96ed96ad7
    product: '2186'
    variant: null
    quantity: 6
    total: 15030
    tax:
      amount: 3156
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6b68e2a3-c5ff-4806-bc3c-4df669fb6abb
    product: '2105'
    variant: null
    quantity: 4
    total: 7600
    tax:
      amount: 1596
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 28132
items_total: 22630
tax_total: 4752
shipping_total: 750
coupon_total: 0
customer: 36029b57-8cbd-4fb0-a6e5-47205dbb5063
---
