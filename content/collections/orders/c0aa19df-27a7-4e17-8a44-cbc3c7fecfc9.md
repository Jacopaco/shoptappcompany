---
id: c0aa19df-27a7-4e17-8a44-cbc3c7fecfc9
blueprint: order
order_number: 1257
order_status: placed
payment_status: paid
items:
  -
    id: 3899ecae-b5f6-4669-b3d2-b7c84502e6ed
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 3acd6304-42b4-4468-a7f5-740bdcbaffdb
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
  -
    id: 128b48db-91da-4daa-9ee2-670be0181562
    product: 6540-1881
    variant: null
    quantity: 1
    total: 9571
    tax: null
    metadata: {  }
grand_total: '239.28'
items_total: '197.75'
tax_total: '41.53'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1257'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1707868800
    data: {  }
  -
    status: paid
    timestamp: 1711715937
    data: {  }
---
