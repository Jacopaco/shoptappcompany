---
id: aea966a0-6cda-4e8e-a7a3-cb0c26adf6f8
blueprint: order
order_number: 1161
order_status: placed
payment_status: paid
items:
  -
    id: fb1c5b78-ccea-4188-9df3-8ea0cc3ed0e2
    product: '2002130'
    variant: null
    quantity: 1
    total: 3720
    tax: null
    metadata: {  }
  -
    id: 340912ca-a476-4071-a097-081d0fd0701f
    product: '2105'
    variant: null
    quantity: 6
    total: 13242
    tax: null
    metadata: {  }
  -
    id: 85d8212e-cac4-4246-a90f-3cb66c5ce4a8
    product: '2135'
    variant: null
    quantity: 6
    total: 11580
    tax: null
    metadata: {  }
  -
    id: e4491ffc-66df-42f6-828b-d19933549f59
    product: '6549'
    variant: null
    quantity: 7
    total: 55300
    tax: null
    metadata: {  }
grand_total: '1,014.49'
items_total: '838.42'
tax_total: '176.07'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1161'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1694044800
    data: {  }
  -
    status: paid
    timestamp: 1711715802
    data: {  }
---
