---
id: 90faedae-69d1-4fe2-8388-71865313aded
blueprint: order
order_number: 1021
order_status: placed
payment_status: paid
items:
  -
    id: da366666-d23b-4dac-968f-bd1ae39e48ba
    product: 6545-180
    variant: null
    quantity: '8'
    total: 71360
    tax: null
    metadata: {  }
grand_total: '863.46'
items_total: '713.60'
tax_total: '149.86'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1021'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1666224000
    data: {  }
  -
    status: paid
    timestamp: 1711715658
    data: {  }
---
