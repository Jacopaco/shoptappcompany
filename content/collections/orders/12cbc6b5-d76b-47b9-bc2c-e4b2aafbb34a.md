---
id: 12cbc6b5-d76b-47b9-bc2c-e4b2aafbb34a
blueprint: order
order_number: 1308
title: '#1308'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Dental Expo'
shipping_street: Europaplein
shipping_number: '24'
shipping_city: Amsterdam
shipping_postal_code: 1234DB
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1712134897
    data: {  }
  -
    status: paid
    timestamp: 1712134898
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 95aa7e48-1e16-41a0-b4f8-e40c6e6982d4
    product: '74415'
    variant: null
    quantity: 1
    total: 2823
    tax:
      amount: 593
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 4166
items_total: 2823
tax_total: 593
shipping_total: 750
coupon_total: 0
customer: b94aecbf-d569-4e0e-9483-8c8be947ec81
---
