---
id: cde699ca-b708-48af-9d10-66fa3d5894da
blueprint: order
order_number: 1636
title: '#1636'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: dee56255-6674-4430-b271-a8398a146676
    product: '2141'
    variant: null
    quantity: 3
    total: 13800
    tax:
      amount: 2898
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 17606
items_total: 13800
tax_total: 3056
shipping_total: 750
coupon_total: 0
customer: e35c03c4-e232-4e12-b229-9f1b24deb3f1
---
