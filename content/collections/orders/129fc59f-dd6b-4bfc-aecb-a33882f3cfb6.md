---
id: 129fc59f-dd6b-4bfc-aecb-a33882f3cfb6
blueprint: order
order_number: 1417
title: '#1417'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Camping de Koornmolen'
shipping_street: 'Tweemanspolder a'
shipping_city: Zevenhuizen
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1719557062
    data: {  }
  -
    status: paid
    timestamp: 1719557062
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 6db744cf-aa50-48ed-b0e4-414e13d415a2
    product: '2105'
    variant: null
    quantity: 25
    total: 49750
    tax:
      amount: 10448
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 60198
items_total: 49750
tax_total: 10448
shipping_total: 0
coupon_total: 0
customer: 3ed9b088-3809-4b23-b621-30e5b45d230a
---
