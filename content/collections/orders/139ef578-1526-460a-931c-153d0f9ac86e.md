---
id: 139ef578-1526-460a-931c-153d0f9ac86e
blueprint: order
order_number: 1448
title: '#1448'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'Lagarde Groep'
shipping_street: Mercuriusweg
shipping_number: '45'
shipping_city: Barneveld
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1722869571
    data: {  }
  -
    status: paid
    timestamp: 1722869571
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 23662e22-7923-4b04-b930-75af002b3b48
    product: '2135'
    variant: null
    quantity: 4
    total: 7800
    tax:
      amount: 1638
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 54c91954-1ac5-47f8-b6e5-b0c1b11c1c54
    product: '2200'
    variant: null
    quantity: 2
    total: 6316
    tax:
      amount: 1326
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 17988
items_total: 14116
tax_total: 3122
shipping_total: 750
coupon_total: 0
customer: 7f976cba-5263-43e6-9a27-729a06c3551a
---
