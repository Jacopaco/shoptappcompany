---
id: 6d62ce46-3a44-46e2-b027-d90399df4d02
blueprint: order
order_number: 1313
title: '#1313'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 7930fc77-9141-4945-af1d-e211801651b5
    product: '5542'
    variant: null
    quantity: 5
    total: 15000
    tax:
      amount: 3150
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 18900
items_total: 15000
tax_total: 3150
shipping_total: 750
coupon_total: 0
---
