---
id: 74dc8101-b383-4bdf-8f0b-34de664f6f33
blueprint: order
order_number: 1476
title: '#1476'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Huize Winterdijk'
shipping_street: Winterdijk
shipping_number: '8'
shipping_city: Gouda
shipping_postal_code: 2801SJ
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1725897180
    data: {  }
  -
    status: paid
    timestamp: 1725897180
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: cd292073-1c94-45a6-ad55-676fe5e1f8d0
    product: '2135'
    variant: null
    quantity: 5
    total: 9975
    tax:
      amount: 2095
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: c242354b-e4b4-465e-ace8-f371909cdec2
    product: '2100'
    variant: null
    quantity: 2
    total: 5850
    tax:
      amount: 1229
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: c4001f2f-e983-4adc-b9ad-0fc3ff78b7b1
    product: '2110'
    variant: null
    quantity: 1
    total: 4900
    tax:
      amount: 1029
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: f3468827-8630-422d-b1e5-04014dc3ec01
    product: cart00960
    variant: null
    quantity: 3
    total: 9012
    tax:
      amount: 1893
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6e096dd6-4cf2-47e9-921f-befa18256b3b
    product: '2250'
    variant: null
    quantity: 1
    total: 3756
    tax:
      amount: 789
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 0f1c82a7-bd3d-4374-b43f-1f58234f4939
    product: '2255'
    variant: null
    quantity: 1
    total: 2601
    tax:
      amount: 546
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 84c88d02-2a56-4269-9492-942d5109ce42
    product: '98990'
    variant: null
    quantity: 20
    total: 4020
    tax:
      amount: 844
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: cfdafb9f-c904-4449-b16f-34a5a10192f2
    product: '83990010'
    variant: null
    quantity: 1
    total: 7191
    tax:
      amount: 1510
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5cbb21a9-314d-44ce-a7ec-95ab5a1c3163
    product: cart00914
    variant: null
    quantity: 20
    total: 5980
    tax:
      amount: 1256
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 3b920643-5031-4131-b54b-36427d42c6c0
    product: cart00915
    variant: null
    quantity: 72
    total: 8424
    tax:
      amount: 1769
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: a350ea9f-49f7-4134-a034-54a1c3d4da41
    product: '74416'
    variant: null
    quantity: 1
    total: 2461
    tax:
      amount: 517
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 77647
items_total: 64170
tax_total: 13477
shipping_total: 0
coupon_total: 0
customer: af849acf-89b2-4eee-9641-de33b297a24c
---
