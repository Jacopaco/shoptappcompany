---
id: 941c0d7a-a86f-4cfc-a919-7aeeaaaff007
blueprint: order
order_number: 1558
title: '#1558'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Stichting Zideris'
shipping_street: Cuneraweg
shipping_number: '12'
shipping_city: Rhenen
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1732020644
    data: {  }
  -
    status: paid
    timestamp: 1732020644
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: c7035f47-553e-41eb-bcb8-1b2356c7b379
    product: cart00914
    variant: null
    quantity: 30
    total: 8970
    tax:
      amount: 1884
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: a643f78a-6d2b-440f-90cb-107182f5b5de
    product: '2145'
    variant: null
    quantity: 6
    total: 22218
    tax:
      amount: 4666
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: f37052c7-bdf2-4f57-905b-d1111ea5d4b4
    product: '2189'
    variant: null
    quantity: 8
    total: 30128
    tax:
      amount: 6327
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: ca9b4809-7b1d-4b8d-9a04-c93f2975f189
    product: '98990'
    variant: null
    quantity: 48
    total: 14640
    tax:
      amount: 3074
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 91907
items_total: 75956
tax_total: 15951
shipping_total: 0
coupon_total: 0
customer: d67cc734-8a62-4a1c-9400-e9f1ffb70939
---
