---
id: 237690ff-fd13-40e9-a73b-9604792e75c3
blueprint: order
order_number: 1096
order_status: placed
payment_status: paid
items:
  -
    id: 7c73afb6-6456-45e0-ad5b-dca8eeb2b302
    product: '2142'
    variant: null
    quantity: '10'
    total: 56260
    tax: null
    metadata: {  }
  -
    id: 01ea1dfb-65a2-4459-a66d-e1d07902bfb7
    product: '98990'
    variant: null
    quantity: '6'
    total: 21960
    tax: null
    metadata: {  }
  -
    id: 501d75d2-6842-4183-8b00-80971a38ec6c
    product: b3ac5eff-8aa3-4430-a86d-43f7d85384d7
    variant: null
    quantity: '100'
    total: 51500
    tax: null
    metadata: {  }
  -
    id: 35360b5b-8520-4ef4-976a-85413e0b64c0
    product: '2110'
    variant: null
    quantity: '2'
    total: 11286
    tax: null
    metadata: {  }
grand_total: '1,706.17'
items_total: '1,410.06'
tax_total: '296.11'
shipping_total: 0
coupon_total: 0
customer: d67cc734-8a62-4a1c-9400-e9f1ffb70939
title: '#1096'
use_shipping_for_billing: true
shipping_name: 'Stichting Zideris'
shipping_street: Cuneraweg
shipping_city: Rhenen
status_log:
  -
    status: placed
    timestamp: 1681776000
    data: {  }
  -
    status: paid
    timestamp: 1711715721
    data: {  }
---
