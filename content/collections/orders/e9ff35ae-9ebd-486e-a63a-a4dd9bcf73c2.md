---
id: e9ff35ae-9ebd-486e-a63a-a4dd9bcf73c2
blueprint: order
order_number: 1124
order_status: placed
payment_status: paid
items:
  -
    id: 4cd558df-eaec-4f15-8a64-1adeb572452b
    product: '1881'
    variant: null
    quantity: 1
    total: 8920
    tax: null
    metadata: {  }
  -
    id: 18066c66-5ab8-44da-9473-372c3d46176c
    product: '188'
    variant: null
    quantity: 1
    total: 8746
    tax: null
    metadata: {  }
  -
    id: bf6b2ee0-d30a-47b8-b7b9-116a6664f2fc
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax: null
    metadata: {  }
  -
    id: 98825e76-3e52-415b-bc9a-5a0e708a7ec4
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: a02226b3-a3b4-4b8c-b4d7-85cc9291a864
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax: null
    metadata: {  }
  -
    id: d2d7e98c-8feb-4648-a973-e1875f26607e
    product: '2170'
    variant: null
    quantity: 1
    total: 2995
    tax: null
    metadata: {  }
  -
    id: 3a3b7f37-6185-401d-a42d-256425b0d5ef
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax: null
    metadata: {  }
grand_total: '396.49'
items_total: '327.68'
tax_total: '68.81'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1124'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1685923200
    data: {  }
  -
    status: paid
    timestamp: 1711715754
    data: {  }
---
