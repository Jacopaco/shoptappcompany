---
id: 423aedfe-eeb1-441a-b60b-eb95b9b98cb5
blueprint: order
order_number: 1547
title: '#1547'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: Beekheim
shipping_street: 'Ds. E. Fransenlaan'
shipping_number: '9'
shipping_city: Barneveld
shipping_postal_code: '3772 TX'
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1731496989
    data: {  }
  -
    status: paid
    timestamp: 1731496989
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: f3adb66a-996e-4312-ba45-3e3e93cfd6d2
    product: '2135'
    variant: null
    quantity: 3
    total: 5850
    tax:
      amount: 1229
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: e1e33674-5ac9-4028-a812-71a251030d8c
    product: '2110'
    variant: null
    quantity: 3
    total: 14700
    tax:
      amount: 3087
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5edd055e-eda3-4ba6-bb0a-919cdc4045a1
    product: cart00914
    variant: null
    quantity: 20
    total: 5980
    tax:
      amount: 1256
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: afdf4872-0ccb-4bef-8ccd-0cd04ef12ffd
    product: cart00913
    variant: null
    quantity: 10
    total: 2990
    tax:
      amount: 628
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 35720
items_total: 29520
tax_total: 6200
shipping_total: 0
coupon_total: 0
customer: bf46aa1e-9b87-40fb-9876-b60c1f133550
---
