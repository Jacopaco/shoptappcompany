---
id: e4fc6029-fb7b-4b68-8471-0f53edc3b7cf
blueprint: order
order_number: 1466
title: '#1466'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: Hollywoud
shipping_street: Sportlaan
shipping_number: '59'
shipping_city: Almkerk
shipping_postal_code: 4286ES
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1724672173
    data: {  }
  -
    status: paid
    timestamp: 1724672173
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 0b0278c4-242b-44ad-af8c-febc3d61f2ae
    product: cart00927
    variant: null
    quantity: 13
    total: 4875
    tax:
      amount: 1024
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 7518f289-d57f-4f70-a54e-ce0886cf12e3
    product: cart00956
    variant: null
    quantity: 7
    total: 2625
    tax:
      amount: 551
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 9983
items_total: 7500
tax_total: 1733
shipping_total: 750
coupon_total: 0
customer: 36029b57-8cbd-4fb0-a6e5-47205dbb5063
---
