---
id: eaf7c485-951f-44d0-8ead-dfb6ff860fdb
blueprint: order
order_number: 1186
order_status: placed
payment_status: paid
items:
  -
    id: c0ba2eff-d6d0-41d9-a6a3-5130f0af6481
    product: '14243'
    variant: null
    quantity: 5
    total: 3000
    tax: null
    metadata: {  }
  -
    id: 914527f3-7234-47ec-8357-bf27671a74e3
    product: '2135'
    variant: null
    quantity: 4
    total: 7800
    tax: null
    metadata: {  }
  -
    id: bba9ef8b-32f5-4cb1-962e-2d1f85ead8ba
    product: 98914-tapp
    variant: null
    quantity: 6
    total: 3900
    tax: null
    metadata: {  }
grand_total: '177.87'
items_total: '147.00'
tax_total: '30.87'
shipping_total: 0
coupon_total: 0
customer: bf46aa1e-9b87-40fb-9876-b60c1f133550
title: '#1186'
use_shipping_for_billing: true
shipping_name: Beekheim
shipping_street: 'Ds. E. Fransenlaan'
shipping_city: Barneveld
shipping_postal_code: '3772 TX'
status_log:
  -
    status: placed
    timestamp: 1698019200
    data: {  }
  -
    status: paid
    timestamp: 1711715835
    data: {  }
---
