---
id: 5bd63acf-ac6d-42b6-b7b7-edba4b38465b
blueprint: order
order_number: 1419
title: '#1419'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_number: '47'
shipping_city: Arnhem
shipping_postal_code: 6815AD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1719913848
    data: {  }
  -
    status: paid
    timestamp: 1719913848
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 6ef217c8-e165-42cd-8d43-340eb7056af8
    product: '2250'
    variant: null
    quantity: 1
    total: 2720
    tax:
      amount: 571
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 893938d7-2a8d-4431-959c-49b258cddc09
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax:
      amount: 811
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 627ca4a6-7e38-473e-bb6d-3936d086a85a
    product: '2105'
    variant: null
    quantity: 1
    total: 2207
    tax:
      amount: 463
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 9e0dd797-3cc8-462f-9d9f-8b0ddc2c9228
    product: '98990'
    variant: null
    quantity: 12
    total: 2580
    tax:
      amount: 542
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 64d7570d-89c0-4caf-9843-d2ae20154f28
    product: '98995'
    variant: null
    quantity: 12
    total: 5940
    tax:
      amount: 1247
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 8062b83e-16bb-49a5-93fc-a868c0d45642
    product: cart00914
    variant: null
    quantity: 2
    total: 598
    tax:
      amount: 126
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 5042546f-dfff-4dd8-9b5d-9d28fd4cf9f6
    product: cart00641
    variant: null
    quantity: 2
    total: 598
    tax:
      amount: 126
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 92d6d01c-b509-49db-bddf-5edaba93840a
    product: cart00647
    variant: null
    quantity: 3
    total: 897
    tax:
      amount: 188
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: a6ee858c-262b-4829-a138-75d3490c6067
    product: '2155'
    variant: null
    quantity: 1
    total: 2360
    tax:
      amount: 496
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 27080
items_total: 21760
tax_total: 4570
shipping_total: 750
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
---
