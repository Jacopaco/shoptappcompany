---
id: fb423585-fb70-481d-ba3f-2fa0e83da5f5
blueprint: order
order_number: 1219
order_status: placed
payment_status: paid
items:
  -
    id: 913dd212-fb98-4b9b-8617-61f8e8938192
    product: '2135'
    variant: null
    quantity: 4
    total: 7800
    tax: null
    metadata: {  }
  -
    id: 301dd934-4892-45fa-ba84-bde94380778d
    product: '2200'
    variant: null
    quantity: 2
    total: 6316
    tax: null
    metadata: {  }
grand_total: '170.80'
items_total: '141.16'
tax_total: '29.64'
shipping_total: 0
coupon_total: 0
customer: 7f976cba-5263-43e6-9a27-729a06c3551a
title: '#1219'
use_shipping_for_billing: true
shipping_name: 'Lagarde Groep'
shipping_street: Mercuriusweg
shipping_city: Barneveld
status_log:
  -
    status: placed
    timestamp: 1702339200
    data: {  }
  -
    status: paid
    timestamp: 1711715880
    data: {  }
---
