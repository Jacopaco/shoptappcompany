---
id: ad399f30-c378-448f-b9c7-550a85a4c387
blueprint: order
order_number: 1268
order_status: placed
payment_status: paid
items:
  -
    id: deb89d0f-a06e-400c-9a30-c85238af0289
    product: '2187'
    variant: null
    quantity: 2
    total: 6656
    tax: null
    metadata: {  }
  -
    id: 2f4b21bc-e9e3-4431-b122-02812da79441
    product: '2250'
    variant: null
    quantity: 1
    total: 3756
    tax: null
    metadata: {  }
  -
    id: fb330936-abf8-43b2-99f9-c0f1b0c24970
    product: '2135'
    variant: null
    quantity: 7
    total: 13650
    tax: null
    metadata: {  }
grand_total: '291.15'
items_total: '240.62'
tax_total: '50.53'
shipping_total: 0
coupon_total: 0
customer: ff9c6ae5-8b31-4b1e-ada1-5f32db8b66b9
title: '#1268'
use_shipping_for_billing: true
shipping_name: Veldheim
shipping_street: Bovenbuurtweg
shipping_city: Ede
shipping_postal_code: '6717 XA'
status_log:
  -
    status: placed
    timestamp: 1708992000
    data: {  }
  -
    status: paid
    timestamp: 1711715952
    data: {  }
---
