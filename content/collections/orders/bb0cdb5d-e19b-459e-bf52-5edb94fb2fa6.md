---
id: bb0cdb5d-e19b-459e-bf52-5edb94fb2fa6
blueprint: order
order_number: 1148
order_status: placed
payment_status: paid
items:
  -
    id: f2b8b02e-750e-4612-ba4d-17f41ea2542a
    product: '6533'
    variant: null
    quantity: 1
    total: 3320
    tax: null
    metadata: {  }
  -
    id: 8e6bdd9b-8d9a-422f-b2d0-b2ac089ebb5a
    product: '2302070'
    variant: null
    quantity: 2
    total: 4640
    tax: null
    metadata: {  }
  -
    id: 07ff89a9-8b5e-47bc-a5e4-dee645003c28
    product: '2002130'
    variant: null
    quantity: 1
    total: 3720
    tax: null
    metadata: {  }
  -
    id: f77da43d-c682-4c40-8f70-930bd67fe0e1
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 6584ef88-90e6-45f2-a999-9575c0ed9f45
    product: '2170'
    variant: null
    quantity: 1
    total: 2995
    tax: null
    metadata: {  }
  -
    id: 9471f59c-3b46-44a5-bc00-f660745cf512
    product: '2135'
    variant: null
    quantity: 8
    total: 15440
    tax: null
    metadata: {  }
  -
    id: 7eab8db2-03d9-4f25-a582-58781643b9c9
    product: '151057'
    variant: null
    quantity: 2
    total: 10062
    tax: null
    metadata: {  }
  -
    id: 17ef5eb7-751b-492c-9572-575a5fd4e3a2
    product: '6549'
    variant: null
    quantity: 6
    total: 47400
    tax: null
    metadata: {  }
grand_total: '1,113.09'
items_total: '919.91'
tax_total: '193.18'
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
title: '#1148'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Deventer'
shipping_street: 'Nico Bolkesteinlaan'
shipping_city: Deventer
shipping_postal_code: '7416 SE'
status_log:
  -
    status: placed
    timestamp: 1689811200
    data: {  }
  -
    status: paid
    timestamp: 1711715786
    data: {  }
---
