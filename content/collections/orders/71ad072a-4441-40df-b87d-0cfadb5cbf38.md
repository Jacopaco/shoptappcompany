---
id: 71ad072a-4441-40df-b87d-0cfadb5cbf38
blueprint: order
order_number: 1087
order_status: placed
payment_status: paid
items:
  -
    id: f9255138-b0f9-4439-800e-3ae8189afdfa
    product: '14257'
    variant: null
    quantity: '1'
    total: 6000
    tax: null
    metadata: {  }
  -
    id: 10510b62-51be-40de-b074-a8e42f83a41d
    product: '2255'
    variant: null
    quantity: '1'
    total: 2930
    tax: null
    metadata: {  }
grand_total: '108.05'
items_total: '89.30'
tax_total: '18.75'
shipping_total: 0
coupon_total: 0
customer: af849acf-89b2-4eee-9641-de33b297a24c
title: '#1087'
use_shipping_for_billing: true
shipping_name: 'Huize Winterdijk'
status_log:
  -
    status: placed
    timestamp: 1680566400
    data: {  }
  -
    status: paid
    timestamp: 1711715713
    data: {  }
---
