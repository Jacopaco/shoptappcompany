---
id: 698c7ed2-a85b-4608-96aa-153b7260d4d1
blueprint: order
order_number: 1522
title: '#1522'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: Streekheim
shipping_street: Kerklaan
shipping_number: '14'
shipping_city: Staphorst
shipping_postal_code: 7951CD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1729500793
    data: {  }
  -
    status: paid
    timestamp: 1729500793
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: e85f4bf7-2af0-4cd4-853a-20b9b734d245
    product: '2105'
    variant: null
    quantity: 5
    total: 7800
    tax:
      amount: 1638
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 05b33469-d4fb-4d95-836c-e84de4e46836
    product: cart00963
    variant: null
    quantity: 50
    total: 14950
    tax:
      amount: 3140
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 75dd7dd9-bb13-4510-b41d-5e33ddf44a08
    product: '2135'
    variant: null
    quantity: 2
    total: 3900
    tax:
      amount: 819
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 32247
items_total: 26650
tax_total: 5597
shipping_total: 0
coupon_total: 0
customer: 0ad5b82e-d57e-419c-bd18-de29fbf06427
---
