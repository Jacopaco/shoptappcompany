---
id: c678b87c-1fe8-4cca-9f1f-e5dd8b5fc88b
blueprint: order
order_number: 1170
order_status: placed
payment_status: paid
items:
  -
    id: e7228b43-a127-4505-9a15-5fb48803b2c4
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
  -
    id: f5fc3084-5bbd-477e-bcc6-ee53f56d016c
    product: 6540-1881
    variant: null
    quantity: 1
    total: 0
    tax: null
    metadata: {  }
  -
    id: 880c0446-e442-4165-aaa6-6ca9f0d064fa
    product: '98995'
    variant: null
    quantity: 12
    total: 4836
    tax: null
    metadata: {  }
grand_total: '128.57'
items_total: '106.26'
tax_total: '22.31'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1170'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1694995200
    data: {  }
  -
    status: paid
    timestamp: 1711715813
    data: {  }
---
