---
id: 535b8203-6102-4ba4-a15f-165727a7663a
blueprint: order
order_number: 1523
title: '#1523'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: Beekheim
shipping_street: 'Ds. E. Fransenlaan'
shipping_number: '9'
shipping_city: Barneveld
shipping_postal_code: '3772 TX'
use_shipping_address_for_billing: '0'
status_log:
  -
    status: placed
    timestamp: 1729510892
    data: {  }
  -
    status: paid
    timestamp: 1729510892
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 2888f192-4e86-4995-86a7-7466fe7680dd
    product: cart00914
    variant: null
    quantity: 30
    total: 8970
    tax:
      amount: 1884
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: d8ea726d-f6c9-4421-8a3c-cfc0ed68649f
    product: cart00647
    variant: null
    quantity: 10
    total: 2990
    tax:
      amount: 628
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 6c57e604-8d74-407d-a8e4-4211dfb1f5da
    product: '2110'
    variant: null
    quantity: 2
    total: 9800
    tax:
      amount: 2058
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 92bbc73f-7fe5-451c-b7cb-bbb9d316296d
    product: '2135'
    variant: null
    quantity: 2
    total: 3900
    tax:
      amount: 819
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 31049
items_total: 25660
tax_total: 5389
shipping_total: 0
coupon_total: 0
customer: bf46aa1e-9b87-40fb-9876-b60c1f133550
---
