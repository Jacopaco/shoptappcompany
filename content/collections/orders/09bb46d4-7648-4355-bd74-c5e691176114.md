---
id: 09bb46d4-7648-4355-bd74-c5e691176114
blueprint: order
order_number: 1647
title: '#1647'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: 4d0a4e33-a826-4a8d-95e2-c7f5deac948c
    product: '2155'
    variant: null
    quantity: 1
    total: 2360
    tax:
      amount: 496
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 77a4a2e9-fad2-4826-b041-d78d57a66005
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax:
      amount: 1216
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 72b35ab1-0ae6-43b0-ae4c-1d587bac2f9b
    product: 6540-1881
    variant: null
    quantity: 2
    total: 20674
    tax:
      amount: 4342
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 4a904414-aae6-4586-a679-3900aa490314
    product: '839160104'
    variant: null
    quantity: 3
    total: 24102
    tax:
      amount: 5061
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 64041
items_total: 52926
tax_total: 11115
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
---
