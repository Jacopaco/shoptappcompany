---
id: 5672b1d3-10c3-4f1c-ba46-ba3b0debfa34
blueprint: order
order_number: 1019
order_status: placed
payment_status: paid
items:
  -
    id: f253da9b-bf70-47f9-b209-196776cdf451
    product: '1881'
    variant: null
    quantity: 1
    total: 8920
    tax: null
    metadata: {  }
  -
    id: 6ae5e00e-c8f2-4d1c-9e3e-8f5f78bd3438
    product: '2250'
    variant: null
    quantity: 2
    total: 5440
    tax: null
    metadata: {  }
  -
    id: 0b193331-1392-4077-8415-cb7a3789f749
    product: '6533'
    variant: null
    quantity: 2
    total: 6640
    tax: null
    metadata: {  }
  -
    id: eb4a854b-eff6-4791-8817-3fa662cee073
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 9e95d841-3891-4113-9f84-182e8664404d
    product: '2135'
    variant: null
    quantity: 2
    total: 3860
    tax: null
    metadata: {  }
  -
    id: 8a4aee01-b86f-40fe-8680-5bca5007cb67
    product: '98995'
    variant: null
    quantity: 1
    total: 5940
    tax: null
    metadata: {  }
  -
    id: dc3d74a9-fe0b-45a9-846d-19f1d766c53c
    product: '98990'
    variant: null
    quantity: 1
    total: 2628
    tax: null
    metadata: {  }
grand_total: '457.89'
items_total: '378.42'
tax_total: '79.47'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1019'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1665532800
    data: {  }
  -
    status: paid
    timestamp: 1711715657
    data: {  }
---
