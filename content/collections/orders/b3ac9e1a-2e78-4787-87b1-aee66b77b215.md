---
id: b3ac9e1a-2e78-4787-87b1-aee66b77b215
blueprint: order
order_number: 1593
title: '#1593'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
shipping_name: 'Griftland College'
shipping_street: Noorderweg
shipping_number: '79'
shipping_city: Soest
shipping_postal_code: 3761EV
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1734615069
    data: {  }
  -
    status: paid
    timestamp: 1734615069
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 3469aa30-e626-4032-89ca-750ace8207a4
    product: '4074'
    variant: null
    quantity: 120
    total: 67200
    tax:
      amount: 14112
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 9c82eb19-366a-4c14-8fd3-c38d0e6177e3
    product: '2110'
    variant: null
    quantity: 5
    total: 24500
    tax:
      amount: 5145
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 23398b25-8a53-4a7f-b72f-6023983d2be2
    product: '2169'
    variant: null
    quantity: 6
    total: 16266
    tax:
      amount: 3416
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: e3e170a9-7ce1-4b37-a1b6-0b65357dec73
    product: '5582'
    variant: null
    quantity: 5
    total: 14735
    tax:
      amount: 3094
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 148468
items_total: 122701
tax_total: 25767
shipping_total: 0
coupon_total: 0
customer: 473e5a0b-63ee-4736-b860-d7a15a4abef1
---
