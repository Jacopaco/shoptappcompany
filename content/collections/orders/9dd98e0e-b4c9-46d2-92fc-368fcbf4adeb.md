---
id: 9dd98e0e-b4c9-46d2-92fc-368fcbf4adeb
blueprint: order
order_number: 1295
title: '#1295'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: c5be4f83-1902-4242-8945-c782d98dcba8
    product: '21419953'
    variant: null
    quantity: 2
    total: 49140
    tax:
      amount: 10319
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 59459
items_total: 49140
tax_total: 10319
shipping_total: 0
coupon_total: 0
customer: b94aecbf-d569-4e0e-9483-8c8be947ec81
---
