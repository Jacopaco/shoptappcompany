---
id: 5746240a-c696-49a8-ba52-310df1db4eb6
blueprint: order
order_number: 1617
title: '#1617'
published: true
shipping_tax:
  amount: 158
  rate: 21
  price_includes_tax: false
shipping_name: 'Rodenburg Zorgoplossingen B.V.'
shipping_street: Fokkerstraat
shipping_number: '19'
shipping_city: Veenendaal
shipping_postal_code: 3905KV
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1737379250
    data: {  }
  -
    status: paid
    timestamp: 1737379250
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 995a023d-8afb-4478-9017-c6919ee8d699
    product: '2135'
    variant: null
    quantity: 3
    total: 5670
    tax:
      amount: 1191
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 026aab04-2361-44a7-9db8-ba8c9307558d
    product: '2120'
    variant: null
    quantity: 3
    total: 6840
    tax:
      amount: 1436
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 16045
items_total: 12510
tax_total: 2785
shipping_total: 750
coupon_total: 0
customer: 090d39da-84df-47b6-b275-14377d47bf2e
---
