---
id: ae874b68-a5aa-4af9-ae5a-7ec2c1bea1b6
blueprint: order
order_number: 1110
order_status: placed
payment_status: paid
items:
  -
    id: db994dce-e549-404a-ac1a-27857b1b5aac
    product: '98917'
    variant: null
    quantity: 1
    total: 489
    tax: null
    metadata: {  }
grand_total: '5.92'
items_total: '4.89'
tax_total: '1.03'
shipping_total: 0
coupon_total: 0
customer: 31b65dea-45c2-4824-9df7-18934a35ed7b
title: '#1110'
use_shipping_for_billing: true
shipping_name: 'Huisarts van Kessel'
shipping_street: Kroostweg
shipping_city: Zeist
status_log:
  -
    status: placed
    timestamp: 1683676800
    data: {  }
  -
    status: paid
    timestamp: 1711715736
    data: {  }
---
