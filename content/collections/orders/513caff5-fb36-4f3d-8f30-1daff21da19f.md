---
id: 513caff5-fb36-4f3d-8f30-1daff21da19f
blueprint: order
order_number: 1026
order_status: placed
payment_status: paid
items:
  -
    id: 88951183-08d7-4a1c-a332-fa3af330543a
    product: '2181'
    variant: null
    quantity: 1
    total: 2525
    tax: null
    metadata: {  }
  -
    id: 91b52e71-c214-48ca-acab-2238966e61a0
    product: 1057aa9f-0403-4213-9f88-3e85adfe7619
    variant: null
    quantity: 10
    total: 5150
    tax: null
    metadata: {  }
grand_total: '92.87'
items_total: '76.75'
tax_total: '16.12'
shipping_total: 0
coupon_total: 0
customer: 1306296e-af27-4a67-9166-cebf43cfa895
title: '#1026'
use_shipping_for_billing: true
shipping_name: 'Van Linschoten Tandartsen'
shipping_street: 'Van Linschotenlaan'
shipping_city: Hilversum
status_log:
  -
    status: placed
    timestamp: 1667952000
    data: {  }
  -
    status: paid
    timestamp: 1711715661
    data: {  }
---
