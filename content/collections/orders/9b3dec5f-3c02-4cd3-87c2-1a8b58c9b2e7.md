---
id: 9b3dec5f-3c02-4cd3-87c2-1a8b58c9b2e7
blueprint: order
order_number: 1422
title: '#1422'
published: true
shipping_tax:
  amount: 0
  rate: 0
  price_includes_tax: false
shipping_name: Streekheim
shipping_street: Kerklaan
shipping_number: '14'
shipping_city: Staphorst
shipping_postal_code: 7951CD
use_shipping_address_for_billing: '1'
status_log:
  -
    status: placed
    timestamp: 1720207985
    data: {  }
  -
    status: paid
    timestamp: 1720207985
    data: {  }
order_status: placed
payment_status: paid
items:
  -
    id: 58bc45b7-af73-4e0b-a0a5-2377d1809359
    product: '2135'
    variant: null
    quantity: 4
    total: 7800
    tax:
      amount: 1638
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 10188
items_total: 7800
tax_total: 1638
shipping_total: 750
coupon_total: 0
customer: 0ad5b82e-d57e-419c-bd18-de29fbf06427
---
