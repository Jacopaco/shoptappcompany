---
id: bbd3adf1-b46d-48cc-bf65-eb068fcc8fa9
blueprint: order
order_number: 1090
order_status: placed
payment_status: paid
items:
  -
    id: 1f70266e-75b0-4bd2-a62c-4f848d1ce1c6
    product: '2110'
    variant: null
    quantity: 10
    total: 39650
    tax: null
    metadata: {  }
  -
    id: 51a2be86-2842-4ce8-adfb-389fcde4c504
    product: 98914-tapp
    variant: null
    quantity: 2
    total: 1330
    tax: null
    metadata: {  }
  -
    id: b8b67b88-d195-40c4-856c-2d4346fda9b1
    product: '54010'
    variant: null
    quantity: 1
    total: 550
    tax: null
    metadata: {  }
grand_total: '502.51'
items_total: '415.30'
tax_total: '87.21'
shipping_total: 0
coupon_total: 0
customer: 8abbdec0-9318-4ac2-a73d-6b36d7253a65
title: '#1090'
org_id: '24195692'
use_shipping_for_billing: true
shipping_name: Ordentall
shipping_street: Westblaak
shipping_city: Rotterdam
status_log:
  -
    status: placed
    timestamp: 1681171200
    data: {  }
  -
    status: paid
    timestamp: 1711715716
    data: {  }
---
