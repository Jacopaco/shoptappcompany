---
id: 471dc922-9958-4ec6-a39b-8556015f8b9e
blueprint: order
order_number: 1509
title: '#1509'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: c6647865-98b0-4e75-b1a7-b5a1661b5158
    product: '2105'
    variant: null
    quantity: 5
    total: 11035
    tax:
      amount: 2317
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: ad89c2ac-e064-468c-a998-de46308d37c3
    product: '2135'
    variant: null
    quantity: 6
    total: 11580
    tax:
      amount: 2432
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 84a74499-2f8b-471d-a2cd-929a03e6e66e
    product: '98990'
    variant: null
    quantity: 1
    total: 215
    tax:
      amount: 45
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: b4f8ce59-3dfb-4a8d-8113-c35fbf84dc63
    product: '98995'
    variant: null
    quantity: 1
    total: 495
    tax:
      amount: 104
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: 05dd7b79-321d-4034-80a2-fcac9ff4e77b
    product: '6549'
    variant: null
    quantity: 6
    total: 41400
    tax:
      amount: 8694
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 78317
items_total: 64725
tax_total: 13592
shipping_total: 0
coupon_total: 0
customer: 1f139421-8f31-4c1d-8fe8-23579393e25c
---
