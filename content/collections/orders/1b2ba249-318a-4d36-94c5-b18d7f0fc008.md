---
id: 1b2ba249-318a-4d36-94c5-b18d7f0fc008
blueprint: order
order_number: 1045
order_status: placed
payment_status: paid
items:
  -
    id: 788eb9bf-8c3a-420f-a243-626a72152531
    product: '2110'
    variant: null
    quantity: 10
    total: 43200
    tax: null
    metadata: {  }
grand_total: '522.72'
items_total: '432.00'
tax_total: '90.72'
shipping_total: 0
coupon_total: 0
customer: 9eddba44-5334-4fe9-9383-20ecc48cc44d
title: '#1045'
use_shipping_for_billing: true
shipping_name: 'Christelijke Basisschool (CBS) Het Kompas'
shipping_street: Gregoriuslaan
shipping_city: Lexmond
status_log:
  -
    status: placed
    timestamp: 1673395200
    data: {  }
  -
    status: paid
    timestamp: 1711715677
    data: {  }
---
