---
id: 7ea33c24-1a2a-47a6-9f06-c3c906e37b5e
blueprint: order
order_number: 1168
order_status: placed
payment_status: paid
items:
  -
    id: 3bc4a4ce-6a52-486c-bf36-1ea43abdece3
    product: '2115'
    variant: null
    quantity: 6
    total: 31170
    tax: null
    metadata: {  }
  -
    id: 1d5663ed-e564-4b33-8d7a-d2cf57263ad2
    product: '2250'
    variant: null
    quantity: 1
    total: 3192
    tax: null
    metadata: {  }
  -
    id: 8faacda7-cb73-4ddc-94a9-efbdc8a8f994
    product: '98915'
    variant: null
    quantity: 1
    total: 980
    tax: null
    metadata: {  }
grand_total: '427.63'
items_total: '353.42'
tax_total: '74.22'
shipping_total: 0
coupon_total: 0
customer: c48526ff-8e7d-4837-9760-efab06ff8f98
title: '#1168'
use_shipping_for_billing: true
shipping_name: 'Eben Haëzer School Barneveld'
shipping_street: Schoutenstraat
shipping_city: Barneveld
status_log:
  -
    status: placed
    timestamp: 1694476800
    data: {  }
  -
    status: paid
    timestamp: 1711715811
    data: {  }
---
