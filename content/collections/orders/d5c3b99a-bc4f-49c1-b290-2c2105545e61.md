---
id: d5c3b99a-bc4f-49c1-b290-2c2105545e61
blueprint: order
order_number: 1646
title: '#1646'
published: true
shipping_tax:
  amount: 0
  rate: 21
  price_includes_tax: false
order_status: cart
payment_status: unpaid
items:
  -
    id: e4d21953-6a58-491d-a9f7-1f453b0345d4
    product: '2110'
    variant: null
    quantity: 8
    total: 45144
    tax:
      amount: 9480
      rate: 21
      price_includes_tax: false
    metadata: {  }
  -
    id: c89178bd-6d1c-4999-a41b-22c1dc0ee783
    product: '2135'
    variant: null
    quantity: 12
    total: 25200
    tax:
      amount: 5292
      rate: 21
      price_includes_tax: false
    metadata: {  }
grand_total: 85116
items_total: 70344
tax_total: 14772
shipping_total: 0
coupon_total: 0
customer: 9eddba44-5334-4fe9-9383-20ecc48cc44d
---
