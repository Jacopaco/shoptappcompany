---
id: 960fd918-b37c-4060-a646-91d91d17b06c
blueprint: order
order_number: 1176
order_status: placed
payment_status: paid
items:
  -
    id: 3f8bc738-3518-47b5-92b4-1dc8c8036f28
    product: '2105'
    variant: null
    quantity: 2
    total: 4414
    tax: null
    metadata: {  }
  -
    id: 4f55f67e-13ea-439e-b417-8310d8a86b98
    product: '2135'
    variant: null
    quantity: 3
    total: 5790
    tax: null
    metadata: {  }
  -
    id: 4e822626-e793-4ec5-b82b-8433dbdb8bbc
    product: 6540-1881
    variant: null
    quantity: 2
    total: 19142
    tax: null
    metadata: {  }
  -
    id: 6fb8d2f6-2b69-45dc-8a78-a09e1f4a1ded
    product: '98995'
    variant: null
    quantity: 12
    total: 6733
    tax: null
    metadata: {  }
grand_total: '436.56'
items_total: '360.79'
tax_total: '75.77'
shipping_total: 0
coupon_total: 0
customer: 359eb21c-7fff-4d32-a2b3-632ec28b7d48
title: '#1176'
org_id: '56840276'
use_shipping_for_billing: true
shipping_name: 'Vestiging Arnhem'
shipping_street: Wagnerlaan
shipping_city: Arnhem
shipping_postal_code: 6815AD
status_log:
  -
    status: placed
    timestamp: 1696204800
    data: {  }
  -
    status: paid
    timestamp: 1711715821
    data: {  }
---
