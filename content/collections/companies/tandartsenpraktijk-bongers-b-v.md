---
id: af9b6bfa-831d-406e-b186-218ccb70be55
blueprint: companies
title: '90000 Tandartsenpraktijk Bongers B.V.'
tel: 0495-542188
billing_adress: 'Boshoverweg 73A'
billing_zip: 6002AM
billing_city: Weert
billing_country: Nederland
order_index: 1
client_prices:
  -
    id: s7W3awWz
    product: '2130'
    client_price: 3395
  -
    id: 2l3uudfq
    product: '2180'
    client_price: 3395
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728632440
locations:
  - 7392b23f-6bcc-42b9-b010-fc0375fdd35a
client_prices.product:
  - '2130'
  - '2180'
kvk: '14125055'
---
