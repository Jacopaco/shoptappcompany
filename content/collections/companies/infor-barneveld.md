---
id: 6d0190b5-4139-49e0-899e-76ea795c5036
blueprint: companies
title: 'KL00930 Infor (Barneveld) B.V.'
billing_adress: 'Mercuriusweg 47'
billing_zip: '3771 NC'
billing_city: Barneveld
billing_country: Nederland
order_index: 1
client_prices:
  -
    id: G9e8bEXU
    product: '2135'
    client_price: 1950
  -
    id: LrX0ud3u
    product: '2200'
    client_price: 3158
  -
    id: h3r1Z7Mm
    product: '21245800'
    client_price: 5000
  -
    id: Whf68AOr
    product: '2255'
    client_price: 2288
  -
    id: 1STZTztA
    product: '74416'
    client_price: 3750
  -
    id: 4HZEwh3x
    product: 98914-tapp
    client_price: 625
locations:
  - d1c1cc69-093e-4149-ad9e-54af738f056a
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728915594
kvk: '01077394'
---
