---
id: 6a9cdf89-b735-45d5-b0cd-ec4800e00349
blueprint: companies
title: 'Huisartsenpraktijk Driel'
kvk: '09207804'
locations:
  - 7dd86089-bbef-48ea-a9aa-eb199078fec5
client_prices:
  -
    id: m4h54ssh
    product: '2105'
    client_price: 1800
  -
    id: m4h552dp
    product: '74415'
    client_price: 2340
  -
    id: m4h55gy3
    product: '74416'
    client_price: 2040
  -
    id: m4h55poy
    product: '2145'
    client_price: 3703
  -
    id: m4h569qn
    product: '98915'
    client_price: 893
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733755463
---
