---
id: a933caf6-a138-462a-a9ad-76b9c7e0344b
blueprint: companies
title: 'KL00220 Tandartspraktijk SaChaLo B.V.'
tel: '077 466 0792'
billing_adress: 'Kerkveld 2'
billing_zip: '5768 BB'
billing_city: Meijel
billing_country: Nederland
order_index: 5
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728631130
client_prices:
  -
    id: egA9uPMf
    product: '2135'
    client_price: 1910
  -
    id: JMiDkSeE
    product: '74416'
    client_price: 2400
locations:
  - 52571907-9205-4d01-962e-3ba319c62abe
client_prices.product:
  - '2135'
  - '74416'
kvk: '86845152'
---
