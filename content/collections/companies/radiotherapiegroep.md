---
id: bebc5c46-0553-4d62-b6ff-de33648493c4
blueprint: companies
title: '67050 Radiotherapiegroep'
kvk: '56840276'
tel: '+31887790000'
billing_adress: 'Wagnerlaan 47'
billing_zip: 6815AD
billing_city: Arnhem
billing_country: Nederland
order_index: 93
client_prices:
  -
    id: 1V4wPJZt
    product: 6545-180
    client_price: 8920
  -
    id: HXQIDIpZ
    product: '2250'
    client_price: 2720
  -
    id: kQY6U6Ca
    product: '2302070'
    client_price: 2320
  -
    id: UdlX42k8
    product: '2002130'
    client_price: 3720
  -
    id: BS4pRNdU
    product: '21245800'
    client_price: 5000
  -
    id: MIgGzC9M
    product: '13202'
    client_price: 5180
  -
    id: yOxKd3lh
    product: '2105'
    client_price: 2207
  -
    id: Mz1g3yF5
    product: '2155'
    client_price: 2360
  -
    id: KaoNhrnJ
    product: '2170'
    client_price: 2995
  -
    id: UAEPO4i7
    product: '2135'
    client_price: 1930
  -
    id: V2v6rjqP
    product: '98921'
    client_price: 10800
  -
    id: Lp1ydw7Q
    product: '98920'
    client_price: 13200
  -
    id: ls4pjk3x
    product: '98995'
    client_price: 495
  -
    id: ls4pjsta
    product: '98990'
    client_price: 215
  -
    id: ls4pk1i6
    product: '6549'
    client_price: 6900
  -
    id: m0f5jstr
    product: '839160104'
    client_price: 8034
locations:
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 31ab6798-7e55-4a96-8c41-c9d35bf02694
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728900667
client_prices.product:
  - '21245800'
  - '2135'
  - '98995'
  - '98990'
  - '2250'
  - '2170'
  - '2302070'
  - '6549'
  - 6545-180
  - '13202'
  - '2155'
  - '2105'
  - '98921'
  - '98920'
  - '2002130'
---
