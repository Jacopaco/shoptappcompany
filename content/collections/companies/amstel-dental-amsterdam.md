---
id: 1253e6c8-3d48-437f-b074-d8c75a7790fc
blueprint: companies
title: 'KL01470 Amstel Dental B.V.'
tel: '020 723 5353'
billing_adress: 'Wibautstraat 172'
billing_zip: '1091 GR'
billing_city: Amsterdam
billing_country: Nederland
order_index: 1
client_prices:
  -
    id: lm8yvou3
    product: '2130'
    client_price: 2584
  -
    id: lm8yvvcr
    product: '407552'
    client_price: 4000
  -
    id: lm8yw08i
    product: '98990'
    client_price: 315
  -
    id: lm8yw75n
    product: '98995'
    client_price: 425
  -
    id: lm8ywpcz
    product: '74416'
    client_price: 1920
  -
    id: lm8yz677
    product: '21245800'
    client_price: 5000
  -
    id: lm8yzm9g
    product: '2115'
    client_price: 4995
  -
    id: lm8z04bn
    product: '2181'
    client_price: 2524
  -
    id: lnswaqye
    product: cart00914
    client_price: 299
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 89b7b333-1069-4801-8fa9-951e9839806a
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728558892
kvk: '60599758'
client_prices.product:
  - '21245800'
  - '2130'
  - cart00914
  - '98995'
  - '98990'
  - '74416'
  - '407552'
  - '2181'
  - '2115'
---
