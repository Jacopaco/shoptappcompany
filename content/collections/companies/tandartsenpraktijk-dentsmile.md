---
id: c3ed1408-6d23-4d75-bab0-4752835fb279
blueprint: companies
title: 'KL00430 Tandartsenpraktijk DentSmile B.V.'
billing_adress: 'Kerkstraat 232'
billing_zip: '1511 EN'
billing_city: Oostzaan
billing_country: Nederland
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728631998
client_prices:
  -
    id: pBO7qGUA
    product: '2135'
    client_price: 1830
  -
    id: yo1Fn5zo
    product: '2142'
    client_price: 3560
  -
    id: weI6crqF
    product: '98921'
    client_price: 10800
  -
    id: ol2Wr6eZ
    product: '98920'
    client_price: 13992
locations:
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
client_prices.product:
  - '2135'
  - '2142'
  - '98921'
  - '98920'
kvk: '83218521'
---
