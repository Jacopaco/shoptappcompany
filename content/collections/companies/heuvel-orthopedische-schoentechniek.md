---
id: 5d51ad93-e2e6-43c1-a90f-49d97915b08f
blueprint: companies
title: 'KL33444 Heuvel Orthopedische Schoentechniek B.V.'
billing_adress: 'Plesmanstraat 66'
billing_zip: 3905KZ
billing_city: Veenendaal
billing_country: Nederland
order_index: 6
client_prices:
  -
    id: zEoYErpi
    product: '2135'
    client_price: 1800
  -
    id: 6GMgOVrX
    product: '2100'
    client_price: 4600
  -
    id: qfUNqHNa
    product: '2188'
    client_price: 2396
  -
    id: znDlL364
    product: 98914-tapp
    client_price: 650
locations:
  - f2f903e1-4856-4d9b-86e4-16f4e1618fa8
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728570831
client_prices.product:
  - '2135'
  - '2188'
  - 98914-tapp
  - '2100'
kvk: '56822707'
---
