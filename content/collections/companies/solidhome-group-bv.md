---
id: d28ddf0a-4681-415d-a8e8-3199d8a714ac
blueprint: companies
title: 'Solidhome Group BV = failliet'
billing_adress: 'Boylestraat 21A'
billing_zip: '6718 XM'
billing_city: Ede
billing_country: Nederland
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728636909
client_prices:
  -
    id: crowj7Ly
    product: '2135'
    client_price: 1995
  -
    id: zntvrcKm
    product: '2105'
    client_price: 2034
  -
    id: wWn8YrqX
    product: '98915'
    client_price: 995
  -
    id: t1gat6z2
    product: '2250'
    client_price: 2900
  -
    id: G8FKI6XL
    product: '2255'
    client_price: 2930
locations:
  - 1f7effa5-1ac3-44d8-8fb4-b4b06575d101
client_prices.product:
  - '2135'
  - '2255'
  - '2250'
  - '98915'
  - '2105'
---
