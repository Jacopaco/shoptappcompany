---
id: 899acdd9-58f2-4404-81ad-dfb0bfaac689
blueprint: companies
title: '850001 Handels- en Constructiebedrijf H. Hardeman B.V.'
tel: '06 – 13324931'
billing_adress: 'Generatorstraat 21'
billing_zip: 3903LH
billing_city: Veenendaal
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: ihYdXhdN
    product: '98915'
    client_price: 850
  -
    id: mhL6O4I6
    product: '2105'
    client_price: 1641
  -
    id: fRxCykKZ
    product: '21245800'
    client_price: 5000
  -
    id: TKmKldIh
    product: '2302070'
    client_price: 1995
  -
    id: whGzJZPe
    product: '2135'
    client_price: 1830
  -
    id: UXNTTAbo
    product: '2250'
    client_price: 2995
  -
    id: 7EaMgwuJ
    product: '2188'
    client_price: 1855
locations:
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728570375
client_prices.product:
  - '21245800'
  - '2135'
  - '2250'
  - '98915'
  - '2302070'
  - '2188'
  - '2105'
kvk: '30066494'
---
