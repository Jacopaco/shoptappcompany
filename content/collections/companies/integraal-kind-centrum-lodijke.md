---
id: d66ba4d9-ef23-498b-bf5d-e73d3161faf3
blueprint: companies
title: '529001 LPS Kinderopvang Lodijke'
billing_adress: 'Ouderdinge 15'
billing_zip: 4617NL
billing_city: 'Bergen op Zoom'
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: GK7U4L0R
    product: '2922'
    client_price: 3600
  -
    id: co4TVhXV
    product: '2135'
    client_price: 2100
  -
    id: 6nPjG0i0
    product: 98914-tapp
    client_price: 995
  -
    id: dREHQq0k
    product: '2250'
    client_price: 2995
  -
    id: zoga6aR1
    product: '21245800'
    client_price: 5000
locations:
  - fd034e41-e357-4acc-8957-4bb9ed02d77d
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728915349
client_prices.product:
  - '21245800'
  - '2135'
  - '2250'
  - 98914-tapp
  - '2922'
kvk: '41105401'
---
