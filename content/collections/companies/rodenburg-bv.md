---
id: a9bd7d8c-213d-4c7a-aa35-dca7027d50e2
blueprint: companies
title: 'Rodenburg Zorgoplossingen B.V.'
locations:
  - eab656a1-2f8a-41b3-b6b2-5d48bd4e82ef
client_prices:
  -
    id: lyifui1o
    product: '2120'
    client_price: 2280
  -
    id: lyifvcr8
    product: '2135'
    client_price: 1890
  -
    id: lyifvl2s
    product: '98915'
    client_price: 995
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728647304
kvk: '71620095'
---
