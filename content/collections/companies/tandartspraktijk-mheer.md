---
id: 23bd866b-4d41-4746-9cf0-b764f3eee09a
blueprint: companies
title: 'KL01330 Tandartspraktijk Mheer'
billing_adress: 'Burgemeester Beckersweg 43'
billing_zip: '6261 NZ'
billing_city: Mheer
billing_country: Nederland
order_index: 4
client_prices:
  -
    id: JNMj9Hvl
    product: '74415'
    client_price: 2995
  -
    id: CnlydtUn
    product: '2105'
    client_price: 2100
  -
    id: Vk07hFF2
    product: '2142'
    client_price: 4733
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728631407
locations:
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
client_prices.product:
  - '74415'
  - '2142'
  - '2105'
  - b3ac5eff-8aa3-4430-a86d-43f7d85384d7
  - 1057aa9f-0403-4213-9f88-3e85adfe7619
  - 9a204dcc-30dc-467a-afd6-f4c4b917bca7
  - 6990e935-fd66-468f-8c3a-1df9cf44dfd2
kvk: '62983644'
---
