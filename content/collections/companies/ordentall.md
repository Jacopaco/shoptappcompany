---
id: f29dee5f-1953-487d-b766-b1d3baafb2f0
blueprint: companies
title: 'KL00720 Ordentall B.V.'
kvk: '24195692'
tel: '010 - 4121500'
billing_adress: 'Westblaak 113'
billing_zip: '3012 KH'
billing_city: Rotterdam
billing_country: Nederland
order_index: 9
client_prices:
  -
    id: 5kFwBiOI
    product: '2145'
    client_price: 4467
  -
    id: q6kXfco5
    product: '2250'
    client_price: 2676
  -
    id: Sne8GURy
    product: '2110'
    client_price: 5120
  -
    id: uYxLyFAt
    product: 98914-tapp
    client_price: 665
  -
    id: 7AsxAQDO
    product: '54010'
    client_price: 550
  -
    id: 5ja7TGkW
    product: '74416'
    client_price: 1730
  -
    id: 0mQRLPM0
    product: '2302070'
    client_price: 2330
  -
    id: l63I6WgW
    product: '98921'
    client_price: 10800
  -
    id: Wmr3Ntsr
    product: '98920'
    client_price: 13200
  -
    id: lx383js0
    product: '14258'
    client_price: 500
locations:
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728912971
client_prices.product:
  - '74416'
  - '54010'
  - '2250'
  - '2145'
  - '2302070'
  - 98914-tapp
  - '2110'
  - '98921'
  - '98920'
---
