---
id: ff9c79b6-4366-49f2-8e91-3d5b0100bc47
blueprint: companies
title: '410000 Jongsma & Jongsma Orthodontisten'
billing_adress: 'Polderweg  11-15'
billing_zip: '1782 EC'
billing_city: 'Den helder'
billing_country: Nederland
order_index: 1
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728901474
client_prices:
  -
    id: OpI9e8rk
    product: '98917'
    client_price: 5868
  -
    id: 5Wt8juD6
    product: '98918'
    client_price: 13992
  -
    id: QKbtOQGx
    product: '2135'
    client_price: 1950
locations:
  - 225a4a10-f583-4663-aa73-07579bc9b443
client_prices.product:
  - '2135'
  - '98918'
  - '98917'
kvk: '58588183'
---
