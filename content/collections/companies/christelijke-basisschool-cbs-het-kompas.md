---
id: 8ed96843-92e3-4f5e-bf96-e054d740cea6
blueprint: companies
title: 'KL00270 CBS Het Kompas'
tel: '0347 341 237'
billing_adress: 'Gregoriuslaan 40'
billing_zip: '4128 SZ'
billing_city: Lexmond
billing_country: Nederland
order_index: 7
client_prices:
  -
    id: AMxHc1wB
    product: '2110'
    client_price: 5643
  -
    id: yDknRdIS
    product: '2135'
    client_price: 2100
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729156932
locations:
  - 175073cd-07d9-40df-9e81-927b690b393b
client_prices.product:
  - '2135'
  - '2110'
kvk: '11057520'
---
