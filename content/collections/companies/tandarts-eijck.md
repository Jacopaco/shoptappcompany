---
id: b56a4982-42c5-44cc-8e4e-b84a9afbbc3c
blueprint: companies
title: '561000 Tandarts Eijck'
billing_adress: 'Markt 2'
billing_zip: '6074 BB'
billing_city: Melick
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: TdDxDXDZ
    product: '2135'
    client_price: 1885
  -
    id: ZHXtifcV
    product: '2105'
    client_price: 1846
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728632853
locations:
  - df52f315-d21f-435f-9fc2-1317451c68b0
client_prices.product:
  - '2135'
  - '2105'
kvk: '12051990'
---
