---
id: d7654c12-3145-4478-8fa3-970c4b764ae1
blueprint: companies
title: '817008 Van Linschoten Tandartsen'
tel: '035 542 5778'
billing_adress: 'Van Linschotenlaan 1'
billing_zip: '1212 ES'
billing_city: Hilversum
billing_country: Nederland
order_index: 3
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728572100
client_prices:
  -
    id: 52SQp3cQ
    product: '2181'
    client_price: 2525
  -
    id: WAvhigRM
    product: '2135'
    client_price: 1971
  -
    id: gDO5eflY
    product: '74416'
    client_price: 2400
locations:
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
client_prices.product:
  - '2135'
  - '74416'
  - '2181'
kvk: '52981363'
---
