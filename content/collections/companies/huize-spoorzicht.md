---
id: 8e96d2c0-638d-4b77-9c40-9d443a81e60b
blueprint: companies
title: '27801 Huize Spoorzicht B.V.'
locations:
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
client_prices:
  -
    id: lumqjhd2
    product: '2105'
    client_price: 1700
  -
    id: lumqjqri
    product: '2135'
    client_price: 1830
  -
    id: lumqrfxb
    product: 7170001002-1
    client_price: 496
  -
    id: m4gwsrzj
    product: '98915'
    client_price: 893
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733741297
kvk: '70219427'
---
