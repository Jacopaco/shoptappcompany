---
id: e04cab04-8a7b-45a0-9698-aeb7ff898773
blueprint: companies
title: '17950 Kwadrant Mondzorg'
tel: '0317 465 490'
billing_adress: 'Generaal Foulkesweg 27'
billing_zip: '6703 BL'
billing_city: Wageningen
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: KvYlFGyc
    product: '74415'
    client_price: 3495
  -
    id: 0bu9bjtv
    product: '2110'
    client_price: 3995
  -
    id: CP67DTT9
    product: '98915'
    client_price: 850
  -
    id: FoFgDHQQ
    product: '2135'
    client_price: 1885
locations:
  - ce7636a1-a1a1-4e9c-a657-f91857dbac44
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914479
client_prices.product:
  - '2135'
  - '74415'
  - '98915'
  - '2110'
kvk: '56800975'
---
