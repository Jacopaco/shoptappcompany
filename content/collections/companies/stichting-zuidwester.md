---
id: b0c021bd-503f-4f45-84de-dcc0dfc662ac
blueprint: companies
title: 'KL01090 Stichting Zuidwester'
kvk: '41186583'
tel: '0187 898 888'
billing_country: Nederland
order_index: 22
client_prices:
  -
    id: gVy5osPz
    product: '2135'
    client_price: 1950
  -
    id: l2mHIBck
    product: '2105'
    client_price: 1900
  -
    id: 7d8ImRKP
    product: '2186'
    client_price: 2300
  -
    id: 2kwL0vto
    product: '2250'
    client_price: 3270
  -
    id: d7qi2NKM
    product: 98914-tapp
    client_price: 650
  -
    id: Qd9wQpkK
    product: '2110'
    client_price: 4900
  -
    id: pGSe1GFS
    product: '2921033'
    client_price: 3645
  -
    id: CJ3zbUHC
    product: '2130'
    client_price: 2952
  -
    id: llw4r71g
    product: 6545-180
    client_price: 3902
  -
    id: llw4s58b
    product: 6540-1881
    client_price: 3902
  -
    id: llw4skto
    product: '6549'
    client_price: 4000
  -
    id: lmq60h3z
    product: '2254'
    client_price: 3875
  -
    id: ln8v73y0
    product: 5070002002-1
    client_price: 384
  -
    id: ln8v8560
    product: 9130004002-1
    client_price: 522
  -
    id: ln8v8cjm
    product: 7190001002-1
    client_price: 500
  -
    id: ln8v9nqq
    product: 7170001002-1
    client_price: 458
  -
    id: ln8v9vgp
    product: 7120001002-1
    client_price: 495
  -
    id: ln8va2rt
    product: 7180001002-1
    client_price: 527
  -
    id: ln8va7jm
    product: 6970002002-1
    client_price: 300
  -
    id: m2k6o2lu
    product: '98910'
    client_price: 365
locations:
  - be14461b-4601-4a24-a6dc-17853268cc98
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
  - a0be2336-697d-408e-ae76-a075fb4e1f62
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
  - 87ab27e8-a05b-4dc5-9e3d-446fa8e3f4c9
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - 21aaf902-e0ac-4201-9f1e-887260394548
  - c4053601-ae30-4783-9fdc-a26afacce843
  - 74a43c5f-48ad-4b62-a3de-f7ccf9d30320
  - abf442bb-e1b0-458c-bb2a-18d1f1a37b6e
  - fa588bc6-a4f0-48c8-8aef-fc556a945158
  - c2bd9529-b906-48e1-9055-56a781871e1a
  - 35ffadc8-cd7a-458b-b8f3-01732fd6cb74
  - ef78b35c-42e4-458c-a65b-1f2446135a30
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1731495981
client_prices.product:
  - '2130'
  - '2135'
  - '2250'
  - '2254'
  - 9130004002-1
  - 6970002002-1
  - 7120001002-1
  - 7180001002-1
  - 5070002002-1
  - 7170001002-1
  - 7190001002-1
  - '6549'
  - 6540-1881
  - 6545-180
  - '2186'
  - 98914-tapp
  - '2105'
  - '2110'
  - '2921033'
---
