---
id: d267d33a-5684-4194-856e-727b2f4b79ba
blueprint: companies
title: 'KL01670 CBS De Ontdekking'
billing_adress: 'Pleinstraat 13a'
billing_zip: '4126 RT'
billing_city: 'Hei- en Boeicop'
billing_country: Nederland
order_index: 9
client_prices:
  -
    id: LkZRBO8L
    product: '2135'
    client_price: 2100
  -
    id: klWWxSdV
    product: '2110'
    client_price: 5630
locations:
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728559975
client_prices.product:
  - '2135'
  - '2110'
kvk: '40062585'
---
