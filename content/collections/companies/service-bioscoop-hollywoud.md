---
id: cf25c62f-df48-4e88-9c29-33f9eca1deb2
blueprint: companies
title: 'KL01650 Hollywoud B.V.'
kvk: '18069539'
tel: 0183-307286
billing_adress: 'Sportlaan 59'
billing_zip: '4286 ES'
billing_city: Almkerk
billing_country: Nederland
order_index: 3
client_prices:
  -
    id: ln8xex2c
    product: '2186'
    client_price: 2505
  -
    id: ln8xf7xg
    product: 98914-tapp
    client_price: 650
  -
    id: ln8xfyfg
    product: cart00927
    client_price: 375
  -
    id: lpqz4y6y
    product: '2105'
    client_price: 1900
  -
    id: lsbgwo4h
    product: cart00913
    client_price: 375
locations:
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1734617605
client_prices.product:
  - cart00913
  - cart00927
  - '2186'
  - 98914-tapp
  - '2105'
---
