---
id: 1300049a-83d7-47aa-86ff-49b7207633e6
blueprint: companies
title: '817007 H2 Tandartsen'
locations:
  - 82359df9-6fb1-4321-8adf-db6f37f28079
client_prices:
  -
    id: m1riovmv
    product: '2135'
    client_price: 1830
  -
    id: m1ripglm
    product: '2125'
    client_price: 2593
  -
    id: m1riq6jn
    product: '74416'
    client_price: 1760
  -
    id: m1riqmlq
    product: '98915'
    client_price: 995
  -
    id: m1rir3c2
    product: '98918'
    client_price: 900
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728570144
kvk: '09220216'
---
