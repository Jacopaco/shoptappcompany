---
id: 890e8e99-6bec-4865-b34d-e6b4eb963f86
blueprint: companies
title: '850000 Sterkliniek Kerkewijk VOF'
locations:
  - 5ed3871c-f423-4dc3-9755-7ee2d3a676bc
client_prices:
  -
    id: m0xzdpjp
    product: '2130'
    client_price: 2952
  -
    id: m0xzhe87
    product: '2186'
    client_price: 1995
  -
    id: m0xzhyji
    product: '2105'
    client_price: 1760
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728634943
kvk: '30198771'
---
