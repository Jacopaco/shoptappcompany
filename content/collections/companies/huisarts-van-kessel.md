---
id: d8b397ec-2533-4b40-b16e-95ee5348e0cc
blueprint: companies
title: '431 A.H.E.M. van Kessel'
tel: '030-699 20 50'
billing_adress: 'Kroostweg 43a'
billing_zip: 3704EB
billing_city: Zeist
billing_country: Nederland
order_index: 2
client_prices:
  -
    id: B3CRvtvr
    product: '2135'
    client_price: 1995
  -
    id: kfwGqwtD
    product: '98918'
    client_price: 1166
  -
    id: DNUFKXet
    product: '98917'
    client_price: 489
locations:
  - c8eeae72-6c18-4ef1-8628-5087adf3afe9
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728571269
client_prices.product:
  - '2135'
  - '98918'
  - '98917'
kvk: '30266534'
---
