---
id: b69a1f96-5c22-487d-bfd7-be02c150ade5
blueprint: companies
title: '3300 Eben Haëzerschool Barneveld'
tel: '0342 414 923'
billing_adress: 'Schoutenstraat 109'
billing_zip: '3771 CH'
billing_city: Barneveld
billing_country: Nederland
order_index: 4
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728567080
client_prices:
  -
    id: LNvvfOkB
    product: '2115'
    client_price: 5195
  -
    id: ljJmTckB
    product: '2135'
    client_price: 1880
  -
    id: CM8czqCt
    product: '98915'
    client_price: 980
locations:
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
client_prices.product:
  - '2135'
  - '2115'
  - '98915'
---
