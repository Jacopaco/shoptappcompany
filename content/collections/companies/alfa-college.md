---
id: 80c036b9-c38a-4b36-83da-60dd3c72170e
blueprint: companies
title: 'KL01940 Alfa-college, locatie Hoogeveen'
tel: '0528 287 600'
billing_adress: 'Voltastraat 33'
billing_zip: '7903 AA'
billing_city: Hoogeveen
billing_country: Nederland
order_index: 0
locations:
  - 28a7c695-6e63-45b3-b248-504e489c1bac
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728557089
kvk: '41011946'
---
