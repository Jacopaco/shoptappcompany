---
id: 08b3a7c5-f348-4839-99b5-bb51bea2dace
blueprint: companies
title: '??? KraamzorgXL'
locations:
  - ad521bf5-2669-452d-81b0-0b55218e7ac4
client_prices:
  -
    id: lwqgqubs
    product: '2135'
    client_price: 1995
  -
    id: lwqgr4ms
    product: '2105'
    client_price: 1900
  -
    id: lwqgs819
    product: cart00641
    client_price: 325
  -
    id: lwqgtmh8
    product: cart00638
    client_price: 325
  -
    id: lwqgtqw4
    product: cart00637
    client_price: 325
  -
    id: lwqgtw5x
    product: cart00647
    client_price: 325
  -
    id: lwqgv1lx
    product: '98995'
    client_price: 425
  -
    id: lwqgvaik
    product: '98990'
    client_price: 215
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728570537
---
