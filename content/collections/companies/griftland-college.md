---
id: 465917bf-fcad-40f2-8dae-0de9505b610e
blueprint: companies
title: '40507708 Griftland College'
locations:
  - c1577f6a-6b53-4c66-a7ad-2c69fb7df643
client_prices:
  -
    id: luqqjzjl
    product: '4074'
    client_price: 560
  -
    id: luqqk5zf
    product: '2110'
    client_price: 4900
  -
    id: luqqma4v
    product: 98914-tapp
    client_price: 750
  -
    id: luqqmpqp
    product: '14243'
    client_price: 750
  -
    id: luqqonxs
    product: '2169'
    client_price: 2711
  -
    id: luqqq6x3
    product: '74416'
    client_price: 2750
  -
    id: luqqx077
    product: '5584'
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728569043
kvk: '40507708'
---
