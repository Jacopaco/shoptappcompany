---
id: f4210288-9577-4af7-80f5-49ae25057d41
blueprint: companies
title: '815000 E.W. Söentken, Tandarts'
billing_adress: 'Reeenspoor 8'
billing_zip: '3871 JP'
billing_city: Hoevelaken
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: 19TjyXZp
    product: '2142'
    client_price: 4500
  -
    id: HuV3KGnK
    product: '2135'
    client_price: 2100
locations:
  - 1cbc4fc8-faea-4a79-a9c1-410949fac628
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728563890
client_prices.product:
  - '2135'
  - '2142'
kvk: '32157712'
---
