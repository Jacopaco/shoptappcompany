---
id: 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
blueprint: companies
title: 'KL00570 Istanbul Steakhouse B.V.'
tel: '020 774 7420'
billing_adress: 'Slotermeerlaan 119'
billing_zip: '1063 JN'
billing_city: Amsterdam
order_index: 11
client_prices:
  -
    id: 7UnN2WM3
    product: '2161'
    client_price: 3470
  -
    id: 79IUkZNB
    product: '2200'
    client_price: 3410
  -
    id: gcQEF15F
    product: '54010'
    client_price: 4090
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914750
locations:
  - e6090873-ae14-4d62-b52a-da6089045f90
client_prices.product:
  - '54010'
  - '2161'
  - '2200'
kvk: '69320063'
---
