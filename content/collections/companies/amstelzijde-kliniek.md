---
id: 5162e19a-84e6-4f47-83e6-760863dc5c91
blueprint: companies
title: 'KL02040 Amstelzijde Kliniek'
locations:
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
client_prices:
  -
    id: lvoydqbw
    product: '2115'
    client_price: 5100
  -
    id: lvoye3ra
    product: '2105'
    client_price: 1890
  -
    id: lvoyela9
    product: '2135'
    client_price: 1890
  -
    id: m0qmb6hl
    product: 6543-188
    client_price: 7643
  -
    id: m0qmkwr2
    product: '98995'
    client_price: 495
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728559704
kvk: '67524133'
---
