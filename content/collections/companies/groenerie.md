---
id: 874c75e4-628e-4ba0-ad89-24504acf3789
blueprint: companies
title: 'KL01610 De Groenerie - V.O.F. Gebroeders Hardeman'
tel: '06 30759136'
billing_adress: 'Velkemeensedijk 9'
billing_zip: '6732 GE'
billing_city: Harskamp
billing_country: Nederland
order_index: 0
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728569934
kvk: '09221644'
---
