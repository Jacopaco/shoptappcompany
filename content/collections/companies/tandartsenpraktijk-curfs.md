---
id: 0129a894-dfa3-49ca-b580-6107c5786701
blueprint: companies
title: 'KL01150 Tandartsenpraktijk Curfs'
tel: '043 450 3098'
billing_adress: 'Rijksweg 101'
billing_zip: '6271 AD'
billing_city: Gulpen
billing_country: Nederland
order_index: 1
locations:
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728632215
kvk: '56943733'
---
