---
id: 19806848-2c70-4b45-953e-d0ab15a5024b
blueprint: companies
title: 'KL01170 FunXtion International B.V.'
tel: '+31 6 48 42 70 34'
billing_adress: 'Sloterweg 796'
billing_zip: 1066CN
billing_city: Amsterdam
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: AMz6R0fT
    product: '2125'
    client_price: 2693
  -
    id: Yxn97yYk
    product: '2135'
    client_price: 1995
  -
    id: DZ6nehCy
    product: '2130'
    client_price: 2693
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728567615
locations:
  - 3bc12b37-5aff-4aed-8cd9-66f8ed6eae3b
client_prices.product:
  - '2130'
  - '2135'
  - '2125'
kvk: '57141762'
---
