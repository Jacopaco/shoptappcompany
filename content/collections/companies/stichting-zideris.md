---
id: 4e00303b-45d0-49d8-9aad-8a8ef0e22948
blueprint: companies
title: '986000 Stichting Zideris'
billing_adress: 'Cuneraweg 12'
billing_zip: '3911 RN'
billing_city: Rhenen
billing_country: Nederland
order_index: 16
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728634521
client_prices:
  -
    id: ZqzT9jHP
    product: '2142'
    client_price: 5626
  -
    id: tdBHdQoH
    product: '2186'
    client_price: 3113
  -
    id: OdYS5wev
    product: '2110'
    client_price: 5643
  -
    id: lo2pqiy2
    product: '98990'
    client_price: 305
locations:
  - e6713451-367d-43ef-91ec-87d84a4c609f
client_prices.product:
  - '98990'
  - '2142'
  - '2186'
  - '2110'
kvk: '64138836'
---
