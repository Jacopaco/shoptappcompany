---
id: 821664cd-db7e-417e-8c02-9d697d99fa7b
blueprint: companies
title: 'KL01780 Praktijk voor Tandheelkunde Op Dubbeldam B.V.'
locations:
  - c83569ed-a907-4878-9fe2-ebcddaf260e8
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1736946996
kvk: '88831922'
client_prices:
  -
    id: m5xvab5u
    product: '2140'
    client_price: 3314
---
