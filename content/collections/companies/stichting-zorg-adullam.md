---
id: 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
blueprint: companies
title: '254000 Stichting Zorg Adullam'
billing_adress: 'Theaterplein 5'
billing_zip: '3772 KX'
billing_city: Barneveld
billing_country: Nederland
order_index: 26
client_prices:
  -
    id: D6WpQCc5
    product: '2135'
    client_price: 1950
  -
    id: vHe3ABer
    product: 98914-tapp
    client_price: 650
  -
    id: r8GaNnuS
    product: '2110'
    client_price: 4900
  -
    id: lmqcn0kk
    product: '14245'
    client_price: 600
  -
    id: lmqcn6cc
    product: '14258'
    client_price: 600
  -
    id: lmqcnclf
    product: '14243'
    client_price: 600
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9febd33c-508e-4088-80ef-1681be040e54
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 1f727611-7eac-4fc0-978b-0e9b171f437a
  - ff938854-4adb-42f4-aae1-2b3f0d52b517
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 0b83c198-6db3-49db-843a-ae2c3dd953cc
  - 84dec7cd-ea0a-46e7-a7a9-b254693d1da8
  - 11006000-90bb-4728-b1e4-eb18e446374d
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1735546194
client_prices.product:
  - '2135'
  - '14243'
  - '14245'
  - '14258'
  - 98914-tapp
  - '2110'
kvk: '41053643'
---
