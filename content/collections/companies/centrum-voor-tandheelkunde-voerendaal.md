---
id: 6aa35b46-fb7b-4809-9144-37d95fecd2d6
blueprint: companies
title: '902000 Centrum voor Tandheelkunde Voerendaal'
billing_adress: 'Raadhuisplein 4'
billing_zip: '6367 ED'
billing_city: Voerendaal
billing_country: Nederland
order_index: 1
client_prices:
  -
    id: x6LEL4qD
    product: '98920'
    client_price: 1166
  -
    id: 451uosTu
    product: '98921'
    client_price: 900
  -
    id: hWpC3XE7
    product: '2135'
    client_price: 2100
  -
    id: 2lGCmJiS
    product: '2130'
    client_price: 2600
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728562926
locations:
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
client_prices.product:
  - '2130'
  - '2135'
  - '98921'
  - '98920'
kvk: '57963509'
---
