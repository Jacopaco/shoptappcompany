---
id: 20a3c4dd-dfc7-4b88-a8de-adf592de239c
blueprint: companies
title: '817009 Tandartsenpraktijk Wimmers B.V.'
tel: '0113 213 942'
billing_adress: 'Westhavendijk 109A'
billing_zip: '4463 AJ'
billing_city: Goes
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: lt2wksc9
    product: '2135'
    client_price: 1900
  -
    id: lt2wl1g9
    product: cart00647
    client_price: 299
  -
    id: lt2wl8tt
    product: cart00914
    client_price: 299
  -
    id: lt2wleih
    product: cart00913
    client_price: 299
  -
    id: lt2wll8h
    product: cart00641
    client_price: 299
  -
    id: lt2wlr69
    product: '74416'
    client_price: 1760
locations:
  - 21fd8138-93dd-4a6e-992b-066f60627391
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728631633
client_prices.product:
  - '2135'
  - cart00914
  - '74416'
  - cart00913
  - cart00647
  - cart00641
kvk: '20141373'
---
