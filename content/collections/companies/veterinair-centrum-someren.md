---
id: 2fec2271-8935-4b02-b3b1-dbddeb746b69
blueprint: companies
title: '86510 Veterinair Centrum Someren B.V.'
locations:
  - 3d925d9b-d247-486b-9eee-f541cb605f5b
client_prices:
  -
    id: lzwhchu4
    product: '2135'
    client_price: 1950
  -
    id: lzwhcy4i
    product: '2105'
    client_price: 1700
  -
    id: lzwhe8vo
    product: 6540-1881
    client_price: 7319
  -
    id: lzwhep3e
    product: '2186'
    client_price: 1995
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1736949108
kvk: '17206995'
---
