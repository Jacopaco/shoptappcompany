---
id: d34a55b1-19c5-45db-9ecb-38efb7906da8
blueprint: companies
title: '595000 Graaf Jan van Nassauschool'
tel: '0182 512 691'
billing_adress: 'Steijnpad 1'
billing_zip: '2805 JE'
billing_city: Gouda
billing_country: Nederland
order_index: 7
client_prices:
  -
    id: LU9i0YQk
    product: '2250'
    client_price: 3110
  -
    id: O0lHdMvd
    product: '2135'
    client_price: 2037
  -
    id: osjw8EPP
    product: '2115'
    client_price: 5271
  -
    id: rkFVFvRL
    product: '2188'
    client_price: 2189
  -
    id: 8bpfazX8
    product: '98915'
    client_price: 1050
  -
    id: iJps4z4P
    product: '74416'
    client_price: 2495
  -
    id: u6c5Y5Vu
    product: '2110'
    client_price: 5100
locations:
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728568647
kvk: '41172323'
---
