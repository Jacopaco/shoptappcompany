---
id: bfdce2db-c069-4c13-909b-6ba84fa04608
blueprint: companies
title: '24465753 Sport- en Gezondheidscentrum "De Koornmolen" B.V.'
tel: (31+)0180–631654
billing_adress: 'Tweemanspolder 6 a'
billing_zip: '2761 ED'
billing_city: Zevenhuizen
billing_country: Nederland
order_index: 0
client_prices:
  -
    id: ltijqist
    product: '2105'
    client_price: 1990
  -
    id: ltijv9fc
    product: '98915'
    client_price: 815
locations:
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728900251
client_prices.product:
  - '98915'
  - '2105'
kvk: '24465753'
---
