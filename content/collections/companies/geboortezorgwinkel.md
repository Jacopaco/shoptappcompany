---
id: 12c4ab0f-5529-4781-abd4-19c5cef834ec
blueprint: companies
title: 'KL02100 Kraamzorg XL B.V.- Geboortezorgwinkel'
client_prices:
  -
    id: lwozr2dr
    product: '2135'
    client_price: 1995
  -
    id: lwozrf2n
    product: '98990'
    client_price: 235
  -
    id: lwozrp8u
    product: '98995'
    client_price: 495
  -
    id: lwozs6pg
    product: cart00647
    client_price: 349
  -
    id: lwozsg4o
    product: cart00637
    client_price: 349
  -
    id: lwozssii
    product: cart00913
    client_price: 349
  -
    id: lwozt2l2
    product: cart00963
    client_price: 349
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732788585
kvk: '75844931'
---
