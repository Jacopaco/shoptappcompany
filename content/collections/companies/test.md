---
id: f0a40d3e-b1c2-4568-994c-c4aecb027482
blueprint: companies
title: test
order_index: 10
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1712062863
kvk: '1234'
tel: '1234'
billing_adress: 'Kerkewijk 65'
billing_zip: 3901EC
billing_city: Veenendaal
billing_country: Nederland
client_prices:
  -
    id: lh7q889v
    product: '8230'
    client_price: 456
locations:
  - 0b3b7ada-ec29-4914-b256-fa56a8d98eae
  - 11d7f953-43aa-43d8-a76d-a6c9c45f0621
client_prices.product:
  - '8230'
---
