---
id: 50f975cc-aa02-494f-81aa-fbbd2580a184
blueprint: companies
title: '6000 Van de Merwe, buro voor bouwadvies B.V.'
billing_adress: 'Einsteinstraat 59'
billing_zip: '3902 HN'
billing_city: Veenendaal
billing_country: Nederland
order_index: 1
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728572414
client_prices:
  -
    id: YbzFle9G
    product: '2141'
    client_price: 4423
  -
    id: drjb9qQa
    product: '2105'
    client_price: 2100
locations:
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
client_prices.product:
  - '2105'
  - '2141'
kvk: '30125096'
---
