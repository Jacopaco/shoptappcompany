---
id: 691ea65f-48c3-4e02-82bb-23afe017ed72
blueprint: companies
title: 'KL02020 Mondzorglinie B.V.'
locations:
  - 851491c7-811a-41ec-a665-8715a2ec61a9
client_prices:
  -
    id: lvf48kv1
    product: '2142'
    client_price: 3712
  -
    id: lvf4977m
    product: '74416'
    client_price: 1760
  -
    id: lvf4axsz
    product: '2105'
    client_price: 1900
  -
    id: lvf4knkr
    product: '74415'
    client_price: 2995
  -
    id: lvf4kxw2
    product: '98915'
    client_price: 1040
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914205
kvk: '84815280'
---
