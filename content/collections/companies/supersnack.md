---
id: b9b8267b-e180-4df5-85fb-eed80e02f836
blueprint: companies
title: 'KL01580 Willem Geerts Supersnack'
kvk: '63810549'
tel: '0318 505 240'
billing_adress: 'Ronde Erf 64'
billing_zip: '3902 CZ'
billing_city: Veenendaal
billing_country: Nederland
order_index: 0
locations:
  - ffa8f396-2f25-4d2b-9178-abbf960171cb
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728633016
---
