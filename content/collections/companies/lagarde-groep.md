---
id: 342ca54a-98c1-451c-8ea8-f377ef5519e9
blueprint: companies
title: 'KL01320 Lagarde Groep'
tel: '088 16 16 016'
billing_adress: 'Mercuriusweg 45'
billing_zip: 3771NC
billing_city: Barneveld
billing_country: Nederland
order_index: 11
client_prices:
  -
    id: uekg0iqh
    product: '2135'
    client_price: 1950
  -
    id: 3orBa9Nd
    product: '2200'
    client_price: 3158
  -
    id: 5J8f7ygz
    product: '21245800'
    client_price: 5000
  -
    id: zrAC7L8L
    product: '2255'
    client_price: 2288
  -
    id: a3HLLNbX
    product: '2250'
    client_price: 3400
  -
    id: DLgmGkO5
    product: '74416'
    client_price: 3750
  -
    id: mSMmxZOi
    product: 98914-tapp
    client_price: 625
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914122
locations:
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
client_prices.product:
  - '21245800'
  - '2135'
  - '74416'
  - '2255'
  - '2250'
  - 98914-tapp
  - '2200'
kvk: '08063057'
---
