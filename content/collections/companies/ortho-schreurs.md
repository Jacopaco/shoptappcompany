---
id: ef60def1-f195-4b3b-a291-64fb129e6368
blueprint: companies
title: '747000 OrthodontieSchreurs bvba'
billing_adress: 'Weg naar As 172b1'
billing_zip: '3600'
billing_city: Genk
billing_country: België
order_index: 0
client_prices:
  -
    id: sBKrjPUe
    product: '98921'
    client_price: 10800
  -
    id: IUs9BfE8
    product: '98920'
    client_price: 13200
  -
    id: iZUEHcyi
    product: '2135'
    client_price: 1830
  -
    id: AhA6BKAK
    product: '2105'
    client_price: 1900
locations:
  - 20e12315-9ed8-4b7d-b67e-9763ff902563
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728901210
client_prices.product:
  - '2135'
  - '2105'
  - '98921'
  - '98920'
---
