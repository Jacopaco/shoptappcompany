---
id: 3ebf0d4b-09e1-41b0-8a1a-165cb2328bad
blueprint: companies
title: 'KL00560 CCIC EUROPE Food Test B.V.'
tel: '+ 31(0)850861800'
billing_adress: 'Runderweg 6'
billing_zip: '8219 PK'
billing_city: Lelystad
billing_country: Nederland
order_index: 23
locations:
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728562673
kvk: '66758483'
---
