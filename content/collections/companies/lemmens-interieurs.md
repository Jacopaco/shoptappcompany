---
id: ab594c46-8493-4a0c-b4f7-a8e838482d7a
blueprint: companies
title: 'KL01040 Lemmens Interieurs B.V.'
locations:
  - 356d30dd-aa1b-4538-a106-2d9d0b09c317
  - 2a829d12-52ab-4978-9f58-55950918c9ab
  - 675d8b55-da7c-4661-978e-0153e395ca74
client_prices:
  -
    id: m051pcw6
    product: '2135'
    client_price: 2100
  -
    id: m0qfdopy
    product: '98995'
    client_price: 495
  -
    id: m0qfearz
    product: '98990'
    client_price: 315
  -
    id: m0qfelyv
    product: cart00647
    client_price: 325
  -
    id: m0qferfc
    product: cart00914
    client_price: 325
  -
    id: m0qfeylk
    product: cart00913
    client_price: 325
  -
    id: m0qff9lb
    product: cart00641
    client_price: 325
  -
    id: m0qfkg73
    product: '74416'
    client_price: 1900
  -
    id: m0qfknn3
    product: '407552'
    client_price: 3900
  -
    id: m0qfl2en
    product: '2142'
    client_price: 3700
order_index: 0
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914167
kvk: '68450907'
---
