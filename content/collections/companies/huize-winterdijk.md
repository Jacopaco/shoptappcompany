---
id: 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
blueprint: companies
title: '959000 Huize winterdijk'
order_index: 22
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728916043
client_prices:
  -
    id: g2siKLYf
    product: cart00917
    client_price: 7138
  -
    id: dBr7W9Aj
    product: '2135'
    client_price: 1995
  -
    id: lv0t3fn5
    product: '2110'
    client_price: 4900
client_prices.product:
  - '2135'
  - cart00917
kvk: '41172086'
---
