---
id: 83e1c8b8-f158-42f2-b532-a990b0d5040c
blueprint: brand
title: PAPIER
alt_price: true
alt_price_source: cost_price
alt_price_type: percentage
alt_price_percentage: 130
categories:
  - 2b3ca86a-26af-461e-b819-7415564e5b92
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1695373663
---
