---
id: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
blueprint: brand
title: 'SANTRAL PLUS'
alt_price: true
alt_price_source: unit_price
alt_price_type: percentage
alt_price_percentage: 90
categories:
  - 1cbc45df-96d8-4022-8d2e-2ee96c7728be
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1695373726
duplicated_from: f77a48e2-462d-4719-8961-2637e36823d5
---
