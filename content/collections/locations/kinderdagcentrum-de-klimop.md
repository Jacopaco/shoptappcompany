---
id: 0b83c198-6db3-49db-843a-ae2c3dd953cc
blueprint: locations
title: 'De Klimop'
company_adress: false
adress: 'Willem Barentszstraat 72'
zip: '8023 WS'
city: Zwolle
country: Nederland
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1695029774
company: 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
shipping_name: 'De Klimop'
shipping_street: 'Willem Barentszstraat'
shipping_number: '72'
shipping_city: Zwolle
shipping_postal_code: '8023 WS'
location_products:
  - '2157'
  - '74416'
  - cart00914
  - cart00913
  - cart00641
  - cart00915
  - '7316'
  - '150212100'
  - '180204100'
---
