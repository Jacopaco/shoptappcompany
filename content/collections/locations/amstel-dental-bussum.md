---
id: 89b7b333-1069-4801-8fa9-951e9839806a
blueprint: locations
title: 'Amstel Dental Bussum B.V., kvk 84184248'
company_adress: false
adress: 'Bensdorpplein 10'
zip: '1406 PZ'
city: Bussum
country: Nederland
template_location_products: 3af04335-2263-411c-a5cb-7ff339ae2a46
company: 1253e6c8-3d48-437f-b074-d8c75a7790fc
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728558892
shipping_name: 'Amstel Dental Bussum B.V., kvk 84184248'
shipping_street: Bensdorpplein
shipping_number: '10'
shipping_city: Bussum
shipping_postal_code: '1406 PZ'
use_shipping_address_for_billing: true
location_products:
  - '180204100'
---
