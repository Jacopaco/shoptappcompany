---
id: fd034e41-e357-4acc-8957-4bb9ed02d77d
blueprint: locations
title: 'IKC Lodijke'
company_adress: false
adress: 'Ouderdinge 15'
zip: 4617NL
city: 'Bergen op Zoom'
country: Nederland
location_products:
  - 98914-tapp
  - '2922'
  - '2135'
  - '2250'
  - '21245800'
  - cart00960
  - '180204100'
company: d66ba4d9-ef23-498b-bf5d-e73d3161faf3
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914818
shipping_name: 'IKC Lodijke'
shipping_street: Ouderdinge
shipping_number: '15'
shipping_city: 'Bergen op Zoom'
shipping_postal_code: 4617NL
use_shipping_address_for_billing: true
---
