---
id: 11d7f953-43aa-43d8-a76d-a6c9c45f0621
blueprint: locations
title: test-link
company_adress: false
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688548190
company: f0a40d3e-b1c2-4568-994c-c4aecb027482
location_products:
  - '8230'
  - '5671'
  - '83990010'
  - '5472'
  - 21415312-afp-c
shipping_name: test-link
---
