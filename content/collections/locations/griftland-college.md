---
id: c1577f6a-6b53-4c66-a7ad-2c69fb7df643
blueprint: locations
title: 'Griftland College'
shipping_name: 'Griftland College'
shipping_street: Noorderweg
shipping_number: '79'
shipping_postal_code: 3761EV
shipping_city: Soest
use_shipping_address_for_billing: true
location_products:
  - '4074'
  - '2110'
  - 98914-tapp
  - '2169'
  - '74416'
  - '14243'
  - '5584'
  - '5655'
  - '5502'
  - '14242'
  - '54046'
  - cart00959
  - cart00960
  - '21245800'
  - '2255'
  - '2188'
  - '150204000'
  - '180202500'
  - '180204100'
company: 465917bf-fcad-40f2-8dae-0de9505b610e
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718350461
---
