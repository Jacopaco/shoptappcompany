---
id: 82359df9-6fb1-4321-8adf-db6f37f28079
blueprint: locations
title: 'H2 Tandartsen'
shipping_name: 'H2 Tandartsen'
shipping_street: Haterseveldweg
shipping_number: '285'
shipping_postal_code: 6532XR
shipping_city: Nijmegen
use_shipping_address_for_billing: true
location_products:
  - cart00647
  - cart00637
  - cart00638
  - cart00963
  - '2135'
  - '74416'
  - '2125'
  - '98915'
  - '98918'
  - '180204100'
company: 1300049a-83d7-47aa-86ff-49b7207633e6
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727852366
---
