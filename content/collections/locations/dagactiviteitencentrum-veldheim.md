---
id: 46b7a552-571b-454e-8f7b-d9475ea2ce79
blueprint: locations
title: Veldheim
company_adress: false
adress: 'Bovenbuurtweg 33'
zip: '6717 XA'
city: Ede
country: Nederland
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727269634
company: 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
shipping_name: Veldheim
shipping_street: Bovenbuurtweg
shipping_number: '33'
shipping_city: Ede
shipping_postal_code: '6717 XA'
use_shipping_address_for_billing: true
location_products:
  - '2186'
  - 5070002002-1
  - 7170001002-1
  - 7190001002-1
  - 7180001002-1
  - 7120001002-1
  - 6970002002-1
  - 9130004002-1
  - '74416'
  - '2921033'
  - '7316'
  - cart00647
  - cart00927
  - cart00926
  - cart00956
  - '81010003'
  - '81010004'
  - '150204000'
  - '180204100'
---
