---
id: 4e8302b0-f18d-40a8-865e-7040d27f3085
blueprint: locations
title: 'CBS de Ontdekking'
company_adress: true
location_products:
  - '2250'
  - '2135'
  - '2157'
  - '74416'
  - '2188'
  - 98914-tapp
  - '2110'
  - cart00960
  - '561455055'
  - '180204100'
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712059226
company: d267d33a-5684-4194-856e-727b2f4b79ba
shipping_name: 'CBS de Ontdekking'
shipping_street: Pleinstraat
shipping_number: 13a
shipping_city: 'Hei- en Boeicop'
shipping_postal_code: '4126 RT'
use_shipping_address_for_billing: true
---
