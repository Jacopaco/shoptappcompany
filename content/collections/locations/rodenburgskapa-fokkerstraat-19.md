---
id: eab656a1-2f8a-41b3-b6b2-5d48bd4e82ef
blueprint: locations
title: 'Rodenburg Zorgoplossingen B.V.'
shipping_name: 'Rodenburg Zorgoplossingen B.V.'
shipping_street: Fokkerstraat
shipping_number: '19'
shipping_postal_code: 3905KV
shipping_city: Veenendaal
use_shipping_address_for_billing: true
location_products:
  - '2135'
  - '2120'
  - '98915'
  - '14243'
  - '21245800'
  - '180204100'
company: a9bd7d8c-213d-4c7a-aa35-dca7027d50e2
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728647156
---
