---
id: 3af04335-2263-411c-a5cb-7ff339ae2a46
blueprint: locations
title: 'Amstel Dental Wibautstraat, kvk 60599758'
company_adress: true
location_products:
  - cart00914
  - cart00913
  - cart00641
  - '74416'
  - '2130'
  - '407552'
  - '98990'
  - '98995'
  - '2115'
  - '21245800'
  - '2250'
  - '2181'
  - cart00960
  - cart00647
  - '561455055'
  - '180204100'
company: 1253e6c8-3d48-437f-b074-d8c75a7790fc
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728558596
shipping_name: 'Amstel Dental Wibautstraat, kvk 60599758'
shipping_street: Wibautstraat
shipping_number: '172'
shipping_city: Amsterdam
shipping_postal_code: '1091 GR'
use_shipping_address_for_billing: true
---
