---
id: 84dec7cd-ea0a-46e7-a7a9-b254693d1da8
blueprint: locations
title: 'Centraal Bureau Barneveld'
company_adress: false
adress: 'Theaterplein 5'
zip: 3772KX
city: Barneveld
country: Nederland
company: 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1695029783
shipping_name: 'Centraal Bureau Barneveld'
shipping_street: Theaterplein
shipping_number: '5'
shipping_city: Barneveld
shipping_postal_code: 3772KX
location_products:
  - '2157'
  - '74416'
  - cart00914
  - cart00913
  - cart00641
  - cart00915
  - '7316'
  - '160109600'
  - '180204100'
---
