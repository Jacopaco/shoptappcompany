---
id: 675d8b55-da7c-4661-978e-0153e395ca74
blueprint: locations
title: 'Voorbeeld 1'
shipping_name: 'Voorbeeld 1'
shipping_street: ads
shipping_number: '2'
shipping_postal_code: '2803 XA'
shipping_city: Bergeijk
use_shipping_address_for_billing: true
location_products:
  - '5723184202'
company: ab594c46-8493-4a0c-b4f7-a8e838482d7a
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1725735224
---
