---
id: 425b24e6-8d16-4aec-a49f-5353d0902b91
blueprint: locations
title: 'CCIC EUROPE Food Test B.V.'
company_adress: false
adress: 'Runderweg 6'
zip: '8219 PK'
city: Lelystad
location_products:
  - '2186'
  - '2115'
  - '74416'
  - '2250'
  - '21245800'
  - '2302070'
  - '54010'
  - cart00960
  - '561455055'
  - '180204100'
company: 3ebf0d4b-09e1-41b0-8a1a-165cb2328bad
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712059280
shipping_name: 'CCIC EUROPE Food Test B.V.'
shipping_street: Runderweg
shipping_number: '6'
shipping_city: Lelystad
shipping_postal_code: '8219 PK'
use_shipping_address_for_billing: true
---
