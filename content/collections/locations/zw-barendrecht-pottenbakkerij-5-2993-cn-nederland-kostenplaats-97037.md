---
id: 74a43c5f-48ad-4b62-a3de-f7ccf9d30320
blueprint: locations
title: 'ZW Barendrecht Pottenbakkerij 5, 2993 CN Nederland (kostenplaats 97037)'
company_adress: false
adress: 'Pottenbakkerij 5'
zip: '2993 CN'
city: Barendrecht
country: Nederland
company: b0c021bd-503f-4f45-84de-dcc0dfc662ac
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1697028905
shipping_name: 'ZW Barendrecht Pottenbakkerij 5, 2993 CN Nederland (kostenplaats 97037)'
shipping_street: Pottenbakkerij
shipping_number: '5'
shipping_city: Barendrecht
shipping_postal_code: '2993 CN'
location_products:
  - '2135'
  - 98914-tapp
  - '2186'
  - '98910'
  - '2105'
  - '150212100'
---
