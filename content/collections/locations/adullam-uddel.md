---
id: 0d65ba09-ad21-48c3-b963-f2bd0d31321b
blueprint: locations
title: Kroonheim
company_adress: false
adress: 'Garderenseweg 59'
zip: '3888 LB'
city: Uddel
country: Nederland
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1695029619
company: 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
shipping_name: Kroonheim
shipping_street: Garderenseweg
shipping_number: '59'
shipping_city: Uddel
shipping_postal_code: '3888 LB'
location_products:
  - '2157'
  - '74416'
  - cart00914
  - cart00913
  - cart00641
  - cart00915
  - '7316'
  - '160109600'
  - '150204000'
  - '150212100'
  - '180204100'
---
