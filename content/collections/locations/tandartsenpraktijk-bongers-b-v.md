---
id: 7392b23f-6bcc-42b9-b010-fc0375fdd35a
blueprint: locations
title: 'Tandartsenpraktijk Bongers B.V.'
company_adress: true
location_products:
  - '2130'
  - '2180'
  - '180204100'
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1688649992
company: af9b6bfa-831d-406e-b186-218ccb70be55
shipping_name: 'Tandartsenpraktijk Bongers B.V.'
shipping_street: Boshoverweg
shipping_number: 73A
shipping_city: Weert
---
