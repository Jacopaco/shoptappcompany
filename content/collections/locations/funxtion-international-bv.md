---
id: 3bc12b37-5aff-4aed-8cd9-66f8ed6eae3b
blueprint: locations
title: 'FunXtion International BV'
company_adress: false
adress: 'Sloterweg 796'
zip: 1066CN
city: Amsterdam
country: Nederland
location_products:
  - '2130'
  - '2135'
  - '2125'
  - '180204100'
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1668068346
company: 19806848-2c70-4b45-953e-d0ab15a5024b
shipping_name: 'FunXtion International BV'
shipping_street: Sloterweg
shipping_number: '796'
shipping_city: Amsterdam
shipping_postal_code: 1066CN
---
