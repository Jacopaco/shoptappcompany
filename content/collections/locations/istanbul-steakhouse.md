---
id: e6090873-ae14-4d62-b52a-da6089045f90
blueprint: locations
title: 'Istanbul Steakhouse'
company_adress: false
adress: 'Slotermeerlaan 119'
zip: '1063 JN'
city: A
country: Nederland
location_products:
  - '54010'
  - '2200'
  - '2161'
  - '180204100'
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728914639
company: 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
shipping_name: 'Istanbul Steakhouse'
shipping_street: Slotermeerlaan
shipping_number: '119'
shipping_city: A
shipping_postal_code: '1063 JN'
use_shipping_address_for_billing: true
---
