---
id: 175073cd-07d9-40df-9e81-927b690b393b
blueprint: locations
title: 'KL00270 CBS Het Kompas'
company_adress: true
location_products:
  - '2135'
  - '2250'
  - '74415'
  - '2157'
  - '74416'
  - '2188'
  - 98914-tapp
  - cart00960
  - '561455055'
  - '180204100'
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729156932
company: 8ed96843-92e3-4f5e-bf96-e054d740cea6
shipping_name: 'KL00270 CBS Het Kompas'
shipping_street: Gregoriuslaan
shipping_number: '40'
shipping_city: Lexmond
shipping_postal_code: 4148SZ
use_shipping_address_for_billing: true
---
