---
id: '12601'
blueprint: products
art_nr: '12601'
title: 'Straight grab bar 537 mm RVS, BR0400CS'
slug: straight-grab-bar-537-mm-rvs-br0400cs
price: 5730
cost_price: 3438
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar recht 455 mm.
  De stang wordt d.m.v. de 2 ronde uiteinden van Ø 81 mm op de wand gemonteerd.
unit_price: 5730
vip_price: 5157
prod_img: prod_img/12601.JPG
---
