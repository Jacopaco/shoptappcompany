---
id: '21415523'
blueprint: products
art_nr: '21415523'
title: 'Zeepdisp.aut.alu 500ml IMP E A touchless'
slug: zeepdispautalu-500ml-imp-e-a-touchless
price: 25190
cost_price: 16122
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/21415523.JPG
unit_price: 25190
vip_price: 22671
---
