---
id: '98910'
blueprint: product
art_nr: '98910'
title: 'Pouch zeepzak 800ml'
slug: pouch-zeepzak-800ml
brands: 3375fa99-d12c-4998-8110-4d347cec808b
cost_price: 250
unit_price: 337
price: 390
vip_price: 325
locations:
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729583616
client_prices:
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
---
