---
id: cart00044
blueprint: products
art_nr: CART00044
title: 'Stofzuiger metaal met lang snoer en 7 meter slang'
slug: stofzuiger-metaal-met-lang-snoer-en-7-meter-slang
price: 280.8
cost_price: 180
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/cart00044.jpg
unit_price: 180
vip_price: 234.0
---
