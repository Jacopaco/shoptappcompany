---
id: '74416'
blueprint: products
title: 'Oppervlakte desinfectie alcohol 80% 5 liter'
price: 2953
description: Denteck
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/74416_oppervlakte_alcohol_1.jpg'
art_nr: 74416
slug: oppervlakte-desinfectie-alcohol-80-5-liter
brands: 4de0e459-caba-4ced-b92b-af23ce5c7614
categories: 9cccf0be-a99a-470c-8102-34557f942914
cost_price: 1360
prod_img: prod_img/74416.JPG
unit_price: 0
vip_price: 2461
documents:
  -
    id: lnsxzhhd
    document_title: 'Veiligheidsblad oppervlakte alcohol 80%'
    document_file: prod_doc/veiligheidsblad-74416.pdf
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1697463666
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - 6d0190b5-4139-49e0-899e-76ea795c5036
  - 342ca54a-98c1-451c-8ea8-f377ef5519e9
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - 20a3c4dd-dfc7-4b88-a8de-adf592de239c
  - a933caf6-a138-462a-a9ad-76b9c7e0344b
  - d7654c12-3145-4478-8fa3-970c4b764ae1
  - 465917bf-fcad-40f2-8dae-0de9505b610e
  - 691ea65f-48c3-4e02-82bb-23afe017ed72
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 1300049a-83d7-47aa-86ff-49b7207633e6
  - 6a9cdf89-b735-45d5-b0cd-ec4800e00349
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 175073cd-07d9-40df-9e81-927b690b393b
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
  - df52f315-d21f-435f-9fc2-1317451c68b0
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 21fd8138-93dd-4a6e-992b-066f60627391
  - 52571907-9205-4d01-962e-3ba319c62abe
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
  - be14461b-4601-4a24-a6dc-17853268cc98
  - a618e884-3fb2-4394-9a3f-effd139ef796
---
