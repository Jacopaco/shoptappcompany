---
id: '12921'
blueprint: product
art_nr: '12921'
title: 'Handdoekdispenser RVS hoogglans, DT2106C'
slug: handdoekdispenser-rvs-hoogglans-dt2106c
cost_price: 5730
unit_price: 9550
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9550
vip_price: 8595
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Als u houdt van een beetje luxe, springen de rvs hoogglans producten van Mediclinics u direct in het oog. Het strakke design gecombineerd met de hoogglans look is een verrassend accent in elke toiletruimte.

  Naast het opvallende design biedt deze handdoekdispenser voldoende voorraad voor een flinke belasting. Met meer dan 400 handdoekjes zijn uw gasten altijd verzekerd van droge handen. En dreigt er toch een tekort te ontstaan? De voorraad is eenvoudig aangevuld door gebruik te maken van het veerslot aan de voorzijde.
prod_img: prod_img/dt2106c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477331
---
