---
id: '2154'
blueprint: products
art_nr: '2154'
title: 'SUNSHINE 2154-FSC,recycled tissue Z handdoekjes 23x25 cm - 2 laags - 3200 stuks'
slug: handdoekjes-z-vouw-recycled-tissue-2laags
price: 2223
cost_price: 1425
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
description: |-
  Handdoekjes Z-vouw recycled tissue 2 laags 23 x 25 cm

  Code 2154-FSC
  Materiaal recycled tissue
  Afmetingen 23 x 25 cm
  2 laags
  Verpakking 20 x 160 vel in doos

  FSC certificaat
  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
prod_img: prod_img/2154.jpg
unit_price: 0
vip_price: 1852
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729154402
---
