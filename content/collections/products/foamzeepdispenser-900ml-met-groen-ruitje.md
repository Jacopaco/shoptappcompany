---
id: 5502-green
published: false
blueprint: products
art_nr: 5502-GREEN
title: 'Foamzeepdispenser 900ml met groen ruitje'
slug: foamzeepdispenser-900ml-met-groen-ruitje
price: 3875
cost_price: 2906
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
description: |-
  Foamzeepdispenser voor wandmontage.
  Met push cover voor het doseren van de vulgoederen, duwrichting naar de muur.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters. 
  Met slot en witte PQ sleutel.
  Met navulfles en foampomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo. 
unit_price: 3875
vip_price: 3487
prod_img: prod_img/5502.JPG
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
