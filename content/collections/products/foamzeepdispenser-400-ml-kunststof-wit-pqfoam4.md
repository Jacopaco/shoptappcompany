---
id: '5509'
blueprint: products
art_nr: '5509'
title: 'Foamzeepdispenser 400 ml kunststof wit, PQFoam4'
slug: foamzeepdispenser-400-ml-kunststof-wit-pqfoam4
price: 3400
cost_price: 2142
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met push cover voor het doseren van de vulgoederen, duwrichting naar de muur.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Met slot en witte PQ sleutel.
  Geschikt voor bedrukking met uw logo.
  Met navulfles en foampomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem. 
prod_img: prod_img/5509.JPG
unit_price: 3400
vip_price: 3060
---
