---
id: '17285'
blueprint: product
art_nr: '17285'
title: 'Haardroger zwart, Excel 1800 TF'
slug: haardroger-zwart-excel-1800-tf
cost_price: 2117
unit_price: 5600
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 5600
vip_price: 5040
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Haardroger zwart met zwart glad snoer. 6 combinatiemogelijkheden voor temperatuur/luchtopbrengst.
  Koude luchtstand. Extra warmte cut-off (veiligheidsschakelaar) Met verwisselbare luchtfilter. Met smal mondstuk.
  Wordt standaard geleverd met ringvormige transparante wandhouder.


  - Artikelnummer 17285
  - Model Excel 1800 TF
  - Materiaal Kunststof
  - Kleur Zwart
  - Hoogte 	240 mm
  - Breedte 	260 mm
  - Diepte 	88 mm
  - Gewicht 	0,35 kg (zonder snoer)
  - Garantie 	1 jaar na aankoopdatum
  - Toepassing Handmodel
  - Toepassing Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
  - Eigenschappen 6 combinatiestanden temperatuur/luchtopbrengst, koude luchtstand incl. extra transparante wandhouder
  Wattage 	1800 Watt
prod_img: prod_img/17285_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714730468
---
