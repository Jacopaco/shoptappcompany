---
id: '17270'
published: false
blueprint: product
art_nr: '17270'
title: 'Wandhaardroger wit met slang en shaver aansluiting, Hotello Shaver'
slug: wandhaardroger-wit-met-slang-en-shaver-aansluiting-hotello-shaver
cost_price: 6275
unit_price: 16500
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 16500
vip_price: 14850
categories: f832415b-6165-4830-aa09-66452dc3e2ec
---
