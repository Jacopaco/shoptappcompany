---
id: 22201473-afp-c
blueprint: products
art_nr: '22201473 AFP-C'
title: 'Poetsroldispenser mini RVS afp-c, BEU 1 E ST'
slug: poetsroldispenser-mini-rvs-afp-c-beu-1-e-st
price: 14000
cost_price: 8400
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: d1ca567f-2127-432a-aec4-29fef09a974d
description: |-
  Poetsroldispenser mini voor wandmontage.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Papieruitname vanaf de onderzijde.
  Ook in wit verkrijgbaar!
prod_img: prod_img/22201473-afp-c.JPG
unit_price: 14000
vip_price: 12600
---
