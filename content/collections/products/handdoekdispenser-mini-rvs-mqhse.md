---
id: '8170'
blueprint: products
art_nr: '8170'
title: 'Handdoekdispenser mini RVS, MQHSE'
slug: handdoekdispenser-mini-rvs-mqhse
price: 13710
cost_price: 8226
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
prod_img: prod_img/8170.JPG
unit_price: 13710
vip_price: 12339
---
