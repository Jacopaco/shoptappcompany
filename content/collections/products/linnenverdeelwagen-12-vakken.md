---
id: cart00639
blueprint: products
art_nr: CART00639
title: 'Linnenverdeelwagen 12 vakken '
slug: linnenverdeelwagen-12-vakken
price: 735.1344
cost_price: 471.24
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
unit_price: 471.24
vip_price: 612.612
prod_img: prod_img/cart00639.jpg
---
