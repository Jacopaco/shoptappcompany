---
id: 5472-br
published: false
blueprint: products
art_nr: 5472-BR
title: 'Foamzeepdispenser 1200 ml automatisch kunststof wit, PQAFoam12'
slug: foamzeepdispenser-1200-ml-automatisch-kunststof-wit-pqafoam12
cost_price: 4363
unit_price: 7395
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 7395
vip_price: 6655
---
