---
id: '13710'
blueprint: product
art_nr: '13710'
title: 'Jumboroldispenser mini wit, PR2783'
slug: jumboroldispenser-mini-wit-pr2783
cost_price: 3546
unit_price: 5910
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 5910
vip_price: 5319
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Als u geen grootverbruiker bent, maar wel altijd voldoende toiletpapier op voorraad wilt hebben, is deze mini jumboroldispenser van Mediclinics dé oplossing.

  Het bescheiden formaat van deze mini jumboroldispenser is praktisch in te passen in elk toilet. De eenvoudige uitstraling en stijlvolle afwerking zorgen voor een boeiend accent in elke sanitaire ruimte. De mini jumboroldispenser is eenvoudig in gebruik; door de afscheurranden aan de onderzijde van de dispenser kan uw gast eenvoudig de juiste hoeveelheid toiletpapier gebruiken.

  Kiest u voor de praktisch inpasbare mini jumboroldispenser van Mediclinics?
prod_img: prod_img/afbeelding-pr2783c-1719320926.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320930
---
