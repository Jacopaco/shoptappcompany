---
id: prfl50
blueprint: products
art_nr: PRFL50
title: 'WC reiniger eco 6x750 ml'
slug: wc-reiniger-eco-6x750-ml
price: 3876
cost_price: 2275
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/prfl50.JPG
unit_price: 1455
vip_price: 3230
---
