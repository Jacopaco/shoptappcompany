---
id: '13661'
blueprint: product
art_nr: '13661'
title: 'Poetsroldispenser midi RVS hoogglans, DT0303C'
slug: poetsroldispenser-midi-rvs-hoogglans-dt0303c
cost_price: 12396
unit_price: 20660
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 20660
vip_price: 18594
categories: d1ca567f-2127-432a-aec4-29fef09a974d
description: |-
  Uw facilitaire dienst doet er alles aan om uw sanitaire ruimte zo clean mogelijk te houden. Een aantal goede voorzieningen, zoals deze poetsrolhouder van Mediclinics, helpt uw medewerkers hun werk zo eenvoudig mogelijk te maken.

  Met zijn strakke lijnen staat deze hoogglans poetsrolhouder garant voor een design-accent in uw interieur. Het eenvoudige gebruik van de dispenser maakt dat uw gasten bij ongewenst vuil of vocht al snel naar de poetsrol grijpen. De schuifdeur aan de zijkant van de dispenser maakt het wisselen van de rol een fluitje van een cent.

  Maakt u het uw facilitaire dienst graag gemakkelijk?
prod_img: prod_img/afbeelding-dt0303c-1719321508.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321512
---
