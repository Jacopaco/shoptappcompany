---
id: '180202500'
blueprint: product
art_nr: '180202500'
title: 'Afvalzakken 60x80cm grijs'
slug: afvalzakken-60x80cm-grijs
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 2177
unit_price: 3200
price: 3996
vip_price: 3330
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
locations:
  - 11006000-90bb-4728-b1e4-eb18e446374d
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - 9febd33c-508e-4088-80ef-1681be040e54
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - 3d925d9b-d247-486b-9eee-f541cb605f5b
  - c1577f6a-6b53-4c66-a7ad-2c69fb7df643
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
prod_img: prod_img/img-(1)-1733305461.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733306691
---
