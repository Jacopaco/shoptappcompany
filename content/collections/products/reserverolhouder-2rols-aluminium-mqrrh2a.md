---
id: '8415'
blueprint: products
art_nr: '8415'
title: 'Reserverolhouder 2rols aluminium, MQRRH2A'
slug: reserverolhouder-2rols-aluminium-mqrrh2a
price: 5460
cost_price: 3276
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Duo reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 4puntsbevestiging.
prod_img: prod_img/8415.JPG
unit_price: 5460
vip_price: 4914
---
