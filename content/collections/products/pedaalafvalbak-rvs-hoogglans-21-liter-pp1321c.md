---
id: '11096'
blueprint: product
art_nr: '11096'
title: 'Pedaalafvalbak RVS hoogglans 21 liter, PP1321C'
slug: pedaalafvalbak-rvs-hoogglans-21-liter-pp1321c
cost_price: 5328
unit_price: 8880
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8880
vip_price: 7992
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Algemene beschrijving

  - Ronde bak, staal AISI 430 glanzend afgewerkt, inhoud 20 L.
  - Opening met pedaal.
  - Geschikt voor drukbezochte sanitaire ruimtes.
  - Gaat normaal gesproken samen met een toiletpapierdispenser, een toiletborstelhouder en een warmelucht
  handendroger. 
  Afm.: ±4%

  Onderdelen en materialen

  - Ronde behuizing, staal AISI 430 glanzend afgewerkt.
  - Geurwerend deksel.
  - Binnenbak van polypropyleen met metalen handvat.
  - Handgreep in bovenste gedeelte om het transport te vergemakkelijken.
  - Zwart thermoplastisch antislippedaal.
  - Zwarte kunststof antislipbodem, die de bodem van de bak vochtvrij houdt.
prod_img: prod_img/mediclinics-pp1321c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719480299
---
