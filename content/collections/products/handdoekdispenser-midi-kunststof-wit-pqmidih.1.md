---
id: 5542-br
published: false
blueprint: products
art_nr: 5542-BR
title: 'Handdoekdispenser midi kunststof wit, PQMidiH'
slug: handdoekdispenser-midi-kunststof-wit-pqmidih
cost_price: 1770
unit_price: 3000
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 3000
vip_price: 2700
---
