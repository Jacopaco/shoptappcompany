---
id: 5470-br
published: false
blueprint: products
art_nr: 5470-BR
title: 'Zeepdispenser 1200 ml automatisch kunststof wit, PQASoap12'
slug: zeepdispenser-1200-ml-automatisch-kunststof-wit-pqasoap12
cost_price: 3940
unit_price: 6680
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 6680
vip_price: 6012
---
