---
id: '13673'
blueprint: product
art_nr: '13673'
title: 'Facial tissue dispenser RVS, AI0985CS'
slug: facial-tissue-dispenser-rvs-ai0985cs
cost_price: 2550
unit_price: 3900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 3900
vip_price: 3510
categories: 4c580ad2-fca1-4d85-921b-911b48345e74
description: |-
  Met een verkoudheidsvirus in de lucht lijkt iedereen te snotteren. Het is dan ook handig als er voldoende tissues op voorraad zijn om uw gasten en medewerkers te voorzien van een hygiënische opfrisbeurt. Met de rvs facial tissue dispenser van Mediclinics bent u verzekerd van een flinke voorraad tissues.

  De facial tissue dispenser van Mediclinics is niet alleen mooi, maar waarborgt ook nog eens een optimaal hygiënisch gebruik. Door de speciaal aangebrachte anti-fingerprintcoating zijn vingerafdrukken verleden tijd. Bovendien is de dispenser zeer eenvoudig in gebruik; de tissues liggen duidelijk zichtbaar aan de oppervlakte, en zijn daardoor met één beweging uit de dispenser te trekken.

  Verkoudheidsvirus in de lucht? Met de rvs facial tissue dispenser van Mediclinics heeft u altijd voldoende tissues bij de hand.
prod_img: prod_img/afbeelding-ai0985cs-1719321013.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321019
---
