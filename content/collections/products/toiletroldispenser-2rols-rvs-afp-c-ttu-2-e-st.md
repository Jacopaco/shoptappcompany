---
id: '3301047'
blueprint: products
art_nr: '3301047'
title: 'Toiletroldispenser 2rols RVS afp-c, TTU 2 E ST'
slug: toiletroldispenser-2rols-rvs-afp-c-ttu-2-e-st
price: 26430
cost_price: 15858
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
prod_img: prod_img/3301047.JPG
unit_price: 26430
vip_price: 23787
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
