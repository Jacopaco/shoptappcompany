---
id: 6543-188
blueprint: products
art_nr: '6543 - 188'
title: 'Onderzoeksbankrollen cellulose 2 lgs 59 cm x 150 m 4st'
slug: onderzoeksbankrollen-cellulose-2-lgs-59-cm-x-150-m-4st
price: 11523
cost_price: 5649
brands: d2e2fc4b-bc98-488c-90a2-f0adb231736a
categories: 8218d088-f732-41b2-a36e-13cfec06fed9
prod_img: prod_img/6543---188.jpg
unit_price: 8920
vip_price: 9603
locations:
  - 87ab27e8-a05b-4dc5-9e3d-446fa8e3f4c9
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
  - 35ffadc8-cd7a-458b-b8f3-01732fd6cb74
  - ef78b35c-42e4-458c-a65b-1f2446135a30
client_prices:
  - 5162e19a-84e6-4f47-83e6-760863dc5c91
  - 5162e19a-84e6-4f47-83e6-760863dc5c91
---
