---
id: '14316'
blueprint: product
art_nr: '14316'
title: 'Babyverschoontafel verticaal RVS, CP0016VCS'
slug: babyverschoontafel-verticaal-rvs-cp0016vcs
cost_price: 52740
unit_price: 87900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 87900
vip_price: 79110
categories: b3e1fbfb-c2e5-4dec-87c7-096657368819
description: |-
  Babyverschoontafel verticaal RVS, CP0016VCS

  RVS Babyverschoontafel verticaal.
  Geschikt voor wandmontage.
  Gemaakt van bacterie resistent kunststof.
  Inclusief afsluitbare dispenser voor verschoonpapier/doeken.
  Voorzien van antibacteriële Biocote® bescherming.
  Met nylon veiligheidsgordel.
  Met aan weerskanten ophanghaken voor persoonlijke bezittingen.

  ###Aanbevolen installatiehoogte:
  800 mm vanaf het laagste punt
  700 mm vanaf het laagste punt op minder valide toiletten.
  Voldoet volledig aan de EN 12221-1 en EN 12221-2 richtlijnen.
  Laat uw baby onder geen enkele omstandigheid alleen of buiten uw bereik achter op de babyverschoontafel.

  Niet geschikt voor montage op gipsplaten of holle muren!! 

  Artikelnummer 	14316
  Materiaal 	RVS
  Hoogte 	480 mm
  Breedte 	550 mm
  Diepte 	100 mm (890 mm open)
  Gewicht 	16,6 kg
  Garantie 	2 jaar na aankoopdatum
  Toepassing 	Wandmontage
  Eigenschappen 	Inclusief dispenser voor verschoonpapier/doeken, twee ophanghaken en veiligheidsgordel
  Versie 	Verticaal
prod_img: prod_img/14316_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714742421
---
