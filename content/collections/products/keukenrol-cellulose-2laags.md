---
id: '2157'
blueprint: products
title: 'STARLIGHT 2157 keukenrollen 32x45 vel - 2 laags'
price: 2020
description: |-
  Keukenrollen cellulose 2 laags

  Code 2157
  Materiaal cellulose
  Kleur wit
  Afmetingen 45 vel op rol
  2 laags
  Verpakking 16 x 2 rollen in folie
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2157_2.jpg'
art_nr: 2157
slug: keukenrol-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 1295
prod_img: prod_img/2157_2-1688653667.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729157379
unit_price: 1648
vip_price: 1683
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 175073cd-07d9-40df-9e81-927b690b393b
  - df52f315-d21f-435f-9fc2-1317451c68b0
categories: 460f024c-aa79-468c-a359-5c0a55680bf8
product_type: physical
---
