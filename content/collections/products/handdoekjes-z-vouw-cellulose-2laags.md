---
id: '2135'
blueprint: products
title: 'STARLIGHT 2135-FSC,Z vouw handdoekjes 22x24 cm - 2 laags - 3200 stuks'
price: 2106
description: |-
  Helder witte vouwhanddoeken van 100% cellulose. Premium kwaliteit met hoogste reinigingskracht en absorptievermogen. Vel-voor-vel-dosering voor verminderd verbruik en verhoogde hygiëne.

  - Materiaal	100% cellulose
  - kleur wit
  - Afmetingen	22 x 24 cm
  - Lagen	2 laags
  - Verpakking	20 x 160 doekjes
  - FSC certificaat

  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer. 
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2135_3_2.jpg'
art_nr: 2135
slug: handdoekjes-z-vouw-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 1350
unit_price: 1995
vip_price: 1755
prod_img: img/2135_3.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1740391999
client_prices:
  - d267d33a-5684-4194-856e-727b2f4b79ba
  - 6aa35b46-fb7b-4809-9144-37d95fecd2d6
  - 8ed96843-92e3-4f5e-bf96-e054d740cea6
  - f4210288-9577-4af7-80f5-49ae25057d41
  - b69a1f96-5c22-487d-bfd7-be02c150ade5
  - 19806848-2c70-4b45-953e-d0ab15a5024b
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - 5d51ad93-e2e6-43c1-a90f-49d97915b08f
  - d8b397ec-2533-4b40-b16e-95ee5348e0cc
  - 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
  - 6d0190b5-4139-49e0-899e-76ea795c5036
  - d66ba4d9-ef23-498b-bf5d-e73d3161faf3
  - e04cab04-8a7b-45a0-9698-aeb7ff898773
  - 342ca54a-98c1-451c-8ea8-f377ef5519e9
  - ff9c79b6-4366-49f2-8e91-3d5b0100bc47
  - ef60def1-f195-4b3b-a291-64fb129e6368
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - d28ddf0a-4681-415d-a8e8-3199d8a714ac
  - 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - b56a4982-42c5-44cc-8e4e-b84a9afbbc3c
  - c3ed1408-6d23-4d75-bab0-4752835fb279
  - 20a3c4dd-dfc7-4b88-a8de-adf592de239c
  - a933caf6-a138-462a-a9ad-76b9c7e0344b
  - d7654c12-3145-4478-8fa3-970c4b764ae1
  - 8e96d2c0-638d-4b77-9c40-9d443a81e60b
  - 5162e19a-84e6-4f47-83e6-760863dc5c91
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - a9bd7d8c-213d-4c7a-aa35-dca7027d50e2
  - 2fec2271-8935-4b02-b3b1-dbddeb746b69
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 1300049a-83d7-47aa-86ff-49b7207633e6
  - d267d33a-5684-4194-856e-727b2f4b79ba
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 175073cd-07d9-40df-9e81-927b690b393b
  - 1cbc4fc8-faea-4a79-a9c1-410949fac628
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
  - 3bc12b37-5aff-4aed-8cd9-66f8ed6eae3b
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - f2f903e1-4856-4d9b-86e4-16f4e1618fa8
  - c8eeae72-6c18-4ef1-8628-5087adf3afe9
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - fd034e41-e357-4acc-8957-4bb9ed02d77d
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
  - 225a4a10-f583-4663-aa73-07579bc9b443
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - 1f7effa5-1ac3-44d8-8fb4-b4b06575d101
  - df52f315-d21f-435f-9fc2-1317451c68b0
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 21fd8138-93dd-4a6e-992b-066f60627391
  - 52571907-9205-4d01-962e-3ba319c62abe
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
  - 0b583266-c93f-467d-89b8-83c65b7fb392
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - be14461b-4601-4a24-a6dc-17853268cc98
  - a0be2336-697d-408e-ae76-a075fb4e1f62
product_type: physical
fixed_price: 1995
---
