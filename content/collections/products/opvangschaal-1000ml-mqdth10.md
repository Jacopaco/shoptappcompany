---
id: '8105'
blueprint: products
art_nr: '8105'
title: 'Opvangschaal 1000ml MQDTH10'
slug: opvangschaal-1000ml-mqdth10
price: 5960
cost_price: 3576
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Opvangschaal.
  Wordt over de dispenser heen gehangen.
  Met uitneembare opvangschaal.
prod_img: prod_img/8105.JPG
unit_price: 5960
vip_price: 5364
---
