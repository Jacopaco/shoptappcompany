---
id: cart00963
published: false
blueprint: products
art_nr: CART00963
title: 'Abena Classic nitril blauw maat XL '
slug: abena-classic-nitril-blauw-maat-xl
cost_price: 222
unit_price: 299
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
price: 299
vip_price: 299
locations:
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
  - 78839325-c105-4e00-88cd-9b808c86a969
  - 851491c7-811a-41ec-a665-8715a2ec61a9
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
  - ad521bf5-2669-452d-81b0-0b55218e7ac4
  - 5ed3871c-f423-4dc3-9755-7ee2d3a676bc
  - 82359df9-6fb1-4321-8adf-db6f37f28079
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
client_prices:
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
---
