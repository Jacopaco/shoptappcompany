---
id: '5526'
blueprint: products
art_nr: '5526'
title: 'Modular GREY kunststof toiletpapierhouder bulkpack'
slug: toilet-tissue-dispenser-bulkpack-kunststof-wit-pqtissue
price: 2075
cost_price: 1307
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Toilet tissue dispenser voor wandmontage.
  Met ovalen onderzijde voor perfecte uitname van de papiertjes.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
prod_img: prod_img/5526.JPG
unit_price: 2075
vip_price: 1867
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728992277
---
