---
id: '150790945'
published: false
blueprint: product
art_nr: '150790945'
title: 'Afvalzakken incontinentie 50x60cm wit'
slug: afvalzakken-incontinentie-50x60cm-wit
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 3820
unit_price: 5730
price: 7013
vip_price: 5844
locations:
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - 9febd33c-508e-4088-80ef-1681be040e54
  - 1f727611-7eac-4fc0-978b-0e9b171f437a
  - 0b83c198-6db3-49db-843a-ae2c3dd953cc
  - ff938854-4adb-42f4-aae1-2b3f0d52b517
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
prod_img: prod_img/afvalzakken-incontinentie-50x60cm-wit-1733305153.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733305166
---
