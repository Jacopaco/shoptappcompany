---
id: '12011'
blueprint: product
art_nr: '12011'
title: 'Haardroger wit, SC0010'
slug: haardroger-wit-sc0010
cost_price: 3211
unit_price: 5840
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 5840
vip_price: 5256
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/12011-sc0010.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479529
---
