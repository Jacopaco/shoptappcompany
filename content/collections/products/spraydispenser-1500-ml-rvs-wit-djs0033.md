---
id: '13165'
blueprint: product
art_nr: '13165'
title: 'Spraydispenser 1500 ml RVS wit, DJS0033'
slug: spraydispenser-1500-ml-rvs-wit-djs0033
cost_price: 8061
unit_price: 13050
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 13050
vip_price: 11745
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  In een ruimte waar alles draait om hygiëne, mag de Mediclinics spraydispenser zeker niet ontbreken.

  Met deze stralend witte zeepdispenser van Mediclinics voorziet u uw gast op elk moment van de dag van een ontsmettende hoeveelheid alcohol. Dat deze spraydispenser een unicum is in zijn soort, is duidelijk. De witte-rvs look van de dispenser, sluit naadloos aan bij elke smaakvol ingerichte sanitaire ruimte. Door een simpele druk op de pushknop doseert de dispenser de exacte hoeveelheid alcohol die nodig is om uw handen of uw toiletseat te reinigen. Navullen van de dispenser is ook eenvoudig; met de speciaal bijgevoegde sleutel opent en sluit u makkelijk de navulruimte.
prod_img: prod_img/afbeelding-djs0033c-1719323188.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323198
---
