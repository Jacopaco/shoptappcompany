---
id: '5540'
blueprint: products
art_nr: '5540'
title: 'INLEGPLAATJE HANDDOEKDISP. PQ LINE'
slug: inlegplaatje-handdoekdisp-pq-line
price: 850
cost_price: 519
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Inlegadapter met ovalen opening. t.b.v. interfold handdoeken in de PlastiQline
  handdoekdispensers PQMiniH en PQMidiH
  Voor gebruik in de PlastiQline - PQMiniH en PQMidiH handdoekdispenser.
prod_img: prod_img/5540.JPG
unit_price: 850
vip_price: 765
---
