---
id: '13742'
blueprint: product
art_nr: '13742'
title: 'Toiletroldispenser 3rols RVS, PR2789CS'
slug: toiletroldispenser-3rols-rvs-pr2789cs
cost_price: 5988
unit_price: 9980
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9980
vip_price: 8982
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Uw gasten hechten misschien weinig waarde aan toiletpapier, totdat ze geconfronteerd worden me dat laatste velletje. Op dat moment heeft toiletpapier topprioriteit. De 3rolshouder van Mediclinics voorkomt dit probleem.

  Met drie rollen op voorraad komen uw gasten nooit met lege handen te staan. De gebruiksvriendelijke houder, is voorzien van een speciale kijkgleuf voor een makkelijke inhoudscontrole. Hierdoor heeft uw facilitaire dienst altijd zicht op de voorraad. De hoogglans rvs buitenzijde maakt deze 3rolshouder een elegante toevoeging in uw sanitaire ruimte.

  Wilt u altijd verzekerd zijn van voldoende voorraad in uw toiletruimte? Kies dan voor de 3rolshouder rvs van Mediclinics.
prod_img: prod_img/afbeelding-pr2789cs-1719320647.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320653
---
