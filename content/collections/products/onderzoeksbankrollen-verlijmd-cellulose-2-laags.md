---
id: '6540'
published: false
blueprint: products
art_nr: '6540'
title: 'Onderzoeksbankrollen verlijmd cellulose 2 laags'
slug: onderzoeksbankrollen-verlijmd-cellulose-2-laags
price: 87
cost_price: 56.3
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: 2b3ca86a-26af-461e-b819-7415564e5b92
description: '39 cm | 6 x 150 meter | Ø 16 cm'
unit_price: 0
vip_price: 73
prod_img: prod_img/6540-1881.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1686224572
---
