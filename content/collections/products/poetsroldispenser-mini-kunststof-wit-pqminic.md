---
id: '5536'
blueprint: products
art_nr: '5536'
title: 'Poetsroldispenser mini kunststof wit, PQMiniC'
slug: poetsroldispenser-mini-kunststof-wit-pqminic
price: 2465
cost_price: 1553
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: d1ca567f-2127-432a-aec4-29fef09a974d
description: |-
  Poetsroldispenser voor wandmontage.
  Met slot en witte PQ sleutel.
  Met V voor afscheuring van de center pull rollen.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
prod_img: prod_img/5536.JPG
unit_price: 2465
vip_price: 2218
---
