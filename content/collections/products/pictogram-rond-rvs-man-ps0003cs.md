---
id: '14284'
blueprint: product
art_nr: '14284'
title: 'Pictogram rond RVS MAN, PS0003CS'
slug: pictogram-rond-rvs-man-ps0003cs
cost_price: 780
unit_price: 1300
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 1300
vip_price: 1170
categories: 33e55e5a-60bb-4897-9444-80c306b1c4b5
description: |-
  Maak het uw gasten gemakkelijk!

  Bij binnenkomst vinden de meeste mensen het fijn als het toilet gemakkelijk te vinden is. Dit RVS pictogram geeft uw gasten duidelijk aan waar het herentoilet zich bevindt. Installatie is eenvoudig en het pictogram wordt geleverd inclusief sterke tape voor installatie op effen oppervlakten.
prod_img: prod_img/afbeelding-ps0002cs-1719323751.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323757
---
