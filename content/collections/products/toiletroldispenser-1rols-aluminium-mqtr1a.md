---
id: '8395'
blueprint: products
art_nr: '8395'
title: 'Toiletroldispenser 1rols aluminium, MQTR1A'
slug: toiletroldispenser-1rols-aluminium-mqtr1a
price: 5715
cost_price: 3429
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  1rolshouder voor wandmontage.
  Met RVS beugel.
  Zelf remmend systeem.
prod_img: prod_img/8395.JPG
unit_price: 5715
vip_price: 5143
---
