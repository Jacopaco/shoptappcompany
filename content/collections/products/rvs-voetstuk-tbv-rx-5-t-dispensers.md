---
id: '21422671'
blueprint: products
art_nr: '21422671'
title: 'RVS voetstuk t.b.v. RX 5 T dispensers'
slug: rvs-voetstuk-tbv-rx-5-t-dispensers
price: 10440
cost_price: 6000
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/21422671.jpg
unit_price: 16136
vip_price: 8700
---
