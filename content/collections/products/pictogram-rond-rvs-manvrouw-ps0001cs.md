---
id: '14282'
blueprint: product
art_nr: '14282'
title: 'Pictogram rond RVS MAN/VROUW, PS0001CS'
slug: pictogram-rond-rvs-manvrouw-ps0001cs
cost_price: 780
unit_price: 1300
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 1300
vip_price: 1170
categories: 33e55e5a-60bb-4897-9444-80c306b1c4b5
description: |-
  Met deze man/vrouw (RVS) pictogram zorgt u ervoor dat uw gasten het toillet gemakkelijk kunnen vinden.

  De pictogram is eenvoudig te installeren en wordt geleverd inclusief sterke tape, zodat de pictogram ook op effen oppervlakten geïnstalleerd kan worden. Dankzij het neutrale, strakke design past deze pictogram bij bijna elk interieur.
prod_img: prod_img/afbeelding-ps0001cs-1719323951.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323957
---
