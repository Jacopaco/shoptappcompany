---
id: '7317'
blueprint: products
art_nr: '7317'
title: 'LAVANDA SmartNose GeurStick'
slug: geurstick-lavendel
cost_price: 295
unit_price: 369
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
price: 369
vip_price: 369
description: |-
  Smart Nose navulling Lavanda.

  Geurmengesel van lavendel, citrus, eucalyptus, oranje bloemen en sandelhout.
  Heeft een geurafgifte van ca. 30 dagen.

  Compacte luchtverfrissing.
  Bevat een frisse parfum dat nare luchtjes absorbeert in plaats van enkel te maskeren.
  Geschikt voor alle kleine ruimten en (hygiëne)afvalbakken.
  Geen spraytechniek, laat hierdoor geen residue achter.
  Geen stroom of batterijen nodig.
  Kan met dispenser (artikel 98957) gebruikt worden.

  Heeft een lange geurafgifte van ongeveer één maand;
  Bespaart kosten ten opzichte van traditionele geurapplicaties;
  Neemt weinig ruimte in in de opslag of op de schoonmaakkar;
  Bevat een fris parfum dat nare luchtjes absorbeert in plaats van enkel te maskeren;
  Heeft geen stroom of batterijen nodig;
  Geschikt voor kleine ruimtes, zoals liften, toiletten, prullenbakken, kamerentree, vergaderruimtes;
  Maakt gebruik van een 100% veilig systeem;
  Gebruikt geen spraytechniek en laat geen residu achter;
  Is bijna niet zichtbaar door zijn kleine formaat en daarom bestand tegen vandalisme;
  Is makkelijk aan de muur te bevestigen met onze speciale RVS houder, zonder te boren.
  Dit product is geschikt voor sanitaire ruimtes, entree hallen, kantoren, hotelkamers en woningen in diverse sectoren:

  Ziekenhuizen
  Nationaal en internationaal vervoer
  Vakantieparken
  Verzorgingstehuizen
  Hogescholen
  Wellness centra
  Hotels
  Woningcorporaties
  Zorgcomplexen
  Revalidatie centra
  GGD locaties
prod_img: prod_img/7317_01.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729153067
---
