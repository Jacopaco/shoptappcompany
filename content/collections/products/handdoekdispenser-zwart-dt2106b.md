---
id: '12925'
blueprint: product
art_nr: '12925'
title: 'Handdoekdispenser zwart, DT2106B'
slug: handdoekdispenser-zwart-dt2106b
cost_price: 3702
unit_price: 6170
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6170
vip_price: 5553
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Goede handhygiëne is een must in elke openbare sanitaire ruimte. Het voorkomt verspreiding van bacteriën, en geeft uw gasten ook nog eens een lekker schoon gevoel. Met de handdoekdispenser van Mediclinics kiest u niet alleen voor een opvallend design, maar ook voor praktisch gebruik.

  Dat een sanitaire ruimte zeker niet saai hoeft te zijn, bewijst deze stoere zwarte handdoekdispenser van Mediclinics. De opvallende zwarte kleur en het robuuste design maken van deze handdoekdispenser een regelrechte blikvanger. De dispenser biedt ruimte aan 400-600 gevouwen handdoekjes en is eenvoudig te monteren aan de wand.
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477221
prod_img: prod_img/dt2106b.jpg
---
