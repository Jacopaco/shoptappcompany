---
id: '180205100'
blueprint: product
art_nr: '180205100'
title: 'Afvalzakken 80x110cm blauw'
slug: afvalzakken-80x110cm-blauw
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 2409
unit_price: 3614
price: 4422
vip_price: 3685
locations:
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 5f99d252-324c-462e-afe7-b35bac11105d
  - 31ab6798-7e55-4a96-8c41-c9d35bf02694
prod_img: prod_img/180205100.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733306574
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
---
