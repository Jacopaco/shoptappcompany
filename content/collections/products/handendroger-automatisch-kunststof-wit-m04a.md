---
id: '12100'
blueprint: product
art_nr: '12100'
title: 'Handendroger automatisch kunststof wit, M04A'
slug: handendroger-automatisch-kunststof-wit-m04a
cost_price: 6930
unit_price: 12600
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 12600
vip_price: 11340
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Handendroger wit automatisch, M04A (Smart Flow)
  Automatische handendroger 1100 Watt.
  Bruto luchtopbrengst ca. 27l/sec.
  Luchtsnelheid: 85 km/u.
  Luchttemperatuur 47˚C.
  60dB.
  Droogtijd: 39 seconden.
prod_img: prod_img/mediclinics-m04a.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479214
---
