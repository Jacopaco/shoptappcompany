---
id: '8423'
blueprint: products
art_nr: '8423'
title: 'Reserverolhouder 1rol RVS, MQRRH1E'
slug: reserverolhouder-1rol-rvs-mqrrh1e
price: 6475
cost_price: 3885
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
prod_img: prod_img/8423.JPG
unit_price: 6475
vip_price: 5827
---
