---
id: '13105'
blueprint: products
art_nr: '13105'
title: 'Mondmaskerdispenser RVS, MQ FMD'
slug: mondmaskerdispenser-rvs-mq-fmd
price: 5300
cost_price: 3180
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
prod_img: prod_img/13105.JPG
unit_price: 5300
vip_price: 4770
categories: 2507084c-0b96-4778-bb5a-2b2350a2e7f4
---
