---
id: '11097'
blueprint: product
art_nr: '11097'
title: 'Pedaalafvalbak RVS 20 liter, PP1321CS'
slug: pedaalafvalbak-rvs-20-liter-pp1321cs
cost_price: 5394
unit_price: 8990
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8990
vip_price: 8091
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Algemene beschrijving

  - Ronde bak, staal AISI 430 satijn afwerking, 20 L inhoud.
  - Opening met pedaal.
  - Geschikt voor drukbezochte sanitaire ruimtes.
  - Gaat meestal samen met een toiletpapierdispenser, een toiletborstelhouder en een warmeluchthandendroger.
  handendroger. 
  Afm.: ±4%

  Onderdelen en materialen

  - Ronde behuizing, staal AISI 430 satijnglans.
  - Geurwerend deksel.
  - Binnenbak van polypropyleen met metalen handvat.
  - Handgreep in bovenste gedeelte om het transport te vergemakkelijken.
  - Zwart thermoplastisch antislippedaal.
  - Zwarte kunststof antislipbodem die de bodem van het bakje vochtvrij houdt.
prod_img: prod_img/mediclinics-pp1321cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479969
---
