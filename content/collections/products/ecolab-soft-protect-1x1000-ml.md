---
id: '8353067650'
blueprint: products
art_nr: '8353067650'
title: 'Ecolab SOFT PROTECT 1x1000 ml'
slug: ecolab-soft-protect-1x1000-ml
price: 15.4284
cost_price: 9.89
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
unit_price: 14.05
vip_price: 12.857
prod_img: prod_img/8353067650.jpg
---
