---
id: '13173'
blueprint: product
art_nr: '13173'
title: 'Toiletborstelhouder zwart, ES0010B'
slug: toiletborstelhouder-zwart-es0010b
cost_price: 4770
unit_price: 7950
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 7950
vip_price: 7155
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Over smaak valt niet te twisten. Maar er zijn accessoires die nagenoeg iedereen mooi moet vinden. En dat is handig, want daarmee komt u tegemoet aan al de bezoekers van uw sanitaire ruimte.

  Onze Mediclinics-lijn is speciaal ontworpen voor mensen met oog voor detail. De aandachtige bezoeker zal direct het gebruik van de verschillende materialen opvallen. Het strakke witte ontwerp wordt afgewisseld met een luxueus rvs deksel. De afwisseling van deze materialen zorgt voor een speels effect bij deze toiletborstelhouder. Bovendien wordt het praktische gebruik niet uit het oog verloren: het nauwkeurig aan de steel bevestigde deksel, zorgt voor een hygiënische afsluiting van de houder.
prod_img: prod_img/es0010b_1-1719322575.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322588
---
