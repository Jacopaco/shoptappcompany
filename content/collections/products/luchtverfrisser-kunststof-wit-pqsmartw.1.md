---
id: 14242-br
published: false
blueprint: products
art_nr: 14242-BR
title: 'Luchtverfrisser kunststof wit, PQSmartW'
slug: luchtverfrisser-kunststof-wit-pqsmartw
cost_price: 2700
unit_price: 4500
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
price: 4500
vip_price: 4050
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1708099220
---
