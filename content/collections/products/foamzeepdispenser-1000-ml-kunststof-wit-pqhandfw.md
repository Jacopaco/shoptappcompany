---
id: '14214'
blueprint: products
art_nr: '14214'
title: 'Foamzeepdispenser 1000 ml kunststof wit, PQHandFW '
slug: foamzeepdispenser-1000-ml-kunststof-wit-pqhandfw
price: 2800
cost_price: 1680
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met sleutel.
  Wordt met navulfles geleverd.
  Met grijze drukknop, duwrichting naar de muur.
  Met kijkvenster voor inhoudscontrole
prod_img: prod_img/14214.JPG
unit_price: 2800
vip_price: 2520
---
