---
id: '5677'
blueprint: products
art_nr: '5677'
title: 'Afvalbak 43 liter swing zilver, PQSA43M'
slug: afvalbak-43-liter-swing-zilver-pqsa43m
price: 8370
cost_price: 5273
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met swing deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling. 
prod_img: prod_img/5677.JPG
unit_price: 8370
vip_price: 7533
---
