---
id: '8165'
blueprint: products
art_nr: '8165'
title: 'Handdoekdispenser mini wit, MQHSP'
slug: handdoekdispenser-mini-wit-mqhsp
price: 8760
cost_price: 5256
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser klein.
  Met zichtvenster voor inhoudscontrole.
  Afsluitbaar, slot zit aan de voorzijde.
  Cover gaat aan de voorzijde open.
  Met 3punts bevestiging.
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/8165.JPG
unit_price: 8760
vip_price: 7884
---
