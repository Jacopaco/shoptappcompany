---
id: '13662'
blueprint: product
art_nr: '13662'
title: 'Poetsroldispenser midi RVS, DT0303CS'
slug: poetsroldispenser-midi-rvs-dt0303cs
cost_price: 12396
unit_price: 20660
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 20660
vip_price: 18594
categories: d1ca567f-2127-432a-aec4-29fef09a974d
description: |-
  Mediclinics DT0303CS rvs poetsroldispenser voor wandmontage. Afsluitbaar met een schuifdeurtje en drukslot. Max. Ø rol is 250 mm. Max. hoogte rol is 270 mm. Geschikt voor o.a. de Tork poetsrollen.

  Specificaties
  - Artikelnummer 13662
  - Model DT030CS
  - Materiaal RVS
  - Hoogte 365 mm
  - Breedte 261 mm
  - Diepte 261 mm
  - Gewicht 3,1 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Vulling 1 midi poetsrol van max. Ø 250 mm. Rolhoogte: max. 270 mm
  - Eigenschappen Met schuifdeur
prod_img: prod_img/13662_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714742229
---
