---
id: '12320'
blueprint: product
art_nr: '12320'
title: 'Handendroger automatisch RVS, M03ACS'
slug: handendroger-automatisch-rvs-m03acs
cost_price: 28820
unit_price: 52400
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 52400
vip_price: 47160
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Een perfecte combinatie van de juiste accessoires, geeft uw sanitaire ruimte de sfeer die bij uw bedrijf past. Tijdens het ontwerp van de Mediclinics-lijn is al rekening gehouden met de combinatie tussen de diverse accessoires. Alle producten binnen de lijn sluiten daarom naadloos aan bij elkaar.

  Als u kiest voor rvs, weet u dat u kiest voor robuust. Het stevige materiaal maakt van deze handendroger direct een boeiend accent voor in uw sanitaire ruimte. De onderkant van de handendroger bestaat uit verschillende uitgangen, waardoor de lucht zich over een relatief groot vlak verspreidt. Hierdoor heeft de droger slechts een halve minuut nodig om zijn werk optimaal uit te voeren.
prod_img: prod_img/mediclinics-m03acs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478152
---
