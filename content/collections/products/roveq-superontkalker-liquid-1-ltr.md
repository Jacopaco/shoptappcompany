---
id: 100-150
blueprint: products
art_nr: 100-150
title: 'ROVEQ Superontkalker LIQUID 1 ltr '
slug: roveq-superontkalker-liquid-1-ltr
cost_price: 1300
unit_price: 1773
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 2215
vip_price: 1846
prod_img: prod_img/100-150.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1696237819
---
