---
id: '8205'
blueprint: products
art_nr: '8205'
title: 'Afvalbak 6 liter wit, MQWB6P'
slug: afvalbak-6-liter-wit-mqwb6p
price: 13840
cost_price: 8304
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak met klepdeksel.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8205.JPG
unit_price: 13840
vip_price: 12456
---
