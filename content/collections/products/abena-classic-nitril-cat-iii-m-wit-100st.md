---
id: cart00636
published: false
blueprint: products
art_nr: CART00636
title: 'Abena Classic Nitril CAT III M blauw 200st'
slug: abena-classic-nitril-cat-iii-m-blauw-200st
price: 580
cost_price: 444
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
prod_img: prod_img/cart00636.JPG
unit_price: 580
vip_price: 580
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1693385787
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
