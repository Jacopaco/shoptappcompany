---
id: '853087050'
blueprint: products
art_nr: '853087050'
title: 'Ecolab zachte reiniginglotion 1x1000 ml'
slug: ecolab-zachte-reiniginglotion-1x1000-ml
price: 8.5644
cost_price: 5.49
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/853087050.JPG
unit_price: 7.97
vip_price: 7.137
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
