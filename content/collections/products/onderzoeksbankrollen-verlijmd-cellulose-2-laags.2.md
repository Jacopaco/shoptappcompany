---
id: '6545'
published: false
blueprint: products
art_nr: '6545'
title: 'Onderzoeksbankrollen verlijmd cellulose 2 laags'
slug: onderzoeksbankrollen-verlijmd-cellulose-2-laags
price: 54
cost_price: 34.8
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: 2b3ca86a-26af-461e-b819-7415564e5b92
description: '50 cm | 6 x 80 meter | Ø 13,5 cm'
unit_price: 0
vip_price: 45
prod_img: prod_img/6545.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1686224649
---
