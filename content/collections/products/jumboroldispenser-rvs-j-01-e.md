---
id: '23400224'
blueprint: products
art_nr: '23400224'
title: 'Jumboroldispenser RVS, J 01 E'
slug: jumboroldispenser-rvs-j-01-e
cost_price: 4711
unit_price: 9200
brands: f77a48e2-462d-4719-8961-2637e36823d5
price: 9200
vip_price: 8280
prod_img: prod_img/23400224.png
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
