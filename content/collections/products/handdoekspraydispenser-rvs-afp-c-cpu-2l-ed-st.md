---
id: 21419955-afp-c
blueprint: products
art_nr: '21419955 AFP-C'
title: 'Handdoek/Spraydispenser RVS afp-c, CPU 2L E/D ST'
slug: handdoekspraydispenser-rvs-afp-c-cpu-2l-ed-st
cost_price: 13024
unit_price: 27300
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 27300
vip_price: 24570
prod_img: prod_img/21419955-afp-c.png
---
