---
id: '12270'
blueprint: product
art_nr: '12270'
title: 'Handendroger automatisch wit, E05A'
slug: handendroger-automatisch-wit-e05a
cost_price: 22495
unit_price: 40900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 40900
vip_price: 36810
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Een zorgvuldig ingerichte sanitaire ruimte getuigt van smaak. Een flinke pre in het oog van uw gasten. Want uw gasten willen niets liever dan ontspannen in een smaakvol ingerichte sanitaire ruimte.

  Dat onze ontwerpers weten wat gemak is, bewijst deze handendroger van Mediclinics. Het eenvoudige gebruik en de smetteloze uitstraling maken deze witte gepoedercoate variant een echte bestseller. De droger wordt automatisch geactiveerd wanneer huidcontact plaatsvindt. Door de snelle verplaatsing van de warme lucht is activatie van een halve minuut genoeg voor volledig droge handen.
prod_img: prod_img/mediclinics-e05a.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478492
---
