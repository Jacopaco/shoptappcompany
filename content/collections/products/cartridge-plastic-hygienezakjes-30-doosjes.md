---
id: '21245800'
blueprint: products
title: 'HYG.ZAK 30 st/25 omdoos HYGB K'
price: 91
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/dameshygienzak.jpg'
art_nr: 21245800
slug: hygzak-30-st25-omdoos-hygb-k
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 0e559e3a-a5c9-444d-b968-d93cc759839f
cost_price: 50
description: |-
  Plastic hygiënezakjes.
  30 stuks in een catridge.
  Met zijplooien en vloeistofdichte naden.
  Afname per doos = 25 catridges
unit_price: 200
vip_price: 76
prod_img: prod_img/dameshygienzak.jpg
updated_by: 222ea5d9-8bb0-4bc0-9a96-53629f69c900
updated_at: 1711728080
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - 6d0190b5-4139-49e0-899e-76ea795c5036
  - d66ba4d9-ef23-498b-bf5d-e73d3161faf3
  - 342ca54a-98c1-451c-8ea8-f377ef5519e9
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - fd034e41-e357-4acc-8957-4bb9ed02d77d
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - be14461b-4601-4a24-a6dc-17853268cc98
product_type: physical
---
