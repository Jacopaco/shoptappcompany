---
id: '8035'
blueprint: products
art_nr: '8035'
title: 'Zeepdisp. alu UP 1000ml KB, MQ10A'
slug: zeepdisp-alu-up-1000ml-kb-mq10a
price: 11425
cost_price: 6855
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig afgekit kan worden.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8035.JPG
unit_price: 11425
vip_price: 10282
---
