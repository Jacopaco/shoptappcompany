---
id: '12635'
blueprint: products
art_nr: '12635'
title: 'Grab bar met 3 steunpunten RVS, BFD600CS'
slug: grab-bar-met-3-steunpunten-rvs-bfd600cs
price: 19370
cost_price: 11622
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Wandgreep voor montage rechts van toiletten, wasbakken etc.
  Met 3 steunpunten.
  Grote stanglengte: 615 mm.
  Korte stanglengte: 245 mm
prod_img: prod_img/12635.JPG
unit_price: 19370
vip_price: 17433
---
