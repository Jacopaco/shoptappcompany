---
id: '11083'
blueprint: product
art_nr: '11083'
title: 'Pedaalafvalbak RVS hoogglans 5 liter, PP1305C'
slug: pedaalafvalbak-rvs-hoogglans-5-liter-pp1305c
cost_price: 2982
unit_price: 4970
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 4970
vip_price: 4473
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1305c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845191
---
