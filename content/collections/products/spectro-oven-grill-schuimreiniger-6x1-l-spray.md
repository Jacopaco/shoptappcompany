---
id: 84600202f
blueprint: products
art_nr: 84600202F
title: 'SPECTRO A004060 oven & grill schuimreiniger 6x1 L Spray'
slug: spectro-oven-grill-schuimreiniger-6x1-l-spray
price: 6816
cost_price: 4000
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/84600202f.JPG
unit_price: 4000
vip_price: 5680
description: |-
  Verpakking: 6 x liter Spray Bottle

  Gebruiksklare, schuimende, sterk alkalische reiniger speciaal ontwikkeld voor het grondig reinigen van de oven, grill en bakplaat.

  Gebruiksaanwijzing
  Zorg dat de oppervlaktetemperatuur lager is dan 70 °C (40 °C is optimaal). Product onverdund aanbrengen en circa 10 minuten in laten werken (niet in laten drogen). Verwijder los vuil met een krasvrije schuurspons of borstel. Vervolgens spoelen met schoon warm water en aan de lucht laten drogen.

  Let op: Oven & Grill Schuimreiniger kan worden gebruikt op RVS, ijzer, steen en kunststof. Niet gebruiken op aluminium en geverfde oppervlakken.

  Toepassingen
  Oven & Grill Schuimreiniger verwijdert moeiteloos ingebrande vetten en aangekoekte suikers. Te gebruiken in ovens, grillapparatuur, bakplaten en barbecues. Door de schuimende werking van Oven & Grill Schuimreiniger in combinatie met een verlengd spruitstuk of speciale schuimtrigger verbetert het bereik en beperkt het de dampvorming. Hierdoor is het product veiliger voor de ademhaling.

  Technische Eigenschappen
  Kleur: Bruin
  Geur: Ongeparfumeerd
  pH waarde: 13,5
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729238043
---
