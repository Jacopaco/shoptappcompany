---
id: '12922'
blueprint: product
art_nr: '12922'
title: 'Handdoekdispenser RVS, DT2106CS'
slug: handdoekdispenser-rvs-dt2106cs
cost_price: 5730
unit_price: 9550
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9550
vip_price: 8595
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Na een kort toiletbezoek is het prettig als u uw handen kunt drogen. De oplossing is eenvoudig: met de handdoekdispenser van Mediclinics laat u al uw gasten schoon en droog uit het toilet komen.

  Wanneer u houdt van elegantie, valt uw oog al snel op rvs. De glanzende en luxueuze uitstraling geven elke sanitaire ruimte de indruk van een stijlvol design. Met deze rvs handdoekdispenser geeft u elke sanitaire ruimte een boeiend accent. De dispenser biedt ruimte aan meer dan 400 handdoekjes. Hierdoor bent u ook op piekmomenten verzekerd van voldoende voorraad. Aanvullen is mogelijk via de voorzijde, door het speciale veerslot met bijbehorende sleutel.
prod_img: prod_img/paper-towel-dispensers-dt2106cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477293
---
