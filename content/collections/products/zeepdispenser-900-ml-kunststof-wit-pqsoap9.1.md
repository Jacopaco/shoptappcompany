---
id: 5500-br
published: false
blueprint: products
art_nr: 5500-BR
title: 'Zeepdispenser 900 ml kunststof wit, PQSoap9'
slug: zeepdispenser-900-ml-kunststof-wit-pqsoap9
cost_price: 1870
unit_price: 3170
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 3170
vip_price: 2853
---
