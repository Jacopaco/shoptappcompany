---
id: '2921033'
blueprint: products
title: 'Stofwisdoeken 60x25cm geïmpregneerd doos 1000 stuks'
price: 5889
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727269577
art_nr: '2921033'
slug: wisdoeken-geimpregneerd-wit-80cm
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
cost_price: 2583
unit_price: 2583
vip_price: 4907
prod_img: prod_img/2921033.jpg
description: |-
  Stofwisdoeken geïmpregneerd 60x25cm: Geïmpregneerde wisdoeken voor de stofwisframe. Deze doeken zijn licht geïmpregneerd en nemen het fijnste vuil op. Zeer geschikt voor diverse vloeren. Trekken stof, pluisjes, katten-en hondenhaar aan en houden het vast. Voorkomen rondvliegende deeltjes en zijn ideaal voor parket, gladde tegels, pvc-vloerbekleding en handig in gebruik als stofdoek.

  Met de stofwisdoeken 60×25 verwijdert u gemakkelijk stof en vuil op een hele snelle en zeer hygiënische manier. De stofwisdoeken zijn zeer geschikt voor harde vloeren, zoals hout, tegels, laminaat, linoleum, enz..

  De stofwisdoekjes zijn geschikt voor alle soorten stofwissers met een maximale lengte van 60cm.
  Kleur: wit
  Gewicht: 18 gr / m2.
  Verpakking: 1.000 stuk per doos.
  Afmeting per doekje: 60×25 centimeter.
  Stofwissen:
  Stofwissen is geschikt voor gladde, gesloten vloeren (dus zonder kieren, gaten en spleten). Bij het stofwissen wordt alleen losliggend stof en ander lichtgewicht vervuiling zoals (dieren)haren verwijderd. Gebruik eerst de stofzuiger indien er sprake is van overwegend zwaar en/of grof vuil, wat niet meegenomen kan worden door de wisser en wisdoekjes.
client_prices:
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
  - be14461b-4601-4a24-a6dc-17853268cc98
  - a0be2336-697d-408e-ae76-a075fb4e1f62
  - 9febd33c-508e-4088-80ef-1681be040e54
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - d8c0c7a9-4285-4f48-ab4d-0f80c181a289
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
  - 74a43c5f-48ad-4b62-a3de-f7ccf9d30320
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - abf442bb-e1b0-458c-bb2a-18d1f1a37b6e
  - c4053601-ae30-4783-9fdc-a26afacce843
  - 21aaf902-e0ac-4201-9f1e-887260394548
categories: 2eed0a1b-425e-4ba1-a75e-bdc973a77dba
product_type: physical
---
