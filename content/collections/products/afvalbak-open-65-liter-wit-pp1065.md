---
id: '11063'
blueprint: product
art_nr: '11063'
title: 'Afvalbak open 65 liter wit, PP1065'
slug: afvalbak-open-65-liter-wit-pp1065
cost_price: 12900
unit_price: 21500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 21500
vip_price: 19350
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak wit 65 liter open, PP1065
  Afvalbak open.
  Staat op 4 kunststof voetjes.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/mediclinics-pp1065.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845846
---
