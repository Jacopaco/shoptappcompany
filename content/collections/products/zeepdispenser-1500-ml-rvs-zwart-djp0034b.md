---
id: '14060'
blueprint: product
art_nr: '14060'
title: 'Zeepdispenser 1500 ml RVS zwart, DJP0034B'
slug: zeepdispenser-1500-ml-rvs-zwart-djp0034b
cost_price: 7015
unit_price: 12755
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 12755
vip_price: 11479
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  MEDICLINICS DJP0034B zeepdispenser met beugel 1500 ml.


  - Artikelnummer 14060
  - Model DJP0034B
  - Materiaal RVS
  - Kleur Zwart gepoedercoat
  - Hoogte 240 mm
  - Breedte 110 mm
  - Diepte 133 mm
  - Gewicht 1,25 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Toepassing Navulbaar
  - Vulling Vloeibare zeep, lotions en crèmes
  - Dosering 1,5 ml per slag
  - Inhoud 1500 ml
prod_img: prod_img/14060_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715070849
---
