---
id: '160109600'
blueprint: product
art_nr: '160109600'
title: 'Afvalzakken bio 60x80cm groen'
slug: afvalzakken-bio-60x80cm-groen
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 5250
unit_price: 7875
price: 9639
vip_price: 8032
locations:
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - 9febd33c-508e-4088-80ef-1681be040e54
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - 11006000-90bb-4728-b1e4-eb18e446374d
  - 84dec7cd-ea0a-46e7-a7a9-b254693d1da8
prod_img: prod_img/60-liter-gft.jpeg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733305297
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
---
