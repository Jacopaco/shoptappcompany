---
id: 220142ip
blueprint: products
art_nr: 220142IP
title: 'Inlegplaat tbv interfold HSU15 en HSU31'
slug: inlegplaat-tbv-interfold-hsu15-en-hsu31
price: 1200
cost_price: 720
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
prod_img: prod_img/220142ip.JPG
unit_price: 1200
vip_price: 1080
---
