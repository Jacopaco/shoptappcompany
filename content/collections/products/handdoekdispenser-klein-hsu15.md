---
id: 21412267-afp-c
blueprint: products
art_nr: '21412267 AFP-C'
title: 'Handdoekdispenser klein HSU15'
slug: handdoekdispenser-klein-hsu15
price: 16660
cost_price: 8996
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
prod_img: prod_img/21412267-afp-c.JPG
unit_price: 16660
vip_price: 14994
---
