---
id: '17147'
blueprint: product
art_nr: '17147'
title: 'Wandhaardroger zwart/zwart met spiraalsnoer, Action Super Plus 1600 All Black'
slug: wandhaardroger-zwartzwart-met-spiraalsnoer-action-super-plus-1600-all-black
cost_price: 2580
unit_price: 7400
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 7400
vip_price: 6660
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  VALERA ACTION SUPER PLUS 1600 ALL BLACK Wandhaardroger zwart met zwart spiraalsnoer

  - Met 2 standen voor de luchtopbrengst, 3 standen voor de temperatuur en koude luchtstand
  - Met aan/uit drukknop en smal mondstuk
  - Wandhouder met: aan/uit veiligheidsschakelaar
  - IP20
prod_img: prod_img/17147.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714739097
---
