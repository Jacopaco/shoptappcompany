---
id: '4403270'
published: false
blueprint: products
art_nr: '4403270'
title: 'Untouchable KX Unt dispenser soap - no pump'
slug: untouchable-kx-unt-dispenser-soap-no-pump
price: 10757
cost_price: 10757
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
unit_price: 10757
vip_price: 10757
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
