---
id: '8065'
blueprint: products
art_nr: '8065'
title: 'Zeepdisp.RVS 1000ml LB+afsluitpl.MQLV10E'
slug: zeepdisprvs-1000ml-lbafsluitplmqlv10e
price: 17520
cost_price: 10512
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/8065.JPG
unit_price: 17520
vip_price: 15768
---
