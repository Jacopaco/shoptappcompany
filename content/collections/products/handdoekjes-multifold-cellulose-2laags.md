---
id: '2130'
blueprint: products
title: 'Handdoekjes multifold cellulose 2laags'
price: 3411
description: '20,5 x 24 cm | 20 x 153 vellen in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2131-3_1.jpg'
art_nr: 2130
slug: handdoekjes-multifold-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 2187
unit_price: 0
vip_price: 2843
prod_img: prod_img/2131-3_1-1688653489.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653492
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - 6aa35b46-fb7b-4809-9144-37d95fecd2d6
  - 19806848-2c70-4b45-953e-d0ab15a5024b
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - af9b6bfa-831d-406e-b186-218ccb70be55
  - 890e8e99-6bec-4865-b34d-e6b4eb963f86
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 3bc12b37-5aff-4aed-8cd9-66f8ed6eae3b
  - 7392b23f-6bcc-42b9-b010-fc0375fdd35a
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
  - a0be2336-697d-408e-ae76-a075fb4e1f62
---
