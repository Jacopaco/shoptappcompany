---
id: '2250'
published: false
blueprint: products
title: 'Afvalzakken T50 60 x 80 | 50 liter grijs vervallen'
price: 4507
description: '20 x 25 in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2250.jpg'
art_nr: 2250
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654194
slug: afvalzakken-t50-60-x-80-50-liter-grijs-vervallen
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 2455
unit_price: 0
vip_price: 3756
prod_img: prod_img/2250.jpg
client_prices:
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - 6d0190b5-4139-49e0-899e-76ea795c5036
  - d66ba4d9-ef23-498b-bf5d-e73d3161faf3
  - 342ca54a-98c1-451c-8ea8-f377ef5519e9
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - d28ddf0a-4681-415d-a8e8-3199d8a714ac
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 175073cd-07d9-40df-9e81-927b690b393b
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - fd034e41-e357-4acc-8957-4bb9ed02d77d
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
  - 1f7effa5-1ac3-44d8-8fb4-b4b06575d101
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
  - be14461b-4601-4a24-a6dc-17853268cc98
  - a618e884-3fb2-4394-9a3f-effd139ef796
---
