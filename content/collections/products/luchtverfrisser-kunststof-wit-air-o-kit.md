---
id: '12940'
blueprint: products
art_nr: '12940'
title: 'Luchtverfrisser kunststof wit, Air-O-Kit'
slug: luchtverfrisser-kunststof-wit-air-o-kit
price: 1600
cost_price: 960
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
description: |-
  Luchtverfrisser voor montage op de deur op vlak naast de deur.
  Met sleutel.
  Met aan weerskanten openingen voor optimale geurbeleving.
  Werkt d.m.v. luchtstroom.
  Zonder batterijen.
  Geschikt voor de air-o-kit vullingen.
prod_img: prod_img/12940.JPG
unit_price: 1600
vip_price: 1440
---
