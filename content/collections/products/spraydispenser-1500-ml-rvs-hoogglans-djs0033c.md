---
id: '13167'
blueprint: product
art_nr: '13167'
title: 'Spraydispenser 1500 ml RVS hoogglans, DJS0033C'
slug: spraydispenser-1500-ml-rvs-hoogglans-djs0033c
cost_price: 7122
unit_price: 11870
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11870
vip_price: 10683
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Warme handdoekjes, hygiënische toiletten en natuurlijk een goede spraydispenser, zodat uw gasten verzekerd zijn van optimaal comfort. Met de Mediclinics Spraydispenser RVS hoogglans weet u zeker dat u een blijvende indruk achterlaat bij uw gasten.

  U hoeft geen kenner te zijn om in een oogopslag te zien dat deze spraydispenser duidelijk het neusje van de zalm is binnen de Mediclinics-lijn. De strakke afwerking met hoogwaardig hoogglans rvs biedt een chic accent in elk interieur. De dispenser, die gevuld kan worden met handalcohol of oppervlakte desinfectie-alcohol, is onmisbaar in ruimten met een hoge bezoekersfrequentie.  

  Kiest u voor een speelse knipoog in uw sanitaire ruimte? Dan is de keus voor de Mediclinics Spraydispenser RVS hoogglans 1500 ml snel gemaakt. Vraag nu de offerte aan.
prod_img: prod_img/afbeelding-djs0033c-1719323105.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323112
---
