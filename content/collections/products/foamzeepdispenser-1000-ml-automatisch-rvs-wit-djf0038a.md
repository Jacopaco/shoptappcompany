---
id: '14050'
blueprint: product
art_nr: '14050'
title: 'Foamzeepdispenser 1000 ml automatisch RVS wit, DJF0038A'
slug: foamzeepdispenser-1000-ml-automatisch-rvs-wit-djf0038a
cost_price: 10338
unit_price: 17230
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 17230
vip_price: 15507
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  MEDICLINICS DJF0038A sensor foamzeepdispenser 1000 ml.

  - Artikelnummer 14050
prod_img: prod_img/14050_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715072476
---
