---
id: '12360'
blueprint: product
art_nr: '12360'
title: 'Handendroger automatisch RVS hoogglans, M06AC'
slug: handendroger-automatisch-rvs-hoogglans-m06ac
cost_price: 28875
unit_price: 52500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 52500
vip_price: 47250
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Wie kiest voor de serie van Mediclinics, maakt een bewuste keuze voor kwaliteit. Elk product uit deze lijn bestaat uit hoogwaardige materialen en een eigentijds design. Dat geldt zeker voor deze automatische handendroger.

  Dat een handendroger ook glamour kan zijn, bewijst dit exemplaar. De strakke vormgeving en de hoogglans look versterken elkaar als het gaat om luxueuze uitstraling. Dat doet overigens niets af aan de optimale praktische werking. Met een optimale droogtijd van slechts 10 - 12 seconden, en een high-speedmotor van 1150 watt, is deze handendroger en rechtstreekse snelheidsmaniak.
prod_img: prod_img/afbeelding-m06ac-1719477963.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477968
---
