---
id: 84600421a
blueprint: products
art_nr: 84600421A
title: 'SPECTRO A004032 keukenreiniger 2x5 liter'
slug: spectro-keukenreiniger-2x5-liter
price: 6816
cost_price: 4000
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/84600421a.JPG
unit_price: 4000
vip_price: 5680
description: |-
  Geconcentreerde, laagschuimende, ongeparfumeerde universele keukenreiniger voor de reiniging van alle waterbestendige oppervlaktes. Tevens geschikt als inweekmiddel voor de vaat en als vloerreiniger in de schrobzuigmachine.

  Gebruiksaanwijzing
  Keuken: doseer, afhankelijk van de vervuiling 40 tot 60 ml per 5 liter in lauwwarm water.

  Inweekmiddel: doseer maximaal 50 ml in een met koud water gevulde wasbak.

  Vloeren: doseer 5 tot 10 ml per liter water in het reservoir van de schrobzuigmachine of mopemmer.

  Toepassingen
  Keukenreiniger kan gebruikt worden voor de reiniging van alle waterbestendige oppervlaktes zoals vloeren, wanden, werkbladen, keukenapparatuur, ketels, containers, etc. Omdat het product laagschuimend is kan het prima gebruikt worden als inweekmiddel voor de vaat (voorkomt schuimvorming in de vaatwasser). Door de unieke reinigende eigenschappen ook geschikt als eenvoudige vloerreiniger in de schrobzuigmachine. Voorkomt schuimvorming in apparatuur.

  Technische Eigenschappen
  Kleur: Oranje
  Geur: Ongeparfumeerd
  pH waarde: 9,0
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729238413
---
