---
id: '3820'
blueprint: products
art_nr: '3820'
title: 'Afvalkorf 50 liter wit, MQ-P60'
slug: afvalkorf-50-liter-wit-mq-p60
price: 4840
cost_price: 2904
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalkorf rechthoekig, vrijstaand of voor wandmontage.
  Met 4 geïntegreerde ophangpunten.
prod_img: prod_img/3820.JPG
unit_price: 4840
vip_price: 4356
---
