---
id: '5670'
blueprint: products
art_nr: '5670'
title: 'Toiletborstelhouder kunststof zilver, PQBrushM'
slug: toiletborstelhouder-kunststof-zilver-pqbrushm
price: 3490
cost_price: 2199
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Toiletborstelhouder voor wandmontage.
  De ronde dichte onderzijde zit met een muurbevestiging aan de wand.
  Met kunststof zwarte vervangbare borstelkop.
prod_img: prod_img/5670.JPG
unit_price: 3490
vip_price: 3141
---
