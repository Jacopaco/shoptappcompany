---
id: 21411588-afp-c
blueprint: products
art_nr: '21411588 AFP-C.'
title: 'Reserveroldispenser 1rol RVS afp-c, ERU E ST'
slug: reserveroldispenser-1rol-rvs-afp-c-eru-e-st
price: 5290
cost_price: 2857
brands: f77a48e2-462d-4719-8961-2637e36823d5
description: |-
  Reserverolhouder voor wandmontage.
  Voor 1 toiletrol.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21411588-afp-c..JPG
unit_price: 5290
vip_price: 4761
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
