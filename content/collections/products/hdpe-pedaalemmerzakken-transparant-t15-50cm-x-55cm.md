---
id: '150203000'
blueprint: product
art_nr: '150203000'
title: 'Afvalzakken 50x55cm transparant'
slug: afvalzakken-50x55cm-transparant
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 1659
unit_price: 2489
price: 3045
vip_price: 2538
locations:
  - 9febd33c-508e-4088-80ef-1681be040e54
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
  - 31ab6798-7e55-4a96-8c41-c9d35bf02694
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - 3d925d9b-d247-486b-9eee-f541cb605f5b
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
  - 21aaf902-e0ac-4201-9f1e-887260394548
  - c4053601-ae30-4783-9fdc-a26afacce843
  - abf442bb-e1b0-458c-bb2a-18d1f1a37b6e
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - 74a43c5f-48ad-4b62-a3de-f7ccf9d30320
  - be14461b-4601-4a24-a6dc-17853268cc98
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
  - a0be2336-697d-408e-ae76-a075fb4e1f62
prod_img: prod_img/150203000.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733306711
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
---
