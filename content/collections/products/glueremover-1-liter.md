---
id: '7436948412439'
published: false
blueprint: products
art_nr: '7436948412439'
title: 'GLUEREMOVER 1 liter'
slug: glueremover-1-liter
price: 4196
cost_price: 2690
brands: 3375fa99-d12c-4998-8110-4d347cec808b
prod_img: prod_img/7436948412439.
unit_price: 2690
vip_price: 3497
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685626585
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
