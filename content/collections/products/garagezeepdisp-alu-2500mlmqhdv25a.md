---
id: '8305'
blueprint: products
art_nr: '8305'
title: 'Garagezeepdisp. alu 2500ml,MQHDV25A'
slug: garagezeepdisp-alu-2500mlmqhdv25a
price: 15615
cost_price: 9369
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Garagezeepdispenser met korte bedieningsbeugel en RVS doseerpomp.
  Inclusief afsluitplaat met zichtvenster.
  Navulfles wordt standaard meegeleverd.
prod_img: prod_img/8305.JPG
unit_price: 15615
vip_price: 14053
---
