---
id: '11070'
blueprint: product
art_nr: '11070'
title: 'Afvalbak 80 liter wit, PP0080'
slug: afvalbak-80-liter-wit-pp0080
cost_price: 21600
unit_price: 36000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 36000
vip_price: 32400
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak wit 80 liter gesloten, PP0080
  Afvalbak met push klep.
  Staat op 4 kunststof voetjes.
  Deksel is in zijn geheel van de onderbak af te nemen.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/pp0080_4.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718805539
---
