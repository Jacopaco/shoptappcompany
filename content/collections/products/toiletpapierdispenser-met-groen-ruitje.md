---
id: 5584-green
published: false
blueprint: products
art_nr: 5584-GREEN
title: 'Toiletpapierdispenser met groen ruitje'
slug: toiletpapierdispenser-met-groen-ruitje
price: 0
cost_price: 4800
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
description: |-
  Toiletroldispenser voor 2 rollen voor wandmontage.
  De toiletkokers zitten om 2 kunststof assen, die zorgen voor automatische toiletrolwisseling.
  Assen blijven met lege kokers in de dispenser zitten.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Modulair systeem.
prod_img: prod_img/5584.JPG
unit_price: 0
vip_price: 0
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
