---
id: '21414690'
blueprint: products
art_nr: '21414690'
title: 'Toiletborstelhouder RVS wit, WBU 3 P IW'
slug: toiletborstelhouder-rvs-wit-wbu-3-p-iw
price: 17680
cost_price: 9547
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
prod_img: prod_img/21414690.jpg
unit_price: 17680
vip_price: 15912
---
