---
id: '8325'
blueprint: products
art_nr: '8325'
title: 'Trio Zeepdisp.RVS KB 1000ml, MQTV10E'
slug: trio-zeepdisprvs-kb-1000ml-mqtv10e
price: 40750
cost_price: 24450
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  TRIO zeep-& desinfectiemiddeldispenser 1000 ml met korte bedieningsbeugel en RVS doseerpomp.
  Met RVS achterplaat en afsluitplaat met zichtvenster.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8325.JPG
unit_price: 40750
vip_price: 36675
---
