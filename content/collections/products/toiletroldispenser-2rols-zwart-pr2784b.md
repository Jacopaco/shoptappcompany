---
id: '13119'
blueprint: product
art_nr: '13119'
title: 'Toiletroldispenser 2rols zwart, PR2784B'
slug: toiletroldispenser-2rols-zwart-pr2784b
cost_price: 5385
unit_price: 8975
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8975
vip_price: 8077
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Dat zelfs een ‘doodgewone’ toiletrolhouder een echte eyecatcher kan zijn, bewijst deze zwart gepoedercoate 2-rolshouder van Mediclinics. Het stoere zwarte frame springt direct in het oog van elke bezoeker met gevoel voor design.

  Met deze 2-rolshouder is dat ‘beruchte’ laatste vel verleden tijd. De houder biedt ruimte aan twee toiletrollen, zodat u altijd verzekerd bent van voldoende voorraad. Het strak vormgegeven frame houdt op een subtiele manier de toiletrollen uit het oog van de toiletbezoeker. Via de creatief ontworpen uitsparing aan de onderzijde beschikt de toiletbezoeker precies over de gewenste hoeveelheid toiletpapier.
prod_img: prod_img/pr2784b.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719476705
---
