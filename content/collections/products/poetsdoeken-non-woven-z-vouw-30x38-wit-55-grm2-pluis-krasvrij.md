---
id: '2240'
blueprint: products
art_nr: '2240'
title: 'STARLIGHT 2240 poetsdoeken non woven 15x50 stuks'
slug: poetsdoeken-non-woven-z-vouw-30x38-wit-55-grm2-pluis-krasvrij
price: 5294
cost_price: 2322
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
description: |-
  Poetsdoeken non woven Z-vouw wit 55 gr/m2 pluis- & krasvrij
  30 x 38 cm | 10 x 50 stuks in doos
prod_img: prod_img/2240.jpg
unit_price: 0
vip_price: 4411
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729157512
---
