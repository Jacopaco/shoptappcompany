---
id: '2184'
blueprint: products
art_nr: '2184'
title: 'Poetsrol mini cellulose 1laags'
slug: poetsrol-mini-cellulose-1laags
cost_price: 1310
unit_price: 2861
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
price: 2043
vip_price: 1703
prod_img: prod_img/2184.jpg
description: '19 cm | 12x 120 meter in folie'
---
