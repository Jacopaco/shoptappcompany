---
id: '2186'
blueprint: products
title: 'STARLIGHT 2186-FSC,centerfeed poetspapier 6x300 meter 1 laags'
price: 2488
description: |-
  STARLIGHT centerfeed poetsrollen 6x300 meter

  1-laags hulsloos poetspapier voor comfort en hygiëne.
  Wit, zacht en stevig.
  Breed toepasbare handoekrol.

  Hulsloze centerfeed poetsrol
  Roldiameter: 19 mm
  Colli inhoud: 6
  100% cellulose

  FSC gecertificeerd
  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2186_001.jpg'
art_nr: 2186
slug: poetsrol-midi-cellulose-coreless-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1595
unit_price: 0
vip_price: 2073
prod_img: prod_img/2186_001.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729001201
client_prices:
  - cf25c62f-df48-4e88-9c29-33f9eca1deb2
  - 4e00303b-45d0-49d8-9aad-8a8ef0e22948
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - 2fec2271-8935-4b02-b3b1-dbddeb746b69
  - 890e8e99-6bec-4865-b34d-e6b4eb963f86
  - cf25c62f-df48-4e88-9c29-33f9eca1deb2
locations:
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - 112c9b1d-3004-44b3-a872-aa1f80e862ce
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - df52f315-d21f-435f-9fc2-1317451c68b0
  - be14461b-4601-4a24-a6dc-17853268cc98
product_type: physical
---
