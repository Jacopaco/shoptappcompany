---
id: '12462'
blueprint: product
art_nr: '12462'
title: 'Luchtverfrisser vulling lavendel, PQ14'
slug: luchtverfrisser-vulling-lavendel-pq14
cost_price: 660
unit_price: 1100
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 1100
vip_price: 990
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
description: |-
  Uw gasten een frisse en aangename ruimte aanbieden? Dan mag een luchtverfrisser zeker niet missen. 

  De lavendel luchtverfrisser is een frisse geurneutralisator voor een schonere en aangename sfeer voor in de sanitaire ruimte. Deze luchtverfrisser is geschikt voor de filterhouder van de Twinflow handendroger. De luchtverfrissers zitten verpakt per 4 stuks.

  Wilt u het uw gasten comfortabel maken?
prod_img: prod_img/afbeelding-lavendel-navulling-1719477644.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477649
---
