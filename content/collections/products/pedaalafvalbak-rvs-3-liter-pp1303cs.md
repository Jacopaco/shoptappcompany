---
id: '11081'
blueprint: product
art_nr: '11081'
title: 'Pedaalafvalbak RVS 3 liter, PP1303CS'
slug: pedaalafvalbak-rvs-3-liter-pp1303cs
cost_price: 1920
unit_price: 3200
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 3200
vip_price: 2880
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1303cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845344
---
