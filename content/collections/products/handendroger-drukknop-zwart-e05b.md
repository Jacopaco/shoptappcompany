---
id: '12285'
blueprint: product
art_nr: '12285'
title: 'Handendroger drukknop zwart, E05B'
slug: handendroger-drukknop-zwart-e05b
cost_price: 24090
unit_price: 43800
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 43800
vip_price: 39420
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Dat eenvoud ook stijlvol kan zijn, bewijst deze zwarte handendroger van Mediclinics. De subtiele variant van de E88B biedt, ondanks zijn beperkte omvang, dezelfde kracht en comfort als zijn grote broer. Het zwart gepoedercoate frame onderstreept de robuustheid van deze subtiele handendroger.

  Gebruik is eenvoudig; de handendroger slaat aan, zodra de toiletbezoeker de knop indrukt. De perfecte temperatuur in combinatie met de nauwkeurig afgestelde luchtsnelheid zorgt ervoor dat uw handen binnen een halve minuut weer helemaal droog zijn.

  Eenvoudig maar stijlvol
  Blaast warme lucht
  Slechts een halve minuut droogtijd
prod_img: prod_img/e05b.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478403
---
