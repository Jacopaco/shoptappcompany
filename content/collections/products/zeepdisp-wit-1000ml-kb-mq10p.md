---
id: '8040'
blueprint: products
art_nr: '8040'
title: 'Zeepdisp. wit 1000ml KB, MQ10P'
slug: zeepdisp-wit-1000ml-kb-mq10p
price: 12310
cost_price: 7386
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig afgekit kan worden.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8040.JPG
unit_price: 12310
vip_price: 11079
---
