---
id: '2163'
blueprint: products
art_nr: '2163'
title: 'Handdoekrol Mini Matic XL wit 2laags'
slug: handdoekrol-mini-matic-xl-wit-2laags
price: 101.4
cost_price: 65
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: 2b3ca86a-26af-461e-b819-7415564e5b92
description: '18,3 cm | 6 x 165 meter in doos'
unit_price: 0
vip_price: 84.5
prod_img: prod_img/2163.jpg
---
