---
id: 21413896-afp-c
blueprint: products
art_nr: '21413896 AFP-C.'
title: 'Afvalbak open 50 liter RVS afp-c, EBU 50 E ST'
slug: afvalbak-open-50-liter-rvs-afp-c-ebu-50-e-st
price: 28820
cost_price: 15563
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak 50 liter.
  Met open cover en binnenring.
  Vrijstaand of voor wandmontage.
  Frame is makkelijk afneembaar van de onderbak.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21413896-afp-c..JPG
unit_price: 28820
vip_price: 25938
---
