---
id: '12310'
blueprint: product
art_nr: '12310'
title: 'Handendroger automatisch RVS hoogglans, M03AC'
slug: handendroger-automatisch-rvs-hoogglans-m03ac
cost_price: 28820
unit_price: 52400
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 52400
vip_price: 47160
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  De hoogglans accessoires van Mediclinics laten zien dat sfeer in een klein hoekje zit. De accessoires zijn hebben luxueuze uitstraling, zonder kitscherig te worden. Een perfect accessoire voor wie van een beetje glamour houdt.

  Deze hoogglans handendroger van Mediclinics is een bijzondere eyecatcher in uw sanitaire ruimte. Het strakke design en de verrassend frisse uitstraling maken deze handendroger nog aantrekkelijker in gebruik. De droger is voorzien van een automatisch startsysteem, dat reageert zodra uw gast de handen onder het luchtrooster plaatst.
prod_img: prod_img/mediclinics-m03ac.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478199
---
