---
id: '8240'
blueprint: products
art_nr: '8240'
title: 'Hyiënebak + zakjesdispenser 6 liter Aluminium, MQWB6HBHA'
slug: hyienebak-zakjesdispenser-6-liter-aluminium-mqwb6hbha
price: 16380
cost_price: 9828
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënebak met klepdeksel en gemonteerde zakjeshouder.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8240.JPG
unit_price: 16380
vip_price: 14742
---
