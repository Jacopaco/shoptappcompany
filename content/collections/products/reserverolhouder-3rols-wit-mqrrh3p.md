---
id: '8469'
blueprint: products
art_nr: '8469'
title: 'Reserverolhouder 3rols wit, MQRRH3P'
slug: reserverolhouder-3rols-wit-mqrrh3p
price: 6984
cost_price: 4191
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Trio reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 4puntsbevestiging.
prod_img: prod_img/8469.JPG
unit_price: 6984
vip_price: 6285
---
