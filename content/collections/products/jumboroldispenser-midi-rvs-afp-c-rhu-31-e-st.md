---
id: 2201453-afp-c
blueprint: products
art_nr: '2201453 AFP-C.'
title: 'Jumboroldispenser midi RVS afp-c, RHU 31 E ST'
slug: jumboroldispenser-midi-rvs-afp-c-rhu-31-e-st
price: 20950
cost_price: 12570
brands: f77a48e2-462d-4719-8961-2637e36823d5
description: |-
  Jumboroldispenser voor wandmontage.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2201453-afp-c..JPG
unit_price: 20950
vip_price: 18855
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
