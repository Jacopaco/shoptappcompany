---
id: '14317'
blueprint: product
art_nr: '14317'
title: 'Babyverschoontafel verticaal zwart, CP0016VCSB'
slug: babyverschoontafel-verticaal-zwart-cp0016vcsb
cost_price: 63300
unit_price: 105500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 105500
vip_price: 94950
categories: b3e1fbfb-c2e5-4dec-87c7-096657368819
description: |-
  Babyverschoontafel verticaal zwart, CP0016VCSB

  Geschikt voor wandmontage.
  Inclusief afsluitbare dispenser voor verschoonpapier/doeken.
  Voorzien van antibacteriële Biocote® bescherming.
  Met nylon veiligheidsgordel.
  Met aan weerskanten ophanghaken voor persoonlijke bezittingen.

  Aanbevolen installatiehoogte:
  800 mm vanaf het laagste punt
  700 mm vanaf het laagste punt op minder valide toiletten.

  Voldoet volledig aan de EN 12221-1 en EN 12221-2 richtlijnen.
  Laat uw baby onder geen enkele omstandigheid alleen of buiten uw bereik achter op de babyverschoontafel.

  Niet geschikt voor montage op gipsplaten of holle muren!!

  - Artikelnummer	14317
  - Materiaal	RVS zwart gepoedercoat
  - Hoogte	480 mm
  - Breedte	550 mm
  - Diepte	100 mm (890 mm open)
  - Gewicht	16,6 kg
  - Garantie	2 jaar na aankoopdatum
related_products:
  - cart00638
documents:
  -
    id: lvqnjcy2
    document_title: 'Data Sheet'
    document_file: data_sheet_eu.pdf
prod_img: prod_img/14317_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714739465
---
