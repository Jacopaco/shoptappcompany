---
id: '13750'
blueprint: product
art_nr: '13750'
title: 'Jumboroldispenser midi wit, PR2787'
slug: jumboroldispenser-midi-wit-pr2787
cost_price: 4146
unit_price: 6909
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6909
vip_price: 6218
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Niemand wil tijdens een toiletbezoek geconfronteerd worden met dat laatste velletje. Met een flinke voorraad toiletpapier bij de hand, stelt u uw gasten nooit teleur.

  Deze jumboroldispenser is ongetwijfeld een van de grootste exemplaren in dit assortiment. Met ruimte voor een flinke jumborol van ruim 27 centimeter doorsnee, hoeft u nooit bang te zijn dat uw bezoekers met lege handen komen te staan. De speciale afscheurranden aan de onderzijde van de dispenser zorgen voor een eenvoudige verwijdering van het toiletpapier.

  Als u graag aandacht geeft aan vormgeving en optimaal gebruiksgemak, is deze Mediclinics jumboroldispenser helemaal voor u.
prod_img: prod_img/afbeelding-pr2787.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715088177
---
