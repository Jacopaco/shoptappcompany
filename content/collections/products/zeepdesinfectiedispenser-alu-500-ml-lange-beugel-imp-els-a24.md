---
id: '21416000'
blueprint: products
art_nr: '21416000'
title: 'Zeep/Desinfectiedispenser alu 500 ml lange beugel, IMP ELS A/24'
slug: zeepdesinfectiedispenser-alu-500-ml-lange-beugel-imp-els-a24
price: 4750
cost_price: 3995
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp met gebogen aanzuigbuis.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/21416000.JPG
unit_price: 4750
vip_price: 4275
---
