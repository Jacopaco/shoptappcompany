---
id: '12350'
blueprint: product
art_nr: '12350'
title: 'Handendroger automatisch wit, M06A'
slug: handendroger-automatisch-wit-m06a
cost_price: 26125
unit_price: 47500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 47500
vip_price: 42750
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Een mooi en smaakvol ontwerp in uw sanitaire ruimte springt direct in het oog van uw gasten. Het getuigt van smaak en aandacht voor detail. Met deze strak vormgegeven handendroger zorgt u voor een spannend accent in uw sanitaire ruimte.

  De automatische handendroger reageert direct op huidcontact. De high-speed motor zorgt voor een optimale droogcapaciteit en maximale efficiëntie. Door de automatische switch-off is de handendroger beveiligd tegen inefficiënt gebruik.
prod_img: prod_img/afbeelding-m06ac-1719478004.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478008
---
