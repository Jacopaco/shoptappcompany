---
id: '84600773'
blueprint: products
art_nr: '84600773'
title: 'SPECTRO A004065 wc reiniger Eco 6x750 ml'
slug: spectro-a004065-wc-reiniger-eco-6x750-ml
cost_price: 2100
unit_price: 3925
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 3578
vip_price: 2982
description: |-
  Ecologische, gebruiksklare, fris geparfumeerde, zure reiniger geschikt voor de periodieke ontkalking van de sanitaire ruimte.

  Toiletten en urinoirs:
  Toilet doorspoelen en product gelijkmatig aanbrengen door de flacon in te knijpen, eventueel aanbrengen en inborstelen met behulp van toiletborstel. Laat het product enkele minuten inwerken en daarna doorspoelen.

  Overige toepassingen:
  Gelijkmatig aanbrengen op het te ontkalken oppervlak. Maximaal 10 minuten laten inwerken. Eventueel opborstelen en daarna goed naspoelen met schoon water.

  Toepassingen
  WC Reiniger Eco is veilig te gebruiken op alle water en zuurbestendige ondergronden. zoals; keramiek, glazuur, chroom, glas, kunststoffen, tegelwerk, etc. Met uitzondering van kalkhoudende materialen.

  Technische Eigenschap
  Kleur: Rood
  Geur: Fris geparfumeerd
  pH waarde: 2,5
  Toepassing: Flacon met schuine hals die ingezet wordt voor WC reinigers
  Materiaal: 95% PCR HDPE.
  Sluiting: Kindveilige schroefdop met tuitje.

  Verpakking:
  Doos 6 x 750 ml knikhalsfles (A004065)

  Recycle Bottles
  Gebruikte producten verdienen een nieuw leven. Recycling en hergebruik zijn voor ons dan ook belangrijke aandachtspunten in het productieproces. Mooi voorbeeld hiervan zijn onze ‘Recycle Bottles’: gemaakt van tot wel 100% gerecycled kunststof. Recycle Bottles leveren een CO2 besparing op van maximaal 75% ten opzichte van nieuw kunststof én zorgen voor een besparing van 95% op het gebruik van fossiele grondstoffen.
prod_img: prod_img/84600773_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729238243
---
