---
id: 21414689-afp-c
blueprint: products
art_nr: '21414689 AFP-C.'
title: 'Toiletborstelhouder RVS afp-c, WBU 3 E ST'
slug: toiletborstelhouder-rvs-afp-c-wbu-3-e-st
price: 17680
cost_price: 9655
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Toiletborstelhouder voor wandmontage.
  Met uitneembare kunststof opvangschaal.
  Met kunststof zwarte vervangbare toiletborstel en RVS steel.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21414689-afp-c..JPG
unit_price: 17680
vip_price: 15912
---
