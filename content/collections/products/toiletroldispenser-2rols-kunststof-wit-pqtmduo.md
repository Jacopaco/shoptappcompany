---
id: '5582'
blueprint: products
art_nr: '5582'
title: 'Toiletroldispenser 2rols kunststof wit, PQTMDuo'
slug: toiletroldispenser-2rols-kunststof-wit-pqtmduo
price: 3275
cost_price: 2063
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  2rolshouder horizontaal voor standaard rollen.
  Voor wandmontage.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster.
  op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Modulair systeem.
prod_img: prod_img/5582.JPG
unit_price: 3275
vip_price: 2947
---
