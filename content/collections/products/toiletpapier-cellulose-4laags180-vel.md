---
id: '2190'
blueprint: products
title: 'Toiletpapier cellulose 4laags/180 vel'
price: 48.86
description: 'Verpakking van 16 x 4 rollen in folie'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2200_3.jpg'
art_nr: 2190
slug: toiletpapier-cellulose-4laags180-vel
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
cost_price: 34.9
prod_img: prod_img/2200_3-1688654133.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654136
---
