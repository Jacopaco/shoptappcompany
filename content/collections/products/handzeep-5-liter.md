---
id: '98915'
blueprint: products
title: 'Handzeep Eco 5liter can - Premium label'
price: 730
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/Handzeep_Eco_21_3.jpg'
art_nr: 98915
slug: handzeep-eco-5liter-can-premium-label
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: de901aec-80dd-40c6-be90-f2c5d8a24a1e
cost_price: 468
description: |-
  Deze handzeep 5 liter is een fris geparfumeerde, milde
  handreiniger met een mooie parelmoer uitstraling.
  Het product heeft een neutrale pH waarde, vergelijkbaar met de huid.
  Deze handzeep is uitermate geschikt voor kantoren, openbare gebouwen,
  restaurants etc.
unit_price: 0
vip_price: 608
prod_img: prod_img/handzeep_eco_21_3.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1740392090
client_prices:
  - b69a1f96-5c22-487d-bfd7-be02c150ade5
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - e04cab04-8a7b-45a0-9698-aeb7ff898773
  - bfdce2db-c069-4c13-909b-6ba84fa04608
  - d28ddf0a-4681-415d-a8e8-3199d8a714ac
  - 691ea65f-48c3-4e02-82bb-23afe017ed72
  - a9bd7d8c-213d-4c7a-aa35-dca7027d50e2
  - 1300049a-83d7-47aa-86ff-49b7207633e6
  - 8e96d2c0-638d-4b77-9c40-9d443a81e60b
  - 6a9cdf89-b735-45d5-b0cd-ec4800e00349
locations:
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - 1f7effa5-1ac3-44d8-8fb4-b4b06575d101
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - a618e884-3fb2-4394-9a3f-effd139ef796
fixed_price: 900
product_type: physical
---
