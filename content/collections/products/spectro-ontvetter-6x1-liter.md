---
id: 84600422f
blueprint: products
art_nr: 84600422F
title: 'SPECTRO A004059 ontvetter 6x1 liter'
slug: spectro-ontvetter-6x1-liter
price: 3408
cost_price: 2000
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/84600422f.JPG
unit_price: 2000
vip_price: 2840
description: |-
  Geconcentreerde, ongeparfumeerde, sterk alkalische universele reiniger voor het verwijderen van diverse zwaardere vervuilingen van alle alkalibestendige oppervlakken. Het product is geschikt voor zowel dagelijks als periodiek gebruik.

  Gebruiksaanwijzing
  Keuken dagelijks: doseer, afhankelijk van de vervuiling 50 ml tot 100 ml per liter in lauwwarm water.

  Keuken periodiek: doseer minimaal 100 ml per liter in lauwwarm water.

  Vloeren: doseer, afhankelijk van de vervuiling, 25 ml tot 50 ml per liter water in de mopemmer.

  Toepassingen
  Ontvetter kan gebruikt worden voor het reinigen van hardnekkige vervuiling door o.a. eiwitten, oliën, vetten op installaties, wanden, vloeren, werkbladen, afzuigkappen etc.

  Technische Eigenschappen
  Kleur: Geel
  Geur: Ongeparfumeerd
  pH waarde: 13,5
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729238320
---
