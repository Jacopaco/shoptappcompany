---
id: '8497'
blueprint: products
art_nr: '8497'
title: 'Handschoendispenser trio aluminium, MQTGDA'
slug: handschoendispenser-trio-aluminium-mqtgda
price: 13144
cost_price: 7887
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Trio handschoendispenser voor wandmontage.
  Geschikt voor verschillende maten handschoendoosjes.
  Een verende RVS klembeugel houdt de doosjes op de plaats.
  Kan zowel horizontaal als verticaal gemonteerd worden.
  Met 4puntsbevestiging. 
prod_img: prod_img/8497.JPG
unit_price: 13144
vip_price: 11829
---
