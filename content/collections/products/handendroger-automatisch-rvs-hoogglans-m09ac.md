---
id: '12385'
blueprint: product
art_nr: '12385'
title: 'Handendroger automatisch RVS hoogglans, M09AC'
slug: handendroger-automatisch-rvs-hoogglans-m09ac
cost_price: 28600
unit_price: 52000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 52000
vip_price: 46800
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Maak kennis met de Ferrari onder de handendrogers. Deze efficiënte en strak uitziende droger voldoet aan alle eisen van snelheidsaanbidders.

  Voor de liefhebber van snelheid en design, is de keus al snel gemaakt. Deze strak vormgegeven handendroger past in een omgeving waar aandacht voor interieur een grote rol speelt. De hoogglans uitstraling benadrukt het strakke design van de droger. Met een droogtijd van 10 - 12 seconden mag deze droger zich een regelrechte winnaar noemen.
prod_img: prod_img/afbeelding-m09ac.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1717499013
---
