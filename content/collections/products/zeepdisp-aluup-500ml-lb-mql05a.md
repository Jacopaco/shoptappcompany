---
id: '8020'
blueprint: products
art_nr: '8020'
title: 'Zeepdisp. alu.UP 500ml LB, MQL05A'
slug: zeepdisp-aluup-500ml-lb-mql05a
price: 9450
cost_price: 5670
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig af gekit kan worden.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8020.JPG
unit_price: 9450
vip_price: 8505
---
