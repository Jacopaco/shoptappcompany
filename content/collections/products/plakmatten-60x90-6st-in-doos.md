---
id: cart00921
published: false
blueprint: products
art_nr: CART00921
title: 'Plakmatten 60x90 6st in doos'
slug: plakmatten-60x90-6st-in-doos
price: 68.64
cost_price: 44.0
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
unit_price: 63.36
vip_price: 57.2
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687853200
---
