---
id: '6533'
published: false
blueprint: products
title: 'Afvalzakken T50 - 70x110 cm blauw vervallen '
price: 3605
description: '10 x 20 stuks in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/Afvalzak-HDPE-90x110cm-blauw-T30-1rol--25st-110P9-15028101.jpg'
art_nr: 6533
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712059291
slug: afvalzakken-t50-70x110-cm-blauw-vervallen
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 1964
unit_price: 0
vip_price: 3004
prod_img: prod_img/afvalzak-hdpe-90x110cm-blauw-t30-1rol--25st-110p9-15028101.jpg
product_type: physical
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
