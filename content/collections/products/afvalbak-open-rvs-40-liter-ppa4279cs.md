---
id: '11032'
blueprint: product
art_nr: '11032'
title: 'Afvalbak open RVS 40 liter, PPA4279CS'
slug: afvalbak-open-rvs-40-liter-ppa4279cs
cost_price: 14700
unit_price: 24500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 24500
vip_price: 22050
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak RVS 40 liter open, PPA4279CS
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/afbeelding-ppa4279cs-1718806475.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718806533
---
