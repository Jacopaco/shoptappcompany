---
id: '2182'
blueprint: products
title: 'Poetsrol mini cellulose 2laags'
price: 2113
description: '20 cm | 12 x 72 meter in folie'
img_url: 'https://all-care.eu/content/files/Images/TAPP_Company_BV/2182_2.jpg'
art_nr: 2182
slug: poetsrol-mini-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1355
unit_price: 1829
vip_price: 1761
prod_img: prod_img/2182_2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653201
---
