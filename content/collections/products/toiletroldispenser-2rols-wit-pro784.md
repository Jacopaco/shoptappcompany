---
id: '13211'
blueprint: product
art_nr: '13211'
title: 'Toiletroldispenser 2rols wit, PRO784'
slug: toiletroldispenser-2rols-wit-pro784
cost_price: 3779
unit_price: 6300
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6300
vip_price: 5670
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Als u van een eenvoudig ingerichte ruimte houdt, moeten uw accessoires niet te veel opvallen.
  De basislijn van Mediclinics vertaalt topkwaliteit naar een eenvoudig ontwerp.

  Met deze gebruiksvriendelijke 2rolshouder komen uw gasten nooit met lege handen te staan. De dispenser biedt ruimte aan twee standaardtoiletrollen. De rollen zijn eenvoudig vervangbaar. Via kijkgleuven voor inhoudscontrole kunt u de tweede rol eenvoudig aanduwen voor gebruik.
prod_img: prod_img/pro781-784_g-1719322028.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322035
---
