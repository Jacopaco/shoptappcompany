---
id: 2201423-afp-c
blueprint: products
art_nr: '2201423 AFP-C.'
title: 'Handdoekdispenser RVS afp-c, HSU 31 E ST'
slug: handdoekdispenser-rvs-afp-c-hsu-31-e-st
price: 17680
cost_price: 10608
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser voor wandmontage.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Geschikt voor C- en Z-gevouwen handdoekjes.
  Interfold handdoekjes passen samen in combinatie met adapter 2201421IP
prod_img: prod_img/2201423-afp-c..JPG
unit_price: 17680
vip_price: 15912
---
