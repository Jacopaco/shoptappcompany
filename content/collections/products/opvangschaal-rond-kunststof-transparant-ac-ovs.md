---
id: '13202'
blueprint: products
title: 'Opvangschaal rond kunststof transparant, AC-OVS Streng'
price: 26
img_url: 'https://www.all-care.eu/content/files/Images/Alle_producten_All_Care/Verbruiksproducten/AC-OVS_2.jpg'
art_nr: 13202
description: |-
  Kunststof transparante opvangschaal.
  Los onderdeel t.b.v. PQX,MQ en Wings toiletborstelhouders.
  Afname: per streng = 140 stuks
slug: opvangschaal-rond-kunststof-transparant-ac-ovs-streng
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
cost_price: 17
unit_price: 37
vip_price: 22
prod_img: prod_img/ac-ovs_2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653258
client_prices:
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - bebc5c46-0553-4d62-b6ff-de33648493c4
locations:
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
---
