---
id: '8030'
blueprint: products
art_nr: '8030'
title: 'Zeepdisp.RVS UP 500ml LB, MQL05E'
slug: zeepdisprvs-up-500ml-lb-mql05e
price: 12700
cost_price: 7620
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig af gekit kan worden.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8030.JPG
unit_price: 12700
vip_price: 11430
---
