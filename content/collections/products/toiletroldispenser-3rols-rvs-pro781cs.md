---
id: '13318'
blueprint: product
art_nr: '13318'
title: 'Toiletroldispenser 3rols RVS, PRO781CS'
slug: toiletroldispenser-3rols-rvs-pro781cs
cost_price: 5940
unit_price: 9900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9900
vip_price: 8910
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Een simpele rekensom bewijst dat bij een intensief gebruikte sanitaire ruimte er meerdere rollen per dag doorheen gaan. Voldoende voorraad toiletpapier binnen handbereik voorkomt een onnodige inzet van uw facilitaire dienst.

  Met de 3rolshouder van Mediclinics behoren uw zorgen over de voorraad tot de verleden tijd. De houder biedt ruimte aan drie standaardtoiletrollen, die verwisselbaar zijn door het eenvoudig aanduwen van de bovenste rollen. Met deze rvs 3rolshouder laat u zien dat u aandacht besteedt aan de accessoires in uw toiletruimte.
prod_img: prod_img/afbeelding-pr0781cs-1719321616.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321620
---
