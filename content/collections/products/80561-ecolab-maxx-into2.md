---
id: '80561'
blueprint: products
title: 'Ecolab MAXX Into2 Doos 2x5L'
price: 12193
description: '80561 Ecolab MAXX Into2 geconc. zure sanitairreiniger dagelijks/periodiek Doos 2x5L'
img:
  - product_images/ecolab-maxx-into2-9084720.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515450
art_nr: '80561'
slug: ecolab-maxx-into2-doos-2x5l
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 7156
unit_price: 7873
vip_price: 10161
prod_img: prod_img/80561.jpg
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
