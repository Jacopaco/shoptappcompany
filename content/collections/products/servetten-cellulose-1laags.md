---
id: '2155'
blueprint: products
title: 'Servetten cellulose 1laags'
price: 4650
description: '33 x 33 cm 1/4 vouw | 6 x 340 in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2155_3.jpg'
art_nr: 2155
slug: servetten-cellulose-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 2981
unit_price: 0
vip_price: 3875
prod_img: prod_img/2155_3.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653630
client_prices:
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - bebc5c46-0553-4d62-b6ff-de33648493c4
locations:
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
categories: b04aaee2-199c-4a4b-a694-6aa915ac1f86
---
