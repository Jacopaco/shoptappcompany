---
id: '12650'
blueprint: products
art_nr: '12650'
title: 'Hoek grab bar op 2 muren RVS, BD0700CS'
slug: hoek-grab-bar-op-2-muren-rvs-bd0700cs
price: 18215
cost_price: 10929
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Wandgreep voor montage in een hoek.
  Stanglengte: 795 x 795 mm
prod_img: prod_img/12650.JPG
unit_price: 18215
vip_price: 16393
---
