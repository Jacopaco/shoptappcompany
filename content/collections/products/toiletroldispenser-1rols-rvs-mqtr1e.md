---
id: '8365'
blueprint: products
art_nr: '8365'
title: 'Toiletroldispenser 1rols RVS, MQTR1E'
slug: toiletroldispenser-1rols-rvs-mqtr1e
price: 5715
cost_price: 3429
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  1rolshouder voor wandmontage.
  Met RVS beugel.
  Zelf remmend systeem.
prod_img: prod_img/8365.JPG
unit_price: 5715
vip_price: 5143
---
