---
id: '12176'
blueprint: product
art_nr: '12176'
title: 'Handendroger automatisch RVS, M99ACS'
slug: handendroger-automatisch-rvs-m99acs
cost_price: 20323
unit_price: 36950
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 36950
vip_price: 33255
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Productinformatie Mediclinics M99ACS,OPTIMA handdroger

  Mediclinics Optima. Een modern ontwerp voor een betrouwbare, stille handendroger met uitstekende prestatie.
  Geen onderhoud dankzij de borstelloze motor. Makkelijk schoon te maken.
  Hygiënisch dankzij de automatische bediening die voorkomt dat u de handdroger moet aanraken.
  Instelbare handdetectieafstand.
  Een van de stilste machines in zijn categorie (slechts 57 dBA).
  Het verwarmingselement en de motor hebben een ingebouwde veiligheids-thermische uitschakeling.
  Een ideale balans tussen prestaties en prijs.
  Specificaties
  Artikelnummer 12176
  Model M99ACS
  Materiaal RVS
  Hoogte 302 mm
  Breedte 260 mm
  Diepte 150 mm
  Gewicht 4,3 kg
  Garantie 2 jaar na aankoopdatum
  Toepassing Wandmontage
  Eigenschappen Automatisch, 65 km/u, droogtijd: 38 seconden, 57 dB
  Wattage 1640 Watt
prod_img: prod_img/12176_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1717513156
---
