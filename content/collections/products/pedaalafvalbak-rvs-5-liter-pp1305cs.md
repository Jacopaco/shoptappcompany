---
id: '11084'
blueprint: product
art_nr: '11084'
title: 'Pedaalafvalbak RVS 5 liter, PP1305CS'
slug: pedaalafvalbak-rvs-5-liter-pp1305cs
cost_price: 3156
unit_price: 5260
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 5260
vip_price: 4734
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1305cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845158
---
