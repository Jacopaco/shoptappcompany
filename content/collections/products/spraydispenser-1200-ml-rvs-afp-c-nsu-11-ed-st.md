---
id: 21415819-afp-c
blueprint: products
art_nr: '21415819 AFP-C'
title: 'Spraydispenser 1200 ml RVS afp-c, NSU 11 E/D ST'
slug: spraydispenser-1200-ml-rvs-afp-c-nsu-11-ed-st
price: 17680
cost_price: 9547
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Spraydispenser voor wandmontage.
  Kan ook als toilet seat cleaner gebruikt worden.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415819-afp-c.JPG
unit_price: 17680
vip_price: 15912
---
