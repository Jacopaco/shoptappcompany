---
id: 5542-green
published: false
blueprint: products
art_nr: 5542-GREEN
title: 'Handdoekdispenser MIDI met groen ruitje'
slug: handdoekdispenser-midi-met-groen-ruitje
price: 3000
cost_price: 2250
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
description: |-
  Handdoekdispenser voor wandmontage.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Geschikt voor: C- en Z gevouwen papieren handdoekjes met max. 360 mm stapelhoogte
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
unit_price: 3000
vip_price: 2700
prod_img: prod_img/5542.JPG
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
