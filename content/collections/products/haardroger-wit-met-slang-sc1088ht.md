---
id: '12003'
blueprint: product
art_nr: '12003'
title: 'Haardroger wit met slang, SC1088HT'
slug: haardroger-wit-met-slang-sc1088ht
cost_price: 34595
unit_price: 62900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 62900
vip_price: 56610
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Haardroger drukknop wit, SC0088HT
  Wandhaardroger.
  Met een drukknopbediende timer van 90 seconden.
  Met slang en mondstuk.
  Bruto luchtopbrengst ca. 16l/sec.
  Luchtsnelheid: 65 km/u.
  Luchttemperatuur 65˚C.
  70dB.
prod_img: prod_img/mediclinics-sc1088ht.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479851
---
