---
id: '12460'
blueprint: product
art_nr: '12460'
title: 'Handendroger HANDS-IN automatisch wit, PQ14A'
slug: handendroger-hands-in-automatisch-wit-pq14a
cost_price: 49900
unit_price: 72500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 72500
vip_price: 65250
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Als uw gasten net een ontspannen moment achter de rug hebben, zitten ze niet te wachten op een lawaaierige handendroger. De ontwerpers van de exclusieve hands-in-handendroger hebben daar in het ontwerp al rekening mee gehouden.

  Deze hands-in-handendroger typeert zich door een subtiele geluidssterkte en ongekend snelle droogtijd. De droger heeft, in tegenstelling tot andere exemplaren, een zorgvuldig ingebouwde geluidsreductie, waardoor uw gasten niet meer opgeschrikt door ongewenste blaasgeluiden. De droger is voorzien van een speciale Biocote bescherming, die het hygiënisch gebruik van de droger waarborgt.
prod_img: prod_img/12460-pq14a-1719477779.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477785
---
