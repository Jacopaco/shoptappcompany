---
id: '17381'
blueprint: product
art_nr: '17381'
title: 'Wandhaardroger RVS look met shaver aansluiting, Excel 1600 Shaver Silver'
slug: wandhaardroger-rvs-look-met-shaver-aansluiting-excel-1600-shaver-silver
cost_price: 6500
unit_price: 13500
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 13500
vip_price: 12150
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Wandhaardroger RVS look met zwart spiraalsnoer en shaver aansluiting, 1600W. Met 2 standen voor de luchtopbrengst, 3 standen voor de temperatuur en koude luchtstand.
  - 6 Schakelcombinaties.
  - Met verwisselbare luchtfilter en aan/uit schakelaar.

  Wandhouder met:
  - Aan/uit veiligheidsschakelaar
  - Veiligheidstransformator
  - Scheerstopcontact (110-120 V & 220-240 V).
  - IP20
prod_img: prod_img/17381.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714730064
---
