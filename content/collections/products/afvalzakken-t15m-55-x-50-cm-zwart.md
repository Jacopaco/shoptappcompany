---
id: '2253'
published: false
blueprint: products
art_nr: '2253'
title: 'Afvalzakken T15m 55 x 50 cm zwart'
slug: afvalzakken-t15m-55-x-50-cm-zwart
price: 3304
cost_price: 1800
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
description: '1000 stuks in doos'
prod_img: prod_img/2253.JPG
unit_price: 0
vip_price: 2754
---
