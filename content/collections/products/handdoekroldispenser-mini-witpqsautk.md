---
id: '14160'
blueprint: products
art_nr: '14160'
title: 'Handdoekroldispenser MINI wit,PQSAutK'
slug: handdoekroldispenser-mini-witpqsautk
price: 8800
cost_price: 5280
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekroldispenser voor wandmontage.
  De papieren handdoek wordt bij een lengte van 23 cm afgesneden.
  Volledig recyclebaar.
  Met slot en speciale sleutel.
  Papieren handdoekrollen met de volgende aanbevolen afmetingen:
  Rol diameter: max. Ø 210 mm
  Rol breedte: max. 210 mm
  Koker diameter: min. Ø 37 mm
prod_img: prod_img/14160.JPG
unit_price: 8800
vip_price: 7920
---
