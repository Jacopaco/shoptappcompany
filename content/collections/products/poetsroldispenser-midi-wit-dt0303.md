---
id: '13660'
blueprint: product
art_nr: '13660'
title: 'Poetsroldispenser midi wit, DT0303'
slug: poetsroldispenser-midi-wit-dt0303
cost_price: 8490
unit_price: 14150
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 14150
vip_price: 12735
categories: d1ca567f-2127-432a-aec4-29fef09a974d
description: |-
  Een ongeluk zit in een klein hoekje. Zeker wanneer er water of zeep in het spel is. Een goed getrainde facilitaire dienst of nette gast wil dat natuurlijk zo snel mogelijk opruimen. Met de poetsroldispenser van Mediclinics hebben uw bezoekers en medewerkers altijd genoeg poetsdoekjes bij de hand voor de kleine en grote ongelukjes.

  Voor uw gasten houdt u het graag gemakkelijk. Het strak vormgegeven ontwerp van de poetsroldispenser van Mediclinics is daarom vooral gericht op eenvoudig gebruik. U trekt de doekjes uit de onderkant van de dispenser en snijdt ze op de gewenste lengte af aan de daarvoor bestemde rand. Het wisselen van de rol is even eenvoudig als praktisch; u schuift de rol door het schuifluikje in de dispenser, en de dispenser is weer op volle kracht.

  Op zoek naar een poetsroldispenser die zowel eenvoudig als praktisch is?
prod_img: prod_img/afbeelding-dt0303-1719321554.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321562
---
