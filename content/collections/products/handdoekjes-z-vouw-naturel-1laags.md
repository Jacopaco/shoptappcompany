---
id: '2152'
blueprint: products
title: 'SUNSHINE 2152-FSC,recycled tissue Z handdoekjes 23x25 cm - 1 laags - 5000 stuks'
price: 2313
description: |-
  Handdoekjes Z-vouw naturel 1 lgs 23 x 25 cm

  Code 2152
  Materiaal naturel recycled
  Afmetingen 23 x 25 cm
  1 laags
  Verpakking 20 x 250 vel in doos
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2152_3_2.jpg'
art_nr: 2152
slug: handdoekjes-z-vouw-recycled-wit-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 1483
unit_price: 0
vip_price: 1927
prod_img: prod_img/2152_3_2.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729154374
product_type: physical
---
