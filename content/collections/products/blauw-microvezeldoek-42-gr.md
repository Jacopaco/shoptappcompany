---
id: '5723184202'
blueprint: products
art_nr: '5723184202'
title: ' Blauw Microvezeldoek 42 gr'
slug: blauw-microvezeldoek-42-gr
cost_price: 72
unit_price: 149
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
categories: c764401b-cfda-4ea9-8e67-0db8b881128e
price: 164
vip_price: 136
prod_img: prod_img/5723184202.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1696945796
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - ffa8f396-2f25-4d2b-9178-abbf960171cb
  - bbae8e44-4575-431e-8728-181691d314e6
---
