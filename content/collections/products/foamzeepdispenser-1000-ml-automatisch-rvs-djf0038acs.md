---
id: '14053'
blueprint: product
art_nr: '14053'
title: 'Foamzeepdispenser 1000 ml automatisch RVS, DJF0038ACS'
slug: foamzeepdispenser-1000-ml-automatisch-rvs-djf0038acs
cost_price: 9996
unit_price: 16660
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16660
vip_price: 14994
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  MEDICLINICS DJF0038ACS sensor foamzeepdispenser 1000 ml.

  - Artikelnummer 14053
prod_img: prod_img/14053_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715070916
---
