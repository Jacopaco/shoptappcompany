---
id: '14258'
blueprint: products
art_nr: '14258'
title: 'Geurpotje Komkommer/Meloen'
slug: geurpotje-komkommermeloen
price: 600
cost_price: 360
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
description: |-
  De komkommer-meloen geur is gebaseerd op essentiële oliën afkomstig van vruchten en bloemen.
  Bevatten in tegenstelling tot andere geuren geen drijfgassen en synthetische geuren,
  wat bijdraagt aan een milieuvriendelijker wereld. Met inkeping aan de achterzijde
  Gaat ca. 30-60 dagen mee.
  Afname: per doos = 10 stuks
prod_img: prod_img/14258.JPG
unit_price: 600
vip_price: 600
client_prices:
  - 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
categories: f4cb9934-b741-4f26-bf31-378b213ae616
---
