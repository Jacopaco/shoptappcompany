---
id: '17148'
blueprint: product
art_nr: '17148'
title: 'Wandhaardroger zwart/chroom met spiraalsnoer, Action Super Plus 1600 zwart'
slug: wandhaardroger-zwartchroom-met-spiraalsnoer-action-super-plus-1600-zwart
cost_price: 2804
unit_price: 7400
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 7400
vip_price: 6660
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Action Super Plus zwart/chroom ABS kunststof wandhaardroger. 1600 Watt. Voor de aansluiting op een wandcontactdoos. 2 blaas standen, 3 temperatuur standen, koude lucht stand. Aan/uit schakelaar.

  - Artikelnummer 17148
  - Model Action Super Plus 1600 zwart
  - Materiaal Kunststof
  - Kleur Zwart/Chroom
  - Hoogte 450 mm
  - Breedte 250 mm
  - Diepte 115 mm
  - Gewicht 0,7 kg
  - Garantie 1 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Eigenschappen 2 standen luchtopbrengst, 2 standen temperatuur, koude luchtstand
  - Eigenschappen Wandhouder met aan/uit veiligheidsschakelaar
  - Wattage 1600 Watt
  - Toepassing Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
prod_img: prod_img/17148_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714739023
---
