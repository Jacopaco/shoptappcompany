---
id: '11011'
published: false
blueprint: product
art_nr: '11011'
title: 'Pedaalafvalbak vierkant 6 liter RVS hoogglans, PP1206C'
slug: pedaalafvalbak-vierkant-6-liter-rvs-hoogglans-pp1206c
cost_price: 3000
unit_price: 5000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 5000
vip_price: 4500
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
---
