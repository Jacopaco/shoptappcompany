---
id: nsu11-ps-mn
blueprint: products
art_nr: 'NSU11 P/S MN'
title: 'SanTRAL® Classic TOUCHLESS Midnight zeepdispenser'
slug: santral-classic-touchless-midnight-zeepdispenser
price: 17680
cost_price: 10608
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/nsu11-ps-mn.JPG
unit_price: 17680
vip_price: 15912
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
