---
id: '4074'
blueprint: products
art_nr: '4074'
title: 'HANDDOEKROL  NON WOVEN 40MTR SOFT'
slug: handdoekrol-non-woven-40mtr-soft
cost_price: 405
unit_price: 0
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
price: 631
vip_price: 526
prod_img: prod_img/4074.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715598383
categories: 211858e8-70c5-4a2e-a296-f0a9cd783d2f
client_prices:
  - 465917bf-fcad-40f2-8dae-0de9505b610e
  - 465917bf-fcad-40f2-8dae-0de9505b610e
description: |-
  Non-woven handdoekrollen 6x40 meter


  - Materiaal Non Woven
  - Kleur Wit
  - Breedte 200 mm
  - Diameter 133 mm (incl. koker)
  - Gewicht 0,6 kg
  - Eigenschappen ca. 40 meter
  - Verpakking 6 stuks
product_type: physical
---
