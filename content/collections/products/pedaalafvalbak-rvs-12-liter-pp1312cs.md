---
id: '11087'
blueprint: product
art_nr: '11087'
title: 'Pedaalafvalbak RVS 12 liter, PP1312CS'
slug: pedaalafvalbak-rvs-12-liter-pp1312cs
cost_price: 4338
unit_price: 7230
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 7230
vip_price: 6507
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1312cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845048
---
