---
id: '8265'
blueprint: products
art_nr: '8265'
title: 'Hygiënezakjesdispenser (papier) aluminium, MQHBPAA'
slug: hygienezakjesdispenser-papier-aluminium-mqhbpaa
price: 5080
cost_price: 3048
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënezakjesdispenser voor montage op een afvalbak of aan de wand.
  Uitname en navulling vanaf de bovenzijde.
  Met 2puntsbevestiging.
prod_img: prod_img/8265.JPG
unit_price: 5080
vip_price: 4572
---
