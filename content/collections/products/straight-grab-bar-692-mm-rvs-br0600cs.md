---
id: '12602'
blueprint: products
art_nr: '12602'
title: 'Straight grab bar 692 mm RVS, BR0600CS'
slug: straight-grab-bar-692-mm-rvs-br0600cs
price: 6430
cost_price: 3858
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar recht 455 mm.
  De stang wordt d.m.v. de 2 ronde uiteinden van Ø 81 mm op de wand gemonteerd.
unit_price: 6430
vip_price: 5787
prod_img: prod_img/12602.JPG
---
