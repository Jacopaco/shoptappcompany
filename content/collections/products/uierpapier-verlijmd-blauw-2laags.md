---
id: '2230'
blueprint: products
title: 'Uierpapier verlijmd blauw 2laags'
price: 2928
description: '22 cm | 2 x 360 meter in folie | ø 27,5 cm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2230_1_1.jpg'
art_nr: 2230
slug: uierpapier-verlijmd-blauw-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 1877
unit_price: 0
vip_price: 2440
prod_img: prod_img/2230_1_1.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654169
categories: bb31f7ae-4f77-4bd6-bb69-6b7c69019093
---
