---
id: 21415312-afp-c
blueprint: products
art_nr: '21415312 AFP-C.'
title: 'Foamzeepdispenser 1200 ml RVS afp-c, NSU 11 E/F ST'
slug: foamzeepdispenser-1200-ml-rvs-afp-c-nsu-11-ef-st
price: 18920
cost_price: 10217
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415312-afp-c..JPG
unit_price: 18920
vip_price: 17028
locations:
  - 11d7f953-43aa-43d8-a76d-a6c9c45f0621
---
