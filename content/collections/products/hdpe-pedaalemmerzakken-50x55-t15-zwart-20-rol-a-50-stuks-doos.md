---
id: b560505516
published: false
blueprint: products
art_nr: '150204000'
title: 'HDPE afvalzak 50x55cm T15 zwart (20x50) 1000 stuks'
slug: hdpe-afvalzak-50x55cm-t15-zwart-20x50-1000-stuks
cost_price: 1659
unit_price: 0
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
price: 3045
vip_price: 2538
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732786715
---
