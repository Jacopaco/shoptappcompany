---
id: '8468'
blueprint: products
art_nr: '8468'
title: 'Reserverolhouder 3rols aluminium, MQRRH3A'
slug: reserverolhouder-3rols-aluminium-mqrrh3a
price: 6220
cost_price: 3732
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Trio reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 4puntsbevestiging.
prod_img: prod_img/8468.JPG
unit_price: 6220
vip_price: 5598
---
