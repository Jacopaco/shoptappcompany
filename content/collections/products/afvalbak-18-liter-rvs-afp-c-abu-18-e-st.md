---
id: 2301053-afp-c
blueprint: products
art_nr: '2301053 AFP-C.'
title: 'Afvalbak 18 liter RVS afp-c, ABU 18 E ST'
slug: afvalbak-18-liter-rvs-afp-c-abu-18-e-st
price: 26580
cost_price: 15947
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak 18 liter gesloten.
  Met RVS push deksel en binnenring.
  Vrijstaand of voor wandmontage.
  Deksel is makkelijk afneembaar van de onderbak.
prod_img: prod_img/2301053-afp-c..JPG
unit_price: 26580
vip_price: 23922
---
