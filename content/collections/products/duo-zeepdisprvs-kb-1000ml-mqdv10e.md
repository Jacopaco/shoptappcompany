---
id: '8315'
blueprint: products
art_nr: '8315'
title: 'Duo zeepdisp.RVS KB 1000ml, MQDV10E'
slug: duo-zeepdisprvs-kb-1000ml-mqdv10e
price: 33135
cost_price: 19881
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/8315.JPG
unit_price: 33135
vip_price: 29821
---
