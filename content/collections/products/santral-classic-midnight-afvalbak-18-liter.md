---
id: ebu-18-p-mn
published: false
blueprint: products
art_nr: 'EBU 18 P MN'
title: 'SanTRAL® Classic Midnight afvalbak 18 liter'
slug: santral-classic-midnight-afvalbak-18-liter
price: 20010
cost_price: 12006
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/ebu-18-p-mn.JPG
unit_price: 20010
vip_price: 18009
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
