---
id: cart00919
published: false
blueprint: products
art_nr: CART00919
title: 'Chloortabletten 6x300st vervangen'
slug: chloortabletten-6x300st-vervangen
price: 23412
cost_price: 13740
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
unit_price: 19900
vip_price: 19510
prod_img: prod_img/cart00919.JPG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685542950
---
