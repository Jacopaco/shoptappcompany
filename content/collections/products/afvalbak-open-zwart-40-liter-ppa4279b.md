---
id: '11033'
blueprint: product
art_nr: '11033'
title: 'Afvalbak open zwart 40 liter, PPA4279B'
slug: afvalbak-open-zwart-40-liter-ppa4279b
cost_price: 10140
unit_price: 16900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16900
vip_price: 15210
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak zwart 40 liter open, PPA4279B
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/afbeelding-ppa4279b.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718806383
---
