---
id: '98813'
blueprint: products
art_nr: '98813'
title: 'Zeepdisp.kunstst. autom.1000ml, MQA10K'
slug: zeepdispkunstst-autom1000ml-mqa10k
price: 19750
cost_price: 11850
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser automatisch.
  Volledig afgesloten.
  Opvangschaal wordt separaat meegeleverd.
  Met slot en sleutel.
  Wordt excl. navulfles en batterijen geleverd.
  Pompen dienen elke 2 maanden gewisseld te worden.
prod_img: prod_img/98813.JPG
unit_price: 19750
vip_price: 17775
---
