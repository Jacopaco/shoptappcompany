---
id: '2194'
blueprint: products
art_nr: '2194'
title: 'STARLIGHT 2194 poetspapier 6x250 meter 1 laags'
slug: poetsrol-midi-blauw-6x-300-meter-1-lgs
price: 2917
cost_price: 1870
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
description: |-
  Poetsrol midi blauw recycled 1 laags
  19 cm | 6 x ca. 250 meter (1,5 kg) folie | ø 20 cm
prod_img: prod_img/2194.jpg
unit_price: 2098
vip_price: 2431
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729157610
---
