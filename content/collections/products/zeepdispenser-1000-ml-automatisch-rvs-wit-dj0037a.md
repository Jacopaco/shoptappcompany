---
id: '14040'
blueprint: product
art_nr: '14040'
title: 'Zeepdispenser 1000 ml automatisch RVS wit, DJ0037A'
slug: zeepdispenser-1000-ml-automatisch-rvs-wit-dj0037a
cost_price: 9996
unit_price: 16660
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16660
vip_price: 14994
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  U houdt van hygiëne en gemak? Lees dan snel verder. 

  De zeepdispenser van Mediclinics is hygiënisch én gemakkelijk. Dankzij de infra-rood sensor is aanraking met de dispenser niet nodig en komt de juiste doseerhoeveelheid uit de dispenser. Het kijkvenster op de dispenser zorgt ervoor dat de inhoud gemakkelijk gecontroleerd kan worden. Mocht de vulling op zijn, dan kan de navulfles verwijdert worden. Alles kan goed worden schoon gehouden. Het is ook mogelijk de vulling van bovenaf te vullen. Deze dispenser is geschikt voor wandmontage en werkt op batterijen. Een automatische zeepdispenser creëert naast hygiëne en gemak ook een luxebeeld. 

  Wilt u uw sanitaire ruimte een luxe en hygiënische uitstraling geven? Vraag dan een offerte aan voor de automatische zeepdispenser van Mediclinics of bestel het hier.
prod_img: prod_img/afbeelding-dj0037a-1719324205.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719324212
---
