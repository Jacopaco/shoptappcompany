---
id: '14257'
published: false
blueprint: products
title: 'Geurpotje Herb Essence'
price: 600
description: |-
  De Grapefruit geur is gebaseerd op essentiële oliën afkomstig van vruchten en bloemen.
  Bevatten in tegenstelling tot andere geuren geen drijfgassen en synthetische geuren,
  wat bijdraagt aan een milieuvriendelijker wereld. Met inkeping aan de achterzijde
  Gaat ca. 30-60 dagen mee.
  Afname: per doos = 10 stuks
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712057459
art_nr: '14257'
slug: geurpotje-herb-essence
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
cost_price: 600
unit_price: 600
vip_price: 600
prod_img: prod_img/14257.JPG
product_type: physical
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
