---
id: '2160'
published: false
blueprint: products
title: 'STARLIGHT 2160-FSC Matic handdoekrollen 6x150 meter 2 laags'
price: 5330
description: |-
  Handdoekrol cellulose 2 laags 21 cm x 150 meter

  Code 2160-FSC
  Materiaal cellulose
  Kleur wit
  Afmetingen 21 cm
  2 laags
  ø 175 mm
  Verpakking doos met 6 rollen

  FSC certificaat
  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2160_3_2.jpg'
art_nr: 2160
slug: handdoekrol-matic-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 3417
unit_price: 0
vip_price: 4442
prod_img: prod_img/2160_3_2.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729750767
locations:
  - a618e884-3fb2-4394-9a3f-effd139ef796
categories: 211858e8-70c5-4a2e-a296-f0a9cd783d2f
product_type: physical
---
