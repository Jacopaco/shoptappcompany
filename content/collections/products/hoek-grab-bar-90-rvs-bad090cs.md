---
id: '12625'
blueprint: products
art_nr: '12625'
title: 'Hoek grab bar 90° RVS, BAD090CS'
slug: hoek-grab-bar-90-rvs-bad090cs
price: 15800
cost_price: 9480
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar met 90˚ hoek naar rechts.
  Verticale stang lengte: 375 mm.
  Horizontale stang lengte: 700 mm
prod_img: prod_img/12625.JPG
unit_price: 15800
vip_price: 14220
---
