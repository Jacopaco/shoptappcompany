---
id: '193501'
blueprint: products
art_nr: '193501'
title: 'Raamtrekker prof. Koper (compleet) 35cm'
slug: raamtrekker-prof-koper-compleet-35cm
price: 14.43
cost_price: 9.25
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/193501.JPG
unit_price: 9.25
vip_price: 12.025
---
