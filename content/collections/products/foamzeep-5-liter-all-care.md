---
id: '98916'
blueprint: products
art_nr: '98916'
title: 'ALL-CARE foamzeep 1x5 liter'
slug: foamzeep-5-liter-all-care
price: 1193
cost_price: 765
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: de901aec-80dd-40c6-be90-f2c5d8a24a1e
description: |-
  Deze foamzeep 5 liter is transparant van kleur en heeft een neutrale geur.

  Het product heeft een neutrale pH waarde, vergelijkbaar met de huid.
  Deze foamzeep is uitermate geschikt voor kantoren, openbare gebouwen,
  restaurants etc.
prod_img: prod_img/98916.JPG
unit_price: 0
vip_price: 994
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729239420
---
