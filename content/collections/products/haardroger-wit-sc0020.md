---
id: '12013'
blueprint: product
art_nr: '12013'
title: 'Haardroger wit, SC0020'
slug: haardroger-wit-sc0020
cost_price: 3515
unit_price: 6390
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6390
vip_price: 5751
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/12013-sc0020.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479440
---
