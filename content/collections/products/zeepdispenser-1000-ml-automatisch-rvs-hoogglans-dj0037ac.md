---
id: '14042'
blueprint: product
art_nr: '14042'
title: 'Zeepdispenser 1000 ml automatisch RVS hoogglans, DJ0037AC'
slug: zeepdispenser-1000-ml-automatisch-rvs-hoogglans-dj0037ac
cost_price: 9510
unit_price: 15850
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 15850
vip_price: 14265
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Mediclinics zeepdispenser automatisch RVS gepolijst 1000 ml, DJ0037AC

  De zeepdispenser van Mediclinics is hygiënisch én gemakkelijk. Dankzij de infra-rood sensor is aanraking met de dispenser niet nodig en komt de juiste doseerhoeveelheid uit de dispenser. Het kijkvenster aan de voorzijde van de dispenser zorgt ervoor dat de inhoud gemakkelijk gecontroleerd kan worden. Mocht de vulling op zijn, dan kan de navulfles eenvoudig verwijdert worden. Alles kan goed worden schoon gehouden. Deze dispenser is geschikt voor wandmontage en werkt op batterijen.

  Zeepdispenser automatisch voor wandmontage.
  Met infra-rood sensor voor het doseren van de vulgoederen.
  Met veerslot en Mediclinics sleutel.
  Met kijkgleuf voor inhoudscontrole.
  Met verwijderbare navulfles.
  Werkt op 6 AA batterijen. (niet meegeleverd)
  Aanbevolen installatiehoogte: 25-30 cm boven de oppervlakte.
  Specificaties
  Artikelnummer 14042
  Model DJ0037AC
  Materiaal RVS
  Kleur Gepolijst
  Hoogte 240 mm
  Breedte 110 mm
  Diepte 120 mm
  Gewicht 1,25 kg
  Garantie 2 jaar na aankoopdatum
  Toepassing Wandmontage
  Toepassing Navulbaar
  Vulling Vloeibare zeep
  Dosering 1,5 ml per slag
  Inhoud 1000 ml
prod_img: prod_img/14042_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715073636
---
