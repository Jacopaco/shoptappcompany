---
id: '12600'
blueprint: products
art_nr: '12600'
title: 'Straight grab bar 387 mm RVS, BR0300CS'
slug: straight-grab-bar-387-mm-rvs-br0300cs
price: 5150
cost_price: 3090
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar recht 287mm.
  De stang wordt d.m.v. de 2 ronde uiteinden van Ø 82 mm op de wand gemonteerd.
unit_price: 5150
vip_price: 4635
prod_img: prod_img/12600.JPG
---
