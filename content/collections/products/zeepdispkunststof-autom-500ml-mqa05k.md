---
id: '98812'
blueprint: products
art_nr: '98812'
title: 'Zeepdisp.kunststof autom. 500ml, MQA05K'
slug: zeepdispkunststof-autom-500ml-mqa05k
price: 17850
cost_price: 10710
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser automatisch.
  Volledig afgesloten.
  Opvangschaal wordt separaat meegeleverd.
  Met slot en sleutel.
  Wordt excl. navulfles en batterijen geleverd.
  Pompen dienen elke 2 maanden gewisseld te worden.
prod_img: prod_img/98812.JPG
unit_price: 17850
vip_price: 16065
---
