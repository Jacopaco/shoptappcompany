---
id: '8045'
blueprint: products
art_nr: '8045'
title: 'Zeepdisp. RVS UP 1000ml KB, MQ10E'
slug: zeepdisp-rvs-up-1000ml-kb-mq10e
price: 13460
cost_price: 8076
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig afgekit kan worden.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8045.JPG
unit_price: 13460
vip_price: 12114
---
