---
id: '2150'
published: false
blueprint: products
title: '2150 Handdoekjes Z-vouw cellulose 2laags'
price: 39
description: '22 x 25 cm | 15 x 215 vellen in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2145-3_1.jpg'
art_nr: 2150
slug: handdoekjes-z-vouw-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
cost_price: 25.57
prod_img: prod_img/2145-3_1-1688653584.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728992773
client_prices:
  - 20d13338-c49a-4eaf-a6c0-8f6653f76af1
  - 20d13338-c49a-4eaf-a6c0-8f6653f76af1
vip_price: 33
product_type: physical
---
