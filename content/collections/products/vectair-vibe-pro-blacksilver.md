---
id: '8331301'
blueprint: products
art_nr: '8331301'
title: 'VECTAIR VIBE PRO Black/Silver'
slug: vectair-vibe-pro-blacksilver
cost_price: 8441
unit_price: 11394
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
price: 11394
vip_price: 11394
prod_img: prod_img/8331301.JPG
---
