---
id: '54017'
blueprint: products
art_nr: '54017'
title: 'Luchtverfrisser navulling Mango'
slug: luchtverfrisser-navulling-mango
price: 450
cost_price: 270
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
description: |-
  Mango navulling.
  Met ribbels voor extra geurbeleving.
  Geen belasting voor het milieu (PP)
  Gaat ca. 30 dagen mee.
  Afname: per doos = 20 stuks
prod_img: prod_img/54017.JPG
unit_price: 450
vip_price: 450
categories: f4cb9934-b741-4f26-bf31-378b213ae616
---
