---
id: kx125t-0083-c1
published: false
blueprint: products
art_nr: KX125T-0083-C1
title: 'KX Dispenser 1000ml Touchless'
slug: kx-dispenser-1000ml-touchless
price: 7995
cost_price: 3996
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
prod_img: prod_img/kx125t-0083-c1.png
unit_price: 7995
vip_price: 7995
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
