---
id: '2188'
blueprint: products
title: 'Poetsrol midi coreless cellulose 2laags'
price: 2340
description: '20 cm | 6 x 160 meter in folie'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2188_4_3.jpg'
art_nr: 2188
slug: poetsrol-midi-coreless-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1500
unit_price: 2049
vip_price: 1950
prod_img: prod_img/2188_4_3-1688654060.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654067
client_prices:
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - 5d51ad93-e2e6-43c1-a90f-49d97915b08f
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
locations:
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 175073cd-07d9-40df-9e81-927b690b393b
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - f2f903e1-4856-4d9b-86e4-16f4e1618fa8
---
