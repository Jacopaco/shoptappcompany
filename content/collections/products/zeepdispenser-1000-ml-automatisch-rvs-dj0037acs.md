---
id: '14043'
blueprint: product
art_nr: '14043'
title: 'Zeepdispenser 1000 ml automatisch RVS, DJ0037ACS'
slug: zeepdispenser-1000-ml-automatisch-rvs-dj0037acs
cost_price: 9474
unit_price: 15790
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 15790
vip_price: 14211
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Deze automatische zeepdispenser geeft dankzij het RVS materiaal een degelijke en stoere uitstraling van het sanitair. Doordat de zeepdispenser automatisch is, dankzij een infra-rood sensor, creëert deze dispenser ook een luxe uitstraling. Uiteraard is hygiëne van groot belang. Met deze dispenser zit u op het gebied van hygiëne helemaal goed, aanraking met de dispenser is niet nodig dankzij de sensor. Telkens wordt de juiste hoeveelheid gedoseerd en via het kijkvenster aan de voorzijde kunt u de vulling gemakkelijk controleren. 

  Zeepdispenser automatisch voor wandmontage.
  Met infra-rood sensor voor het doseren van de vulgoederen.
  Met veerslot en Mediclinics sleutel.
  Met kijkgleuf voor inhoudscontrole.
  Met verwijderbare navulfles.
  Ook eenvoudig vanaf bovenaf na te vullen.
  Werkt op 6 AA batterijen.
  Aanbevolen installatiehoogte: 25-30 cm boven de oppervlakte.

  - Artikelnummer	14043
  - Materiaal	RVS
  - Hoogte	240 mm
  - Breedte	110 mm
  - Diepte	120 mm
  - Gewicht	1,6 kg
  - Garantie	2 jaar na aankoopdatum
  - Toepassing	Wandmontage
  - Toepassing	Navulbaar
  - Vulling	Vloeibare zeep, lotions en crèmes
  - Dosering	1,5 ml per slag
  - Inhoud	1000 ml
prod_img: prod_img/14043_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715072566
---
