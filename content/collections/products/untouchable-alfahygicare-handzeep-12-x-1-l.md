---
id: '98917'
blueprint: products
title: 'TAPP SOAP EXELLENT 1000ML Untouchable OS'
price: 478
description: 'Open systeem'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/handalcohol_1000ml_Tapp_Company.JPG'
art_nr: 98917
slug: tapp-soap-exellent-1000ml-untouchable-os
brands: 841597b5-0184-4fd2-98a0-6535c97b8673
categories: de901aec-80dd-40c6-be90-f2c5d8a24a1e
cost_price: 189
prod_img: prod_img/98917.JPG
unit_price: 478
vip_price: 478
client_prices:
  - d8b397ec-2533-4b40-b16e-95ee5348e0cc
  - ff9c79b6-4366-49f2-8e91-3d5b0100bc47
  - d8b397ec-2533-4b40-b16e-95ee5348e0cc
locations:
  - c8eeae72-6c18-4ef1-8628-5087adf3afe9
  - 225a4a10-f583-4663-aa73-07579bc9b443
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
---
