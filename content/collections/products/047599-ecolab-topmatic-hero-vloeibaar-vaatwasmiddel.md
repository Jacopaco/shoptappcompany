---
id: '047599'
blueprint: products
title: 'Ecolab Topmatic Hero'
price: 16949
description: '047599 Ecolab Topmatic Hero vloeibaar vaatwasmiddel chloorhoudend Can 12kg'
img:
  - product_images/ecolab-topmatic-hero-9013960.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687524273
art_nr: '047599'
slug: ecolab-topmatic-hero
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 9947
unit_price: 10941
vip_price: 14124
prod_img: prod_img/047599.jpg
---
