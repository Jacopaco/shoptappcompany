---
id: '98817'
blueprint: products
art_nr: '98817'
title: 'Zeepdisp.kunststof 500ml LB , MQL05K'
slug: zeepdispkunststof-500ml-lb-mql05k
price: 5300
cost_price: 3180
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Volledig afgesloten.
  Met kunststof pomp, die regelmatig vervangen dient te worden.
  Opvangschaal wordt separaat meegeleverd.
  Wordt excl. navulfles geleverd
prod_img: prod_img/98817.JPG
unit_price: 5300
vip_price: 4770
---
