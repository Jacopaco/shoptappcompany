---
id: '5653'
blueprint: products
art_nr: '5653'
title: 'Hygiënebak 23 liter wit, PQH23'
slug: hygienebak-23-liter-wit-pqh23
price: 5330
cost_price: 3358
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiënebak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met gesloten inworpklep.
  In de deksel is er ruimte voor een geurblokje.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
prod_img: prod_img/5653.JPG
unit_price: 5330
vip_price: 4797
---
