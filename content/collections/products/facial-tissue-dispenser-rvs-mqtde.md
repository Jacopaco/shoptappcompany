---
id: '8360'
blueprint: products
art_nr: '8360'
title: 'Facial tissue dispenser RVS, MQTDE'
slug: facial-tissue-dispenser-rvs-mqtde
price: 9080
cost_price: 5448
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Tissuebox voor wandmontage.
  Met slot en sleutel.
  Met 4puntsbevestiging.
  Met ovale opening voor optimale uitname van de tissues.
prod_img: prod_img/8360.JPG
unit_price: 9080
vip_price: 8172
---
