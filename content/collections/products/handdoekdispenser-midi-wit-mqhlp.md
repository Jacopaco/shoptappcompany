---
id: '8180'
blueprint: products
art_nr: '8180'
title: 'Handdoekdispenser midi wit, MQHLP'
slug: handdoekdispenser-midi-wit-mqhlp
price: 10790
cost_price: 6473
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser groot.
  Met zichtvenster voor inhoudscontrole.
  Afsluitbaar, slot zit aan de voorzijde.
  Cover gaat aan de voorzijde open.  
  Met 4punts bevestiging
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/8180.JPG
unit_price: 10790
vip_price: 9711
---
