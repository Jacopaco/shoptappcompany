---
id: '331413894'
blueprint: products
art_nr: '331413894'
title: 'Afvalbak open 25 liter RVS afp-c, OBU 25 E ST'
slug: afvalbak-open-25-liter-rvs-afp-c-obu-25-e-st
price: 21710
cost_price: 13026
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/331413894.JPG
unit_price: 21710
vip_price: 19539
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1697118753
---
