---
id: '080553'
blueprint: products
art_nr: '080553'
title: 'Ecolab MAXX Windus C2'
slug: ecolab-maxx-windus-c2
cost_price: 618
unit_price: 678
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 1053
vip_price: 877
prod_img: prod_img/080553.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687525999
---
