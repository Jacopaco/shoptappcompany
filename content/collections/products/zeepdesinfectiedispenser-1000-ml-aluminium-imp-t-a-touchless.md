---
id: '21415524'
blueprint: products
art_nr: '21415524'
title: 'Zeep/Desinfectiedispenser 1000 ml aluminium, IMP T A TOUCHLESS'
slug: zeepdesinfectiedispenser-1000-ml-aluminium-imp-t-a-touchless
price: 25190
cost_price: 16122
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/21415524.JPG
unit_price: 25190
vip_price: 22671
---
