---
id: '11082'
blueprint: product
art_nr: '11082'
title: 'Pedaalafvalbak wit 5 liter, PP1305'
slug: pedaalafvalbak-wit-5-liter-pp1305
cost_price: 2814
unit_price: 4690
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 4690
vip_price: 4221
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1305-1719845297.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845305
---
