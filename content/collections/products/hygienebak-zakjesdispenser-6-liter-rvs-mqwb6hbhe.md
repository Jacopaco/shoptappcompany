---
id: '8250'
blueprint: products
art_nr: '8250'
title: 'Hygiënebak + zakjesdispenser 6 liter RVS, MQWB6HBHE'
slug: hygienebak-zakjesdispenser-6-liter-rvs-mqwb6hbhe
price: 19300
cost_price: 11580
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënebak met klepdeksel en gemonteerde zakjeshouder.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8250.JPG
unit_price: 19300
vip_price: 17370
---
