---
id: 2301047-afp-c
blueprint: products
art_nr: '2301047 AFP-C'
title: 'Toiletroldispenser 2rols RVS afp-c, TRU 2-2 E ST'
slug: toiletroldispenser-2rols-rvs-afp-c-tru-2-2-e-st
price: 22200
cost_price: 13319
brands: f77a48e2-462d-4719-8961-2637e36823d5
description: |-
  2-rolshouder voor wandmontage. 
  Met automatische toiletrolwisseling.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2301047-afp-c.JPG
unit_price: 22200
vip_price: 19980
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
