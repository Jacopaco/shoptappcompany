---
id: '21423145'
blueprint: products
art_nr: '21423145'
title: 'Handdoek/Spraydispenser RVS zwart, CPU 2L P/D MN'
slug: handdoekspraydispenser-rvs-zwart-cpu-2l-pd-mn
cost_price: 13024
unit_price: 27300
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 27300
vip_price: 24570
prod_img: prod_img/21423145.png
---
