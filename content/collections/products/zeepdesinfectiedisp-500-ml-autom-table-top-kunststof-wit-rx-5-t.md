---
id: '24401117'
blueprint: products
art_nr: '24401117'
title: 'Zeep/desinfectiedisp. 500 ml autom. table-top kunststof wit, RX 5 T'
slug: zeepdesinfectiedisp-500-ml-autom-table-top-kunststof-wit-rx-5-t
price: 13098
cost_price: 7528
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/24401117.jpg
unit_price: 11450
vip_price: 10915
---
