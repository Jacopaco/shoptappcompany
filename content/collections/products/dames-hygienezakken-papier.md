---
id: '2900212'
blueprint: products
title: 'Hygiënezakjes papier 1000 stuks, HYGB'
price: 5094
description: |-
  Papieren hygiënezakken.
  Afname: per doos = 1000 zakken
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515502
categories: 0e559e3a-a5c9-444d-b968-d93cc759839f
art_nr: '2900212'
slug: hygienezakjes-papier-1000-stuks-hygb
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 2775
unit_price: 5250
vip_price: 4245
prod_img: prod_img/2900212.JPG
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
---
