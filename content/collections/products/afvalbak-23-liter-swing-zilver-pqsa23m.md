---
id: '5675'
blueprint: products
art_nr: '5675'
title: 'Afvalbak 23 liter swing zilver, PQSA23M'
slug: afvalbak-23-liter-swing-zilver-pqsa23m
price: 7900
cost_price: 4977
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met swing deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling. 
prod_img: prod_img/5675.JPG
unit_price: 7900
vip_price: 7110
---
