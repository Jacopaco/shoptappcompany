---
id: '98961'
published: false
blueprint: products
art_nr: '98961'
title: 'CleanPro vaatwasmiddel Chloorvrij 10L'
slug: cleanpro-vaatwasmiddel-chloorvrij-10l
price: 3201
cost_price: 2052
brands: 3375fa99-d12c-4998-8110-4d347cec808b
prod_img: prod_img/98961.
unit_price: 3200
vip_price: 2667
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685627392
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
