---
id: '2922'
blueprint: products
title: 'Toiletpapier Compact natuur wit eco 2lgs'
price: 2414
description: '24 x 100 meter in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2922-2_1.jpg'
art_nr: 2922
slug: toiletpapier-compact-natuur-wit-eco-2lgs
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 1548
unit_price: 0
vip_price: 2012
prod_img: prod_img/2922-2_1.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654223
client_prices:
  - d66ba4d9-ef23-498b-bf5d-e73d3161faf3
  - d66ba4d9-ef23-498b-bf5d-e73d3161faf3
locations:
  - fd034e41-e357-4acc-8957-4bb9ed02d77d
---
