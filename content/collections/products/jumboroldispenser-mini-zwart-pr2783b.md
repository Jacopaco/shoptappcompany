---
id: '13715'
blueprint: product
art_nr: '13715'
title: 'Jumboroldispenser mini zwart, PR2783B'
slug: jumboroldispenser-mini-zwart-pr2783b
cost_price: 3546
unit_price: 5910
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 5910
vip_price: 5319
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  De keuze voor deze mini jumboroldispenser getuigt van een flinke portie lef. De zwart gepoedercoate behuizing springt direct in het oog van de toiletbezoeker. Combineer dat met een heerlijke voorraad toiletpapier, en u biedt uw bezoekers een toiletbezoek om nooit meer te vergeten.

  Het design mag dan opvallend zijn, maar ook de praktische kant is door de ontwerpers zeker niet vergeten; de mini jumboroldispenser biedt namelijk ruimte aan een flinke voorraad toiletpapier, en is bovendien eenvoudig te gebruiken. De voorraad controleert u snel via de handige kijkgleuf aan de voorzijde van de dispenser.

  Kiest u voor een echte eyecatcher?
prod_img: prod_img/afbeelding-pr2783b-1719320784.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320788
---
