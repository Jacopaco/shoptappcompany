---
id: '839160104'
blueprint: product
art_nr: '839160104'
title: 'Vileda MicronSolo Roll wit blauw 180 vel per 4 rollen'
slug: vileda-micronsolo-roll-wit-blauw-180-vel-per-4-rollen
brands: 3375fa99-d12c-4998-8110-4d347cec808b
cost_price: 7392
unit_price: 8034
price: 11531
vip_price: 9609
locations:
  - 31ab6798-7e55-4a96-8c41-c9d35bf02694
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
client_prices:
  - '839160104'
  - bebc5c46-0553-4d62-b6ff-de33648493c4
prod_img: prod_img/839160104.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1724830826
---
