---
id: hbu-6-p-mn
published: false
blueprint: products
art_nr: 'HBU 6 P MN'
title: 'SanTRAL® Classic Midnight hygiëne afvalbak 6 liter'
slug: santral-classic-midnight-hygiene-afvalbak-6-liter
price: 26220
cost_price: 15732
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/hbu-6-p-mn.JPG
unit_price: 26220
vip_price: 23598
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
