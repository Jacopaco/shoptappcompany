---
id: '21423265'
blueprint: products
art_nr: '21423265'
title: 'Handschoendispenser TRIO RVS, 3BF'
slug: handschoendispenser-trio-rvs-3bf
cost_price: 3842
unit_price: 7309
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
price: 7309
vip_price: 6578
prod_img: prod_img/21423265.png
description: |-
  Handschoendispenser voor 3 dozen handschoenen.
  Met veer voor het optimaal op de plaats houden van
  verschillende maten doosjes.
  Afmeting dispenser: H 510 x B 254 x D 110 mm.
---
