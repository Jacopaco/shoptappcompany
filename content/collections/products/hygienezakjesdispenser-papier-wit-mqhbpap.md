---
id: '8270'
blueprint: products
art_nr: '8270'
title: 'Hygiënezakjesdispenser (papier) wit, MQHBPAP'
slug: hygienezakjesdispenser-papier-wit-mqhbpap
price: 5715
cost_price: 3429
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënezakjesdispenser voor montage op een afvalbak of aan de wand.
  Uitname en navulling vanaf de bovenzijde.
  Met 2puntsbevestiging.
prod_img: prod_img/8270.JPG
unit_price: 5715
vip_price: 5143
---
