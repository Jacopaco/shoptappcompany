---
id: 21423919-afp-c
blueprint: products
art_nr: '21423919 AFP-C'
title: 'Handdoekdispenser maxi RVS afp-c, PTU 45 E ST'
slug: handdoekdispenser-maxi-rvs-afp-c-ptu-45-e-st
cost_price: 9135
unit_price: 18630
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 18630
vip_price: 16767
prod_img: prod_img/21423919-afp-c.png
---
