---
id: '13111'
blueprint: product
art_nr: '13111'
title: 'Toiletroldispenser 2rols wit, PR2784'
slug: toiletroldispenser-2rols-wit-pr2784
cost_price: 5385
unit_price: 8975
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8975
vip_price: 8077
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Uw facilitaire dienst houdt de voorraad toiletrollen natuurlijk goed in de gaten. Toch wilt u niet het risico lopen dat uw bezoeker zonder die noodzakelijke rol komt te zitten. Met de handige 2-rolshouder van Mediclinics is uw gast altijd verzekerd van voldoende toiletpapier binnen handbereik.

  Deze 2-rolshouder is het summum van een flink staaltje strak design. Het wit gepoedercoate frame valt prachtig weg tegen elke witte wand. Daarnaast is de vorm bewust subtiel gehouden, zodat het ontwerp volledig opgaat in de omgeving. De houder biedt ruimte aan twee toiletrollen, zodat u altijd een nieuwe rol voorhanden heeft.
prod_img: prod_img/mediclinics_pr2784.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719476880
---
