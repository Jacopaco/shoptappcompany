---
id: '5530'
blueprint: products
art_nr: '5530'
title: 'Jumboroldispenser mini kunststof wit, PQMiniJ'
slug: jumboroldispenser-mini-kunststof-wit-pqminij
price: 2300
cost_price: 1449
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Jumboroldispenser mini voor wandmontage.
  Met kartelrand aan de voorzijde en zijkanten voor optimale afscheuring van de rol.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
prod_img: prod_img/5530.jpg
unit_price: 2300
vip_price: 2070
---
