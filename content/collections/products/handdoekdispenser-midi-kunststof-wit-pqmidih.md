---
id: '5542'
blueprint: products
art_nr: '5542'
title: 'Handdoekdispenser midi kunststof wit, PQMidiH'
slug: handdoekdispenser-midi-kunststof-wit-pqmidih
price: 3000
cost_price: 1889
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser voor wandmontage.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Geschikt voor: C- en Z gevouwen papieren handdoekjes met max. 360 mm stapelhoogte
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/5542.JPG
unit_price: 3000
vip_price: 2700
---
