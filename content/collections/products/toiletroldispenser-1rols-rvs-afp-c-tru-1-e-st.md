---
id: 21411586-afp-c
blueprint: products
art_nr: '21411586 AFP-C.'
title: 'Toiletroldispenser 1rols RVS afp-c, TRU 1 E ST'
slug: toiletroldispenser-1rols-rvs-afp-c-tru-1-e-st
price: 6480
cost_price: 3542
brands: f77a48e2-462d-4719-8961-2637e36823d5
description: |-
  1-rolshouder voor wandmontage.
  Met RVS beugel.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21411586-afp-c..JPG
unit_price: 6480
vip_price: 5832
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
