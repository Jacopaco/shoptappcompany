---
id: '8175'
blueprint: products
art_nr: '8175'
title: 'Handdoekdispenser midi aluminium, MQHLA'
slug: handdoekdispenser-midi-aluminium-mqhla
price: 9840
cost_price: 5904
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser groot.
  Met zichtvenster voor inhoudscontrole.
  Afsluitbaar, slot zit aan de voorzijde.
  Cover gaat aan de voorzijde open.
  Met 4punts bevestiging.
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/8175.JPG
unit_price: 9840
vip_price: 8856
---
