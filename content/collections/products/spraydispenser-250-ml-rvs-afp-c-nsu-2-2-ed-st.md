---
id: 21415829-afp-c
blueprint: products
art_nr: '21415829 AFP-C.'
title: 'Spraydispenser 250 ml RVS afp-c, NSU 2-2 E/D ST'
slug: spraydispenser-250-ml-rvs-afp-c-nsu-2-2-ed-st
price: 16970
cost_price: 9164
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Spraydispenser voor wandmontage.
  Met navulbaar reservoir.
  Met verborgen speciaal slot.
  Kan ook als toilet seat cleaner gebruikt worden.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415829-afp-c..JPG
unit_price: 16970
vip_price: 15273
---
