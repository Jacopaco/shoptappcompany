---
id: '5534'
blueprint: products
art_nr: '5534'
title: 'Jumboroldispenser mini + restrol kunststof wit, PQMiniSRJ'
slug: jumboroldispenser-mini-restrol-kunststof-wit-pqminisrj
price: 2590
cost_price: 1632
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Jumboroldispenser mini + restrol voor wandmontage.
  Met kartelrand aan de onderzijde voor optimale afscheuring van de rol.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
prod_img: prod_img/5534.JPG
unit_price: 2590
vip_price: 2331
---
