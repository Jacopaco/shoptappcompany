---
id: '12016'
blueprint: product
art_nr: '12016'
title: 'Haardroger shaver zwart/zilver, SC0030CS'
slug: haardroger-shaver-zwartzilver-sc0030cs
cost_price: 5192
unit_price: 9440
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9440
vip_price: 8496
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/mediclinics-sc0030cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479300
---
