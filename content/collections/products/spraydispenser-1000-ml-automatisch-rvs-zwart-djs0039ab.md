---
id: '14071'
blueprint: product
art_nr: '14071'
title: 'Spraydispenser 1000 ml automatisch RVS zwart, DJS0039AB'
slug: spraydispenser-1000-ml-automatisch-rvs-zwart-djs0039ab
cost_price: 10338
unit_price: 17230
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 17230
vip_price: 15507
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  MEDICLINICS DJS0039AB Sensor desinfectie spray dispenser 1000 ml in roestvrij staal AISI 304

  - Automatische spray dispenser met een capaciteit van 1 liter, vervaardigd in roestvrij staal AISI 304, 0,8 mm dik

  - Verkrijgbaar in vier afwerkingen wit, zwart, RVS glans en RVS mat

  - Geschikt voor drukke voorzieningen en voor openbaar gebruik

  - Niveauweergave in het voorste gedeelte van de dispenser. Het geeft constant het niveau van de beschikbare vloeistof in de dispenser aan.

  - Werkt op 6 AA alkalinebatterijen (niet meegeleverd) of optioneel met een AC-adapter. Bestel referentie DJS0039A/B/C/CS-TRAFO om de automatische zeepdispenser te ontvangen die met de AC-adapter op het lichtnet kan worden aangesloten.

  - Maximaal alcoholpercentage van 85%

  - Schroeven en plastic pluggen voor montage op een bakstenen muur worden meegeleverd.
prod_img: prod_img/14071_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715070573
---
