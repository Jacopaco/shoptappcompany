---
id: 2301043-afp-c
blueprint: products
art_nr: '2301043 AFP-C.'
title: 'Afvalbak 50 liter RVS afp-c, ABU 50 E ST'
slug: afvalbak-50-liter-rvs-afp-c-abu-50-e-st
price: 35520
cost_price: 21312
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak 50 liter gesloten.
  Met RVS push deksel en binnenring.
  Vrijstaand of voor wandmontage.
  Deksel is makkelijk afneembaar van de onderbak.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2301043-afp-c..JPG
unit_price: 35520
vip_price: 31968
---
