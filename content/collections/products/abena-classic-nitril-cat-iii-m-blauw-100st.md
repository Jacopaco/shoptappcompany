---
id: cart00637
published: false
blueprint: products
art_nr: CART00637
title: 'Abena Classic Nitril CAT III M blauw 100 stuks wegwerphandschoenen'
slug: abena-classic-nitril-cat-iii-m-blauw-100-stuks-wegwerphandschoenen
price: 349
cost_price: 222
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
prod_img: prod_img/cart00637.JPG
unit_price: 349
vip_price: 349
locations:
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
  - 78839325-c105-4e00-88cd-9b808c86a969
  - 851491c7-811a-41ec-a665-8715a2ec61a9
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
  - ad521bf5-2669-452d-81b0-0b55218e7ac4
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
  - 356d30dd-aa1b-4538-a106-2d9d0b09c317
  - 5ed3871c-f423-4dc3-9755-7ee2d3a676bc
  - 7d0c81d5-f6d6-4305-ba84-89f822c63ab7
  - 82359df9-6fb1-4321-8adf-db6f37f28079
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
client_prices:
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732788596
---
