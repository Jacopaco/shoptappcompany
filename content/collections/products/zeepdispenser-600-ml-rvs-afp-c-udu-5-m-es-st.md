---
id: '331411929'
blueprint: products
art_nr: '331411929'
title: 'Zeepdispenser 600 ml RVS afp-c, UDU 5 M E/S ST'
slug: zeepdispenser-600-ml-rvs-afp-c-udu-5-m-es-st
price: 16660
cost_price: 9996
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
prod_img: prod_img/331411929.JPG
unit_price: 16660
vip_price: 14994
---
