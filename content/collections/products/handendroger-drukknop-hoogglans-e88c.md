---
id: '12200'
blueprint: product
art_nr: '12200'
title: 'Handendroger drukknop hoogglans, E88C'
slug: handendroger-drukknop-hoogglans-e88c
cost_price: 38225
unit_price: 69500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 69500
vip_price: 62550
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Productinformatie Mediclinics E88C,SANIFLOW handdroger met drukknop
  Mediclinics Saniflow

  Een klassiek, robuust en betrouwbaar ontwerp voor de 21e eeuw.
  Motor met een hoge snelheid en een lange levensduur.
  Een constante luchtstroom voor het volledig drogen van de handen.
  Robuuste, betrouwbare handdroger van hoge kwaliteit.
  Een van de krachtigste luchtstromen op de markt, binnen zijn klasse.
  Het verwarmingselement en de motor hebben ingebouwde veiligheid thermische afsluiting.
  Verchroomde 360° roterende nozzle.
  Vandaal bestendig.
  Eenvoudige reiniging en onderhoud.
  Specificaties
  Artikelnummer 12200
  Model E88C
  Materiaal Staal
  Kleur Hoogglans verchroomd
  Hoogte 248 mm
  Breedte 278 mm
  Diepte 212 mm
  Gewicht 5,9 kg
  Garantie 2 jaar na aankoopdatum
  Toepassing Wandmontage
  Eigenschappen Drukknop (35 sec.), 100 km/u, droogtijd: 29 seconden, 68 dB
  Wattage 2250 Watt
prod_img: prod_img/mediclinics-e88c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478750
---
