---
id: '778900'
blueprint: products
art_nr: '778900'
title: 'ingo-man HS 31 A paper towel dispenser'
slug: ingo-man-hs-31-a-paper-towel-dispenser
price: 7993
cost_price: 4594
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: 1b689325-711f-439f-9fff-b59122f29d67
prod_img: prod_img/778900.JPG
unit_price: 8352
vip_price: 6661
---
