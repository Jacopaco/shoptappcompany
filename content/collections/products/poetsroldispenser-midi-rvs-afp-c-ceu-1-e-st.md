---
id: 2201473-afp-c
blueprint: products
art_nr: '2201473 AFP-C'
title: 'Poetsroldispenser midi RVS afp-c, CEU 1 E ST'
slug: poetsroldispenser-midi-rvs-afp-c-ceu-1-e-st
price: 20950
cost_price: 12570
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: d1ca567f-2127-432a-aec4-29fef09a974d
description: |-
  Poetsroldispenser midi voor wandmontage.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Papieruitname vanaf de onderzijde.
  Aanbevolen rolafmetingen:
  max. diameter: 200 mmm
  max. hoogte: 275 mm
  Ook in wit verkrijgbaar!
prod_img: prod_img/2201473-afp-c.JPG
unit_price: 20950
vip_price: 18855
---
