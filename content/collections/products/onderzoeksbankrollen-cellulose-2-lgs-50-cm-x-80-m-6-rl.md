---
id: 6545-180
blueprint: products
title: 'Onderzoeksbankrollen cellulose 2 lgs 50 cm x 80 m 6 rl'
price: 7097
description: '50 cm | 6 x 80 meter | Ø 13,5 cm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/813-600x600.jpg'
art_nr: '6545 - 180'
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687520729
slug: onderzoeksbankrollen-cellulose-2-lgs-50-cm-x-80-m-6-rl
brands: d2e2fc4b-bc98-488c-90a2-f0adb231736a
categories: 8218d088-f732-41b2-a36e-13cfec06fed9
cost_price: 3479
unit_price: 8920
vip_price: 5914
prod_img: prod_img/6545.jpg
client_prices:
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - bebc5c46-0553-4d62-b6ff-de33648493c4
locations:
  - 87ab27e8-a05b-4dc5-9e3d-446fa8e3f4c9
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
  - 35ffadc8-cd7a-458b-b8f3-01732fd6cb74
  - ef78b35c-42e4-458c-a65b-1f2446135a30
---
