---
id: '13752'
blueprint: product
art_nr: '13752'
title: 'Jumboroldispenser midi RVS, PR2787CS'
slug: jumboroldispenser-midi-rvs-pr2787cs
cost_price: 6414
unit_price: 10690
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 10690
vip_price: 9621
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Mediclinics PR2787CS toiletrolhouder voor 1 jumbo toiletrol. RVS mat. Geschikt voor rollen tot ca. Ø 274 mm. De minimale koker Ø is 45 mm. Venster voor inhoudscontrole. Afsluitbaar doormiddel van een drukslot.

  Specificaties
  - Artikelnummer 13752
  - Model PR278CS
  - Materiaal RVS
  - Hoogte 329 mm
  - Breedte 310 mm
  - Diepte 129 mm
  - Gewicht 1,7 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Vulling 1 jumborol van max. Ø 275 mm
prod_img: prod_img/afbeelding-pr2789cs-1719320497.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320501
---
