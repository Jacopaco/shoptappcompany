---
id: '140280'
blueprint: products
art_nr: '140280'
title: 'Tork 140280 Facial Tissue Premium 3000st Extra Soft 20,8x20c 30x100 stuks'
slug: tork-facial-tissue-premium-3000st-extra-soft-208x20c-30x100-stuks
cost_price: 4700
unit_price: 0
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
price: 7332
vip_price: 6110
description: |-
  De Tork Extra Zachte Facial Tissues bieden extra zachtheid in een mooie, moderne platte doos om uw gasten comfort te bieden. De tissuedoos is geschikt voor de Tork Facial Tissue Dispenser en kan overal worden geplaatst.

  Extra zachte tissue die zacht is voor de huid
  Hoge helderheid voor een hygiënische indruk
  Sterk en absorberend voor extra comfort
prod_img: prod_img/tork_140280.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729152702
---
