---
id: '407552'
blueprint: products
art_nr: '407552'
title: 'Papernet vouwhanddoek multifold DryTech '
slug: papernet-vouwhanddoek-multifold-drytech
price: 4831
cost_price: 3097
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
prod_img: prod_img/407552.JPG
unit_price: 3913
vip_price: 4026
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
---
