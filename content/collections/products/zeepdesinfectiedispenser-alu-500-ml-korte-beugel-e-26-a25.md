---
id: '2110931'
blueprint: products
art_nr: '2110931'
title: 'Zeep/Desinfectiedispenser alu 500 ml korte beugel, E 26 A/25'
slug: zeepdesinfectiedispenser-alu-500-ml-korte-beugel-e-26-a25
price: 5630
cost_price: 3236
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/2110931.JPG
unit_price: 0
vip_price: 4692
---
