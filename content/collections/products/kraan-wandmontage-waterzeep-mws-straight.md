---
id: '24402198'
blueprint: products
art_nr: '24402198'
title: 'Kraan wandmontage water/zeep MWS STRAIGHT'
slug: kraan-wandmontage-waterzeep-mws-straight
price: 41324
cost_price: 41324
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
categories: bc9df93b-d180-4c05-8fd8-719b6f9e1b0b
prod_img: prod_img/24402198.JPG
unit_price: 41324
vip_price: 41324
---
