---
id: '12661'
blueprint: products
art_nr: '12661'
title: 'Grab bar wand/vloer met 3 steunpunten RVS, BSI020CS'
slug: grab-bar-wandvloer-met-3-steunpunten-rvs-bsi020cs
price: 20295
cost_price: 12177
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar voor montage van wand tot vloer.
  Met 3 steunpunten, extra steunpunt zit links.
  Horizontale stanglengte: 775 mm.
  Verticale stanglengte groot: 750 mm
  Verticale stanglengte klein: 321 mm
prod_img: prod_img/12661.JPG
unit_price: 20295
vip_price: 18265
---
