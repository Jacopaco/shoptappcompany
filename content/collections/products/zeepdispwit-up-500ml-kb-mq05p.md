---
id: '8005'
blueprint: products
art_nr: '8005'
title: 'Zeepdisp.wit UP 500ml KB, MQ05P'
slug: zeepdispwit-up-500ml-kb-mq05p
price: 10600
cost_price: 6360
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig afgekit kan worden.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
unit_price: 10600
vip_price: 9540
prod_img: prod_img/8005.JPG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1686224705
---
