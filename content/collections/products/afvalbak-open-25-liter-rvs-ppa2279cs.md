---
id: '11042'
blueprint: product
art_nr: '11042'
title: 'Afvalbak open 25 liter RVS, PPA2279CS'
slug: afvalbak-open-25-liter-rvs-ppa2279cs
cost_price: 10440
unit_price: 17400
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 17400
vip_price: 15660
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak RVS 25 liter open, PPA2279CS
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/ppa2279cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718806076
---
