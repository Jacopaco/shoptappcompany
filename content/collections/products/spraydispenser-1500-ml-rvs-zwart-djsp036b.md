---
id: '14062'
blueprint: product
art_nr: '14062'
title: 'Spraydispenser 1500 ml RVS zwart, DJSP036B'
slug: spraydispenser-1500-ml-rvs-zwart-djsp036b
cost_price: 9645
unit_price: 16075
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16075
vip_price: 14467
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  MEDICLINICS DJP0036B desinfectiemiddelen dispenser met beugel 1500 ml.


  - Artikelnummer 14062
  - Model DJP0036B
  - Materiaal RVS
  - Kleur Zwart gepoedercoat
  - Hoogte 240 mm
  - Breedte 110 mm
  - Diepte 133 mm
  - Gewicht 1,25 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Toepassing Navulbaar
  - Vulling Dunne alcohol (handalcohol of oppervlakte desinfectie alcohol)
  - Dosering 0,8 ml per slag
  - Inhoud 1500 ml
prod_img: prod_img/14062_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715070697
---
