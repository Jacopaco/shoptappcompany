---
id: '183545740'
blueprint: products
art_nr: '183545740'
title: 'Zoppie dagelijkse vloerreiniger 5 liter – Niet schuimend'
slug: zoppie-dagelijkse-vloerreiniger-5-liter-niet-schuimend
cost_price: 850
unit_price: 1105
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 1448
vip_price: 1207
prod_img: prod_img/183545740.JPG
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
  - ffa8f396-2f25-4d2b-9178-abbf960171cb
  - bbae8e44-4575-431e-8728-181691d314e6
---
