---
id: '12220'
blueprint: product
art_nr: '12220'
title: 'Handendroger drukknop RVS look, E88CS'
slug: handendroger-drukknop-rvs-look-e88cs
cost_price: 38445
unit_price: 69900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 69900
vip_price: 62910
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Misschien ziet u het al helemaal voor u; een donkere muur, met matzwarte tegels, waartegen de rvs accessoires helemaal tot hun recht komen. Helaas moet u hiervoor vaak dieper in de buidel tasten dan u wilt. Behalve als u kiest voor de stijlvolle accessoires van onze Mediclinics-lijn; door het nauwkeurige samenspel van verschillende materialen bent u verzekerd van luxueus rvs design, dat bovendien betaalbaar blijft.

  Neem deze handendroger: de droger is gemaakt van stevig staal, met een authentieke rvs look. De hoogglans tuit en drukknop geven dit accessoire een extra spannende look. De handendroger kenmerkt zich door zijn gebruiksvriendelijkheid; met een druk op de knop wordt de timer ten minste een halve minuut geactiveerd.
prod_img: prod_img/mediclinics-e88cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478633
---
