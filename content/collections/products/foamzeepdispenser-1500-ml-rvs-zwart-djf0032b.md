---
id: '13160'
blueprint: product
art_nr: '13160'
title: 'Foamzeepdispenser 1500 ml RVS zwart, DJF0032B'
slug: foamzeepdispenser-1500-ml-rvs-zwart-djf0032b
cost_price: 7830
unit_price: 13050
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 13050
vip_price: 11745
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  De sanitaire stop is dan vooral noodzakelijk, het is ook een moment van rust en ontspanning. Een stijlvol ingerichte sanitaire ruimte is dan ook dé plek om weer even helemaal tot uzelf te komen. Een keus voor de elegante en smaakvolle Mediclinics-lijn is dan vanzelfsprekend.

  Neem nu deze foamzeepdispenser. Eenvoudig, stijlvol uiterlijk, opvallende warme kleur. Duidelijk een fraaie en praktische aanvulling op uw sanitaire ruimte. Tel daar het optimale gebruiksgemak, de flinke inhoud en de voordelen van foamzeep bij op, en u heeft uw keus gemaakt.

  Wanneer u waarde hecht aan stijl en gebruiksgemak is de keus voor de Mediclinics Foamzeepdispenser RVS zwart 1500 ml snel gemaakt.
prod_img: prod_img/afbeelding-dj0032b.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1716905004
---
