---
id: '5543'
blueprint: products
art_nr: '5543'
title: 'Garagezeepdispenser 2000 ml kunststof wit, PQSoapG'
slug: garagezeepdispenser-2000-ml-kunststof-wit-pqsoapg
price: 5485
cost_price: 3456
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Garagezeepdispenser voor wandmontage.
  Met navulfles en standaard industriële pomp.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Op aanvraag ook te gebruiken met pouches/cartridges en met een extreme en pumice pomp.
  Geschikt voor bedrukking met uw logo.
  Universeel navulbaar
  Modulair systeem.
prod_img: prod_img/5543.JPG
unit_price: 5485
vip_price: 4936
---
