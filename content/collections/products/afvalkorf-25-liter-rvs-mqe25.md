---
id: '8460'
blueprint: products
art_nr: '8460'
title: 'Afvalkorf 25 liter RVS, MQE25'
slug: afvalkorf-25-liter-rvs-mqe25
price: 12060
cost_price: 7236
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalkorf voor wandmontage.
  Wandhouder wordt standaard meegeleverd.
  Met afgeronde kanten.
prod_img: prod_img/8460.JPG
unit_price: 12060
vip_price: 10854
---
