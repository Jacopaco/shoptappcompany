---
id: '5541'
blueprint: products
art_nr: '5541'
title: 'Handdoekdispenser mini kunststof wit, PQMiniH'
slug: handdoekdispenser-mini-kunststof-wit-pqminih
price: 2565
cost_price: 1616
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser voor wandmontage.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Geschikt voor: C- en Z gevouwen papieren handdoekjes met max. 200 mm stapelhoogte
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/5541.JPG
unit_price: 2565
vip_price: 2308
---
