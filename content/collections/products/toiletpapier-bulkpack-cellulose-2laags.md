---
id: '2200'
blueprint: products
title: 'Toiletpapier bulkpack cellulose 2laags'
price: 3237
description: '11,2 x 21 cm | 40 x 225 vel in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2200_3.jpg'
art_nr: 2200
slug: toiletpapier-bulkpack-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 2075
unit_price: 0
vip_price: 2697
prod_img: prod_img/2200_3.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654117
client_prices:
  - 6d0190b5-4139-49e0-899e-76ea795c5036
  - 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
  - 342ca54a-98c1-451c-8ea8-f377ef5519e9
  - 6d0190b5-4139-49e0-899e-76ea795c5036
locations:
  - e6090873-ae14-4d62-b52a-da6089045f90
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
---
