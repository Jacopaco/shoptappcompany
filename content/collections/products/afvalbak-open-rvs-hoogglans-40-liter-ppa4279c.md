---
id: '11031'
blueprint: product
art_nr: '11031'
title: 'Afvalbak open RVS hoogglans 40 liter, PPA4279C'
slug: afvalbak-open-rvs-hoogglans-40-liter-ppa4279c
cost_price: 14700
unit_price: 24500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 24500
vip_price: 22050
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak hoogglans 40 liter open, PPA4279C
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/afbeelding-ppa4279c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718806621
---
