---
id: '2195'
blueprint: products
art_nr: '2195'
title: 'Poetsrol midi blauw recycled tissue 1laags'
slug: poetsrol-midi-blauw-recycled-tissue-1laags
price: 3447
cost_price: 2210
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
description: '19,5 cm | 6 x 300 meter in folie'
prod_img: prod_img/2195.jpg
unit_price: 0
vip_price: 2873
---
