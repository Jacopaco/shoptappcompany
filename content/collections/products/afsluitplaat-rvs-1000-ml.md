---
id: '8146'
published: false
blueprint: products
art_nr: '8146'
title: 'Afsluitplaat RVS 1000 ml'
slug: afsluitplaat-rvs-1000-ml
price: 4065
cost_price: 2439
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
description: |-
  Afsluitplaat met zichtvenster voor inhoudscontrole.
  Met slot en sleutel voor het beveiligen van de vulgoederen.
prod_img: prod_img/8146.JPG
unit_price: 4065
vip_price: 3658
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712059304
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
