---
id: '14217'
blueprint: products
art_nr: '14217'
title: 'Foamzeepdispenser 1000 ml automatisch kunststof zilver, PQAutFoamM '
slug: foamzeepdispenser-1000-ml-automatisch-kunststof-zilver-pqautfoamm
price: 9750
cost_price: 5850
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser automatisch.
  Met slot en sleutel.
  Wordt met navulfles geleverd.
  Met pictogram aan de voorzijde.
  Werkt op 3 stuks LR14 batterijen.
  Wordt excl. batterijen geleverd.
prod_img: prod_img/14217.JPG
unit_price: 9750
vip_price: 8775
---
