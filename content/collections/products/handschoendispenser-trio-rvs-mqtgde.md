---
id: '8501'
blueprint: products
art_nr: '8501'
title: 'MediQo 8501 RVS handschoendispenser voor 3 doosjes wegwerphandschoenen'
slug: handschoendispenser-trio-rvs-mqtgde
price: 12315
cost_price: 7389
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  MediQo-Line RVS handschoendispenser voor 3 dozen. Dispenser is voorzien van een veerklem, zodat ook kleinere doosjes strak in de houder zitten. Kan verticaal of horizontaal op de wand worden gemonteerd.

  Specificaties
  Artikelnummer 8501
  Materiaal RVS
  Hoogte 403 mm
  Breedte 240 mm
  Diepte 99 mm
  Gewicht 3 kg
  Garantie 2 jaar na aankoopdatum
  Toepassing Wandmontage
  Vulling 3 handschoendoosjes met max. binnenmaat: H 133 x B 240 x D 96 mm
prod_img: prod_img/8501.JPG
unit_price: 12315
vip_price: 11083
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729231911
---
