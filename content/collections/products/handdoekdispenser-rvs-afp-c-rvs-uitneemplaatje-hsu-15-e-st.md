---
id: 21417717-afp-c
blueprint: products
art_nr: 21417717-AFP-C
title: 'Handdoekdispenser RVS afp-c + RVS uitneemplaatje, HSU 15 E ST'
slug: handdoekdispenser-rvs-afp-c-rvs-uitneemplaatje-hsu-15-e-st
cost_price: 7977
unit_price: 16660
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 16660
vip_price: 14994
prod_img: prod_img/21417717-afp-c.png
---
