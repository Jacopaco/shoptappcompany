---
id: '12620'
blueprint: products
art_nr: '12620'
title: 'Hoek grab bar 90° RVS, BAI090CS'
slug: hoek-grab-bar-90-rvs-bai090cs
price: 15800
cost_price: 9480
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar met 90˚ hoek naar links.
  Verticale stang lengte: 375 mm.
  Horizontale stang lengte: 700 mm
unit_price: 15800
vip_price: 14220
prod_img: prod_img/12620.JPG
---
