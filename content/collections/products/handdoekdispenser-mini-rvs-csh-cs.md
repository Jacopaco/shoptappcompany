---
id: 3804-2
blueprint: products
art_nr: 3804-2
title: 'Handdoekdispenser mini RVS, CSH-CS'
slug: handdoekdispenser-mini-rvs-csh-cs
cost_price: 3885
unit_price: 6475
brands: 8bbcdfde-2ae5-4424-b85b-e29137fffb02
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 7785
vip_price: 6487
prod_img: prod_img/3804-2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1697012456
---
