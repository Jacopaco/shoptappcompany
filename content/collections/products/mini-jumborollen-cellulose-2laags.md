---
id: '2120'
blueprint: products
title: 'STARLIGHT 2120-FSC mini jumborollen 12x180 meter - 2 laags'
price: 2177
description: |-
  Toiletpapier Mini Jumbo 100 % cellulose 2 laags 12x180 meter

  Materiaal 100% cellulose
  Kleur wit
  Afmetingen 180 meter op een rol
  2 laags
  Rol Ø 18,5 cm
  Koker Ø 5,7 cm
  Verpakking 12 rollen in folie
  FSC gecertificeerd
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2117_1_1.jpg'
art_nr: 2120
slug: mini-jumborollen-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 1396
unit_price: 2280
vip_price: 1814
prod_img: prod_img/2117_1_1-1688653424.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728992005
client_prices:
  - a9bd7d8c-213d-4c7a-aa35-dca7027d50e2
  - a9bd7d8c-213d-4c7a-aa35-dca7027d50e2
product_type: physical
---
