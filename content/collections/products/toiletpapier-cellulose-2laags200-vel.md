---
id: '2102'
blueprint: products
title: 'STARLIGHT 2102-FSC toiletpapier 40x200 vel - 2 laags'
price: 1394
description: |-
  Het Starlight tissue 100% cellulose 2-laags toiletpapier geeft een zeer zacht en comfortabel gevoel. De rol bevat 200 vellen. Dit toiletpapier is uitermate geschikt voor sanitaire ruimten die een luxe uitstraling hebben, maar waarbij sprake is van een laag aantal bezoekers per dag.

  Code 2102
  Materiaal 100% cellulose
  Kleur wit
  Afmetingen 200 vel op rol
  2 laags
  Verpakking 10 x 4 rollen in folie

  FSC certificaat
  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2102_1_2.jpg'
art_nr: 2102
slug: toiletpapier-cellulose-2laags200-vel
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 894
unit_price: 0
vip_price: 1162
prod_img: prod_img/2102_1_2.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728993485
product_type: physical
---
