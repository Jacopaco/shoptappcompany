---
id: 21415828-afp-c
blueprint: products
art_nr: '21415828 AFP-C'
title: 'Zeepdispenser 250 ml RVS afp-c, NSU 2 E/S ST'
slug: zeepdispenser-250-ml-rvs-afp-c-nsu-2-es-st
price: 16970
cost_price: 9164
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met verborgen speciaal slot.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415828-afp-c.JPG
unit_price: 16970
vip_price: 15273
---
