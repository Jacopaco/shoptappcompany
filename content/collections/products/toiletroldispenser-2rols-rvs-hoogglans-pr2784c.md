---
id: '13114'
blueprint: product
art_nr: '13114'
title: 'Toiletroldispenser 2rols RVS hoogglans, PR2784C'
slug: toiletroldispenser-2rols-rvs-hoogglans-pr2784c
cost_price: 6600
unit_price: 11000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11000
vip_price: 9900
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Een hygiënische en strak ingerichte sanitaire ruimte biedt elke toiletbezoeker optimaal comfort. Zeker wanneer uw toiletbezoeker de beschikking heeft over een flinke voorraad toiletpapier.

  Dat eenvoud siert, laat deze hoogglans 2-rolshouder van Mediclinics duidelijk zien. Deze gebruiksvriendelijke compacte hoogglans rvs 2-rolshouder is ideaal voor toiletten met een beperkte bezoekfrequentie. De houder biedt ruimte aan twee toiletrollen. U houdt de voorraad eenvoudig in de gaten via het doorzichtige venster aan de zijkant van de houder.

  Geef uw toiletbezoekers een comfortabel gevoel en bestel deze 2-rolstoilethouder.
prod_img: prod_img/mediclinics_pr2784c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477009
---
