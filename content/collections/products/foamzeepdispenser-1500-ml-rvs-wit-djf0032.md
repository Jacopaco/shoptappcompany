---
id: '13155'
blueprint: product
art_nr: '13155'
title: 'Foamzeepdispenser 1500 ml RVS wit, DJF0032'
slug: foamzeepdispenser-1500-ml-rvs-wit-djf0032
cost_price: 8061
unit_price: 13050
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 13050
vip_price: 11745
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Wit: steriel, kil en saai? Fabeltje! Een licht ingerichte sanitaire ruimte laat namelijk een hygiënische en schone indruk achter bij uw gasten. Juist daarom hebben veel ontwerpers een keus gemaakt voor deze witte uitvoering van de Mediclinics Foamzeepdispenser RVS 1500 ml.

  Stijl, design en gebruiksgemak. Dat zijn de drie sleutelwoorden geweest bij het ontwerp van deze moderne foamzeepdispenser van Mediclinics. Door de strakke lijnen past deze unieke dispenser in bijna elke moderne sanitaire ruimte. Op de voorzijde van de smetteloos witte behuizing zorgt de rvs drukknop voor een extra geraffineerd stijlaccent. De dispenser biedt ruimte aan 1500 ml foamzeep, wat de dispenser ideaal maakt voor gebruik in ruimtes met een hoge bezoekersfrequentie.
prod_img: prod_img/djf0032.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719475947
---
