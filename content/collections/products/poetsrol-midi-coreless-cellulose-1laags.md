---
id: '2189'
blueprint: products
title: 'Poetsrol midi coreless cellulose 1laags'
price: 4519
description: '20 cm | 6 x 300 meter in folie'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2188_4_3.jpg'
art_nr: 2189
slug: poetsrol-midi-coreless-cellulose-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 2897
unit_price: 0
vip_price: 3766
prod_img: prod_img/2188_4_3-1688654040.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654044
locations:
  - a0be2336-697d-408e-ae76-a075fb4e1f62
---
