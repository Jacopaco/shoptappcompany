---
id: '5651'
blueprint: products
art_nr: '5651'
title: 'Afvalbak 23 liter half open wit, PQA23'
slug: afvalbak-23-liter-half-open-wit-pqa23
price: 3825
cost_price: 2410
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand. 
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging. 
  Met semi-open deksel. 
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling. 
prod_img: prod_img/5651.JPG
unit_price: 3825
vip_price: 3442
locations:
  - dd5c7f1d-49b0-4c07-bc33-057b67f293fd
---
