---
id: cart00927
blueprint: products
art_nr: CART00927
title: 'Handschoen zwart nitril maat M'
slug: handschoen-zwart-nitril-maat-m
cost_price: 287
unit_price: 374
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
price: 374
vip_price: 374
prod_img: prod_img/cart00927.JPG
client_prices:
  - cf25c62f-df48-4e88-9c29-33f9eca1deb2
  - cf25c62f-df48-4e88-9c29-33f9eca1deb2
locations:
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - 112c9b1d-3004-44b3-a872-aa1f80e862ce
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
users:
  - 36029b57-8cbd-4fb0-a6e5-47205dbb5063
---
