---
id: '6543'
published: false
blueprint: products
art_nr: '6543'
title: 'Onderzoeksbankrollen verlijmd cellulose 2 laags'
slug: onderzoeksbankrollen-verlijmd-cellulose-2-laags
price: 81
cost_price: 52.3
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: 2b3ca86a-26af-461e-b819-7415564e5b92
description: '59 cm | 4 x 150 meter | Ø 14,7 cm'
unit_price: 0
vip_price: 67
prod_img: prod_img/6543---188.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1686224607
---
