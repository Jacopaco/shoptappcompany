---
id: '5645'
blueprint: products
art_nr: '5645'
title: 'Hygiënebak 8 liter kunststof wit, PQH8'
slug: hygienebak-8-liter-kunststof-wit-pqh8
price: 3879
cost_price: 2444
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiënebak 8 liter.
  Vrijstaand of voor wandmontage.
  Met gesloten inworpklep.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
  Met extra meegeleverde wand support.
prod_img: prod_img/5645.JPG
unit_price: 3879
vip_price: 3491
---
