---
id: '98940'
blueprint: product
art_nr: '98940'
title: 'Zonnebrandcrème SPF30 POUCH 800 ml (8st)'
slug: zonnebrandcreme-spf30-pouch-800-ml-8st
brands: 3375fa99-d12c-4998-8110-4d347cec808b
cost_price: 4095
unit_price: 5528
price: 6388
vip_price: 5323
locations:
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 31ab6798-7e55-4a96-8c41-c9d35bf02694
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1724831260
---
