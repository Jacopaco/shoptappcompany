---
id: '5671'
blueprint: products
art_nr: '5671'
title: 'Afvalbak 23 liter half open zilver, PQA23M'
slug: afvalbak-23-liter-half-open-zilver-pqa23m
price: 6190
cost_price: 3900
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/5671.JPG
unit_price: 6190
vip_price: 5571
locations:
  - 0b3b7ada-ec29-4914-b256-fa56a8d98eae
  - 11d7f953-43aa-43d8-a76d-a6c9c45f0621
---
