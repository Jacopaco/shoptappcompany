---
id: '11060'
blueprint: product
art_nr: '11060'
title: 'Afvalbak 65 liter wit, PP0065'
slug: afvalbak-65-liter-wit-pp0065
cost_price: 18300
unit_price: 30500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 30500
vip_price: 27450
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak wit 65 liter gesloten, PP0065
  Afvalbak met push klep.
  Staat op 4 kunststof voetjes.
  Deksel is in zijn geheel van de onderbak af te nemen.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/mediclincs-pp0065.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845899
---
