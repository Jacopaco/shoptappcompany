---
id: '13145'
blueprint: product
art_nr: '13145'
title: 'Zeepdispenser 1500 ml RVS wit, DJ0031'
slug: zeepdispenser-1500-ml-rvs-wit-dj0031
cost_price: 7653
unit_price: 12380
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 12380
vip_price: 11142
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Dat de ontwerpers van de Mediclinics-lijn voor de top gaan, is duidelijk. Het exclusieve design, gecombineerd met optimaal gebruiksgemak, is een aanwinst in elke sanitaire ruimte. Zo ook deze zeepdispenser RVS wit 1500 ml.

  De ruime inhoud van deze zeepdispenser laat in een oogopslag zien dat hij ontworpen is voor sanitaire ruimtes met een flinke doorloop. De dispenser biedt ruimte aan ruim 1500 ml vloeibare zeep, en is makkelijk en snel te bedienen. Door één druk op de pushbutton op de voorzijde – speels accent in rvs – doseert de dispenser automatisch de juiste hoeveelheid zeep. Navullen is mogelijk via de bovenzijde van de dispenser.
prod_img: prod_img/dj0031.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719476591
---
