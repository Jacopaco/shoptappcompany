---
id: '5666'
blueprint: products
art_nr: '5666'
title: 'Afvalbak 20 liter kunststof wit kniebediening, PQKBL'
slug: afvalbak-20-liter-kunststof-wit-kniebediening-pqkbl
price: 9350
cost_price: 5891
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak voor wandmontage met kniebediening.
  De klepdeksel klapt open, zodra de onderbak met de knie wordt aangeduwd.
  Deksel klapt automatische weer terug, zodra de onderbak wordt losgelaten.
  Voorzien van een uitneembare binnenbak met vier zijflappen.
prod_img: prod_img/5666.JPG
unit_price: 9350
vip_price: 8415
locations:
  - dd5c7f1d-49b0-4c07-bc33-057b67f293fd
---
