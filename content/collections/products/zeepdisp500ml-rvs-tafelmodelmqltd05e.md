---
id: '8070'
blueprint: products
art_nr: '8070'
title: 'Zeepdisp.500ml RVS tafelmodel,MQLTD05E'
slug: zeepdisp500ml-rvs-tafelmodelmqltd05e
price: 14075
cost_price: 8445
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Tafelmodel zeep-& desinfectiemiddeldispenser met lange bedieningsbeugel.
  Wordt inclusief kunststof witte opvangschaal en navulfles geleverd.
  Met RVS pomp, die makkelijk vanaf de voorkant uitneembaar is.
  Inclusief clip met tekst Handdesinfectie.
  Staat op 4 slipvrije voeten.
  Stabiliteit is gewaarborgd door een diepliggend zwaartepunt.
prod_img: prod_img/8070.JPG
unit_price: 14075
vip_price: 12667
---
