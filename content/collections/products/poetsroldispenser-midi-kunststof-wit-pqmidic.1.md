---
id: 5537-br
published: false
blueprint: products
art_nr: 5537-BR
title: 'Poetsroldispenser midi kunststof wit, PQMidiC'
slug: poetsroldispenser-midi-kunststof-wit-pqmidic
cost_price: 1811
unit_price: 3070
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: d1ca567f-2127-432a-aec4-29fef09a974d
price: 3070
vip_price: 2763
---
