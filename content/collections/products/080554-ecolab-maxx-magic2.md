---
id: '080554'
blueprint: products
title: 'Ecolab MAXX Magic2 - 1 L'
price: 1378
description: '080554 Ecolab MAXX Magic2 geconc. allesreiniger superbevochtigend Flacon 1L'
img:
  - product_images/ecolab-maxx-magic2-9084500.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687524300
art_nr: '080554'
slug: ecolab-maxx-magic2-1-l
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 809
unit_price: 890
vip_price: 1148
prod_img: prod_img/8055.JPG
---
