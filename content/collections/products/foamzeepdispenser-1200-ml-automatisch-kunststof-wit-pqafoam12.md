---
id: '5472'
blueprint: products
art_nr: '5472'
title: 'Foamzeepdispenser 1200 ml automatisch kunststof wit, PQAFoam12'
slug: foamzeepdispenser-1200-ml-automatisch-kunststof-wit-pqafoam12
price: 7395
cost_price: 4659
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser automatisch voor wandmontage.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Met slot en witte speciale sleutel.
  Met navulfles en foampomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo.
  Werkt op 4 stuks C-Cell (LR14)batterijen.
  Aanbevolen installatiehoogte: 25-30 cm boven de oppervlakte.
prod_img: prod_img/5472.JPG
unit_price: 7395
vip_price: 6655
locations:
  - 11d7f953-43aa-43d8-a76d-a6c9c45f0621
---
