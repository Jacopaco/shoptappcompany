---
id: '183204844'
blueprint: products
art_nr: '183204844'
title: 'Zoppie dagelijkse sanitairreiniger 5 liter'
slug: zoppie-dagelijkse-sanitairreiniger-5-liter
cost_price: 1147
unit_price: 1595
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 1954
vip_price: 1628
prod_img: prod_img/183204844.JPG
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
---
