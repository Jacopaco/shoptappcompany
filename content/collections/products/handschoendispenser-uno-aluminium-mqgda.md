---
id: '8473'
blueprint: products
art_nr: '8473'
title: 'Handschoendispenser uno aluminium, MQGDA'
slug: handschoendispenser-uno-aluminium-mqgda
price: 7490
cost_price: 4494
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Handschoendispenser voor wandmontage.
  Geschikt voor verschillende maten handschoendoosjes.
  Een verende RVS klembeugel houdt de doosjes op de plaats.
  Kan zowel horizontaal als verticaal gemonteerd worden.
  Met 2puntsbevestiging.
prod_img: prod_img/8473.JPG
unit_price: 7490
vip_price: 6741
---
