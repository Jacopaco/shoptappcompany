---
id: 21411929-afp-c
blueprint: products
art_nr: '21411929 AFP-C.'
title: 'SanTRAL Classic NSU 5 E/S ST zeepdispenser 600 ml'
slug: zeepdispenser-600-ml-rvs-afp-c-nsu-5-es-st
price: 16660
cost_price: 8996
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
prod_img: prod_img/21411929-afp-c..JPG
unit_price: 16660
vip_price: 14994
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728992955
---
