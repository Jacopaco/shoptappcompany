---
id: '072399'
blueprint: products
title: 'Ecolab Kristalin Bio'
price: 3559
description: '072399 Ecolab Kristalin Bio geconc. sanitairreiniger Flacon 1L'
img:
  - product_images/ecolab-kristalin-bio-1l.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687524216
art_nr: '072399'
slug: ecolab-kristalin-bio
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 2089
unit_price: 2298
vip_price: 2966
prod_img: prod_img/072399.jpg
---
