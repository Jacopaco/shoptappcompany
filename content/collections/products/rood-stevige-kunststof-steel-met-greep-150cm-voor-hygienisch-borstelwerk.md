---
id: '356169636'
blueprint: products
art_nr: '356169636'
title: 'ROOD Stevige kunststof steel met greep 150cm – Voor hygiënisch borstelwerk'
slug: rood-stevige-kunststof-steel-met-greep-150cm-voor-hygienisch-borstelwerk
cost_price: 1141
unit_price: 2100
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 1944
vip_price: 1620
---
