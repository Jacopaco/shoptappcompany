---
id: '8370'
blueprint: products
art_nr: '8370'
title: 'Toiletborstelhouder RVS, MQTBHE'
slug: toiletborstelhouder-rvs-mqtbhe
price: 11935
cost_price: 7161
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Toiletborstelhouder voor wandmontage met uitneembare kunststof zwarte opvangschaal.
  Met kunststof zwarte vervangbare toiletborstel en RVS steel.
  De dichte zijde kan zowel aan de linker- als aan de rechterzijde gemonteerd worden.
  Met 4puntsbevestiging.
prod_img: prod_img/8370.JPG
unit_price: 11935
vip_price: 10741
---
