---
id: '2187'
blueprint: products
title: 'Poetsrol midi cellulose 2laags'
price: 2644
description: '20 cm | 6 x 160 meter in folie | ø 190 mm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2187_4_2.jpg'
art_nr: 2187
slug: poetsrol-midi-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1695
unit_price: 0
vip_price: 2203
prod_img: prod_img/2187_4_2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654002
---
