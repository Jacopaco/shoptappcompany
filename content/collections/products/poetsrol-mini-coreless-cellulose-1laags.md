---
id: '2180'
blueprint: products
title: 'Poetsrol mini coreless cellulose 1laags'
price: 3717
description: '20 cm | 12x 120 meter in folie | ø 110 mm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2180_3.jpg'
art_nr: 2180
slug: poetsrol-mini-coreless-cellulose-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 2383
unit_price: 0
vip_price: 3097
prod_img: prod_img/2180_3-1688653916.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653919
client_prices:
  - af9b6bfa-831d-406e-b186-218ccb70be55
  - af9b6bfa-831d-406e-b186-218ccb70be55
locations:
  - 7392b23f-6bcc-42b9-b010-fc0375fdd35a
---
