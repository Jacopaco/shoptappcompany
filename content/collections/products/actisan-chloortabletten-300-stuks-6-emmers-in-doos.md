---
id: '572183001'
published: false
blueprint: products
art_nr: '572183001'
title: 'Actisan Chloortabletten 300 stuks - 6 emmers in doos vervangen'
slug: actisan-chloortabletten-300-stuks-6-emmers-in-doos-vervangen
cost_price: 13740
unit_price: 19437
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 23412
vip_price: 19510
prod_img: prod_img/572183001.JPG
locations:
  - bbae8e44-4575-431e-8728-181691d314e6
---
