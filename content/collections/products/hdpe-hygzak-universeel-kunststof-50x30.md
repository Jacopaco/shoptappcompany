---
id: '458130'
blueprint: products
art_nr: '458130'
title: 'HDPE hygiënezakjes universeel kunststof (50x30) 1500 stuks'
slug: hdpe-hygienezakjes-universeel-kunststof-50x30-1500-stuks
cost_price: 1340
unit_price: 5000
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 0e559e3a-a5c9-444d-b968-d93cc759839f
price: 2460
vip_price: 2050
prod_img: prod_img/hdpe-hygienezakjes-universeel-kunststof-(50x30)-1500-stuks.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727268010
---
