---
id: '5502'
blueprint: products
art_nr: '5502'
title: 'Foamzeepdispenser 900 ml kunststof wit, PQFoam9'
slug: foamzeepdispenser-900-ml-kunststof-wit-pqfoam9
price: 3875
cost_price: 2441
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met push cover voor het doseren van de vulgoederen, duwrichting naar de muur.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters. 
  Met slot en witte PQ sleutel.
  Met navulfles en foampomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo. 
prod_img: prod_img/5502.JPG
unit_price: 3875
vip_price: 3487
---
