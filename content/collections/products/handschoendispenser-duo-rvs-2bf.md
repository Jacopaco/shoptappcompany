---
id: '21423264'
blueprint: products
art_nr: '21423264'
title: 'Handschoendispenser DUO RVS, 2BF'
slug: handschoendispenser-duo-rvs-2bf
cost_price: 2995
unit_price: 5710
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
price: 5710
vip_price: 5139
prod_img: prod_img/21423264.png
description: |-
  DUO handschoendispenser voor wandmontage.
  Geschikt voor handschoendoosjes in verschillende maten
  H 340 x B 254 x D 110 mm
---
