---
id: '5507'
blueprint: products
art_nr: '5507'
title: 'Zeepdispenser 400 ml kunststof wit, PQSoap4'
slug: zeepdispenser-400-ml-kunststof-wit-pqsoap4
price: 2755
cost_price: 1736
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser voor wandmontage.
  Met push cover, duwrichting naar de muur, voor het doseren van de vulgoederen. 
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Met slot en witte PQ sleutel.
  Met  Met navulfles en zeeppomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo. 
prod_img: prod_img/5507.JPG
unit_price: 2755
vip_price: 2479
---
