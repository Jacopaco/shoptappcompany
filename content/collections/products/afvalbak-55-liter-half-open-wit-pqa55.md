---
id: '5642'
blueprint: products
art_nr: '5642'
title: 'Afvalbak 55 liter half open wit, PQA55'
slug: afvalbak-55-liter-half-open-wit-pqa55
price: 6770
cost_price: 4265
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met semi-open deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
  Inhoud: 55 liter.
prod_img: prod_img/5642.JPG
unit_price: 6770
vip_price: 6093
---
