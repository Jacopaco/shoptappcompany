---
id: '11080'
blueprint: product
art_nr: '11080'
title: 'Pedaalafvalbak RVS hoogglans 3 liter, PP1303C'
slug: pedaalafvalbak-rvs-hoogglans-3-liter-pp1303c
cost_price: 1860
unit_price: 3100
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 3100
vip_price: 2790
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1303c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845374
---
