---
id: '14002'
published: false
blueprint: product
art_nr: '14002'
title: 'Zeeppomp inbouw 480 ml RVS hoogglans, DJ0121C'
slug: zeeppomp-inbouw-480-ml-rvs-hoogglans-dj0121c
cost_price: 3600
unit_price: 6000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6000
vip_price: 5400
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
---
