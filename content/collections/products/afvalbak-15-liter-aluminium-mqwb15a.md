---
id: '8220'
blueprint: products
art_nr: '8220'
title: 'Afvalbak 15 liter aluminium, MQWB15A'
slug: afvalbak-15-liter-aluminium-mqwb15a
price: 16670
cost_price: 10002
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak met klepdeksel.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8220.JPG
unit_price: 16670
vip_price: 15003
---
