---
id: '5585'
blueprint: products
art_nr: '5585'
title: 'Toiletroldispenser 2rols (doprol) kunststof/metaal wit, PQSDuoPlus'
slug: toiletroldispenser-2rols-doprol-kunststofmetaal-wit-pqsduoplus
price: 5300
cost_price: 3800
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Toiletroldispenser voor 2 doprollen voor wandmontage.
  De doprollen worden door 2 veren vastgehouden, die zorgen voor een automatische
  toiletrolwisseling.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Modulair systeem.
prod_img: prod_img/5585.JPG
unit_price: 5300
vip_price: 4770
---
