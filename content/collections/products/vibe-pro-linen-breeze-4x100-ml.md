---
id: '8331303'
blueprint: products
art_nr: '8331303'
title: 'VIBE PRO LINEN BREEZE 4x100 ml '
slug: vibe-pro-linen-breeze-4x100-ml
cost_price: 6375
unit_price: 8606
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
price: 8606
vip_price: 8606
prod_img: prod_img/8331303.JPG
---
