---
id: 21419395-afp-c
blueprint: products
art_nr: '21419395 AFP-C'
title: 'Zeepdispenser 1200 ml RVS afp-c, NSU 11 E/S ST TOUCHLESS'
slug: zeepdispenser-1200-ml-rvs-afp-c-nsu-11-es-st-touchless
price: 29260
cost_price: 15800
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser automatisch voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Werkt op 4 D-batterijen, deze worden NIET meegeleverd.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21419395-afp-c.JPG
unit_price: 29260
vip_price: 26334
---
