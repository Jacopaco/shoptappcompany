---
id: '2141'
published: false
blueprint: products
title: 'Handdoekjes interfold cellulose 2laags'
price: 5520
description: '42 x 22 cm | 20 x 120 vellen in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2135_3_2.jpg'
art_nr: 2141
slug: handdoekjes-interfold-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 3539
unit_price: 0
vip_price: 4600
prod_img: prod_img/2135_3_2.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729156711
client_prices:
  - 50f975cc-aa02-494f-81aa-fbbd2580a184
  - 50f975cc-aa02-494f-81aa-fbbd2580a184
locations:
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
product_type: physical
---
