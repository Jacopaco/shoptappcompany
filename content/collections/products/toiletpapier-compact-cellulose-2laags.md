---
id: '2110'
blueprint: products
title: 'STARLIGHT 2110-FSC compact toiletrollen 36x100 meter - 2 laags'
price: 4828
description: |-
  Toiletpapier Compact 100% cellulose 2 laags 36x100 meter

  - Code	2110
  - Materiaal	cellulose
  - Kleur	wit
  - Afmetingen	100 meter op een rol
  - 2 laags
  - Verpakking	36 rollen in folie
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2110-2.jpg'
art_nr: 2110
slug: toiletpapier-compact-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 3095
unit_price: 0
vip_price: 4023
prod_img: prod_img/2105_1_1.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729000119
client_prices:
  - d267d33a-5684-4194-856e-727b2f4b79ba
  - 8ed96843-92e3-4f5e-bf96-e054d740cea6
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - e04cab04-8a7b-45a0-9698-aeb7ff898773
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - 4e00303b-45d0-49d8-9aad-8a8ef0e22948
  - 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - 465917bf-fcad-40f2-8dae-0de9505b610e
  - 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
  - d267d33a-5684-4194-856e-727b2f4b79ba
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 175073cd-07d9-40df-9e81-927b690b393b
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 112c9b1d-3004-44b3-a872-aa1f80e862ce
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - be14461b-4601-4a24-a6dc-17853268cc98
  - a0be2336-697d-408e-ae76-a075fb4e1f62
product_type: physical
---
