---
id: '83990010'
published: false
blueprint: products
art_nr: '83990010'
title: 'Bio afvalzak 50x60cm 15 my 40x10st'
slug: bio-afvalzak-50x60cm-15-my-40x10st
price: 8629
cost_price: 4700
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
prod_img: prod_img/83990010.jpg
unit_price: 6400
vip_price: 7191
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 11d7f953-43aa-43d8-a76d-a6c9c45f0621
---
