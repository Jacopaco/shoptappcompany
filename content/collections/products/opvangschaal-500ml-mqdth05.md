---
id: '8100'
blueprint: products
art_nr: '8100'
title: 'Opvangschaal 500ml, MQDTH05'
slug: opvangschaal-500ml-mqdth05
price: 5590
cost_price: 3354
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Opvangschaal.
  Wordt over de dispenser heen gehangen.
  Met uitneembare opvangschaal.
prod_img: prod_img/8100.JPG
unit_price: 5590
vip_price: 5031
---
