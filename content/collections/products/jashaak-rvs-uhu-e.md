---
id: '21411590'
blueprint: products
art_nr: '21411590'
title: 'Jashaak RVS, UHU E'
slug: jashaak-rvs-uhu-e
price: 3520
cost_price: 1901
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 313c7a1e-a9b5-4bfc-95f5-37dfc77c7421
description: |-
  Universele jashaak voor wandmontage.
  Inclusief stokschroef en plastic muurranker.
prod_img: prod_img/21411590.JPG
unit_price: 3520
vip_price: 3168
---
