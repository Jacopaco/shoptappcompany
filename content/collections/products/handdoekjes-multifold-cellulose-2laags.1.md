---
id: '2131'
published: false
blueprint: products
title: 'Handdoekjes multifold cellulose 2laags'
price: 4736
description: '20,6 x 32 cm | 25 x 120 vellen in doos'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2131-3_1.jpg'
art_nr: 2131
slug: handdoekjes-multifold-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 3036
unit_price: 0
vip_price: 3946
prod_img: prod_img/2131-3_1.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729239474
product_type: physical
---
