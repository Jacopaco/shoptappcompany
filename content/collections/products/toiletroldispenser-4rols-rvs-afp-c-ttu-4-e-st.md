---
id: '3301503'
blueprint: products
art_nr: '3301503'
title: 'Toiletroldispenser 4rols RVS afp-c, TTU 4 E ST'
slug: toiletroldispenser-4rols-rvs-afp-c-ttu-4-e-st
price: 32330
cost_price: 19398
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
prod_img: prod_img/3301503.JPG
unit_price: 32330
vip_price: 29097
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
