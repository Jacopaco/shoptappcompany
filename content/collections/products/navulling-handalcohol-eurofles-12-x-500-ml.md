---
id: '98995'
blueprint: products
title: 'ALL-CARE handalcohol 70% - 1x500 ml'
price: 673
description: |-
  Handalcohol 70%, Eurofles van 500 ml, desinfecterende en ontsmettende alcohol voor uw handen.

  Toelatingsnummer Ctgb 15097
  Gebruik biociden veilig. Lees vóór gebruik eerst het etiket en de productinformatie.
  Toegestaan is uitsluitend het gebruik als middel ter bestrijding van bacteriën (excl. bacteriesporen en mycobacteriën) en gisten voor hygiënische en chirurgische handdesinfectie in de gezondheidszorg. De gebruiksaanwijzing moet worden aangehouden. Het middel is uitsluitend bestemd voor professioneel gebruik.

  Gebruiksaanwijzing
  Het middel is gebruiksklaar. Het middel toepassen op schone en droge handen en onderarmen. Hygiënische handdesinfectie Vooraf de handen eerst reinigen met water en zeep, naspoelen met water en vervolgens goed drogen. 3 ml middel op de hand nemen en gedurende 30 seconden intensief uitwrijven over beide handen en alle huidplooien goed raken. Zorg dat de handen gedurende de gehele inwerktijd vochtig blijven. Daarna handen goed laten drogen. Dosering: 3 ml middel voor beide handen samen (dispenser instellen op 3 ml; dan 1 x pompen) Minimale inwerktijd: 30 seconden Chirurgische handdesinfectie Vooraf de handen met chirurgische zeep wassen, goed afspoelen met water en afdrogen. 6 ml middel op de hand nemen en gedurende 1,5 minuut intensief inwrijven over beide handen, onderarmen, polsen, nagels en ruimte tussen de vingers. Zo nodig herhalen. Zorg dat de handen gedurende de gehele inwerktijd vochtig blijven. Na afloop van de inwerktijd handen goed laten drogen. Dosering: 6 ml middel voor beide handen samen (dispenser instellen op 3 ml; dan 2 x pompen) Minimale inwerktijd: 1,5 minuut.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/Handalcohol1_1.jpg'
art_nr: 98995
slug: handalcohol-500ml-70
brands: 4de0e459-caba-4ced-b92b-af23ce5c7614
categories: e912fdd5-abf6-4aeb-8d04-d94cf86723ed
cost_price: 310
unit_price: 495
vip_price: 561
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729232105
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 5162e19a-84e6-4f47-83e6-760863dc5c91
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
prod_img: prod_img/98995_001.jpg
product_type: physical
---
