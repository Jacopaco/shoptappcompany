---
id: '8403'
blueprint: products
art_nr: '8403'
title: 'Hygiënebak + zakjeshouder 6 liter RVS, MQWB6HBKE'
slug: hygienebak-zakjeshouder-6-liter-rvs-mqwb6hbke
price: 20440
cost_price: 12264
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
prod_img: prod_img/8403.JPG
unit_price: 20440
vip_price: 18396
---
