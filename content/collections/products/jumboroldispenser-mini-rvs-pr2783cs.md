---
id: '13712'
blueprint: product
art_nr: '13712'
title: 'Jumboroldispenser mini RVS, PR2783CS'
slug: jumboroldispenser-mini-rvs-pr2783cs
cost_price: 5166
unit_price: 8610
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8610
vip_price: 7749
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Toiletpapier is een onmisbaar item in elke toiletruimte. Wat is er dan prettiger als de rol ook nog eens op aantrekkelijke wijze aan u gepresenteerd wordt?

  U hoeft geen grootverbruiker te zijn om de voordelen van deze mini jumboroldispenser te zien. Het strak vormgegeven rvs omhulsel verbergt de rol subtiel uit het oog van uw bezoeker. De praktische kijkgleuven aan de zijkant van de dispenser zorgen voor een eenvoudige inhoudscontrole. Zo wordt uw bezoeker nooit geconfronteerd met een lege toiletrol.

  Kies voor de subtiele vormgeving van de kleine Mediclinics jumboroldispenser.
prod_img: prod_img/afbeelding-pr2783cs-1719320821.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320825
---
