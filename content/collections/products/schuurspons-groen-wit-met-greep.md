---
id: '3826120628'
blueprint: products
art_nr: '3826120628'
title: 'Schuurspons groen / wit met greep'
slug: schuurspons-groen-wit-met-greep
cost_price: 39
unit_price: 59
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 66
vip_price: 55
prod_img: prod_img/3826120628.jpg
---
