---
id: '11062'
blueprint: product
art_nr: '11062'
title: 'Afvalbak 65 liter RVS, PP0065CS'
slug: afvalbak-65-liter-rvs-pp0065cs
cost_price: 25739
unit_price: 42900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 42900
vip_price: 38610
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Deze rvs afvalbak is een regelrechte eyecatcher in uw interieur. Door het robuuste uiterlijk en de ruime inhoud, is de bak niet alleen een stijlvol maar ook een praktische aanvulling in uw sanitaire ruimte. De afvalbak wordt afgesloten door een pushklep, die een hygiënische afdekking van uw afval waarborgt.

  Specificaties
  - Artikelnummer 11062
  - Model PP0065CS
  - Materiaal RVS
  - Hoogte 670 mm
  - Breedte 354 mm
  - Diepte 300 mm
  - Gewicht 8,4 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Toepassing Vrijstaand
  - Uitvoering Pushklep
  - Inhoud  65 liter
prod_img: prod_img/11062_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718805783
---
