---
id: 2201444-afp-c
blueprint: products
art_nr: '2201444 AFP-C'
title: 'Toiletrolhouder (doprollen) 2rols RVS'
slug: toiletrolhouder-doprollen-2rols-rvs
price: 22030
cost_price: 13218
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/2201444-afp-c.JPG
unit_price: 22030
vip_price: 19827
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
