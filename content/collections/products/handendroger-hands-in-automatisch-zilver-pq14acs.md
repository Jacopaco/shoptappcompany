---
id: '12470'
blueprint: product
art_nr: '12470'
title: 'Handendroger HANDS-IN automatisch zilver, PQ14ACS'
slug: handendroger-hands-in-automatisch-zilver-pq14acs
cost_price: 61500
unit_price: 82000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 82000
vip_price: 73800
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Al brengt u misschien niet zoveel tijd door in uw sanitaire ruimte, u wilt dat alles perfect in orde is.

  De stijlvolle sanitaire lijn van PlastiQline biedt u en uw gasten praktische én smaakvolle hygiëneoplossingen.
  Na een toiletbezoek wilt u zo snel mogelijk weer aan het werk. Daarom is een efficiënte en hygiënische afwikkeling van uw toiletbezoek een vereiste.
  De Twinflow handendroger van All Care waarborgt een hygiënische en snelle afhandeling van uw toiletbezoek.
  Door de optimale droogsnelheid, staat u binnen een halve minuut weer buiten. De droger bevat een speciale antibacteriële Biocote bescherming,
  wat een hygiënisch gebruik van de droger waarborgt.

  Medewerkers en gasten zullen uw keuze voor de Twinflow handendroger waarderen.
prod_img: prod_img/pq14acs.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477428
---
