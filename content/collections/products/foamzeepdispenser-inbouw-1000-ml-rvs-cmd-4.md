---
id: '24400987'
blueprint: products
art_nr: '24400987'
title: 'Foamzeepdispenser inbouw 1000 ml RVS, CMD 4'
slug: foamzeepdispenser-inbouw-1000-ml-rvs-cmd-4
cost_price: 4164
unit_price: 8720
brands: 521c8220-6d99-40b7-a175-bb68f20e6cf3
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 8720
vip_price: 7848
prod_img: prod_img/24400987.png
---
