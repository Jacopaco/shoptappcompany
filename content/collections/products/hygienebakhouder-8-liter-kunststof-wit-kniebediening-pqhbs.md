---
id: '5667'
blueprint: products
art_nr: '5667'
title: 'Hygiënebak+houder 8 liter kunststof wit kniebediening, PQHBS'
slug: hygienebakhouder-8-liter-kunststof-wit-kniebediening-pqhbs
price: 8150
cost_price: 5135
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiëne-afvalbak voor wandmontage met kniebediening.
  De klepdeksel klapt open, zodra de onderbak met de knie wordt aangeduwd.
  Deksel klapt automatische weer terug, zodra de onderbak wordt losgelaten.
  Voorzien van een uitneembare binnenbak met vier zijflappen.
  Inclusief gemonteerde hygiënezakjeshouder voor plastic hygiënezakjes aan de zijkant.
prod_img: prod_img/5667.JPG
unit_price: 8150
vip_price: 7335
---
