---
id: '331421703'
blueprint: products
art_nr: '331421703'
title: 'Hygiënebak 6 liter RVS afp-c, HWU 6 E ST'
slug: hygienebak-6-liter-rvs-afp-c-hwu-6-e-st
price: 26220
cost_price: 15732
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/331421703.JPG
unit_price: 26220
vip_price: 23598
---
