---
id: '25400008'
blueprint: products
art_nr: '25400008'
title: 'Untouchable MWSD bent Classic'
slug: untouchable-mwsd-bent-classic
price: 104900
cost_price: 49800
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
categories: bc9df93b-d180-4c05-8fd8-719b6f9e1b0b
prod_img: prod_img/25400008.JPG
unit_price: 104900
vip_price: 104900
---
