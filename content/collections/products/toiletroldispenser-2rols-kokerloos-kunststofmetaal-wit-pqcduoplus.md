---
id: '5586'
blueprint: products
art_nr: '5586'
title: 'Toiletroldispenser 2rols (kokerloos) kunststof/metaal wit, PQCDuoPlus'
slug: toiletroldispenser-2rols-kokerloos-kunststofmetaal-wit-pqcduoplus
price: 5900
cost_price: 4400
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Toiletroldispenser voor kokerloze rollen voor wandmontage.
  De toiletrollen zitten om 2 kunststof pinnen, die zorgen voor automatische toiletrolwisseling.
  Pinnen blijven in de dispenser zitten.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster,
  op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Modulair systeem.
prod_img: prod_img/5586.JPG
unit_price: 5900
vip_price: 5310
---
