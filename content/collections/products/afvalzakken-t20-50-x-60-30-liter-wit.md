---
id: '2256'
published: false
blueprint: products
art_nr: '2256'
title: 'Afvalzakken T20 50 x 60 | 30 liter wit'
slug: afvalzakken-t20-50-x-60-30-liter-wit
price: 5342
cost_price: 2910
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
description: 'Doos van 1000 stuks'
prod_img: prod_img/2256.jpg
unit_price: 0
vip_price: 4452
---
