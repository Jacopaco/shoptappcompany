---
id: '17140'
blueprint: product
art_nr: '17140'
title: 'Haardroger zwart/chroom, Action 1200 Push'
slug: haardroger-zwartchroom-action-1200-push
cost_price: 2007
unit_price: 5300
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 5300
vip_price: 4770
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Action 1200 ABS kunststof haardroger. 1200 Watt. Voorzien van een spiraalsnoer en een smal mondstuk. Voorzien van een schuifschakelaar met 2 standen voor de luchtopbrengst, 3 standen voor de temperatuur en een koude luchtstand.

  - Artikelnummer 17140
  - Model Action 1200 Push
  - Materiaal Kunststof
  - Kleur Zwart/Chroom
  - Hoogte 240 mm
  - Breedte 180 mm
  - Diepte 85 mm
  - Gewicht 0,6 kg
  - Garantie 1 jaar na aankoopdatum
  - Toepassing Handmodel
  - Eigenschappen 2 standen luchtopbrengst, 3 standen temperatuur, koude luchtstand
  - Wattage 1200 Watt
  - Toepassing Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
prod_img: prod_img/17140_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714739238
---
