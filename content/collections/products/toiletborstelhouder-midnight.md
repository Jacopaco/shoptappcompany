---
id: wbu-3-p-mn
published: false
blueprint: products
art_nr: 'WBU 3 P MN'
title: 'Toiletborstelhouder Midnight'
slug: toiletborstelhouder-midnight
price: 17680
cost_price: 10608
brands: f77a48e2-462d-4719-8961-2637e36823d5
unit_price: 17680
vip_price: 15912
prod_img: prod_img/wbu-3-p-mn.JPG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685615302
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
