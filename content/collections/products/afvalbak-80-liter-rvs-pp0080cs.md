---
id: '11072'
blueprint: product
art_nr: '11072'
title: 'Afvalbak 80 liter RVS, PP0080CS'
slug: afvalbak-80-liter-rvs-pp0080cs
cost_price: 28500
unit_price: 47500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 47500
vip_price: 42750
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak RVS 80 liter gesloten, PP0080CS
  Afvalbak met push klep.
  Staat op 4 kunststof voetjes.
  Deksel is in zijn geheel van de onderbak af te nemen.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/mediclinics-pp0080cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845639
---
