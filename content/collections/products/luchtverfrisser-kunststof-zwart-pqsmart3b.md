---
id: '14249'
blueprint: products
art_nr: '14249'
title: 'Luchtverfrisser kunststof zwart, PQSmart3B'
slug: luchtverfrisser-kunststof-zwart-pqsmart3b
price: 5570
cost_price: 3342
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
description: |-
  Instelbare luchtverfrisser voor wandmontage.
  Met slot en sleutel.
  Met automatische ventilator, die de geur van de bijbehorende geurpotjes gelijkmatig verspreidt.
  Met raster openingen aan beide zijkanten, voorzijde en onderzijde voor optimale geurbeleving.
  Werkt op 2 stuks LR20 batterijen.
prod_img: prod_img/14249.JPG
unit_price: 5570
vip_price: 5013
---
