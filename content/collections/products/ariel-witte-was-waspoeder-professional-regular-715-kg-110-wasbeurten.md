---
id: '81010003'
blueprint: product
art_nr: '81010003'
title: 'Ariel (witte was) waspoeder Professional Regular 7,15 kg (110 wasbeurten)'
slug: ariel-witte-was-waspoeder-professional-regular-715-kg-110-wasbeurten
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
cost_price: 3176
unit_price: 4128
price: 5411
vip_price: 4509
description: |-
  Ga voor de beste wasbeurt voor uw witte was met de Ariel Regular waspoeder. Het waspoeder van Ariel reinigt tot diep in de vezels om hardnekkige vlekken te verwijderen en te voorkomen dat de vlekken intrekken. Zelfs het verwijderen van nare luchtjes doet de waspoeder moeiteloos en deze geuren worden zelfs vervangen voor een heerlijke schone geur. Het poeder lost snel op, zodat deze geen sporen achterlaat op uw kleding. Zelfs op 30 graden werkt het waspoeder effectief.

  De doos heeft een inhoud van 5,85 kilo. Dit is voldoende waspoeder voor 90 wasbeurten.
locations:
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
prod_img: 'prod_img/ariel-(witte-was)-waspoeder-professional-regular-7,15-kg-(110-wasbeurten).jpg'
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727268199
---
