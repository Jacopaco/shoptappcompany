---
id: '13118'
blueprint: product
art_nr: '13118'
title: 'Toiletroldispenser 2rols RVS, PR2784CS'
slug: toiletroldispenser-2rols-rvs-pr2784cs
cost_price: 6600
unit_price: 11000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11000
vip_price: 9900
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Een noodzakelijke basisvoorziening, met de uitstraling van topdesign. De elegante 2-rolshouder rvs van Mediclinics biedt alles wat een toiletbezoeker nodig heeft.

  Met de 2-rolshouder rvs behoort een lege toiletrol tot het verleden. Het strak vormgegeven frame houdt op een subtiele manier de toiletrollen uit het oog van de toiletbezoeker. Via de creatief ontworpen uitsparing aan de onderzijde beschikt de toiletbezoeker precies over de gewenste hoeveelheid toiletpapier.
prod_img: prod_img/toilet-paper-dispensers-pr2784cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719476765
---
