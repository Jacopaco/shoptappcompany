---
id: 100-111
blueprint: products
art_nr: 100-111
title: 'ROVEQ Hygiënische reiniger 1 ltr'
slug: roveq-hygienische-reiniger-1-ltr
cost_price: 417
unit_price: 650
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 710
vip_price: 592
prod_img: prod_img/100-111.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1696237739
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
---
