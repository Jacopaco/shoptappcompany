---
id: '2142'
blueprint: products
title: 'STARLIGHT 2142-FSC handdoekjes 32x22 cm - 3 laags - 2500 stuks'
price: 4290
description: |-
  Handdoekjes interfold cellulose 3 laags 32 x 22 cm

  Code 2142-FSC
  Materiaal cellulose
  Kleur wit
  Afmetingen vel 32 x 22 cm
  3 laags
  Verpakking 20 x 125 vel in doos
  FSC certificaat

  Deze Interfold handdoekjes zijn meermaals gevouwen, en hebben een lange lengte waardoor je minder handdoekjes nodig hebt.

  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2135_3_2.jpg'
art_nr: 2142
slug: handdoekjes-interfold-cellulose-3laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 2750
unit_price: 0
vip_price: 3575
prod_img: prod_img/2135_3_2-1688653542.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729000897
client_prices:
  - f4210288-9577-4af7-80f5-49ae25057d41
  - 4e00303b-45d0-49d8-9aad-8a8ef0e22948
  - c3ed1408-6d23-4d75-bab0-4752835fb279
  - 23bd866b-4d41-4746-9cf0-b764f3eee09a
  - 691ea65f-48c3-4e02-82bb-23afe017ed72
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - f4210288-9577-4af7-80f5-49ae25057d41
locations:
  - 1cbc4fc8-faea-4a79-a9c1-410949fac628
  - 225a4a10-f583-4663-aa73-07579bc9b443
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
product_type: physical
---
