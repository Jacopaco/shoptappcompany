---
id: '5527'
blueprint: products
art_nr: '5527'
title: 'Luchtverfrisser kunststof wit, PQGNTL'
slug: luchtverfrisser-kunststof-wit-pqgntl
price: 5965
cost_price: 3758
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
description: |-
  Luchtverfrisser voor wandmontage.
  Met slot en witte PQ sleutel.
  Met automatische ventilator, die de geur van de bijbehorende geurpotjes gelijkmatig verspreidt.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Geschikt voor bedrukking met uw logo.
  Werkt op 2 stuks LR20 batterijen (worden niet meegeleverd).
  Met opening aan de bovenzijde voor maximale geurbeleving. 
  Eén luchtverfrisser is geschikt voor ca. 40 m3. 
  Geschikt voor: de geurpotjes blue note, sun green en grapefruit.
prod_img: prod_img/5527.jpg
unit_price: 5965
vip_price: 5368
---
