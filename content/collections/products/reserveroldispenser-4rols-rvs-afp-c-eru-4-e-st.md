---
id: 21421868-afp-c
blueprint: products
art_nr: '21421868 AFP-C'
title: 'Reserveroldispenser 4rols RVS afp-c, ERU 4 E ST'
slug: reserveroldispenser-4rols-rvs-afp-c-eru-4-e-st
price: 9190
cost_price: 4963
brands: f77a48e2-462d-4719-8961-2637e36823d5
description: |-
  Reserverolhouder voor wandmontage.
  Voor 4 standaard toiletrollen.
  Uitgifte onderaan.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21421868-afp-c.JPG
unit_price: 9190
vip_price: 8271
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
