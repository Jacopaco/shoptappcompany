---
id: cart00917
blueprint: products
title: 'Diversey Suma Bac D10 SmartDose 2x1,4ltr'
price: 11384
description: 'Reinigings- en desinfectiemiddel.'
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515955
img:
  - product_images/suma.PNG
art_nr: CART00917
prod_img: prod_img/cart00917.png
slug: diversey-suma-bac-d10-smartdose-2x14ltr
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 6681
unit_price: 7138
vip_price: 9487
client_prices:
  - 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
  - 023cf6bd-fb56-4ebc-9abb-f8e9c798062f
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
