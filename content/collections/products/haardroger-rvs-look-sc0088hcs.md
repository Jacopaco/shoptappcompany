---
id: '12009'
blueprint: product
art_nr: '12009'
title: 'Haardroger RVS look, SC0088HCS'
slug: haardroger-rvs-look-sc0088hcs
cost_price: 30745
unit_price: 59900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 59900
vip_price: 53910
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Haardroger drukknop RVS look, SC0088HCS
  Wandhaardroger.
  Met een drukknopbediende timer van 90 seconden.
  Met draaibare tuit.
  Bruto luchtopbrengst ca. 90l/sec.
  Luchtsnelheid: 100 km/u.
  Luchttemperatuur 53˚C.
  70dB.
prod_img: prod_img/mediclinics-sc0088hcs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479637
---
