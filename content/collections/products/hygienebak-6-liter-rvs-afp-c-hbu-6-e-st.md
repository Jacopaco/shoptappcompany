---
id: 21421703-afp-c
blueprint: products
art_nr: '21421703 AFP-C'
title: 'Hygiënebak 6 liter RVS afp-c, HBU 6 E ST'
slug: hygienebak-6-liter-rvs-afp-c-hbu-6-e-st
price: 26220
cost_price: 14315
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiënebak 6 liter.
  Met geïntegreerde hygiënezakjeshouder voor plastic zakjes.
  Cartridge wordt aan de voorzijde erin geschoven en wordt op z'n plaats gehouden door een veer.
  Met klepdeksel en binnenring.
  Voor wandmontage. Minimaal 20 cm boven de grond monteren.
  Deksel is makkelijk afneembaar van de onderbak.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21421703-afp-c.JPG
unit_price: 26220
vip_price: 23598
---
