---
id: '21422855'
blueprint: products
art_nr: '21422855'
title: 'Foamzeepdispenser 600 ml RVS zwart, NSU 5 P/F MN'
slug: foamzeepdispenser-600-ml-rvs-zwart-nsu-5-pf-mn
cost_price: 8536
unit_price: 17900
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 17900
vip_price: 16110
prod_img: prod_img/21422855.png
---
