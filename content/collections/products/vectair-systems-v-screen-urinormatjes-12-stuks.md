---
id: '2833601'
blueprint: products
art_nr: '2833601'
title: 'Vectair Systems V-Screen urinormatjes 12 stuks'
slug: vectair-systems-v-screen-urinormatjes-12-stuks
price: 2229
cost_price: 1429
brands: 3375fa99-d12c-4998-8110-4d347cec808b
prod_img: prod_img/2833601.JPG
unit_price: 1429
vip_price: 1857
categories: 946cacfa-7f29-411b-814d-57702d68b4f7
---
