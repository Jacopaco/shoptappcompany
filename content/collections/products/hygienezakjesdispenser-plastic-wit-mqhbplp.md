---
id: '8285'
blueprint: products
art_nr: '8285'
title: 'Hygiënezakjesdispenser (plastic) wit, MQHBPLP'
slug: hygienezakjesdispenser-plastic-wit-mqhbplp
price: 4825
cost_price: 2895
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënezakjesdispenser voor montage op een afvalbak of aan de wand.
  Uitname aan de voorzijde, navulling vanaf de onderzijde.
  Met 2puntsbevestiging.
prod_img: prod_img/8285.JPG
unit_price: 4825
vip_price: 4342
---
