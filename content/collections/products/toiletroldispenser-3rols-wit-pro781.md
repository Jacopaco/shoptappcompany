---
id: '13311'
blueprint: product
art_nr: '13311'
title: 'Toiletroldispenser 3rols wit, PRO781'
slug: toiletroldispenser-3rols-wit-pro781
cost_price: 3990
unit_price: 6650
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6650
vip_price: 5985
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Wanneer u veel waarde hecht aan een propere en hygiënische omgeving, zal de witte accessoirelijn van Mediclinics u ongetwijfeld aanspreken. De serie is een subtiele mix van eenvoud, properheid en elegant design.

  Deze 3rolshouder van Mediclinics is een van de bestsellers van de witte accessoirelijn van Mediclinics. De houder biedt ruimte aan drie standaardtoiletrollen, die eenvoudig verwisselbaar zijn door de bovenste rol aan te duwen. Wanneer de rollen aangevuld moeten worden gebruikt u de subtiel weggewerkte opening aan de voorzijde van de houder.
prod_img: prod_img/afbeelding-pro781-1719322161.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322169
---
