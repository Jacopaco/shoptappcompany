---
id: '5665'
blueprint: products
art_nr: '5665'
title: 'Afvalbak 8 liter kunststof wit kniebediening, PQKBS'
slug: afvalbak-8-liter-kunststof-wit-kniebediening-pqkbs
price: 7100
cost_price: 4473
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak voor wandmontage met kniebediening.
  De klepdeksel klapt open, zodra de onderbak met de knie wordt aangeduwd.
  Deksel klapt automatische weer terug, zodra de onderbak wordt losgelaten.
  Voorzien van een uitneembare binnenbak met vier zijflappen.
  Uitermate geschikt voor montage op medicijnkarren etc.
prod_img: prod_img/5665.JPG
unit_price: 7100
vip_price: 6390
---
