---
id: '23400757'
blueprint: products
art_nr: '23400757'
title: 'Reserverolhouder 4-6 rollen RVS, R 04 E'
slug: reserverolhouder-4-6-rollen-rvs-r-04-e
cost_price: 3459
unit_price: 7900
brands: 521c8220-6d99-40b7-a175-bb68f20e6cf3
price: 7900
vip_price: 7110
prod_img: prod_img/23400757.png
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
