---
id: '8340'
blueprint: products
art_nr: '8340'
title: 'Tampon- en maandverbanddispenser RVS, MQSTD E'
slug: tampon-en-maandverbanddispenser-rvs-mqstd-e
price: 17900
cost_price: 10740
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Geschikt voor wandmontage.
  Geschikt voor ca. 70-80 tampons van normale grootte.
  Geschikt voor ca. 25 maandverband van normale grootte
  Maandverband dient los in de dispenser geplaatst te worden.
  Eenvoudige uitname van het maandverband.
  Overzichtelijke inhoudscontrole.
  H 250 x B 250 x D 101 mm
prod_img: prod_img/8340.JPG
unit_price: 17900
vip_price: 16110
---
