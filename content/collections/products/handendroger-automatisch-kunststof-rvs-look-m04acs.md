---
id: '12105'
blueprint: product
art_nr: '12105'
title: 'Handendroger automatisch kunststof RVS look, M04ACS'
slug: handendroger-automatisch-kunststof-rvs-look-m04acs
cost_price: 9015
unit_price: 16390
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16390
vip_price: 14751
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Handendroger RVS-look automatisch, M04ACS (Smart Flow)
  Automatische handendroger 1100 Watt.
  Bruto luchtopbrengst ca. 27l/sec.
  Luchtsnelheid: 85 km/u.
  Luchttemperatuur 47˚C.
  60dB.
  Droogtijd: 39 seconden.
prod_img: prod_img/afbeelding-m04acs-1719479100.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479106
---
