---
id: ref5011
blueprint: products
art_nr: REF5011
title: 'Steel metaal grijs/groen met schroefdraad 150cm (Stuks)'
slug: steel-metaal-grijsgroen-met-schroefdraad-150cm-stuks
price: 7.8
cost_price: 5
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/ref5011.jpg
unit_price: 5
vip_price: 6.5
---
