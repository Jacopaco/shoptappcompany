---
id: '14225'
blueprint: products
art_nr: '14225'
title: 'Zeepdispenser 750 ml automatisch kunststof wit, AC 750 W'
slug: zeepdispenser-750-ml-automatisch-kunststof-wit-ac-750-w
price: 8700
cost_price: 5220
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser automatisch.
  Met slot en sleutel.
  Wordt met navulfles geleverd.
  Met pictogram aan de voorzijde.
  Werkt op 3 stuks LR14 batterijen.
  Wordt excl. batterijen geleverd. 
prod_img: prod_img/14225.JPG
unit_price: 8700
vip_price: 7830
---
