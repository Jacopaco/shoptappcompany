---
id: 21413898-afp-c
blueprint: products
art_nr: '21413898 AFP-C.'
title: 'Toilet seat cleaner 600 ml RVS afp-c, TSU 5 E/D ST'
slug: toilet-seat-cleaner-600-ml-rvs-afp-c-tsu-5-ed-st
price: 16640
cost_price: 8986
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Toilet seat cleaner voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Met instructiepictogram op de dispenser.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21413898-afp-c..JPG
unit_price: 16640
vip_price: 14976
---
