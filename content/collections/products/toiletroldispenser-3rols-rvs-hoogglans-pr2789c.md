---
id: '13741'
blueprint: product
art_nr: '13741'
title: 'Toiletroldispenser 3rols RVS hoogglans, PR2789C'
slug: toiletroldispenser-3rols-rvs-hoogglans-pr2789c
cost_price: 5988
unit_price: 9980
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9980
vip_price: 8982
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Het is natuurlijk prettig als uw toiletbezoek zo vloeiend mogelijk verloopt. Een goed georganiseerde sanitaire ruimte is daarbij onmisbaar. Met deze 3rolshouder weet u zeker dat u niet geconfronteerd wordt met ‘het laatste velletje’.

  Dat de Mediclinics-lijn niet allen praktisch, maar ook qua design een aantrekkelijk item is, bewijst deze 3rolshouder. De ronde, strak vormgegeven cover biedt ruimte aan drie standaardtoiletrollen, die eenvoudig verwisselbaar zijn. De inhoud is makkelijk te controleren via de kijkgleuf. Hierdoor kunt u tijdig de rollen wisselen, en hebben uw gasten altijd de beschikking over voldoende toiletpapier.

  Op zoek naar een eenvoudige toiletrolhouder met optimaal gebruiksgemak? Kies dan voor de charme van de Mediclinics-lijn.
prod_img: prod_img/afbeelding-pr2789c-1719320683.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320687
---
