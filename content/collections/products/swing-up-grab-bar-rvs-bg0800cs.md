---
id: '12610'
blueprint: products
art_nr: '12610'
title: 'Swing up grab bar RVS, BG0800CS'
slug: swing-up-grab-bar-rvs-bg0800cs
price: 18900
cost_price: 11340
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Swing up bar RVS.
  Beugel kan omhoog gezet worden.
  Met veiligheidssysteem, die ervoor zorgt dat de beugel omhoog blijft staan en niet naar beneden
  valt.
  Met geïntegreerde 1rolshouder.
unit_price: 18900
vip_price: 17010
prod_img: prod_img/12610.JPG
---
