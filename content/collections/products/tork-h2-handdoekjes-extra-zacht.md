---
id: '100297'
blueprint: products
art_nr: '100297'
title: 'Tork 100297,Multifold H2 handdoekjes 34x21.2 cm - 2 laags - 2100 stuks'
slug: tork-h2-handdoekjes-extra-zacht
cost_price: 4475
unit_price: 5817
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
price: 6981
vip_price: 5817
description: |-
  Bied uw gasten superieure handdroging en optimaal comfort met de grote, zachte Premium Tork Xpress Zachte Multifold Handdoeken, die hoogwaardig aanvoelen en zacht zijn voor de huid. Deze handdoeken zijn geschikt voor de Tork Xpress® Multifold Handdoek Dispenser, voor sanitaire ruimten met een middelgroot aantal bezoekers. Hij past in kleine ruimtes en biedt uw gasten zowel comfort als hygiëne.

  Een grote, zachte handdoek van uitstekende kwaliteit die een blijvende indruk maakt. QuickDry ons sterkste en meest absorberende papier, voor efficiënter drogen met minder verspilling. De handdoeken zijn meermaals gevouwen voor vel-voor-vel-dosering, waardoor het verbruik wordt verminderd en de hygiëne verbeterd. Een aantrekkelijk bladontwerp van Tork dat een geweldige indruk maakt. Tork Easy Handling Draagpakketten voor het eenvoudig dragen, openen en weggooien van de verpakking.

  Specificaties
  Artikel 100297
  Uitgevouwen lengte 34 cm
  Systeem H2 - Intergevouwen handdoek systeem
  Uitgevouwen breedte 21.2 cm
  Gevouwen lengte 8.5 cm
  Gevouwen breedte 21.2 cm
  Lagen 2
  Print Ja
  Reliëf Ja
  Kleur Wit
prod_img: prod_img/839100297_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729000730
---
