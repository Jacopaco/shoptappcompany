---
id: '6532'
published: false
blueprint: products
art_nr: '110339002'
title: 'HDPE afvalzak 63x70cm T15 zwart (40x25) 1000 stuks'
slug: afvalzakken-t25-70-x-110-cm-blauw
price: 3855
cost_price: 2100
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
description: '20 x 25 in doos'
unit_price: 3700
vip_price: 3213
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732787070
---
