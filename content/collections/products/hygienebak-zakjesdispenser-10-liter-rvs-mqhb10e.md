---
id: '8260'
blueprint: products
art_nr: '8260'
title: 'Hygiënebak + zakjesdispenser 10 liter RVS, MQHB10E'
slug: hygienebak-zakjesdispenser-10-liter-rvs-mqhb10e
price: 23615
cost_price: 14169
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënebak met gesloten inworpklep en gemonteerde zakjeshouder.
  Met binnenring.
  Deksel is afneembaar voor het wisselen van de afvalzakken.
  Met RVS hendel voor het openen van de deksel.
prod_img: prod_img/8260.JPG
unit_price: 23615
vip_price: 21253
---
