---
id: '1002320'
blueprint: products
title: 'LDPE afvalzakken Blauw 70x110cm T70'
price: 60.47784
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688650834
categories: 2b3ca86a-26af-461e-b819-7415564e5b92
art_nr: '1002320'
slug: ldpe-afvalzakken-blauw-70x110cm-t70
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 32.94
unit_price: 32.94
vip_price: 50.3982
prod_img: prod_img/1002320.jpg
locations:
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - e6713451-367d-43ef-91ec-87d84a4c609f
---
