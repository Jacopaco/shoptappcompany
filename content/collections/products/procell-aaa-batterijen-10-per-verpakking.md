---
id: '8219161096'
blueprint: products
art_nr: '8219161096'
title: 'Procell AAA batterijen 10 per verpakking'
slug: procell-aaa-batterijen-10-per-verpakking
price: 5.46
cost_price: 3.5
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/8219161096.jpg
unit_price: 6.53
vip_price: 4.55
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
