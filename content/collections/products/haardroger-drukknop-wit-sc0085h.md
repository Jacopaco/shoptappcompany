---
id: '12005'
blueprint: product
art_nr: '12005'
title: 'Haardroger drukknop wit, SC0085H'
slug: haardroger-drukknop-wit-sc0085h
cost_price: 43945
unit_price: 79900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 79900
vip_price: 71910
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/mediclinics-sc0085h.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479583
---
