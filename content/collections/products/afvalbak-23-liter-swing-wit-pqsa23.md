---
id: '5657'
blueprint: products
art_nr: '5657'
title: 'Afvalbak 23 liter swing wit, PQSA23'
slug: afvalbak-23-liter-swing-wit-pqsa23
price: 4570
cost_price: 2879
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v.
  bijgeleverde ophangbevestiging.
  Met swing deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
prod_img: prod_img/5657.JPG
unit_price: 4570
vip_price: 4113
---
