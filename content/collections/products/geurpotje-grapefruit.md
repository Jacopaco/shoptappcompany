---
id: '14245'
blueprint: products
art_nr: '14245'
title: 'GRAPEFRUIT SmartAir gel geurpotje'
slug: geurpotje-grapefruit
price: 600
cost_price: 360
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
description: |-
  De Grapefruit geur is gebaseerd op essentiële oliën afkomstig van vruchten en bloemen.
  Bevatten in tegenstelling tot andere geuren geen drijfgassen en synthetische geuren,
  wat bijdraagt aan een milieuvriendelijker wereld. Met inkeping aan de achterzijde
  Gaat ca. 30-60 dagen mee.
  Afname: per doos = 10 stuks
prod_img: prod_img/14245.JPG
unit_price: 600
vip_price: 600
client_prices:
  - 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
  - 831a2ffd-59ea-42dd-a908-fa75c5aff2a5
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
categories: f4cb9934-b741-4f26-bf31-378b213ae616
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729152908
---
