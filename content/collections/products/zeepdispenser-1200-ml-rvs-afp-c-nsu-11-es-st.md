---
id: 2110673-afp-c
blueprint: products
art_nr: '2110673 AFP-C.'
title: 'Zeepdispenser 1200 ml RVS afp-c, NSU 11 E/S ST'
slug: zeepdispenser-1200-ml-rvs-afp-c-nsu-11-es-st
price: 16799
cost_price: 9655
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2110673-afp-c..JPG
unit_price: 17680
vip_price: 13999
---
