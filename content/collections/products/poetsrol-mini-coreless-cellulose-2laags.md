---
id: '2183'
blueprint: products
title: 'Poetsrol mini coreless cellulose 2laags'
price: 35.69
description: '20 cm | 12 x 72 meter in folie | ø 12,5 cm'
img_url: 'https://all-care.eu/content/files/Images/TAPP_Company_BV/2182_2.jpg'
art_nr: 2183
slug: poetsrol-mini-coreless-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
cost_price: 25.49
prod_img: prod_img/2182_2-1.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653238
---
