---
id: 21245700-afp-c
blueprint: products
art_nr: '21245700 AFP-C.'
title: 'Hygiënezakjesdispenser (plastic) RVS afp-c, HB 2 E ST'
slug: hygienezakjesdispenser-plastic-rvs-afp-c-hb-2-e-st
price: 4480
cost_price: 2446
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënezakjeshouder voor plastic hygiënezakjes.
  Voor wandmontage of montage op een (hygiëne) afvalbak.
  Met rond gat voor het uitnemen van de zakjes.
  Vanaf onder navulbaar.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21245700-afp-c..JPG
unit_price: 4480
vip_price: 4032
---
