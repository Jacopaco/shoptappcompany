---
id: '11090'
blueprint: product
art_nr: '11090'
title: 'Hygiënebak 6 liter wit, PP0006'
slug: hygienebak-6-liter-wit-pp0006
cost_price: 5097
unit_price: 8495
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8495
vip_price: 7645
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  (Hygiëne)bak 6 liter wit, PP0006
  (Hygiëne)bak vrijstaand of voor wandmontage.
  Met klepdeksel.
prod_img: prod_img/mediclinics-pp0006.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719480774
---
