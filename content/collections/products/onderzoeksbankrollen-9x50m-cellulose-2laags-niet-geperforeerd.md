---
id: '6549'
blueprint: products
art_nr: '6549'
title: 'Onderzoeksbankrollen 9x50M cellulose 2laags niet geperforeerd'
slug: onderzoeksbankrollen-9x50m-cellulose-2laags-niet-geperforeerd
price: 4141
cost_price: 2030
brands: d2e2fc4b-bc98-488c-90a2-f0adb231736a
categories: 8218d088-f732-41b2-a36e-13cfec06fed9
unit_price: 7900
vip_price: 3451
prod_img: prod_img/6549.jpg
client_prices:
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - bebc5c46-0553-4d62-b6ff-de33648493c4
locations:
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
---
