---
id: cart00959
published: false
blueprint: products
art_nr: '180202500'
title: 'LDPE afvalzak 60x80cm T50 grijs (20x20) 400 stuks'
slug: ldpe-afvalzak-60x80cm-t50-grijs-20x20-400-stuks
cost_price: 2177
unit_price: 3200
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
price: 3996
vip_price: 3330
locations:
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
  - c1577f6a-6b53-4c66-a7ad-2c69fb7df643
  - ad521bf5-2669-452d-81b0-0b55218e7ac4
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
prod_img: prod_img/img-(1).jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733305586
---
