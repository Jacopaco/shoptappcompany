---
id: '14283'
blueprint: product
art_nr: '14283'
title: 'Pictogram rond RVS VROUW, PS0002CS'
slug: pictogram-rond-rvs-vrouw-ps0002cs
cost_price: 780
unit_price: 1300
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 1300
vip_price: 1170
categories: 33e55e5a-60bb-4897-9444-80c306b1c4b5
description: |-
  Uw meeste gasten zullen het prettig vinden om bij binnenkomst snel te zien waar het toilet zich bevindt.

  Dit RVS pictogram met een strak design helpt uw gasten gemakkelijk het damestoilet te vinden. Het pictogram is eenvoudig te installeren en wordt geleverd inclusief sterke tape voor installatie op effen oppervlakten.
prod_img: prod_img/vrouw-1719323856.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323865
---
