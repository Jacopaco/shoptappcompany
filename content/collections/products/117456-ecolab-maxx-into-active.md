---
id: '117456'
blueprint: products
title: 'Ecolab MAXX Into Active '
price: 1303
description: '117456 Ecolab MAXX Into Active geconc. zure sanitairreiniger dagelijks Flacon 1L'
img:
  - product_images/ecolab-maxx-into-wc2-9084560.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515342
art_nr: '117456'
slug: ecolab-maxx-into-active
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 765
unit_price: 842
vip_price: 1086
prod_img: prod_img/117456.jpg
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
