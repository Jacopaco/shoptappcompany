---
id: '12375'
blueprint: product
art_nr: '12375'
title: 'Handendroger automatisch zwart, M06AB'
slug: handendroger-automatisch-zwart-m06ab
cost_price: 26125
unit_price: 47500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 47500
vip_price: 42750
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Mediclinics Speedflow handendroger met high speed motor. Warme lucht handendroger. 1150 Watt. Droogtijd: 10-12 seconde. Luchttemperatuur 41-44°C. Bediening met sensor. UNIVERSAL VOLTAGE 110-240 V. De droger herkent of de stroom 110V is, of 240V, of ergens daar tussenin en past zich daar automatisch op aan.

  De Mediclinics Speedflow®-lijn met handdrogers is gecategoriseerd binnen het eco-snelle assortiment van producten, maar met de toegevoegde waarde dat wordt voldaan aan de vereisten van ADA / UNE voor de toegankelijkheid van openbare wasruimten. Deze regeling vereist dat de diepte van de droger niet groter mag zijn dan 100 mm om toegang en verplaatsing van gehandicapte personen over de wasruimte te vergemakkelijken.

  Deze handendroger is ook ontworpen om milieuvriendelijk te zijn, inclusief een belangrijke toegevoegde waarde, zoals energiebesparing. Speedflow® is een droogoplossing die het energieverbruik en de CO2-uitstoot tot een minimum beperkt:

  - Met een totaal vermogen van 1150 W verbruikt het per droogcyclus gemiddeld 3,83 W, wat een energiebesparing oplevert van meer dan 80% ten opzichte van traditionele handdrogers (gemiddeld verbruik van 19-20 watt / cyclus).

  - Onder normale omstandigheden wordt geschat dat jaarlijks een traditionele handdroger gemiddeld 500 kg CO2 uitstoot. De Speedflow-lijn verbruikt gemiddeld 247 Kg CO2, wat een jaarlijkse emissievermindering van 51% betekent.

  ###Specificaties
  - Artikelnummer 12375
  - Model M06AB
  - Materiaal Staal
  - Kleur Zwart gepoedercoat
  - Hoogte 270 mm
  - Breedte 290 mm
  - Diepte 100 mm
  - Gewicht 3,8 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Eigenschappen Automatisch, 120-180 km/u, droogtijd: 10-12 seconden, 58-67 dB
  - Wattage 1150 Watt
prod_img: prod_img/12375_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714728078
---
