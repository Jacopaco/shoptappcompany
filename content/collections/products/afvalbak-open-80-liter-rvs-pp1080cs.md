---
id: '11075'
blueprint: product
art_nr: '11075'
title: 'Afvalbak open 80 liter RVS, PP1080CS'
slug: afvalbak-open-80-liter-rvs-pp1080cs
cost_price: 20397
unit_price: 33995
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 33995
vip_price: 30595
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak RVS 80 liter open, PP1080CS
  Afvalbak open.
  Staat op 4 kunststof voetjes.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/11075-pp1080cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845497
---
