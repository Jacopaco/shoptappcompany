---
id: '12927'
blueprint: product
art_nr: '12927'
title: 'Handdoekdispenser inbouw RVS, DTM2106'
slug: handdoekdispenser-inbouw-rvs-dtm2106
cost_price: 5202
unit_price: 8670
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8670
vip_price: 7803
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  - Handmatige dispenser voor papieren handdoekjes, te installeren achter een spiegel, met een hoogwaardige AISI 304 roestvrijstalen behuizing, ontworpen en vervaardigd door Mediclinics SA
  - Ontworpen om net achter een spiegel en boven de wastafel in de badkamer te worden geïnstalleerd om de ruimte in de badkamer te maximaliseren.
  - Eenvoudig bij te vullen, weinig ruimte nodig en een uitstekende keuze voor installatie in minimalistische collectieve badkamers.
  - Het kan tot 600 Z-vouw of 400 C-vouw papieren handdoekjes bevatten, zonder dat er een papieren adapter nodig is.
  - Huis, achterplaat en papieruitlaat uit één stuk gevouwen en gelast AISI 304 roestvrij staal.
  - De geometrie van de papieruitvoer minimaliseert het scheuren van de papieren handdoek.
  - Schroeven en pluggen voor muurbevestiging meegeleverd.
prod_img: prod_img/dtm2106-1719477089.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477103
---
