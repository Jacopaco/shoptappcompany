---
id: '8421'
blueprint: products
art_nr: '8421'
title: 'Reserverolhouder 1rol aluminium, MQRRH1A'
slug: reserverolhouder-1rol-aluminium-mqrrh1a
price: 5205
cost_price: 3123
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 2puntsbevestiging.
prod_img: prod_img/8421.JPG
unit_price: 5205
vip_price: 4684
---
