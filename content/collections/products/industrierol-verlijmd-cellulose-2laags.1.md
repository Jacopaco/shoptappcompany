---
id: '2168'
blueprint: products
title: 'Industrierol verlijmd cellulose 2laags'
price: 30.45
description: '24 cm x 340 meter | 2 rollen in folie'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2168_1_2.jpg'
art_nr: 2168
slug: industrierol-verlijmd-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
cost_price: 21.75
prod_img: prod_img/2168_1_2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688654506
---
