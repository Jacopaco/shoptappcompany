---
id: '12630'
blueprint: products
art_nr: '12630'
title: 'Hoek grab bar 130° RVS, BA0130CS'
slug: hoek-grab-bar-130-rvs-ba0130cs
price: 7934
cost_price: 4761
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Angled bar RVS met hoek van 130˚.
  Beide stangen zijn 296 mm.
prod_img: prod_img/12630.JPG
unit_price: 7934
vip_price: 7140
---
