---
id: '5501'
blueprint: products
art_nr: '5501'
title: 'Spraydispenser 900 ml kunststof wit, PQSpray9'
slug: spraydispenser-900-ml-kunststof-wit-pqspray9
price: 3450
cost_price: 2174
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Spraydispenser voor wandmontage.
  Met push cover voor het doseren van de vulgoederen, duwrichting naar de muur.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Met slot en witte PQ sleutel.
  Met navulfles en spraypomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo. 
prod_img: prod_img/5501.JPG
unit_price: 3450
vip_price: 3105
---
