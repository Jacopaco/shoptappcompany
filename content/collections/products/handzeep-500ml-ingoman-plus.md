---
id: '98976'
blueprint: products
art_nr: '98976'
title: 'Handzeep 500ml Ingoman Plus'
slug: handzeep-500ml-ingoman-plus
price: 248
cost_price: 159
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: de901aec-80dd-40c6-be90-f2c5d8a24a1e
prod_img: prod_img/98976.jpg
unit_price: 159
vip_price: 206
updated_by: 4e27b41f-65ae-42df-bbc4-202ac3a07e17
updated_at: 1693384458
locations:
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
---
