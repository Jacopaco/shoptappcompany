---
id: '13214'
blueprint: product
art_nr: '13214'
title: 'Toiletroldispenser 2rols RVS hoogglans, PRO784C'
slug: toiletroldispenser-2rols-rvs-hoogglans-pro784c
cost_price: 5280
unit_price: 8800
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8800
vip_price: 7920
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Een lege rol toiletpapier kan voor flinke frustratie zorgen. En dat wilt u natuurlijk voorkomen. Met deze 2rolshouder heeft uw bezoeker altijd een reserverol voorhanden.

  Deze 2rolshouder geeft een tikje glamour is uw toilet. Het hoogglans uiterlijk zorgt voor een speelse knipoog in uw sanitaire ruimte. Zoals alle dispensers van de Mediclinics-serie is ook deze houder zeer eenvoudig in gebruik. U kunt de rollen aanvullen en verwisselen via de klep aan de bovenzijde van de dispenser. Via de kijkgleuven is de controle handig te controleren.
  Kortom, een dispenser met klasse.
prod_img: prod_img/afbeelding-pro784c-1719321917.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321922
---
