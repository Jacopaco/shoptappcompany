---
id: 21413894-afp-c
blueprint: products
art_nr: '21413894 AFP-C.'
title: 'Afvalbak open 18 liter RVS afp-c, EBU 18 E ST'
slug: afvalbak-open-18-liter-rvs-afp-c-ebu-18-e-st
price: 20010
cost_price: 10805
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak 18 liter.
  Met open cover en binnenring.
  Vrijstaand of voor wandmontage.
  Frame is makkelijk afneembaar van de onderbak.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21413894-afp-c.JPG
unit_price: 20010
vip_price: 18009
---
