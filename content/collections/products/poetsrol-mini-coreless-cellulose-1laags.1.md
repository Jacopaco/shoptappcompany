---
id: '2181'
blueprint: products
title: 'Poetsrol mini coreless cellulose 1laags'
price: 2199
description: '20 cm | 12x 120 meter in folie | ø 130 mm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2181_4.jpg'
art_nr: 2181
slug: poetsrol-mini-coreless-cellulose-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1410
unit_price: 0
vip_price: 1833
prod_img: prod_img/2181_4.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653943
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - d7654c12-3145-4478-8fa3-970c4b764ae1
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
---
