---
id: '14242'
blueprint: products
art_nr: '14242'
title: 'Luchtverfrisser kunststof wit, PQSmartW'
slug: luchtverfrisser-kunststof-wit-pqsmartw
price: 4500
cost_price: 2700
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
description: |-
  Instelbare luchtverfrisser voor wandmontage.
  Met slot en sleutel.
  Met automatische ventilator, die de geur van de bijbehorende geurpotjes gelijkmatig verspreidt.
  Met raster openingen aan beide zijkanten en onderzijde voor optimale geurbeleving.
  Werkt op 2 stuks LR20 batterijen.
prod_img: prod_img/14242.JPG
unit_price: 4500
vip_price: 4050
---
