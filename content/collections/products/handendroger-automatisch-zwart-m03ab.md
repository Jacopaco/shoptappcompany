---
id: '12325'
blueprint: product
art_nr: '12325'
title: 'Handendroger automatisch zwart, M03AB'
slug: handendroger-automatisch-zwart-m03ab
cost_price: 22385
unit_price: 40700
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 40700
vip_price: 36630
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Wanneer u nadenkt over de inrichting van uw sanitair, zijn drie dingen belangrijk: hygiëne, ruimte en stijl. Combineer deze drie dingen op de juiste manier, en uw gasten zijn verzekerd van een ontspannen toiletbezoek.

  Houdt u van een beetje spanning in uw interieur? Dan is deze Mediclinics handendroger voor u gemaakt. De zwarte kleur vormt een prachtig contrast tegen een hygiënisch witte wand. De handendroger is eenvoudig te bedienen. U steekt uw handen onder de tuit, en de droger slaat automatisch aan. Door de nauwkeurig afgestelde temperatuur en droogtijd, zijn uw handen binnen een halve minuut weer kurkdroog.

  Op zoek naar een beetje spanning in uw interieur? Informeer dan naar onze scherpe prijzen. 

  Stijlvol design
  Blaast warme lucht
  Slechts een halve minuut droogtijd
prod_img: prod_img/afbeelding-m03ab-1719478081.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478088
---
