---
id: cart00648
published: false
blueprint: products
art_nr: CART00648
title: 'Elleboogkraan bladmodel'
slug: elleboogkraan-bladmodel
price: 11566
cost_price: 11566
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
prod_img: prod_img/cart00648.jpg
unit_price: 11566
vip_price: 11566
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
