---
id: '12007'
blueprint: product
art_nr: '12007'
title: 'Haardroger wit, SC0088H'
slug: haardroger-wit-sc0088h
cost_price: 28875
unit_price: 52500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 52500
vip_price: 47250
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Haardroger drukknop wit, SC0088H
  Wandhaardroger.
  Met een drukknopbediende timer van 90 seconden.
  Met draaibare tuit.
  Bruto luchtopbrengst ca. 90l/sec.
  Luchtsnelheid: 100 km/u.
  Luchttemperatuur 53˚C.
  70dB.
prod_img: prod_img/12007-sc0088h.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479700
---
