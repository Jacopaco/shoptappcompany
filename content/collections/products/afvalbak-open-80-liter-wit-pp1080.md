---
id: '11073'
blueprint: product
art_nr: '11073'
title: 'Afvalbak open 80 liter wit, PP1080'
slug: afvalbak-open-80-liter-wit-pp1080
cost_price: 15390
unit_price: 25650
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 25650
vip_price: 23085
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak wit 80 liter open, PP1080
  Afvalbak open.
  Staat op 4 kunststof voetjes.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/pp1080_4.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718805337
---
