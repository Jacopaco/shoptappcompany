---
id: '2108'
published: false
blueprint: products
art_nr: '2108'
title: 'Toiletpapier naturel 1laags/400 vel'
slug: toiletpapier-naturel-1laags400-vel
price: 3734
cost_price: 2394
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
description: 'Verpakking van 5 x 8 rollen in folie'
unit_price: 0
vip_price: 3112
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
