---
id: '14285'
blueprint: product
art_nr: '14285'
title: 'Pictogram rond RVS INVALIDE, PS0004CS'
slug: pictogram-rond-rvs-invalide-ps0004cs
cost_price: 780
unit_price: 1300
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 1300
vip_price: 1170
categories: 33e55e5a-60bb-4897-9444-80c306b1c4b5
description: |-
  Zeker voor uw minder valide gasten is het prettig als duidelijk staat aangegeven waar het invalidetoilet zich bevindt.

  Dit gemak kunt u uw gasten gemakkelijk bieden door middel van dit eenvoudig te monteren RVS pictogram. Het pictogram is inclusief sterke tape voor installatie op effen oppervlakten.

  Maak uw gasten het gemakkelijk!
prod_img: prod_img/afbeelding-ps0004cs-1719323655.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323663
---
