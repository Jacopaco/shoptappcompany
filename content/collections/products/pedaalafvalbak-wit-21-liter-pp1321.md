---
id: '11095'
blueprint: product
art_nr: '11095'
title: 'Pedaalafvalbak wit 21 liter, PP1321'
slug: pedaalafvalbak-wit-21-liter-pp1321
cost_price: 5262
unit_price: 8770
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8770
vip_price: 7893
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Algemene beschrijving

  - Ronde bak, staal wit geëpoxeerd, inhoud 20 L.
  - Opening met pedaal.
  - Geschikt voor drukbezochte sanitaire ruimtes.
  - Gaat normaal gesproken samen met een toiletpapierdispenser, een toiletborstelhouder en een warme lucht
  handendroger. 
  Afm.: ±4%

  Onderdelen en materialen

  - Ronde behuizing, staal met witte epoxyafwerking.
  - Geurwerend deksel.
  - Binnenbak van polypropyleen met metalen handvat.
  - Handgreep in bovenste gedeelte om het transport te vergemakkelijken.
  - Zwart thermoplastisch antislippedaal.
  - Zwarte kunststof antislipbodem, die de bodem van de bak vochtvrij houdt.
prod_img: prod_img/mediclinics-pp1321.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719480153
---
