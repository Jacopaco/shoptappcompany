---
id: '14246'
blueprint: products
art_nr: '14246'
title: 'Luchtverfrisser kunststof zilver, PQSmartM'
slug: luchtverfrisser-kunststof-zilver-pqsmartm
price: 4900
cost_price: 2100
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
description: |-
  Instelbare luchtverfrisser voor wandmontage.
  Met slot en sleutel. Met automatische ventilator, die de geur van de bijbehorende geurpotjes
  gelijkmatig verspreidt.
  Met raster openingen aan beide zijkanten en onderzijde voor optimale geurbeleving.
  Werkt op 2 stuks LR20 batterijen.
prod_img: prod_img/14246.JPG
unit_price: 4900
vip_price: 4410
---
