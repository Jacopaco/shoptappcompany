---
id: 1057aa9f-0403-4213-9f88-3e85adfe7619
published: false
blueprint: products
title: 'Handschoen Nitril ongepoederd blauw maat M'
price: 515
description: 'Handschoen Nitril ongepoederd maat M 100st in doos'
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712049504
img: product_images/nitril-handschoenen-doos-100-stuks-maat-l.jpg
product_type: physical
---
