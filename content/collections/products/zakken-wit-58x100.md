---
id: '2002130'
published: false
blueprint: products
title: 'HDPE afvalzak 58x100cm T23 zwart (20x25) 500 stuks'
price: 6872
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2302070.jpg'
art_nr: '150212100'
slug: hdpe-afvalzak-58x100cm-t23-zwart-20x25-500-stuks-uitlopend
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
cost_price: 3743
unit_price: 0
vip_price: 5726
client_prices:
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - bebc5c46-0553-4d62-b6ff-de33648493c4
locations:
  - e6713451-367d-43ef-91ec-87d84a4c609f
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
description: 'HDPE afvalzak 58x100cm T23 zwart (20x25) 500 stuks'
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732786653
---
