---
id: '12012'
blueprint: product
art_nr: '12012'
title: 'Haardroger zwart/zilver, SC0010CS'
slug: haardroger-zwartzilver-sc0010cs
cost_price: 3399
unit_price: 6180
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6180
vip_price: 5562
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/mediclinics-sc0010cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479479
---
