---
id: '12300'
blueprint: product
art_nr: '12300'
title: 'Handendroger automatisch wit, M03A'
slug: handendroger-automatisch-wit-m03a
cost_price: 22385
unit_price: 40700
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 40700
vip_price: 36630
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Het inrichten van een sanitaire ruimte vraagt om kennis van zaken en een goed oog voor detail. Daar speelt de keuze voor uw accessoires een belangrijke rol in.

  Met deze handendroger van Mediclinics weet u zeker dat u goed voor de dag komt bij uw gasten. Het afgeronde design aan de voorzijde, en de nauwkeurige verdeling van de luchtvlakken aan de onderzijde geven de praktische insteek van de ontwerper aan. Iets wat overigens ook goed zichtbaar is in het eenvoudige gebruik van de handendroger; door de automatische starter hoeft uw gast niet meer te doen dan zijn handen uit te steken. De zorgvuldig ingestelde luchtsnelheid en de aangename temperatuur zorgen dat uw gast binnen korte tijd weer met droge handen buiten staat.
prod_img: prod_img/mediclinics-m03a.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478272
---
