---
id: b3ac5eff-8aa3-4430-a86d-43f7d85384d7
published: false
blueprint: products
title: 'Handschoen Nitril ongepoederd blauw maat L'
price: 515
description: 'Handschoen Nitril ongepoederd maat L 100st'
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712048588
img:
  - product_images/nitril-handschoenen-doos-100-stuks-maat-l.jpg
product_type: physical
---
