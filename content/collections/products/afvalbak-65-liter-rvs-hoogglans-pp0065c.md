---
id: '11061'
blueprint: product
art_nr: '11061'
title: 'Afvalbak 65 liter RVS hoogglans, PP0065C'
slug: afvalbak-65-liter-rvs-hoogglans-pp0065c
cost_price: 26339
unit_price: 43900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 43900
vip_price: 39510
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak hoogglans 65 liter gesloten, PP0065C
  Afvalbak met push klep.
  Staat op 4 kunststof voetjes.
  Deksel is in zijn geheel van de onderbak af te nemen.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/mediclinics-pp0065c-1719846011.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719846016
---
