---
id: '83907020320'
blueprint: products
art_nr: '83907020320'
title: 'Wecoline Microvezeldoeken gebreid blauw'
slug: wecoline-microvezeldoeken-gebreid-blauw
price: 460
cost_price: 202
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/83907020320.JPG
unit_price: 310
vip_price: 383
---
