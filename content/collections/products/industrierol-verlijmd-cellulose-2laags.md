---
id: '2164'
blueprint: products
title: 'STARLIGHT 2164 industrierol 26.6 cm wit 2x380 meter 2 laags'
price: 4134
description: |-
  Industrierol verlijmd cellulose 2 laags 26,5 cm

  Code 2164
  Materiaal cellulose
  Kleur wit
  Afmetingen 26,5 cm x 380 meter op een rol
  2 laags
  Verpakking 2 rollen in folie
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2164_3.jpg'
art_nr: 2164
slug: industrierol-verlijmd-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 2650
unit_price: 0
vip_price: 3445
prod_img: prod_img/2164_3.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729157169
categories: bb31f7ae-4f77-4bd6-bb69-6b7c69019093
product_type: physical
---
