---
id: '11086'
blueprint: product
art_nr: '11086'
title: 'Pedaalafvalbak RVS hoogglans 12 liter, PP1312C'
slug: pedaalafvalbak-rvs-hoogglans-12-liter-pp1312c
cost_price: 4188
unit_price: 6980
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6980
vip_price: 6282
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1312c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845078
---
