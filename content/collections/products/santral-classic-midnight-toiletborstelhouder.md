---
id: wbu-2-p-mn
published: false
blueprint: products
art_nr: 'WBU 2 P MN'
title: 'SanTRAL® Classic Midnight toiletborstelhouder'
slug: santral-classic-midnight-toiletborstelhouder
price: 13690
cost_price: 8214
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/wbu-3-p-mn.JPG
unit_price: 13690
vip_price: 12321
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
