---
id: '11092'
blueprint: product
art_nr: '11092'
title: 'Hygiënebak 6 liter RVS, PP0006CS'
slug: hygienebak-6-liter-rvs-pp0006cs
cost_price: 6504
unit_price: 10840
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 10840
vip_price: 9756
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  (Hygiëne)bak 6 liter RVS, PP0006CS
  (Hygiëne)bak vrijstaand of voor wandmontage.
  Met klepdeksel.
prod_img: prod_img/mediclinics-pp0006cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719480521
---
