---
id: '56713'
published: false
blueprint: products
art_nr: '56713'
title: 'Handdoekroldispenser textiel'
slug: handdoekroldispenser-textiel
price: 0
cost_price: 16500
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
description: |-
  Handdoekroldispenser kusntstof wit voor non woven rollen
  en textiel rollen. RETRACTABLE
  H 570 x B 380 x D 270 mm
unit_price: 0
vip_price: 0
prod_img: prod_img/5671.JPG
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
