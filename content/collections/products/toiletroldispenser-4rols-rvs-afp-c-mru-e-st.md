---
id: 2201503-afp-c
blueprint: products
art_nr: '2201503 AFP-C.'
title: 'Toiletroldispenser 4rols RVS afp-c, MRU E ST'
slug: toiletroldispenser-4rols-rvs-afp-c-mru-e-st
price: 32320
cost_price: 19392
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  4-rolshouder voor wandmontage.
  Automatisch rotatie systeem.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
prod_img: prod_img/2201503-afp-c..JPG
unit_price: 32320
vip_price: 29088
---
