---
id: '572318663'
blueprint: products
art_nr: '572318663'
title: 'roze Microvezel droogdoek 61 cm x 46 cm'
slug: roze-microvezel-droogdoek-61-cm-x-46-cm
cost_price: 520
unit_price: 925
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
categories: c764401b-cfda-4ea9-8e67-0db8b881128e
price: 1185
vip_price: 988
prod_img: prod_img/572183663-1696510101.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1696510103
---
