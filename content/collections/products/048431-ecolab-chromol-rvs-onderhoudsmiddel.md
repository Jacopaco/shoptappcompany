---
id: '048431'
blueprint: products
title: 'Ecolab Chromol'
price: 3116
description: '048431 Ecolab Chromol RVS onderhoudsmiddel Sprayflacon 500ml'
img:
  - product_images/knipsel.PNG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687526025
art_nr: '048431'
slug: ecolab-chromol
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 1829
unit_price: 2012
vip_price: 2597
prod_img: prod_img/048431.png
---
