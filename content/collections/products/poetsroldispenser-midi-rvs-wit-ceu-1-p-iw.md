---
id: '2745700'
blueprint: products
art_nr: '2745700'
title: 'Poetsroldispenser midi RVS wit, CEU 1 P IW'
slug: poetsroldispenser-midi-rvs-wit-ceu-1-p-iw
cost_price: 10285
unit_price: 20950
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: d1ca567f-2127-432a-aec4-29fef09a974d
price: 20950
vip_price: 18855
prod_img: prod_img/2745700.png
---
