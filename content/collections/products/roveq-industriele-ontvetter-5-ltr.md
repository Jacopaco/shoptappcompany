---
id: 100-105
blueprint: products
art_nr: 100-105
title: 'ROVEQ Industriële ontvetter 5 ltr'
slug: roveq-industriele-ontvetter-5-ltr
cost_price: 1539
unit_price: 2466
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 2622
vip_price: 2185
prod_img: prod_img/100-105.png
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1696237782
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
  - ffa8f396-2f25-4d2b-9178-abbf960171cb
  - bbae8e44-4575-431e-8728-181691d314e6
---
