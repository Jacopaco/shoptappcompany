---
id: '98920'
blueprint: products
title: 'TAPP desinfectie 1000 ml Untouchable GS'
price: 1166
description: 'Gesloten KX systeem'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/98920.JPG'
art_nr: 98920
slug: tapp-desinfectie-1000-ml-untouchable-gs
brands: 841597b5-0184-4fd2-98a0-6535c97b8673
categories: e912fdd5-abf6-4aeb-8d04-d94cf86723ed
cost_price: 500
prod_img: prod_img/98920.JPG
unit_price: 1166
vip_price: 1166
client_prices:
  - 6aa35b46-fb7b-4809-9144-37d95fecd2d6
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - ef60def1-f195-4b3b-a291-64fb129e6368
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - c3ed1408-6d23-4d75-bab0-4752835fb279
  - 6aa35b46-fb7b-4809-9144-37d95fecd2d6
locations:
  - 28a7c695-6e63-45b3-b248-504e489c1bac
  - 1cbc4fc8-faea-4a79-a9c1-410949fac628
  - df52f315-d21f-435f-9fc2-1317451c68b0
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 21fd8138-93dd-4a6e-992b-066f60627391
  - 52571907-9205-4d01-962e-3ba319c62abe
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
  - 0b583266-c93f-467d-89b8-83c65b7fb392
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
---
