---
id: '14061'
blueprint: product
art_nr: '14061'
title: 'Foamzeepdispenser 1500 ml RVS zwart, DJFP035B'
slug: foamzeepdispenser-1500-ml-rvs-zwart-djfp035b
cost_price: 9630
unit_price: 16050
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16050
vip_price: 14445
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  MEDICLINICS DJP0035B foamzeepdispenser met beugel 1500 ml.


  - Artikelnummer 14061
  - Model DJP0035B
  - Materiaal RVS
  - Kleur Zwart gepoedercoat
  - Hoogte 240 mm
  - Breedte 110 mm
  - Diepte 133 mm
  - Gewicht 1,25 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Toepassing Navulbaar
  - Vulling foamzeep
  - Dosering 1 ml per slag
  - Inhoud 1500 ml
prod_img: prod_img/14061_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715070766
---
