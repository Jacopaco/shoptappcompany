---
id: '8210'
blueprint: products
art_nr: '8210'
title: 'Afvalbak 6 liter RVS, MQWB6E'
slug: afvalbak-6-liter-rvs-mqwb6e
price: 17140
cost_price: 10284
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak met klepdeksel.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandhouder.
  Met 4puntsbevestiging.
prod_img: prod_img/8210.JPG
unit_price: 17140
vip_price: 15426
---
