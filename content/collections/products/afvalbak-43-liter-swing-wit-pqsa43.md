---
id: '5656'
blueprint: products
art_nr: '5656'
title: 'Afvalbak 43 liter swing wit, PQSA43'
slug: afvalbak-43-liter-swing-wit-pqsa43
price: 5225
cost_price: 3292
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met swing deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling. 
prod_img: prod_img/5656.JPG
unit_price: 5225
vip_price: 4702
---
