---
id: '8200'
blueprint: products
art_nr: '8200'
title: 'Afvalbak 6 liter aluminium, MQWB6A'
slug: afvalbak-6-liter-aluminium-mqwb6a
price: 11800
cost_price: 7080
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak met klepdeksel.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8200.JPG
unit_price: 11800
vip_price: 10620
---
