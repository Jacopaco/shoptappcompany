---
id: '5676'
blueprint: products
art_nr: '5676'
title: 'Afvalbak 43 liter half open zilver, PQA43M'
slug: afvalbak-43-liter-half-open-zilver-pqa43m
price: 7430
cost_price: 4681
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met semi-open deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
prod_img: prod_img/5676.JPG
unit_price: 7430
vip_price: 6687
---
