---
id: '2302070'
published: false
blueprint: products
title: 'HDPE pedaalemmerzakken Transparant T15 (50cm x 55cm)'
price: 3514
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/dameshygienzak_1.jpg'
art_nr: 2302070
slug: hdpe-pedaalemmerzakken-transparant-t15-50cm-x-55cm
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
cost_price: 1914
prod_img: prod_img/2302070.JPG
unit_price: 0
vip_price: 2928
client_prices:
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
locations:
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
  - df52f315-d21f-435f-9fc2-1317451c68b0
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
---
