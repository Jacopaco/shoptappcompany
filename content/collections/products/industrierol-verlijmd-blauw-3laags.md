---
id: '2165'
blueprint: products
title: 'STARLIGHT 2165 industrierol 36 cm blauw 1x380 meter'
price: 4132
description: |-
  Industrierol verlijmd blauw 3 laags 36 cm

  Code 2165
  Materiaal cellulose
  Kleur blauw
  Afmetingen 36 cm
  Lagen 3 laags verlijmd
  Verpakking 1 x 380 meter in folie
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2165_2_1.jpg'
art_nr: 2165
slug: industrierol-verlijmd-blauw-3laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 2649
unit_price: 0
vip_price: 3443
prod_img: prod_img/2165_2_1.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728991515
categories: bb31f7ae-4f77-4bd6-bb69-6b7c69019093
product_type: physical
---
