---
id: '24402562'
published: false
blueprint: products
art_nr: '24402562'
title: 'Kraan bladmontage water/zeep/des. MWSD BENT RAL9005-OS'
slug: kraan-bladmontage-waterzeepdes-mwsd-bent-ral9005-os
price: 107900
cost_price: 57900
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
unit_price: 107900
vip_price: 107900
prod_img: prod_img/24402562.png
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
