---
id: '1012283'
blueprint: products
art_nr: '561609040'
title: 'LDPE afvalzak 60x90cm T40 zwart (15x20) 300 stuks'
slug: ldpe-non-komo-afvalzakken-60x90cm-t50-grijs-verpakt-15-rol-a-20-stuks-300st-stuks
price: 5416
cost_price: 2950
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
unit_price: 4888
vip_price: 4513
prod_img: prod_img/1012283.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733305982
---
