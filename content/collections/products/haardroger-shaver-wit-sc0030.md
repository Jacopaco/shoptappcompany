---
id: '12015'
blueprint: product
art_nr: '12015'
title: 'Haardroger shaver wit, SC0030'
slug: haardroger-shaver-wit-sc0030
cost_price: 4763
unit_price: 8660
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8660
vip_price: 7794
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/12015-sc0030.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479353
---
