---
id: 2301073-afp-c
blueprint: products
art_nr: '2301073 AFP-C.'
title: 'Hygiënebak 10 liter RVS afp-c, HBU 10 E ST'
slug: hygienebak-10-liter-rvs-afp-c-hbu-10-e-st
price: 27300
cost_price: 16380
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiënebak 10 liter.
  Met klepdeksel en binnenring.
  Vrijstaand of voor wandmontage.
  Deksel is makkelijk afneembaar van de onderbak.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2301073-afp-c..JPG
unit_price: 27300
vip_price: 24570
---
