---
id: '2115'
blueprint: products
title: 'STARLIGHT 2115 toiletrollen met dop 36x100m - 2 laags'
price: 6232
description: |-
  Dit helderwit 2-laags toiletpapier is bijzonder geschikt voor drukbezochte ruimtes. 1 rol bevat 100 m toiletpapier. Let op: de doos bevat 36 rollen. Het toiletpapier is voorzien van het ECO-label en heeft een dop in de kern. Het past in het Euro Products-adaptersysteem en is ook compatibel met het Vendor- en Katrin-adaptersysteem. Deze toiletrollen worden vaak gebruikt in de horeca.

  Toiletpapier ECO met adapter 100 meter 36 rollen
  Helder wit 2-laags
  Hoge capaciteit
  Alternatief voor de Vendor 1252
  Compatibel met de Vendor dispenser en Katrin
  De diameter van de zwarte binnenkap van dit artikel is 16mm
  PU (karton) = 36 rollen per 100 meter
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2115-2_1.jpg'
art_nr: 2115
slug: toiletpapier-doprol-tissue-wit-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 3995
unit_price: 0
vip_price: 5193
prod_img: prod_img/2115-2_1.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729000034
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - b69a1f96-5c22-487d-bfd7-be02c150ade5
  - d34a55b1-19c5-45db-9ecb-38efb7906da8
  - 5162e19a-84e6-4f47-83e6-760863dc5c91
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
product_type: physical
---
