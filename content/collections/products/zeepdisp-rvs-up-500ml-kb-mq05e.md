---
id: '8010'
blueprint: products
art_nr: '8010'
title: 'Zeepdisp. RVS UP 500ml KB, MQ05E'
slug: zeepdisp-rvs-up-500ml-kb-mq05e
price: 11600
cost_price: 6959
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig af gekit kan worden.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8010.JPG
unit_price: 11600
vip_price: 10440
---
