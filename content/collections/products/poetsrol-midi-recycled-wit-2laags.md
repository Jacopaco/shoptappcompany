---
id: '2167'
blueprint: products
title: 'Poetsrol midi recycled wit 2laags'
price: 2525
description: '6 x 120 meter in folie | ø 180 mm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2167_1_2.jpg'
art_nr: 2167
slug: poetsrol-midi-recycled-wit-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1619
unit_price: 0
vip_price: 2104
prod_img: prod_img/2167_1_2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653820
---
