---
id: '110302000'
published: false
blueprint: product
art_nr: '110302000'
title: 'HDPE afvalzak 63x70cm T15 transparant (40x25) st'
slug: hdpe-afvalzak-45x50cm-t10-transparant-50x50-2500-stuks
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 1942
unit_price: 0
price: 3565
vip_price: 2971
locations:
  - 9febd33c-508e-4088-80ef-1681be040e54
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
prod_img: prod_img/110302000.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732786967
---
