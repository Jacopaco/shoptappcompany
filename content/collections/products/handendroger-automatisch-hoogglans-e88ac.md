---
id: '12210'
blueprint: product
art_nr: '12210'
title: 'Handendroger automatisch hoogglans, E88AC'
slug: handendroger-automatisch-hoogglans-e88ac
cost_price: 38225
unit_price: 69500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 69500
vip_price: 62550
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Stijlvol hoeft niet duur te zijn. Dat bewijst deze handendroger met hoogglans look van de Mediclinics-lijn. De slimme mix van verschillende materialen geeft deze handendroger een extra luxueus tintje. Hiermee is de droger weer een typisch voorbeeld van het praktische design van onze Mediclinics-productenlijn.

  De keus voor deze handendroger zal uw gasten verrassen. De luxueuze uitstraling in combinatie met de praktische werking geeft deze handendroger een ereplaats in de top 10 van onze bestsellers. Bovendien is de werking van de droger zeer eenvoudig; de droger start automatisch wanneer uw bezoeker zijn handen onder het apparaat plaatst. Door de nauwkeurig ingestelde temperatuur en luchtsnelheid heeft de droger slechts 29 seconden nodig voor een optimaal resultaat.
prod_img: prod_img/mediclinics-e88ac.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478692
---
