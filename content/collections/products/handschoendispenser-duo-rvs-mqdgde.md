---
id: '8489'
blueprint: products
art_nr: '8489'
title: 'Handschoendispenser duo RVS, MQDGDE'
slug: handschoendispenser-duo-rvs-mqdgde
price: 11170
cost_price: 6702
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Duo handschoendispenser voor wandmontage.
  Geschikt voor verschillende maten handschoendoosjes.
  Een verende RVS klembeugel houdt de doosjes op de plaats.
  Kan zowel horizontaal als verticaal gemonteerd worden.
  Met 4puntsbevestiging.
prod_img: prod_img/8489.JPG
unit_price: 11170
vip_price: 10053
---
