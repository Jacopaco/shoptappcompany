---
id: '11079'
blueprint: product
art_nr: '11079'
title: 'Pedaalafvalbak rond wit 3 liter, PP1303'
slug: pedaalafvalbak-rond-wit-3-liter-pp1303
cost_price: 1785
unit_price: 2975
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 2975
vip_price: 2677
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1303.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845408
---
