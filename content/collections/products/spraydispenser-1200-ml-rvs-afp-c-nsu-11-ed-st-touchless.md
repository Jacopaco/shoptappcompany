---
id: 21419396-afp-c
blueprint: products
art_nr: '21419396 AFP-C'
title: 'Spraydispenser 1200 ml RVS afp-c, NSU 11 E/D ST TOUCHLESS'
slug: spraydispenser-1200-ml-rvs-afp-c-nsu-11-ed-st-touchless
price: 29260
cost_price: 15800
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Spraydispenser automatisch voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Werkt op 4 D-batterijen, deze worden NIET meegeleverd.
  Ook in wit verkrijgbaar!
unit_price: 29260
vip_price: 26334
prod_img: prod_img/21419396-afp-c.jpg
---
