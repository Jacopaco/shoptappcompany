---
id: 101102502-1
blueprint: product
art_nr: 101102502-1
title: 'Sun Pro Formula All-in-1 eco vaatwastabletten 102 stuks'
slug: sun-pro-formula-all-in-1-eco-vaatwastabletten-102-stuks
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
cost_price: 1162
unit_price: 1626
price: 1980
vip_price: 1650
description: 'Sun Pro Formula All-in-1 Professional Vaatwastabletten 200 stuks XXL is een complete oplossing met ingebouwd glansspoelmiddel en zout. Het glansspoelmiddel zorgt voor een uitzonderlijke glans en het zout voorkomt kalkaanslag. Verwijdert vet en hardnekkige vlekken, zoals koffie, thee en lippenstift. Zorgt voor sprankelend schone kopjes en borden bij elke afwasbeurt. Dit product is bekroond met het EU Ecolabel en voldoet aan hoge milieunormen gedurende de hele levenscyclus, van grondstofwinning tot productie, distributie en afvalverwerking.'
prod_img: prod_img/sun-pro-formula-all-in-1-eco-vaatwastabletten-102-stuks.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727267784
users:
  - c6826b80-e236-41b0-a86b-e6321f7d4e0f
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - ff938854-4adb-42f4-aae1-2b3f0d52b517
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
---
