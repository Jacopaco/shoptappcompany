---
id: '11065'
blueprint: product
art_nr: '11065'
title: 'Afvalbak open 65 liter RVS, PP1065CS'
slug: afvalbak-open-65-liter-rvs-pp1065cs
cost_price: 16800
unit_price: 28000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 28000
vip_price: 25200
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak RVS 65 liter open, PP1065CS
  Afvalbak open.
  Staat op 4 kunststof voetjes.
  Met 4 haken aan de binnenzijde voor ophanging van de afvalzakken.
prod_img: prod_img/mediclinics-pp1065cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845707
---
