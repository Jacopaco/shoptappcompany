---
id: '13169'
blueprint: product
art_nr: '13169'
title: 'Spraydispenser 1500 ml RVS, DJS0033CS'
slug: spraydispenser-1500-ml-rvs-djs0033cs
cost_price: 7122
unit_price: 11870
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11870
vip_price: 10683
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Goede hygiëne is cruciaal. Zeker in ruimten waar de gasten elkaar in hoog tempo afwisselen. De keus voor een alcohol-spraydispenser van Mediclinics is dan ook vanzelfsprekend.

  Ook al kiest u vooral voor het praktisch gebruik van deze spraydispenser, het design moet ook voldoen aan de hoogste designeisen. Iets wat de ontwerpers van deze dispenser duidelijk hebben begrepen. De strakke rvs behuizing met schuine hoeken is een prachtige aanvulling op elke smaakvol ingerichte sanitaire ruimte. Met een druk op de rvs push button, doseert uw gast precies de juiste hoeveelheid alcohol die nodig is voor een optimale ontsmetting van handen of toiletseat. De navulfles kan eenvoudig vervangen worden via de bovenzijde van de dispenser.

  Wilt u dat uw sanitaire ruimte voldoet aan de hoogste designeisen? Dan is de Mediclinics Spraydispenser RVS 1500 ml uw keuze!
prod_img: prod_img/afbeelding-djs0033cs-1719323140.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323147
---
