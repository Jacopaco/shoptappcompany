---
id: '2110901'
blueprint: products
art_nr: '2110901'
title: 'Zeep/Desinfectiedispenser alu 500 ml lange beugel, ELS 26 A/25'
slug: zeepdesinfectiedispenser-alu-500-ml-lange-beugel-els-26-a25
price: 5220
cost_price: 3000
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/2110901.JPG
unit_price: 0
vip_price: 4350
---
