---
id: 21415827-afp-c
blueprint: products
art_nr: '21415827 AFP-C.'
title: 'Foamzeepdispenser 250 ml RVS afp-c, NSU 2 E/F ST'
slug: foamzeepdispenser-250-ml-rvs-afp-c-nsu-2-ef-st
price: 17550
cost_price: 9477
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met verborgen speciaal slot.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415827-afp-c..JPG
unit_price: 17550
vip_price: 15795
---
