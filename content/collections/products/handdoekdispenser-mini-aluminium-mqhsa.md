---
id: '8160'
blueprint: products
art_nr: '8160'
title: 'Handdoekdispenser mini aluminium, MQHSA'
slug: handdoekdispenser-mini-aluminium-mqhsa
price: 7870
cost_price: 4722
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser klein.
  Met zichtvenster voor inhoudscontrole.
  Afsluitbaar, slot zit aan de voorzijde.
  Cover gaat aan de voorzijde open.  
  Met 3punts bevestiging.
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/8160.JPG
unit_price: 7870
vip_price: 7083
---
