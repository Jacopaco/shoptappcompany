---
id: nsu-5-f
blueprint: products
art_nr: 'NSU 5 F'
title: 'Foamzeepdispenser 600 ml inbouw, NSU 5 D'
slug: foamzeepdispenser-600-ml-inbouw-nsu-5-d
price: 118
cost_price: 63.72
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1cbc45df-96d8-4022-8d2e-2ee96c7728be
prod_img: prod_img/nsu-5-ps-mn.JPG
unit_price: 118
vip_price: 94.4
---
