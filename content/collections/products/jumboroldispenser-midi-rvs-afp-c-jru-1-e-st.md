---
id: '3301453'
blueprint: products
art_nr: '3301453'
title: 'Jumboroldispenser midi RVS afp-c, JRU 1 E ST'
slug: jumboroldispenser-midi-rvs-afp-c-jru-1-e-st
price: 20950
cost_price: 12570
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
prod_img: prod_img/3301453.JPG
unit_price: 20950
vip_price: 18855
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
