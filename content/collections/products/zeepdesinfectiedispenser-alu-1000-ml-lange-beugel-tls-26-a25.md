---
id: '2110911'
blueprint: products
art_nr: '2110911'
title: 'Zeep/Desinfectiedispenser alu 1000 ml lange beugel, TLS 26 A/25'
slug: zeepdesinfectiedispenser-alu-1000-ml-lange-beugel-tls-26-a25
price: 6629
cost_price: 3810
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/2110911.JPG
unit_price: 0
vip_price: 5524
---
