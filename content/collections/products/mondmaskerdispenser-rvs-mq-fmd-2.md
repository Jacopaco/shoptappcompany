---
id: '13106'
blueprint: products
art_nr: '13106'
title: 'Mondmaskerdispenser RVS, MQ FMD 2'
slug: mondmaskerdispenser-rvs-mq-fmd-2
price: 4450
cost_price: 2670
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
prod_img: prod_img/13106.JPG
unit_price: 4450
vip_price: 4005
categories: 2507084c-0b96-4778-bb5a-2b2350a2e7f4
---
