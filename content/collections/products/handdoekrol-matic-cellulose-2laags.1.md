---
id: '2161'
blueprint: products
title: 'STARLIGHT 2161-FSC handdoekrollen 6x140 meter - 2 laags'
price: 2784
description: |-
  Handdoekrollen cellulose 2 laags, 18 cm x 140 meter

  Code 2161-FSC
  Materiaal cellulose
  Kleur wit
  Afmetingen 18 cm x 140 meter op rol
  Lagen 2 laags
  Verpakking 6 rollen in folie
  Ø rol 18 cm
  Ø koker 3,8cm

  FSC certificaat
  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2161_2.jpg'
art_nr: 2161
slug: handdoekrol-matic-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 1785
unit_price: 2595
vip_price: 2320
prod_img: prod_img/2161_2.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728991721
client_prices:
  - 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
  - 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
locations:
  - e6090873-ae14-4d62-b52a-da6089045f90
categories: 211858e8-70c5-4a2e-a296-f0a9cd783d2f
product_type: physical
---
