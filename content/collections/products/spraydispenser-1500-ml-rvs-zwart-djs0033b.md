---
id: '13170'
blueprint: product
art_nr: '13170'
title: 'Spraydispenser 1500 ml RVS zwart, DJS0033B'
slug: spraydispenser-1500-ml-rvs-zwart-djs0033b
cost_price: 7830
unit_price: 13050
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 13050
vip_price: 11745
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Gedeelde toiletbrillen kunnen micro-organismen bevatten die allerlei infecties kunnen veroorzaken. Geen prettig idee natuurlijk. Met de Mediclinics Spraydispenser – gevuld met alcohol of desinfectiespray – voorkomt u verspreiding van ongewenste bacteriën.

  In een luxueus ingerichte sanitaire ruimte mag een spraydispenser niet ontbreken. Maar welk model kiest u? De keus voor deze exclusieve zwarte rvs Mediclinics zeepdispenser getuigt duidelijk van lef. De zwarte rvs behuizing is een hoogwaardig staaltje Mediclinics design; iets wat direct in het oog springt bij uw gasten. De dispenser kan gevuld worden met alcohol, zodat uw gasten altijd verzekerd zijn van een optimale (hand)hygiëne.
prod_img: prod_img/afbeelding-djs0033b-1719323045.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719323052
---
