---
id: '12655'
blueprint: products
art_nr: '12655'
title: 'Grab bar wand/vloer inbouw RVS, BS0010CS'
slug: grab-bar-wandvloer-inbouw-rvs-bs0010cs
price: 14015
cost_price: 8409
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar voor montage van wand tot vloer.
  Inbouw in de vloer op 96 mm.
  Horizontale stanglengte: 776 mm.
  Verticale stanglengte: 750 mm
prod_img: prod_img/12655.JPG
unit_price: 14015
vip_price: 12613
---
