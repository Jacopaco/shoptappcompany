---
id: '13711'
blueprint: product
art_nr: '13711'
title: 'Jumboroldispenser mini RVS hoogglans, PR2783C'
slug: jumboroldispenser-mini-rvs-hoogglans-pr2783c
cost_price: 5166
unit_price: 8610
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8610
vip_price: 7749
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Als het om de architectuur van uw sanitaire ruimte gaat, is het belangrijk om op details te letten. Een lijn in de accessoires betekent rust en ruimte in uw toilet. Een sfeer waar uw gasten zich gelijk thuis voelen.

  Duidelijk is dat de ontwerpers van deze mini jumboroldispenser de praktijk niet uit het oog verliezen. De rvs hoogglans look mag dan wel luxueus aandoen, maar het gebruik is vooral eenvoudig en praktisch. De dispenser bestaat uit een kap, waar de rol in opgeborgen kan worden. Via de afscheurranden aan de onderzijde van de dispenser scheurt u het toiletpapier makkelijk af. Eenvoudige inhoudscontrole vindt plaats via de kijkgleuf aan de zijkant van de dispenser.

  Kies voor een fraai vormgegeven en tegelijkertijd praktische Mediclinics jumboroldispenser.
prod_img: prod_img/afbeelding-pr2783c-1719320846.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320851
---
