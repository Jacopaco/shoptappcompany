---
id: 12460-br
published: false
blueprint: product
art_nr: 12460-BR
title: 'Handendroger HANDS-IN automatisch wit, PQ14A'
slug: handendroger-hands-in-automatisch-wit-pq14a
cost_price: 54375
unit_price: 72500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 72500
vip_price: 65250
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
---
