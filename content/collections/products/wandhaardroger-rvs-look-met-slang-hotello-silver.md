---
id: '17272'
blueprint: product
art_nr: '17272'
title: 'Wandhaardroger RVS look met slang, Hotello Silver'
slug: wandhaardroger-rvs-look-met-slang-hotello-silver
cost_price: 8050
unit_price: 15800
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 15800
vip_price: 14220
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Hotello Silver ABS kunststof automatische wandhaardroger. RVS look. 1200 Watt. Droger start automatisch als de handgreep uit de houder gehaald wordt. 2 mogelijkheden voor de aansluiting; met een stekker of op wandcontactdoos.

  - Specificaties
  - Artikelnummer 	17272
  - Materiaal 	Kunststof
  - Kleur 	RVS look
  - Hoogte 	550 mm
  - Breedte 	147 mm
  - Diepte 	112 mm
  - Gewicht 	2 kg
  - Garantie 	1 jaar na aankoopdatum
  - Toepassing 	Wandmontage
  - Toepassing 	Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
  - Eigenschappen 	Start automatisch als de handgreep uit de houder wordt gehaald, cool touch en 8 minuten safety timer
  - Wattage 	1200 Watt
prod_img: prod_img/17272_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714732789
---
