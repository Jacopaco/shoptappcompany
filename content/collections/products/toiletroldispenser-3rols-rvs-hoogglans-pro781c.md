---
id: '13314'
blueprint: product
art_nr: '13314'
title: 'Toiletroldispenser 3rols RVS hoogglans, PRO781C'
slug: toiletroldispenser-3rols-rvs-hoogglans-pro781c
cost_price: 5940
unit_price: 9900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9900
vip_price: 8910
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
prod_img: prod_img/afbeelding-pro781c-1719321712.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321717
---
