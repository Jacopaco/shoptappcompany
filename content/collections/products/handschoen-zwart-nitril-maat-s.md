---
id: cart00926
blueprint: products
art_nr: CART00926
title: 'Handschoen Zwart Nitril maat S'
slug: handschoen-zwart-nitril-maat-s
cost_price: 287
unit_price: 374
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
price: 374
vip_price: 374
prod_img: prod_img/cart00926.JPG
locations:
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
---
