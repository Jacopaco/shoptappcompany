---
id: '8470'
blueprint: products
art_nr: '8470'
title: 'Reserverolhouder 3rols RVS, MQRRH3E'
slug: reserverolhouder-3rols-rvs-mqrrh3e
price: 7234
cost_price: 4341
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Trio reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 4puntsbevestiging.
prod_img: prod_img/8470.JPG
unit_price: 7234
vip_price: 6510
---
