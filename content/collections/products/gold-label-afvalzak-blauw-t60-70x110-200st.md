---
id: cart00960
blueprint: products
art_nr: '180204100'
title: 'LPDE afvalzak 70x110cm T60 blauw (10x20) 200 stuks'
slug: lpde-afvalzak-70x110cm-t60-blauw-10x20-200-stuks
cost_price: 1964
unit_price: 2900
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
price: 3605
vip_price: 3004
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 175073cd-07d9-40df-9e81-927b690b393b
  - 8cbb7514-f57b-447e-8fd2-1cb91058840f
  - e9ea78eb-b4c3-42fa-b3b3-a7f97c71fa91
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - fd034e41-e357-4acc-8957-4bb9ed02d77d
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - be14461b-4601-4a24-a6dc-17853268cc98
  - a0be2336-697d-408e-ae76-a075fb4e1f62
  - c1577f6a-6b53-4c66-a7ad-2c69fb7df643
  - ad521bf5-2669-452d-81b0-0b55218e7ac4
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
prod_img: prod_img/b560506016.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732785554
---
