---
id: '572318661'
blueprint: products
art_nr: '572318661'
title: 'Groen Microvezel droogdoek 61 cm x 46 cm'
slug: groen-microvezel-droogdoek-61-cm-x-46-cm
cost_price: 520
unit_price: 925
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
categories: c764401b-cfda-4ea9-8e67-0db8b881128e
price: 1185
vip_price: 988
prod_img: prod_img/572318661.JPG
---
