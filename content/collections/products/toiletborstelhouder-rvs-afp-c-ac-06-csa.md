---
id: '13206'
blueprint: product
art_nr: '13206'
title: 'Toiletborstelhouder RVS afp-c, AC-06-CSA'
slug: toiletborstelhouder-rvs-afp-c-ac-06-csa
cost_price: 4500
unit_price: 7500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 7500
vip_price: 6750
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Hygiëne is basisregel nummer 1 in elke sanitaire ruimte. Uw bezoeker zal daarom ook altijd alert zijn op rondslingerend afval of onhygiënische sporen in het toilet. Met de toiletborstelhouder van MediQo-line bent u altijd verzekerd van een schoon en streeploos toilet.

  De rvs toiletborstelhouder van MediQo-line is niet alleen praktisch in gebruik, maar getuigt ook van smaak. De strakke vormgeving laat zien dat u oog heeft voor detail. De toiletborstel kan voor gebruik via de linkerzijde van de houder worden verwijderd. Door de speciaal aangebrachte anti-fingerprint coating heeft uw toiletborstelhouder altijd een schone en hygiënische uitstraling.
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322351
prod_img: prod_img/afbeelding-ac-06-csa.jpg
---
