---
id: 21410974-afp-c
blueprint: products
art_nr: '21410974 AFP-C.'
title: 'Handdoek/Zeepdispenser RVS afp-c, CPU 2L E/S ST'
slug: handdoekzeepdispenser-rvs-afp-c-cpu-2l-es-st
price: 27300
cost_price: 14910
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Combinatiedispenser voor wandmontage.
  Zeepdispenser links, handdoekdispenser rechts.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21410974-afp-c..JPG
unit_price: 27300
vip_price: 24570
---
