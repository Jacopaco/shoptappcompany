---
id: '65653'
blueprint: products
art_nr: '65653'
title: 'UV zichtbox RVS t.b.v. trainingen incl. 2x lamp'
slug: uv-zichtbox-rvs-tbv-trainingen-incl-2x-lamp
price: 231.0516
cost_price: 148.11
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/65653.jpg
unit_price: 148.11
vip_price: 192.543
---
