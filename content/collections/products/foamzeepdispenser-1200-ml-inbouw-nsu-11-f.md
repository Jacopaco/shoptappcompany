---
id: '21417496'
blueprint: products
art_nr: '21417496'
title: 'Foamzeepdispenser 1200 ml inbouw, NSU 11 F'
slug: foamzeepdispenser-1200-ml-inbouw-nsu-11-f
cost_price: 6325
unit_price: 12650
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 12650
vip_price: 11385
prod_img: prod_img/21417496.JPG
---
