---
id: '8112'
blueprint: products
art_nr: '8112'
title: 'Opvangschaal wandm. (plat) MQDTW0510-P'
slug: opvangschaal-wandm-plat-mqdtw0510-p
price: 5210
cost_price: 3126
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Opvangschaal voor wandmontage.
  Met naar voren uitschuifbare opvangschaal.
  Inclusief extra bevestigingsclip.
prod_img: prod_img/8112.JPG
unit_price: 5210
vip_price: 4689
---
