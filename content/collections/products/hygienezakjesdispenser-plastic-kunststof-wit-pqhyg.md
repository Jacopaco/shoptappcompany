---
id: '5592'
blueprint: products
art_nr: '5592'
title: 'Hygiënezakjesdispenser (plastic) kunststof wit, PQHyg'
slug: hygienezakjesdispenser-plastic-kunststof-wit-pqhyg
price: 1680
cost_price: 1058
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënezakjeshouder met rond gat aan de voorzijde.
  Wordt standaard met grijs, blauw, rood en transparant venster geleverd.
  Met slot en witte PQ sleutel.
  Voor wandmontage.
prod_img: prod_img/5592.JPG
unit_price: 1680
vip_price: 1512
---
