---
id: '12380'
blueprint: product
art_nr: '12380'
title: 'Handendroger automatisch wit, M09A'
slug: handendroger-automatisch-wit-m09a
cost_price: 26936
unit_price: 48975
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 48975
vip_price: 44077
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  U maakt het uw gasten graag zo comfortabel mogelijk. Daarom kiest u voor een ontspannen ruimte waar rust en eenheid heersen. Iets wat u bereikt door een zorgvuldige selectie van accessoires in uw sanitaire ruimte.

  De handendroger van Mediclinics is een ware bestseller als het om strak design en praktisch gebruiksgemak gaat. Het automatische start- en stopmoment van de droger, voorkomt ongewenst stroomverbruik. Door een eenvoudig verstelbare schuifknop, stelt u de intensiteit van de droogsnelheid in. Hiermee is ook de geluidssterkte regelbaar.
prod_img: prod_img/afbeelding-m09a.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1717499074
---
