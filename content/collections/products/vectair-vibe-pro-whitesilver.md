---
id: '8331300'
blueprint: products
art_nr: '8331300'
title: 'VECTAIR VIBE PRO White/Silver'
slug: vectair-vibe-pro-whitesilver
cost_price: 8441
unit_price: 11254
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
categories: 3afff57b-2df2-4899-a4bc-e0a3106bb2d4
price: 11254
vip_price: 11254
prod_img: prod_img/8331300.JPG
---
