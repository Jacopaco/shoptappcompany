---
id: '13157'
blueprint: product
art_nr: '13157'
title: 'Foamzeepdispenser 1500 ml RVS hoogglans, DJF0032C'
slug: foamzeepdispenser-1500-ml-rvs-hoogglans-djf0032c
cost_price: 7122
unit_price: 11870
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11870
vip_price: 10683
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  In een sanitaire ruimte waar functionaliteit de hoofdrol speelt, kan een stukje glamour uw gasten een heerlijk koninklijk gevoel geven. Want zeg nu zelf, tijdens de sanitaire stop is er toch niks prettiger dan even extra in de watten gelegd te worden?

  Met deze Mediclinics Foamzeepdispenser RVS hoogglans 1500 ml, wanen uw gasten zich in een oogopslag in de meest luxueuze ruimte van het gebouw. En dat is prettig. Want ook al is de sanitaire stop vooral noodzakelijk, het is ook een moment van rust en ontspanning. Deze foamzeepdispenser is niet alleen een architectonisch hoogstandje, hij onderscheidt zich ook nog eens in optimaal gebruiksgemak. De dispenser laat zich eenvoudig bedienen door de pushbutton aan de voorzijde in te drukken. Navullen is mogelijk via de bovenzijde van de dispenser.

  Een koninklijk accent in uw sanitaire ruimte? Dan kiest u voor de Mediclinics Foamzeepdispenser RVS hoogglans 1500 ml.
prod_img: prod_img/afbeelding-djf0032c-1719475898.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719475901
---
