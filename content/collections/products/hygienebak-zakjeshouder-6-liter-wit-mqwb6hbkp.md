---
id: '8405'
blueprint: products
art_nr: '8405'
title: 'Hygiënebak + zakjeshouder 6 liter wit, MQWB6HBKP'
slug: hygienebak-zakjeshouder-6-liter-wit-mqwb6hbkp
price: 19170
cost_price: 11502
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënebak met klepdeksel en gemonteerde zakjeshouder.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8405.JPG
unit_price: 19170
vip_price: 17253
---
