---
id: '8219161089'
blueprint: products
art_nr: '8219161089'
title: 'Procell AA batterijen 10 per verpakking'
slug: procell-aa-batterijen-10-per-verpakking
price: 8.6268
cost_price: 5.53
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/8219161089.jpg
unit_price: 7.95
vip_price: 7.189
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
