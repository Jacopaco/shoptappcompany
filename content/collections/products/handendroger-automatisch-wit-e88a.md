---
id: '12190'
blueprint: product
art_nr: '12190'
title: 'Handendroger automatisch wit, E88A'
slug: handendroger-automatisch-wit-e88a
cost_price: 32918
unit_price: 59850
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 59850
vip_price: 53865
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Productinformatie Mediclinics E88A,SANIFLOW handdroger met sensor
  Mediclinics Saniflow

  Een klassiek, robuust en betrouwbaar ontwerp voor de 21e eeuw.
  Motor met een hoge snelheid en een lange levensduur.
  Een constante luchtstroom voor het volledig drogen van de handen.
  Robuuste, betrouwbare handdroger van hoge kwaliteit.
  Een van de krachtigste luchtstromen op de markt, binnen zijn klasse.
  Het verwarmingselement en de motor hebben ingebouwde veiligheid thermische afsluiting.
  Verchroomde 360° roterende nozzle.
  Vandaal bestendig.
  Eenvoudige reiniging en onderhoud.
  Specificaties
  Artikelnummer 12190
  Model E88A
  Materiaal Staal
  Kleur Wit gepoedercoat
  Hoogte 248 mm
  Breedte 278 mm
  Diepte 212 mm
  Gewicht 5,9 kg
  Garantie 2 jaar na aankoopdatum
  Toepassing Wandmontage
  Eigenschappen Automatisch, 100 km/u, droogtijd: 29 seconden, 68 dB
  Wattage 2250 Watt
prod_img: prod_img/12190_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1717512819
---
