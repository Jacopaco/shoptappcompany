---
id: '8050'
blueprint: products
art_nr: '8050'
title: 'Zeepdisp.alu UP 1000ml LB, MQL10A'
slug: zeepdispalu-up-1000ml-lb-mql10a
price: 11925
cost_price: 7155
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig af gekit kan worden.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8050.JPG
unit_price: 11925
vip_price: 10732
---
