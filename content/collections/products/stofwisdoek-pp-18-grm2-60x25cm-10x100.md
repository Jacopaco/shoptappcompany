---
id: '572318250'
blueprint: products
art_nr: '572318250'
title: 'Stofwisdoek PP 18 gr/m2 + 60x25cm 10x100'
slug: stofwisdoek-pp-18-grm2-60x25cm-10x100
cost_price: 2450
unit_price: 0
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
price: 4174
vip_price: 3479
prod_img: prod_img/572318250.JPG
---
