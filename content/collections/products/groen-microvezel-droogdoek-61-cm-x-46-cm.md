---
id: '572183661'
published: false
blueprint: products
art_nr: '572183661'
title: 'Groen Microvezel droogdoek 61 cm x 46 cm'
slug: groen-microvezel-droogdoek-61-cm-x-46-cm
cost_price: 520
unit_price: 925
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
price: 1185
vip_price: 988
prod_img: prod_img/572183661.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712058959
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
