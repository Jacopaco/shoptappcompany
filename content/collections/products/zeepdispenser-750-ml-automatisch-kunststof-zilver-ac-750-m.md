---
id: '14207'
blueprint: products
art_nr: '14207'
title: 'Zeepdispenser 750 ml automatisch kunststof zilver, AC 750 M'
slug: zeepdispenser-750-ml-automatisch-kunststof-zilver-ac-750-m
price: 9500
cost_price: 5700
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser automatisch.
  Met slot en sleutel.
  Wordt met navulfles geleverd.
  Met pictogram aan de voorzijde.
  Werkt op 3 stuks LR14 batterijen.
  Wordt excl. batterijen geleverd. 
prod_img: prod_img/14207.JPG
unit_price: 9500
vip_price: 8550
---
