---
id: '12014'
blueprint: product
art_nr: '12014'
title: 'Haardroger zwart/zilver, SC0020CS'
slug: haardroger-zwartzilver-sc0020cs
cost_price: 3704
unit_price: 6734
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6734
vip_price: 6060
categories: f832415b-6165-4830-aa09-66452dc3e2ec
prod_img: prod_img/mediclinics-sc0020cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479396
---
