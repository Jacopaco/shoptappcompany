---
id: '8417'
blueprint: products
art_nr: '8417'
title: 'Reserverolhouder 2rols wit, MQRRH2P'
slug: reserverolhouder-2rols-wit-mqrrh2p
price: 6095
cost_price: 3657
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Duo reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 4puntsbevestiging.
prod_img: prod_img/8417.JPG
unit_price: 6095
vip_price: 5485
---
