---
id: '2105'
blueprint: products
title: 'STARLIGHT 2105-FSC toiletpapier 40x400 vel - 2 laags'
price: 1872
description: |-
  Het Starlight tissue 100% cellulose 2-laags toiletpapier geeft een zeer zacht en comfortabel gevoel. De rol bevat 400 vellen. Dit toiletpapier is uitermate geschikt voor luxueuze hotels en kantoorgebouwen die hun klanten het gevoel van “thuis” willen geven.

  - Materiaal 100% cellulose, wit toiletpapier
  - Afmetingen	400 vel op rol
  - Lagen	2 laags
  - Verpakking	10 x 4 rollen in folie
  - FSC certificaat

  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer. 
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2105_1_1.jpg'
art_nr: 2105
slug: toiletpapier-cellulose-2laags400-vel
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 1200
unit_price: 0
vip_price: 1560
prod_img: prod_img/2105_1_1-1688654553.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1740392035
client_prices:
  - 899acdd9-58f2-4404-81ad-dfb0bfaac689
  - ef60def1-f195-4b3b-a291-64fb129e6368
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - bfdce2db-c069-4c13-909b-6ba84fa04608
  - cf25c62f-df48-4e88-9c29-33f9eca1deb2
  - d28ddf0a-4681-415d-a8e8-3199d8a714ac
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - b56a4982-42c5-44cc-8e4e-b84a9afbbc3c
  - 23bd866b-4d41-4746-9cf0-b764f3eee09a
  - 50f975cc-aa02-494f-81aa-fbbd2580a184
  - 8e96d2c0-638d-4b77-9c40-9d443a81e60b
  - 691ea65f-48c3-4e02-82bb-23afe017ed72
  - 5162e19a-84e6-4f47-83e6-760863dc5c91
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - 2fec2271-8935-4b02-b3b1-dbddeb746b69
  - 890e8e99-6bec-4865-b34d-e6b4eb963f86
  - 6a9cdf89-b735-45d5-b0cd-ec4800e00349
locations:
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
  - 825ed51f-64cf-43e9-9e96-2d47e3311cf6
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - 225a4a10-f583-4663-aa73-07579bc9b443
  - 69de8cc9-fe34-4cfc-9a78-2d049cd4af51
  - 1f7effa5-1ac3-44d8-8fb4-b4b06575d101
  - df52f315-d21f-435f-9fc2-1317451c68b0
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 21fd8138-93dd-4a6e-992b-066f60627391
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
  - 52571907-9205-4d01-962e-3ba319c62abe
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - be14461b-4601-4a24-a6dc-17853268cc98
product_type: physical
fixed_price: 1995
---
