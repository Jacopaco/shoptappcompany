---
id: '2107'
published: false
blueprint: products
title: 'Toiletpapier recycled 2laags/400 vel'
price: 2327
description: 'Verpakking van 5 x 8 rollen in folie'
img_url: 'https://all-care.eu/content/files/Images/TAPP_Company_BV/2107_3.jpg'
art_nr: 2107
slug: toiletpapier-recycled-2laags400-vel
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 1492
unit_price: 0
vip_price: 1939
prod_img: prod_img/2107_3.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728993362
product_type: physical
---
