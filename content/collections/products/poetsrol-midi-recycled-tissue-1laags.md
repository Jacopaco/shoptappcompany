---
id: '2185'
blueprint: products
title: 'Poetsrol midi recycled tissue 1laags'
price: 1942
description: '18,5 cm | 6 x 300 meter in folie | ø 190 mm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2185_4_3.jpg'
art_nr: 2185
slug: poetsrol-midi-recycled-tissue-1laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: bc94b399-b67f-46d9-b72c-f1a1a74f4a30
cost_price: 1245
unit_price: 0
vip_price: 1618
prod_img: prod_img/2185_4_3.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653974
---
