---
id: '2170'
blueprint: products
title: 'STARLIGHT 2170 facial tissues 36x100 vel 2 laags'
price: 4430
description: |-
  Facial Tissue cellulose 2 laags

  Extra zachte tissue die zacht is voor de huid
  Hoge helderheid voor een hygiënische indruk
  Sterk en absorberend voor extra comfort

  Code 2170
  Materiaal 100% cellulose
  Kleur wit
  Afmetingen vel 20 x 21 cm
  2 laags
  Verpakking: in een omdoos zitten 36 doosjes met elk 100 tissues
  Prijs is per omdoos
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2170_3.jpg'
art_nr: 2170
slug: facial-tissue-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 2840
unit_price: 0
vip_price: 3692
prod_img: prod_img/2170_3.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729153959
client_prices:
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - bebc5c46-0553-4d62-b6ff-de33648493c4
locations:
  - a0f44b19-31c9-4be4-a49c-3c84bc45a623
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - a618e884-3fb2-4394-9a3f-effd139ef796
categories: b04aaee2-199c-4a4b-a694-6aa915ac1f86
product_type: physical
---
