---
id: '12464'
blueprint: product
art_nr: '12464'
title: 'Losse HEPA-filter, PQ14A'
slug: losse-hepa-filter-pq14a
cost_price: 2340
unit_price: 3900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 3900
vip_price: 3510
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
prod_img: prod_img/afbeelding-losse-hepa-filter-1-1719477596.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477602
---
