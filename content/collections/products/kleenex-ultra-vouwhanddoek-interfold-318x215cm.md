---
id: '6710'
blueprint: products
art_nr: '6710'
title: 'Kleenex Ultra vouwhanddoek interfold 31,8x21,5cm'
slug: kleenex-ultra-vouwhanddoek-interfold-318x215cm
price: 7982
cost_price: 5117
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
prod_img: prod_img/6710.JPG
unit_price: 5836
vip_price: 6652
---
