---
id: '8422'
blueprint: products
art_nr: '8422'
title: 'Reserverolhouder 1rol wit, MQRRH1P'
slug: reserverolhouder-1rol-wit-mqrrh1p
price: 5840
cost_price: 3504
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Reserverolhouder voor wandmontage.
  Eenvoudige uitname.
  Met 2puntsbevestiging.
prod_img: prod_img/8422.JPG
unit_price: 5840
vip_price: 5256
---
