---
id: '54010'
blueprint: products
title: 'Luchtverfrisser navulling Lemon'
price: 450
description: |-
  Lemon navulling.
  Met ribbels voor extra geurbeleving.
  Geen belasting voor het milieu (PP)
  Gaat ca. 30 dagen mee.
  Afname: per doos = 20 stuks
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515425
art_nr: '54010'
slug: luchtverfrisser-navulling-lemon
brands: 42c492f5-835a-428c-8f8f-159465aa12dd
cost_price: 270
unit_price: 450
vip_price: 450
prod_img: prod_img/54010.JPG
client_prices:
  - 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - 36972943-a97a-4a47-9fc5-cd7a6ae1fbe7
locations:
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - e6090873-ae14-4d62-b52a-da6089045f90
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
categories: f4cb9934-b741-4f26-bf31-378b213ae616
---
