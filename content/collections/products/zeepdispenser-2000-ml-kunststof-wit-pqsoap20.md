---
id: '5570'
blueprint: products
art_nr: '5570'
title: 'Zeepdispenser 2000 ml kunststof wit, PQSoap20'
slug: zeepdispenser-2000-ml-kunststof-wit-pqsoap20
price: 5335
cost_price: 3361
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser voor wandmontage.
  Met push cover, duwrichting naar de muur, voor het doseren van de vulgoederen. 
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.  
  Met slot en witte PQ sleutel.
  Met navulfles en zeeppomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo.
prod_img: prod_img/5570.JPG
unit_price: 5335
vip_price: 4801
---
