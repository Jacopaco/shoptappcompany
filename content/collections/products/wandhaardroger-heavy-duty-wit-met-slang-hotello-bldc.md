---
id: '17295'
blueprint: product
art_nr: '17295'
title: 'Wandhaardroger heavy duty wit met slang, Hotello BLDC'
slug: wandhaardroger-heavy-duty-wit-met-slang-hotello-bldc
cost_price: 9217
unit_price: 21850
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 21850
vip_price: 19665
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Een goede haardroger is onmisbaar om het haar droog te maken en in model te brengen. De warmte en luchtstroom zorgen voor een mooi resultaat. Ook als veel mensen er gebruik van maken, moet de haardroger optimaal blijven functioneren. Die zekerheid biedt de Hotello BLDC, de beste wandhaardroger voor extreem gebruik, lees hoge bezoekersfrequentie, in met name publieke ruimtes.

  De heavy duty Hotello BLDC heeft een krachtige, duurzame, borstelloze motor. De (witte) wanddroger start automatisch wanneer de handgreep uit de houder wordt gehaald. De handgreep blijft koel en dankzij de flexibele uitrekbare witte slang, tot 1,2 meter, kan de gebruiker letterlijk alle kanten op. De Hotello BLDC kan op twee manieren worden aangesloten: met een wandcontactdoos of een directe aansluiting. De veiligheid is gewaarborgd door de automatische 8 minuten veiligheidstimer.  

  Wandhaardroger heavy duty wit met slang, Hotello BLDC

  Heavy duty wandhaardroger wit.
  Met extreem krachtige, duurzame, borstelloze motor.
  Automatische 8 minuten veiligheidstimer.
  Start automatisch als de handgreep uit de houder wordt gehaald.
  Flexibele uitrekbare witte slang (tot 1,2 meter)

  2 mogelijkheden voor aansluiting:
  Wandcontactdoos of directe aansluiting.
  Houdt koele handgreep.
  IP34. 

  Artikelnummer 	17295
  Materiaal 	Kunststof
  Kleur 	Wit
  Hoogte 	550 mm
  Breedte 	147 mm
  Diepte 	112 mm
  Gewicht 	4 kg
  Garantie 	1 jaar na aankoopdatum
  Toepassing 	Wandmontage
  Toepassing 	Openbare ruimtes met een hoge bezoekersfrequentie
  Eigenschappen 	Start automatisch als de handgreep uit de houder wordt gehaald. Cool touch en 8 minuten safety timer
  Wattage 	1500 Watt
prod_img: prod_img/17295_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714730368
---
