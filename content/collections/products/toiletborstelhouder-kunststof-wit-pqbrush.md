---
id: '5594'
blueprint: products
art_nr: '5594'
title: 'Toiletborstelhouder kunststof wit, PQBrush'
slug: toiletborstelhouder-kunststof-wit-pqbrush
price: 2740
cost_price: 1726
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Toiletborstelhouder voor wandmontage.
  De ronde dichte onderzijde zit met een muurbevestiging aan de wand.
  Met kunststof zwarte vervangbare borstelkop.
prod_img: prod_img/5594.JPG
unit_price: 2740
vip_price: 2466
---
