---
id: '19250'
blueprint: products
art_nr: '19250'
title: 'GREEN HYGIENE 19250,FRIEDA Z vouw handdoek 25x23 cm - 2 laags - 4000 stuks'
slug: frieda-handdoekjes-z-vouw-100-recycled-2laags
price: 3736
cost_price: 2395
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
description: |-
  FRIEDA - Papieren handdoekjes ZZ-vouw, 4000 vel

  25 x 23 cm | 20 x 200 vellen in doos

  Hygiënepapier gemaakt van 100% gerecycled oud papier en CO2-neutrale en 100% plastic vrije verpakking.

  Artikelnummer 19250
  Materiaal recycled tissue
  Kleur Helder wit, wit 75%
  Afmetingen vel 25 x 23 cm
  Lagen 2 laags
  Volume 200 vellen
  Verpakking 20 pakjes
  Gewicht 9,5 KG
  HYGIËNE PAPIER GEMAAKT VAN 100% GERECYCLED PAPIER,CO2-NEUTRALE EN PLASTIC-VRIJE VERPAKKING.
  MISSIE GREEN HYGIENE
  Elk van onze producten is toegewijd aan een grote taak. Vel voor vel voorkomt het dat CO2 en plastic een steeds grotere impact hebben op onze wereld.

  Productverpakking
  100% PLASTIC VRIJ

  Dit begint al met de verpakking: Het beschermt veilig, zelfs zonder plastic. Omdat we het voor 100% zonder doen. De bruine doos voor onze producten is beschermt goed van productie tot transport en opslag. Zelfs het plakband is van papier gemaakt. Wij hebben het ontwerp van de verpakking teruggebracht tot twee drukkleuren.

  Toilet- en handdoekpapier
  VAN 100% GERECYCLED OUD PAPIER

  Met de Green Hygiene productlijn hebben we nu een goede oplossing voor onze omgeving gerealiseerd. Alle Green Hygiene producten worden van 100% gerecycled oud papier gemaakt. Desalniettemin doen deze producten geen concessies aan de goede kwaliteit die u gewend bent. En daarbij zijn alle producten – van gevouwen handdoeken tot aan de toilet – en handdoekrol -net zo functioneel en handig in gebruik als het minder milieuvriendelijke hygiënepapier dat tot nu toe wordt gebruikt.
prod_img: prod_img/19250.JPG
unit_price: 0
vip_price: 3113
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728999175
---
