---
id: '81010004'
blueprint: product
art_nr: '81010004'
title: 'Ariel (bonte was) waspoeder Professional Regular 7,15 kg (110 wasbeurten)'
slug: ariel-bonte-was-waspoeder-professional-regular-715-kg-110-wasbeurten
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
cost_price: 3176
unit_price: 4128
price: 5411
vip_price: 4509
description: |-
  Ga voor een stralend schone, witte was met het Ariel Regular waspoeder. Wit wasgoed wordt dankzij de unieke formule minder snel dof. Ook als u wast op temperaturen vanaf 30 graden gaat u altijd voor een perfect resultaat.

  De verpakking bevat 8,45 kg aan waspoeder. Dit is genoeg waspoeder voor 130 wasbeurten.
locations:
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
prod_img: prod_img/ariel_professional_ariel_waspoeder_professional_regular_845_kg_130_wasbeurten_sar05109_m1_big.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727268323
---
