---
id: '8055'
blueprint: products
art_nr: '8055'
title: 'Zeepdisp. wit UP 1000ml LB, MQL10P'
slug: zeepdisp-wit-up-1000ml-lb-mql10p
price: 12950
cost_price: 7770
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig af gekit kan worden.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
unit_price: 12950
vip_price: 11655
prod_img: prod_img/8055.JPG
---
