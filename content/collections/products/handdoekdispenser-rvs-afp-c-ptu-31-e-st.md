---
id: '3301423'
blueprint: products
art_nr: '3301423'
title: 'Handdoekdispenser RVS afp-c, PTU 31 E ST'
slug: handdoekdispenser-rvs-afp-c-ptu-31-e-st
price: 17680
cost_price: 10608
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
categories: 1b689325-711f-439f-9fff-b59122f29d67
prod_img: prod_img/3301423.JPG
unit_price: 17680
vip_price: 15912
---
