---
id: '14260'
blueprint: product
art_nr: '14260'
title: 'Handendroger hands-in wit automatisch'
slug: handendroger-hands-in-wit-automatisch
cost_price: 0
unit_price: 0
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 0
vip_price: 0
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  PlastiQline Twinflow PQ14A Hands-in handendroger automatisch voor wandmontage. Met verwarmingselement (slechts 400 Watt) met aan/uit schakelaar. Standaard is de UIT functie ingeschakeld. Met antibacteriële Biocote® bescherming. Laag stroomverbruik en instelbare motorsnelheid 420-1100 Watt, waardoor het geluid gehalveerd wordt. 62-72dB. Met HEPA filter. Verwisselbare watertank 480ml. Luchtsnelheid: 234-410 km/u. Droogtijd: 8-15 seconden. 100% recyclebaar. Met automatische veiligheid switch-off na 30 seconden continue gebruik. 

  Wat zijn de (hygiënische) voordelen?

  Schone lucht
  Dankzij de HEPA filter in de Twinflow wordt de lucht gezuiverd voordat deze op de handen terecht komt. Deze lucht is schoner dan de lucht die in de ruimte hangt.

  Geen rondspattend water 
  Wanneer de handen gedroogd worden spat het water er niet zomaar uit. Het water wordt netjes opgevangen in een, eenvoudig te verwijderen en te reinigen, waterreservoir.

  Antibacteriële Biocote bescherming
  De Twinflows zijn voorzien van een Biocote®, een antibacteriële laag waardoor micromechanismen zich niet aan de droger kunnen hechten.

  Milieuvriendelijk 
  Twinflows zijn milieuvriendelijk, energiezuinig én 100% recyclebaar. De Twinflow is opgenomen op de Nederlandse lijst van Energie investeringen 2017. 

  ##### Geschikt voor druk bezochte ruimtes 
  Vanwege de droogtijd van slechts 8-10 seconden is de Twinflow ideaal bij hoge bezoekersaantallen. De Twinflow is voorzien van een verwarmingselement met een aan/uit schakelaar.

  Specificaties
  - Materiaal: ABS kunststof
  - Kleur: Wit
  - Hoogte: 665 mm
  - Breedte: 320 mm
  - Diepte: 228 mm
  - Gewicht: 8,3 kg
  - Garantie: 2 jaar op onderdelen na aankoopdatum
  - Eigenschappen: Automatisch, met verwarmingselement met aan/uit schakelaar (400 W), 234-410 - km/u, droogtijd: 8-15 seconden, 62-72 dB. Met uitneembaar waterreservoir en HEPA filter
  - Wattage: 420-1100 Watt
prod_img: prod_img/12460_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714743057
---
