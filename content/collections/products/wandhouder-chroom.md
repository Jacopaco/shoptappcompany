---
id: '17144'
blueprint: product
art_nr: '17144'
title: 'Wandhouder chroom'
slug: wandhouder-chroom
cost_price: 457
unit_price: 1585
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 1585
vip_price: 1426
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Jolly, ringvormige verchroomde wandhouder.

  - Artikelnummer 17144
  - Model Jolly Chrome
  - Materiaal Kunststof, kleur Chroom
  - Hoogte 100 mm
  - Breedte 90 mm
  - Diepte 90 mm
  - Gewicht 0,1 kg
  - Garantie 1 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Toepassing Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
prod_img: prod_img/17144_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714739161
---
