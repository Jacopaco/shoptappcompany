---
id: 2301063-afp-c
blueprint: products
art_nr: '2301063 AFP-C.'
title: 'Hygiënebak 18 liter RVS afp-c, HBU 18 E ST'
slug: hygienebak-18-liter-rvs-afp-c-hbu-18-e-st
price: 29260
cost_price: 15979
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiënebak 18 liter.
  Met gesloten inworpklep en binnenring.
  Vrijstaand of voor wandmontage.
  Deksel is makkelijk afneembaar van de onderbak.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2301063-afp-c..JPG
unit_price: 29260
vip_price: 26334
---
