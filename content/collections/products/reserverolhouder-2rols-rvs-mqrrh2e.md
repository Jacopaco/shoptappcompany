---
id: '8419'
blueprint: products
art_nr: '8419'
title: 'Reserverolhouder 2rols RVS, MQRRH2E'
slug: reserverolhouder-2rols-rvs-mqrrh2e
price: 6855
cost_price: 4113
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
prod_img: prod_img/8419.JPG
unit_price: 6855
vip_price: 6169
---
