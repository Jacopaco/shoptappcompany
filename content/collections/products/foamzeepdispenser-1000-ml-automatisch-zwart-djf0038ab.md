---
id: '14051'
blueprint: product
art_nr: '14051'
title: 'Foamzeepdispenser 1000 ml automatisch zwart, DJF0038AB'
slug: foamzeepdispenser-1000-ml-automatisch-zwart-djf0038ab
cost_price: 10338
unit_price: 17230
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 17230
vip_price: 15507
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Mediclinics zwarte foamzeepdispenser automatisch voor wandmontage.

  - Met infra-rood sensor voor het doseren van de vulgoederen.
  - Met veerslot en Mediclinics sleutel.
  - Met kijkgleuf voor inhoudscontrole.
  - Met verwijderbare navulfles.
  - Ook eenvoudig vanaf bovenaf na te vullen.
  - Werkt op 6 AA batterijen. (niet meegeleverd)
  - Aanbevolen installatiehoogte: 25-30 cm boven de oppervlakte. 


  Specificaties

  - Artikelnummer 14051
  - Materiaal RVS
  - Kleur Zwart gepoedercoat
  - Hoogte 240 mm
  - Breedte 110 mm
  - Diepte 120 mm
  - Gewicht 1,25 kg
  - Garantie 2 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Toepassing Navulbaar
  - Vulling Foamzeep
  - Dosering 1,0 ml per slag
  - Inhoud 1000 ml
prod_img: prod_img/djf0038ab_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715071096
---
