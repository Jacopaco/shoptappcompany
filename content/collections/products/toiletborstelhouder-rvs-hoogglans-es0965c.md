---
id: '13195'
blueprint: product
art_nr: '13195'
title: 'Toiletborstelhouder RVS hoogglans, ES0965C'
slug: toiletborstelhouder-rvs-hoogglans-es0965c
cost_price: 5760
unit_price: 9600
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9600
vip_price: 8640
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Als echte ondernemer wilt u uw gasten zoveel mogelijk in de watten leggen. Een sfeervol ingerichte sanitaire ruimte zorgt voor een optimale comfortbeleving van uw gasten. En dat vraagt bijzondere aandacht voor uw accessoires.

  Als u wilt laten zien dat u aandacht heeft voor uw gasten, kiest u ongetwijfeld voor deze hoogglans toiletborstelhouder. Want ook al gaat het maar om een toiletborstelhouder, het design en de uitstraling kloppen in detail. Dat geldt ook voor de praktische uitvoering: het deksel van de toiletborstelhouder is nauwkeurig aan de steel bevestigd, zodat de houder na gebruik volledig afgesloten wordt. Een in het oog springend detail in elke sanitaire ruimte.
prod_img: prod_img/afbeelding-es0010c-1719322797.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322814
---
