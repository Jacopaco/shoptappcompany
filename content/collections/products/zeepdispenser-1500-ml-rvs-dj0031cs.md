---
id: '13149'
blueprint: product
art_nr: '13149'
title: 'Zeepdispenser 1500 ml RVS, DJ0031CS'
slug: zeepdispenser-1500-ml-rvs-dj0031cs
cost_price: 6801
unit_price: 11000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11000
vip_price: 9900
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Bij de inrichting van een groot project, kan het sanitair al snel in het vergeethoekje komen. En dat is jammer. Zeker omdat het sanitair een van de meest bezochte ruimtes van het gebouw is.

  Als u denkt dat u voor mooi design diep in de buidel moet tasten, heeft u het bij het verkeerde eind. De Mediclinics-accessoirelijn staat namelijk bekend om zijn opvallende én betaalbare design. Neem deze praktische en stijlvolle zeepdispenser in stoere rvs uitvoering. Het strakke ontwerp, uitgevoerd in duurzaam materiaal, maakt deze dispenser een prachtige aanwinst in elke toiletruimte. En of u nu de voorkeur geeft aan vloeibare zeep, alcoholgel, lotion of crème; deze dispenser handelt het allemaal.
prod_img: prod_img/dj0031cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719476477
---
