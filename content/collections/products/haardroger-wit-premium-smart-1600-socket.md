---
id: '17323'
blueprint: product
art_nr: '17323'
title: 'Haardroger wit, Premium Smart 1600 Socket'
slug: haardroger-wit-premium-smart-1600-socket
cost_price: 3840
unit_price: 9995
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 9995
vip_price: 8995
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Premium Smart Socket ABS kunststof wandhaardroger 1600 Watt. 

  - Wandhaardroger wit met wit spiraalsnoer
  - Met 2 standen voor de luchtopbrengst, 3 standen voor de temperatuur en koude luchtstand
  - Met aan/uit drukknop en smal mondstuk
  - Wandhouder met: aan/uit veiligheidsschakelaar en standaard EU stopcontact 16A
  - Eco-vriendelijk model
  - IP20
related_products:
  - '17381'
prod_img: prod_img/17323.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714730181
---
