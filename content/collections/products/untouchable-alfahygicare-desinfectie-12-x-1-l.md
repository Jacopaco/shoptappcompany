---
id: '98918'
blueprint: products
title: 'TAPP desinfectie 1000 ml Untouchable OS'
price: 1166
description: 'Open systeem'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/handzeep_1000ml_Tapp_Company.JPG'
art_nr: 98918
slug: tapp-desinfectie-1000-ml-untouchable-os
brands: 841597b5-0184-4fd2-98a0-6535c97b8673
categories: e912fdd5-abf6-4aeb-8d04-d94cf86723ed
cost_price: 357
prod_img: prod_img/98918.JPG
unit_price: 1166
vip_price: 1166
client_prices:
  - d8b397ec-2533-4b40-b16e-95ee5348e0cc
  - ff9c79b6-4366-49f2-8e91-3d5b0100bc47
  - 1300049a-83d7-47aa-86ff-49b7207633e6
  - d8b397ec-2533-4b40-b16e-95ee5348e0cc
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - c8eeae72-6c18-4ef1-8628-5087adf3afe9
  - 225a4a10-f583-4663-aa73-07579bc9b443
  - 9841050c-5fbe-43be-96c8-a2d6a5435499
---
