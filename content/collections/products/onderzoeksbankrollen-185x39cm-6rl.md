---
id: '1881'
published: false
blueprint: products
title: 'Onderzoeksbankrollen 185x39cm (6rl)'
price: 892
description: '39 cm | 6 x 150 meter | Ø 16 cm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/813-600x600.jpg'
art_nr: 1881
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712058702
---
