---
id: '2127'
blueprint: products
title: 'Maxi jumborollen recycled wit 2laags'
price: 23.81
description: 'Verpakking van 6 x 380 meter in folie'
img_url: 'https://all-care.eu/content/files/Images/TAPP_Company_BV/2127_1_1.jpg'
art_nr: 2127
slug: maxi-jumborollen-recycled-wit-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
cost_price: 17.01
prod_img: prod_img/2127_1_1.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653176
---
