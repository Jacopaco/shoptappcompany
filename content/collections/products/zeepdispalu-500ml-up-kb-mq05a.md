---
id: '8000'
blueprint: products
art_nr: '8000'
title: 'Zeepdisp.alu 500ml UP KB MQ05A'
slug: zeepdispalu-500ml-up-kb-mq05a
price: 8700
cost_price: 5220
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met korte bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig afgekit kan worden.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8000.JPG
unit_price: 8700
vip_price: 7830
---
