---
id: '61198'
blueprint: products
art_nr: '61198'
title: 'Handschoendispenser uno kunststof transparant, MQGLV'
slug: handschoendispenser-uno-kunststof-transparant-mqglv
price: 2700
cost_price: 1620
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Tafelmodel handschoendispenser.
  Met ovale opening voor optimale uitname van de handschoenen.
  Doosje kan aan beide zijkanten erin geschoven worden.
prod_img: prod_img/61198.JPG
unit_price: 2700
vip_price: 2430
---
