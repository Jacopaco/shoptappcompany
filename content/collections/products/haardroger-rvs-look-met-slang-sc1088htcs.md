---
id: '12004'
blueprint: product
art_nr: '12004'
title: 'Haardroger RVS look met slang, SC1088HTCS'
slug: haardroger-rvs-look-met-slang-sc1088htcs
cost_price: 37730
unit_price: 68600
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 68600
vip_price: 61740
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Haardroger drukknop RVS look, SC0088HTCS
  Wandhaardroger.
  Met een drukknopbediende timer van 90 seconden.
  Met slang en mondstuk.
  Bruto luchtopbrengst ca. 16l/sec.
  Luchtsnelheid: 65 km/u.
  Luchttemperatuur 65˚C.
  70dB.
prod_img: prod_img/mediclinics-sc1088htcs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479771
---
