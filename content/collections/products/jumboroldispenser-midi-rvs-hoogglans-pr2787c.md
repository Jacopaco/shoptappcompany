---
id: '13751'
blueprint: product
art_nr: '13751'
title: 'Jumboroldispenser midi RVS hoogglans, PR2787C'
slug: jumboroldispenser-midi-rvs-hoogglans-pr2787c
cost_price: 6414
unit_price: 10690
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 10690
vip_price: 9621
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  Wanneer u gastvrijheid hoog in het vaandel heeft staan, wilt u dat uw gasten niets tekort komen. Uw facilitaire dienst houdt daarom scherp in de gaten of dat laatste velletje nog niet in zicht is. Met de jumboroldispenser bent u altijd verzekerd van voldoende toiletpapier.

  Een smaakvol ingericht toilet valt direct op bij uw gasten. De stijlvol afgewerkte kap van deze rvs hoogglans jumboroldispenser laat dan ook direct uw oog voor detail zien. Met deze dispenser heeft u altijd de beschikking over een ruime voorraad toiletpapier. Het toiletpapier laat zich eenvoudig afscheuren langs de afscheurranden aan de onderkant van de dispenser.

  Met deze jumboroldispenser komt u nooit meer toiletpapier tekort
prod_img: prod_img/afbeelding-pr2787c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715087982
---
