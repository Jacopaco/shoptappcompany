---
id: '2169'
blueprint: products
title: 'Industrierol verlijmd blauw 2laags'
price: 2340
description: |-
  Industrierol verlijmd blauw 2 laags 24 cm
  Het papier wordt veel gebruikt in diverse werkplaatsen. 

  - Materiaal cellulose
  - Kleur blauw
  - Breedte rol 24 cm
  - Lagen 2 laags
  - Diameter rol 27 cm
  - Verpakking 2 x rollen van 300 meter in folie
  - Geschikt voor muur- en vloerstandaard
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2169-2_1.jpg'
art_nr: 2169
slug: industrierol-verlijmd-blauw-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
cost_price: 1500
unit_price: 0
vip_price: 1950
prod_img: prod_img/2169-2_1.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715598557
categories: bb31f7ae-4f77-4bd6-bb69-6b7c69019093
client_prices:
  - 465917bf-fcad-40f2-8dae-0de9505b610e
  - 465917bf-fcad-40f2-8dae-0de9505b610e
product_type: physical
---
