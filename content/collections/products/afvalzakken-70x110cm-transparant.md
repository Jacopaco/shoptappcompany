---
id: '150248600'
blueprint: product
art_nr: '150248600'
title: 'Afvalzakken 70x110cm transparant'
slug: afvalzakken-70x110cm-transparant
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 4381
unit_price: 6572
price: 8043
vip_price: 6702
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
locations:
  - 6c6798bc-4091-4300-85c9-01df9bed5053
prod_img: prod_img/150248600.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1734105028
---
