---
id: '13147'
blueprint: product
art_nr: '13147'
title: 'Mediclinics DJ0031C zeepdispenser 1500 ml RVS hoogglans'
slug: zeepdispenser-1500-ml-rvs-hoogglans-dj0031c
cost_price: 6600
unit_price: 11000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11000
vip_price: 9900
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Mediclinics DJ0031C navulbare zeepdispenser voor vloeibare zeep. Met kunststof zeepreservoir, inhoud: 1500 ml. Kijkvenster voor inhoudscontrole. Afsluitbaar met een drukslot. RVS gepolijst.

  Specificaties
  Artikelnummer 13147
  Model DJ0031C
  Materiaal RVS hoogglans
  Hoogte 240 mm
  Breedte 110 mm
  Diepte 133 mm
  Gewicht 1,25 kg
  Garantie 2 jaar na aankoopdatum
  Toepassing Wandmontage
  Toepassing Navulbaar
  Vulling Vloeibare zeep,lotions en crèmes
  Dosering 1,5 ml per slag
  Inhoud 1500 ml
prod_img: prod_img/dj0031c.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729237862
---
