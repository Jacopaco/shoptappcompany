---
id: '5500'
blueprint: products
art_nr: '5500'
title: 'Zeepdispenser 900 ml kunststof wit, PQSoap9'
slug: zeepdispenser-900-ml-kunststof-wit-pqsoap9
price: 3170
cost_price: 1997
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser voor wandmontage.
  Met push cover, duwrichting naar de muur, voor het doseren van de vulgoederen. 
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.  
  Met slot en witte PQ sleutel.
  Met navulfles en zeeppomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo.
prod_img: prod_img/5500.png
unit_price: 3170
vip_price: 2853
---
