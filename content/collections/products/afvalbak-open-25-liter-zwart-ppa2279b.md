---
id: '11044'
blueprint: product
art_nr: '11044'
title: 'Afvalbak open 25 liter zwart, PPA2279B'
slug: afvalbak-open-25-liter-zwart-ppa2279b
cost_price: 8940
unit_price: 14900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 14900
vip_price: 13410
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak zwart 25 liter open, PPA2279B
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/ppa2279b.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719846053
---
