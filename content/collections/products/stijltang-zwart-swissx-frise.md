---
id: '17257'
blueprint: product
art_nr: '17257'
title: "Stijltang zwart, Swiss'X Frise"
slug: stijltang-zwart-swissx-frise
cost_price: 0
unit_price: 8890
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 8890
vip_price: 8001
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  VALERA SWISS'X FRISE Stijltang zwart. Professionele stijltang voor volume en textuur.

  - Met 'Adaptive Heat Control' voor een perfect temperatuurevenwicht
  - Aan/Uit 230 graden Celsius
  - Met vier dunne en ondiepe platen met een uitgesproken profiel voor opvallende golven
  - De golfplaten zijn 24 x 90 cm en 3 mm hoog
  - Warmteisolerende 'Cool Touch' voor een comfortabel en veilig gebruik
  - Vijfvoudig keramische coating met toermalijn voor een betere en gezondere styling
  - Warmt snel op
  - Verende golfplaten voor een perfect en gelijkmatig contact met het haar
  - Spiraalkabel 3 meter
prod_img: prod_img/17257_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714730629
---
