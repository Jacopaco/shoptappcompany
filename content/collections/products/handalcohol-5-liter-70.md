---
id: '74415'
blueprint: products
title: 'Handalcohol 70% 5 liter'
price: 3388
description: Denteck
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/74415_Handalcohol_5_l_70_1.jpg'
art_nr: 74415
slug: handalcohol-70-5-liter
brands: 4de0e459-caba-4ced-b92b-af23ce5c7614
categories: e912fdd5-abf6-4aeb-8d04-d94cf86723ed
cost_price: 1560
prod_img: prod_img/74415.JPG
unit_price: 0
vip_price: 2823
updated_by: 222ea5d9-8bb0-4bc0-9a96-53629f69c900
updated_at: 1711717424
product_type: physical
client_prices:
  - e04cab04-8a7b-45a0-9698-aeb7ff898773
  - 23bd866b-4d41-4746-9cf0-b764f3eee09a
  - 691ea65f-48c3-4e02-82bb-23afe017ed72
  - 6a9cdf89-b735-45d5-b0cd-ec4800e00349
locations:
  - 175073cd-07d9-40df-9e81-927b690b393b
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
  - 6bdf50f6-6278-4fca-b58c-a1098f54fafa
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - a618e884-3fb2-4394-9a3f-effd139ef796
---
