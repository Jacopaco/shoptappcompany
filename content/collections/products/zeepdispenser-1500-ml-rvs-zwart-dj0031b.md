---
id: '13150'
blueprint: product
art_nr: '13150'
title: 'Zeepdispenser 1500 ml RVS zwart, DJ0031B'
slug: zeepdispenser-1500-ml-rvs-zwart-dj0031b
cost_price: 7653
unit_price: 12380
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 12380
vip_price: 11142
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Het is voor uw gasten prettig om na de sanitaire stop met schone handen aan het werk te gaan. Een goede zeepdispenser is daarbij cruciaal.

  Een keus voor de Mediclinics Zeepdispenser RVS zwart 1500 ml staat gelijk aan een keus voor optimale hygiëne, gebruiksgemak en elegant design. De zwarte rvs behuizing is tot in detail afgewerkt, wat deze dispenser zijn uiterst strakke look geeft. Deze dispenser biedt ruimte aan zo’n 1500 ml vloeibare zeep, wat hem erg geschikt maakt voor ruimten met een hoge bezoekersfrequentie. Gebruik is eenvoudig; met een druk op de knop doseert de dispenser precies de juiste hoeveelheid zeep die nodig is voor een hygiënische wasbeurt.
prod_img: prod_img/dj0031b.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719476380
---
