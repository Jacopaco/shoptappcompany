---
id: '14309'
blueprint: product
art_nr: '14309'
title: 'Verschoondoeken babyverschoontafel 80 stuks, CN0020'
slug: verschoondoeken-babyverschoontafel-80-stuks-cn0020
cost_price: 330
unit_price: 550
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 550
vip_price: 495
categories: b3e1fbfb-c2e5-4dec-87c7-096657368819
description: |-
  Verschoondoeken t.b.v. babyverschoontafels, CN0020

  Hygiënische verschoondoeken 40 x 25 cm die exact passen in de dispenser bij de volgende babyverschoontafels:
  - CP0016H - horizontaal wit
  - CP0016HCS - horizontaal RVS
  - CP0016V - verticaal wit
  - CP0016VCS - verticaal RVS

  Artikelnummer 	14309
  Materiaal 	Papier
  Hoogte 	400 mm
  Breedte 	250 mm
  Gewicht 	0,17 kg
  Toepassing 	Mediclinics babyverschoontafels
  Verpakking 	80 stuks
prod_img: prod_img/14309_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714742790
---
