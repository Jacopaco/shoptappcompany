---
id: '8320'
blueprint: products
art_nr: '8320'
title: 'Trio zeepdisp.RVS KB 500ml,MQTV05E'
slug: trio-zeepdisprvs-kb-500mlmqtv05e
price: 39100
cost_price: 23460
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  TRIO zeep-& desinfectiemiddeldispenser 500 ml met korte bedieningsbeugel en RVS doseerpomp.
  Met RVS achterplaat en afsluitplaat met zichtvenster.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8320.JPG
unit_price: 39100
vip_price: 35190
---
