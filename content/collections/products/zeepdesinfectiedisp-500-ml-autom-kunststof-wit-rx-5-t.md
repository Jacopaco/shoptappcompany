---
id: '24401528'
blueprint: products
art_nr: '24401528'
title: 'Zeep/Desinfectiedisp. 500 ml autom. kunststof wit, RX 5 T'
slug: zeepdesinfectiedisp-500-ml-autom-kunststof-wit-rx-5-t
price: 13098
cost_price: 7528
brands: 3d2fd03f-21e1-4d0a-9c57-6a17abfcfc49
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  RX 5 T automatische dispenser staand model.
  Ook voorzien van muurplaat voor wandmontage.
  Geschikt voor vloeibare desinfectie, alcoholgel, zeep en vloeibare lotion.
  Voor 500 ml euroflessen. Met uitwisselbare kunststof pomp. Dosering 1,5 ml per slag
  Inclusief navulfles en 4 C-Batterijen.
prod_img: prod_img/24401528.jpg
unit_price: 12250
vip_price: 10915
---
