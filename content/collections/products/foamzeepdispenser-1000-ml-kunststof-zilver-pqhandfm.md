---
id: '14213'
blueprint: products
art_nr: '14213'
title: 'Foamzeepdispenser 1000 ml kunststof zilver, PQHandFM'
slug: foamzeepdispenser-1000-ml-kunststof-zilver-pqhandfm
price: 3350
cost_price: 2010
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met sleutel.
  Wordt met navulfles geleverd.
  Met grijze drukknop, duwrichting naar de muur.
  Met kijkvenster voor inhoudscontrole
prod_img: prod_img/14213.JPG
unit_price: 3350
vip_price: 3015
---
