---
id: '8477'
blueprint: products
art_nr: '8477'
title: 'Handschoendispenser uno wit, MQGDP'
slug: handschoendispenser-uno-wit-mqgdp
price: 7870
cost_price: 4722
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Handschoendispenser voor wandmontage. Geschikt voor verschillende maten handschoendoosjes.
  Een verende RVS klembeugel houdt de doosjes op de plaats.
  Kan zowel horizontaal als verticaal gemonteerd worden.
  Met 2puntsbevestiging.
  Voor 1 handschoendoosje met max. binnenmaat: H 135 x B 240 x D 96 mm per item
prod_img: prod_img/8477.JPG
unit_price: 7870
vip_price: 7083
---
