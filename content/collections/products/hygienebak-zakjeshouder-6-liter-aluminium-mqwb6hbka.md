---
id: '8400'
blueprint: products
art_nr: '8400'
title: 'Hygiënebak + zakjeshouder 6 liter aluminium, MQWB6HBKA'
slug: hygienebak-zakjeshouder-6-liter-aluminium-mqwb6hbka
price: 14344
cost_price: 8607
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënebak met klepdeksel en gemonteerde zakjeshouder.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8400.JPG
unit_price: 14344
vip_price: 12909
---
