---
id: '5470'
blueprint: products
art_nr: '5470'
title: 'Zeepdispenser 1200 ml automatisch kunststof wit, PQASoap12'
slug: zeepdispenser-1200-ml-automatisch-kunststof-wit-pqasoap12
price: 6680
cost_price: 4208
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeepdispenser automatisch voor wandmontage.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Met slot en witte speciale sleutel.
  Met navulfles en zeeppomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo.
  Werkt op 4 stuks C-Cell (LR14)batterijen.
  Aanbevolen installatiehoogte: 25-30 cm boven de oppervlakte.
prod_img: prod_img/5470.JPG
unit_price: 6680
vip_price: 6012
---
