---
id: '204020'
published: false
blueprint: product
art_nr: '204020'
title: 'Afvalzakken 20 liter B2 wit à 1000 stuks'
slug: tork-afvalzak-wit-20-liter
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 8194
unit_price: 11300
price: 15044
vip_price: 12536
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
locations:
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
prod_img: prod_img/204020.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732785817
---
