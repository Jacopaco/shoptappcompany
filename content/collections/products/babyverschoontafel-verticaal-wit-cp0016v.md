---
id: '14315'
blueprint: product
art_nr: '14315'
title: 'Babyverschoontafel verticaal wit, CP0016V'
slug: babyverschoontafel-verticaal-wit-cp0016v
cost_price: 26700
unit_price: 44500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 44500
vip_price: 40050
categories: b3e1fbfb-c2e5-4dec-87c7-096657368819
description: |-
  Babyverschoontafel verticaal wit.
  Voor wandmontage.
  Gemaakt van bacterie resistent kunststof.
  Inclusief afsluitbare dispenser voor verschoonpapier/doeken.
  Met antibacteriële Biocote® bescherming.
  Met nylon veiligheidsgordel.
  Met aan weerskanten ophanghaken voor uw persoonlijke bezittingen.
  Aanbevolen installatiehoogte:
  800 mm vanaf het laagste punt
  700 mm vanaf het laagste punt op minder valide toiletten.
  Voldoet volledig aan de EN 12221-1 en EN 12221-2 richtlijnen.
  Laat uw baby onder geen enkele omstandigheid alleen of buiten uw bereik achter op de babyverschoontafel. 

  Niet geschikt voor montage op gipsplaten of holle muren!!

  - Artikelnummer	14315
  - Materiaal	Kunststof, Wit
  - Hoogte	480 mm
  - Breedte	550 mm
  - Diepte	100 mm (890 mm open)
  - Gewicht	13,4 kg
  - Eigenschappen 	Inclusief dispenser voor verschoonpapier/doeken, twee ophanghaken en veiligheidsgordel
  - Garantie	2 jaar na aankoopdatum
prod_img: prod_img/14315_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714742494
---
