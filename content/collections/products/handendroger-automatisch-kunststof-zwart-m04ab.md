---
id: '12106'
blueprint: product
art_nr: '12106'
title: 'Handendroger automatisch kunststof zwart, M04AB'
slug: handendroger-automatisch-kunststof-zwart-m04ab
cost_price: 9015
unit_price: 16390
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16390
vip_price: 14751
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Handendroger zwart automatisch, M04AB (Smart Flow)
  Automatische handendroger 1100 Watt.
  Bruto luchtopbrengst ca. 27l/sec.
  Luchtsnelheid: 85 km/u.
  Luchttemperatuur 47˚C.
  60dB.
  Droogtijd: 39 seconden.
prod_img: prod_img/afbeelding-m04ab-1719479043.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479051
---
