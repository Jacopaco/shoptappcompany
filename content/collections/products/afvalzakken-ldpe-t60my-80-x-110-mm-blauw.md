---
id: '2258'
published: false
blueprint: products
art_nr: '2258'
title: 'Afvalzakken LDPE T60my 80 x 110 mm blauw'
slug: afvalzakken-ldpe-t60my-80-x-110-mm-blauw
price: 38
cost_price: 24.5
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
unit_price: 0
vip_price: 31
prod_img: prod_img/2258.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688392551
---
