---
id: '8245'
blueprint: products
art_nr: '8245'
title: 'Hygiënebak + zakjesdispenser 6 liter wit, MQWB6HBHP'
slug: hygienebak-zakjesdispenser-6-liter-wit-mqwb6hbhp
price: 18535
cost_price: 11121
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
description: |-
  Hygiënebak met klepdeksel en gemonteerde zakjeshouder.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8245.JPG
unit_price: 18535
vip_price: 16681
---
