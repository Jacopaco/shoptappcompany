---
id: '2117'
blueprint: products
title: 'Handdoekrol cellulose 3laags'
price: 39.24
description: '22 cm | 10 x 70 meter in folie'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2117_1_1.jpg'
art_nr: 2117
slug: handdoekrol-cellulose-3laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
cost_price: 28.02
prod_img: prod_img/2117_1_1.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653383
---
