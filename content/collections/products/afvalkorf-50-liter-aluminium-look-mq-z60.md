---
id: '3821'
blueprint: products
art_nr: '3821'
title: 'Afvalkorf 50 liter aluminium look, MQ-Z60'
slug: afvalkorf-50-liter-aluminium-look-mq-z60
price: 5150
cost_price: 3090
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalkorf rechthoekig, vrijstaand of voor wandmontage.
  Met 4 geïntegreerde ophangpunten.
prod_img: prod_img/3821.JPG
unit_price: 5150
vip_price: 4635
---
