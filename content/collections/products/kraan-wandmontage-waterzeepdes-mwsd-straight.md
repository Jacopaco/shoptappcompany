---
id: '24402200'
blueprint: products
art_nr: '24402200'
title: 'Kraan wandmontage water/zeep/des. MWSD STRAIGHT'
slug: kraan-wandmontage-waterzeepdes-mwsd-straight
price: 104900
cost_price: 57900
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
categories: bc9df93b-d180-4c05-8fd8-719b6f9e1b0b
prod_img: prod_img/24402200.JPG
unit_price: 104900
vip_price: 104900
---
