---
id: '2100'
blueprint: products
title: 'STARLIGHT 2100-FSC toiletpapier 72x250 vel - 3 laags'
price: 3510
description: |-
  Toiletpapier cellulose 3 laags/250 vel | Verpakking van 9 x 8 rollen in folie. Assortiment van hoogwaardige cellulose. Superzacht, helder wit, sterk absorberend en hygiënisch.

  Code 2100
  Materiaal 100% cellulose, wit toiletpapier
  Afmetingen 250 vel
  Lagen 3 laags
  Verpakking 9 x 8 rollen

  FSC certificaat
  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2100_1_2.jpg'
art_nr: 2100
slug: toiletpapier-cellulose-3laags250-vel
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 2250
unit_price: 3178
vip_price: 2925
prod_img: prod_img/2100_1_2.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728993608
client_prices:
  - 5d51ad93-e2e6-43c1-a90f-49d97915b08f
  - 5d51ad93-e2e6-43c1-a90f-49d97915b08f
locations:
  - f2f903e1-4856-4d9b-86e4-16f4e1618fa8
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
product_type: physical
---
