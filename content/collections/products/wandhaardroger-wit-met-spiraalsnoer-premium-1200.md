---
id: '17211'
blueprint: product
art_nr: '17211'
title: 'Wandhaardroger wit met spiraalsnoer, Premium 1200'
slug: wandhaardroger-wit-met-spiraalsnoer-premium-1200
cost_price: 2171
unit_price: 6275
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 6275
vip_price: 5647
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Premium ABS kunststof wandhaardroger 1200 Watt. Wandhouder met aan/uit veiligheidsschakelaar. Voor de aansluiting op een wandcontactdoos. Met 1 stand voor de luchtopbrengst en temperatuur.

  - Artikelnummer 17211
  - Model Premium 1200
  - Materiaal Kunststof
  - Kleur Wit
  - Hoogte 183 mm
  - Breedte 245 mm
  - Diepte 115 mm
  - Gewicht 0,7 kg
  - Garantie 1 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Eigenschappen 1 stand luchtopbrengst, 1 stand temperatuur
  - Eigenschappen Wandhouder met aan/uit veiligheidsschakelaar
  - Wattage 1200 Watt
  - Toepassing Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
prod_img: prod_img/17211_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714738862
---
