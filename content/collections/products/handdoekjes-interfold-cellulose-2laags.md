---
id: '2140'
blueprint: products
title: 'STARLIGHT 2140-FSC Interfold handdoek 32x22 cm - 2 laags - 3200 stuks'
price: 5388
description: |-
  Handdoekjes interfold cellulose 2 laags 32x22 cm

  Code 2140-FSC
  Materiaal cellulose
  Kleur wit
  Afmetingen vel 32 x 22 cm
  2 laags
  Verpakking 20 bundels x 160 vel in doos

  FSC certificaat
  Deze Interfold handdoekjes zijn meermaals gevouwen, en hebben een lange lengte waardoor je minder handdoekjes nodig hebt.

  De FSC-certificering is een wereldwijd erkend keurmerk dat staat voor verantwoord bosbeheer.
  Bij het gebruik van hygiënepapier met het FSC-label weet u zeker dat het afkomstig is uit bossen die op een duurzame en sociaal verantwoorde manier worden beheerd.
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2135_3_2.jpg'
art_nr: 2140
slug: handdoekjes-interfold-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 3454
unit_price: 0
vip_price: 4490
prod_img: prod_img/2135_3_2-1688653528.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729153796
product_type: physical
client_prices:
  - 821664cd-db7e-417e-8c02-9d697d99fa7b
  - 821664cd-db7e-417e-8c02-9d697d99fa7b
---
