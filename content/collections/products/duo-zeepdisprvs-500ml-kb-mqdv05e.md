---
id: '8310'
blueprint: products
art_nr: '8310'
title: 'Duo zeepdisp.RVS 500ml KB, MQDV05E'
slug: duo-zeepdisprvs-500ml-kb-mqdv05e
price: 31864
cost_price: 19119
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  DUO zeep-& desinfectiemiddeldispenser met korte bedieningsbeugel en RVS doseerpomp.
  Met RVS achterplaat en afsluitplaat met zichtvenster.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8310.JPG
unit_price: 31864
vip_price: 28677
---
