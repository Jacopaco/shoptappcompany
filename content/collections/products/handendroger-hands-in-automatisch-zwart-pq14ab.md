---
id: '12480'
blueprint: product
art_nr: '12480'
title: 'Handendroger HANDS-IN automatisch zwart, PQ14AB'
slug: handendroger-hands-in-automatisch-zwart-pq14ab
cost_price: 49200
unit_price: 82000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 82000
vip_price: 73800
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Na een ontspannen sanitaire stop zit u niet te wachten op het lawaai van een blazende handedroger. De ontwerpers van de PlastiQ Line hebben daar al rekening mee gehouden. Met deze handendroger van PlastiQ Line met speciale geluidsreductie, rond u uw toiletbezoek op een rustige manier af. 

  Er is duidelijk goed nagedacht over het ontwerp van deze ranke handendroger; de zwarte kast is even opvallend als subtiel. Tel daar de subtiele geluidssterkte en ongekend snelle droogtijd bij op, en u heeft een ideaal exemplaar in handen. Gebruik is eenvoudig; de droger slaat aan, zodra u de handen in het daarvoor bestemde gedeelte plaatst. Bovendien is de droger voorzien van een Biocote® bescherming, wat hygiënisch gebruik garandeert.
prod_img: prod_img/12480_-pq14ab-1719477519.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477527
---
