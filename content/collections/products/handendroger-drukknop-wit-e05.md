---
id: '12260'
blueprint: product
art_nr: '12260'
title: 'Handendroger drukknop wit, E05'
slug: handendroger-drukknop-wit-e05
cost_price: 24090
unit_price: 43800
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 43800
vip_price: 39420
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Of uw gasten zich nu in een restaurant of in een bedrijfsruimte bevinden, een schone en smaakvol ingerichte sanitaire ruimte geeft uw gasten een extra ‘thuis-gevoel’. Daarom zorgt u voor stijlvolle accessoires, die duidelijk getuigen van goede smaak.

  Met deze handendroger van Mediclinics kunt u voor de dag komen. Het witte en strak gevormde ontwerp is een boeiende aanvulling in uw sanitaire ruimte. Aan de voorzijde van de droger bevindt zich een tuit, waardoor de warme lucht zich verplaatst. Door de nauwkeurig ingestelde temperatuur, heeft de droger slechts een halve minuut nodig om zijn werk optimaal te doen.
prod_img: prod_img/mediclinics-e05.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478544
---
