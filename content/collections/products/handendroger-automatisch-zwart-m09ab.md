---
id: '12395'
blueprint: product
art_nr: '12395'
title: 'Handendroger automatisch zwart, M09AB'
slug: handendroger-automatisch-zwart-m09ab
cost_price: 27445
unit_price: 48975
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 48975
vip_price: 44077
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  U hoeft geen interieurarchitect te zijn, om in een oogopslag te zien dat deze handendroger een uniek design heeft. De elegante ronde vormen en subtiele droogruimte maken deze handendroger geschikt voor elke stijlvol ingerichte sanitaire ruimte.

  Het eenvoudige gebruik en de krachtige uitstraling maken deze zwarte gepoedercoate variant een echte bestseller. De droger wordt automatisch geactiveerd wanneer huidcontact plaatsvindt. Door de snelle verplaatsing van de warme lucht is activatie van een halve minuut genoeg voor volledig droge handen. 

  Extra hygiëne (aanraking van de handendroger is niet nodig)
  Stijlvol design, passend bij elke sanitaire ruimte
prod_img: prod_img/afbeelding-m09ab-1719477832.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477837
---
