---
id: '13181'
blueprint: product
art_nr: '13181'
title: 'Zeeppomp inbouw 950 ml hoogglans, DJT118'
slug: zeeppomp-inbouw-950-ml-hoogglans-djt118
cost_price: 4770
unit_price: 7950
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 7950
vip_price: 7155
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Als het om hygiëne gaat, moeten uw gasten op u kunnen rekenen. U zorgt dan ook voor een schone en opgeruimde toiletruimte voorzien van de juiste accessoires. Een zeepdispenser is daarbij essentieel.

  Dat de ontwerpers van deze inbouwzeepdispenser weten wat stijl is, blijkt uit het speciale design van deze dispenser. De praktische inbouwpomp houdt op subtiele manier de voorraadflacon uit het oog van uw gasten. Met een simpele druk op de knop doseert u eenvoudig de benodigde hoeveelheid zeep. U kunt de pomp navullen door het reservoir van onderaf los te draaien.
prod_img: prod_img/afbeelding-djt118-1719322918.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322928
---
