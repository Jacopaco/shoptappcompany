---
id: '11010'
published: false
blueprint: product
art_nr: '11010'
title: 'Pedaalafvalbak vierkant 6 liter wit, PP1206'
slug: pedaalafvalbak-vierkant-6-liter-wit-pp1206
cost_price: 2700
unit_price: 4500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 4500
vip_price: 4050
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
---
