---
id: '13196'
blueprint: product
art_nr: '13196'
title: 'Toiletborstelhouder RVS, ES0965CS'
slug: toiletborstelhouder-rvs-es0965cs
cost_price: 5760
unit_price: 9600
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 9600
vip_price: 8640
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Als gastheer hecht u grote waarde aan een schoon en verzorgd toilet. Een toilet waarin uw gasten niet geconfronteerd worden met onhygiënische vlekken of strepen. De toiletborstel van Mediclinics biedt daarvoor de oplossing.

  Een toiletborstelhouder mag dan vooral praktisch ingezet worden, maar het blijft een in het oog springend accessoire. De ontwerpers hebben daarom extra aandacht geschonken aan het design van deze toiletborstel. Het elegante ontwerp laat zien dat zelfs de meest eenvoudige accessoires een stijlvol uiterlijk verdienen. De praktische witte borstelkop is zo ontworpen dat het vuil eenvoudig en efficiënt verwijderbaar is. Zo heeft iedere gast de mogelijkheid het toilet na gebruik keurig achter te laten.
prod_img: prod_img/afbeelding-es0965cs-1719322726.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322733
---
