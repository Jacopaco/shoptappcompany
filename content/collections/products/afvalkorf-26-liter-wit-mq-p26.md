---
id: '3830'
blueprint: products
art_nr: '3830'
title: 'Afvalkorf 26 liter wit, MQ-P26'
slug: afvalkorf-26-liter-wit-mq-p26
price: 4635
cost_price: 2781
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalkorf rechthoekig, vrijstaand of voor wandmontage.
  Met 4 geïntegreerde ophangpunten.
prod_img: prod_img/3830.JPG
unit_price: 4635
vip_price: 4171
---
