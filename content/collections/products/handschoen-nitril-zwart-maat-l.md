---
id: cart00956
blueprint: product
art_nr: CART00956
title: 'Handschoen nitril Zwart maat L'
slug: handschoen-nitril-zwart-maat-l
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
cost_price: 288
unit_price: 375
price: 375
vip_price: 375
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
locations:
  - ce6da27a-85fa-4464-b5b5-820bbbe1f484
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
prod_img: prod_img/cart00927.JPG
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719829859
---
