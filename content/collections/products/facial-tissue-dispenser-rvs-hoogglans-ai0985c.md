---
id: '13672'
blueprint: product
art_nr: '13672'
title: 'Facial tissue dispenser RVS hoogglans, AI0985C'
slug: facial-tissue-dispenser-rvs-hoogglans-ai0985c
cost_price: 2340
unit_price: 3900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 3900
vip_price: 3510
categories: 4c580ad2-fca1-4d85-921b-911b48345e74
description: |-
  Een toilet is niet alleen een praktische gelegenheid, maar ook de ideale plek voor uw gasten om even te ontspannen. Met de Mediclinics facial tissue dispenser zorgt u voor een zacht doekje om het gezicht te verkoelen of make-up te verwijderen.

  Deze facial tissue dispenser geeft uw toiletruimte onmiskenbaar een tikje glamour. De hoogglans buitenkant zorgt ervoor dat deze dispenser direct in het oog van uw gasten springt. Een dispenser die gezien mag worden, zeker wanneer het om facial tissues gaat. De ovalen opening zorgt voor een handige uitname van de tissues. Voor elk damestoilet een onmisbaar item.

  Verwent u uw vrouwelijke gasten graag?
prod_img: prod_img/afbeelding-ai0985c-1719321045.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321054
---
