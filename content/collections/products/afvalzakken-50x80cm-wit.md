---
id: '180200700'
blueprint: product
art_nr: '180200700'
title: 'Afvalzakken 50x80cm wit'
slug: afvalzakken-50x80cm-wit
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 7838
unit_price: 11757
price: 14390
vip_price: 11992
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
locations:
  - 6c6798bc-4091-4300-85c9-01df9bed5053
prod_img: prod_img/180200700.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1734105061
---
