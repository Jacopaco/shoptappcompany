---
id: cart00647
blueprint: products
art_nr: CART00647
title: 'Abena Classic Handschoen Nitril blauw maat S'
slug: abena-classic-handschoen-nitril-blauw-maat-s
price: 349
cost_price: 222
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
prod_img: prod_img/cart00647.JPG
unit_price: 349
vip_price: 349
client_prices:
  - 20a3c4dd-dfc7-4b88-a8de-adf592de239c
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 20a3c4dd-dfc7-4b88-a8de-adf592de239c
locations:
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
  - 21fd8138-93dd-4a6e-992b-066f60627391
  - 0b583266-c93f-467d-89b8-83c65b7fb392
  - be14461b-4601-4a24-a6dc-17853268cc98
  - 68f6aae9-14ba-42f3-80aa-6e6c306fb98c
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
  - c83569ed-a907-4878-9fe2-ebcddaf260e8
  - 46b7a552-571b-454e-8f7b-d9475ea2ce79
  - 78839325-c105-4e00-88cd-9b808c86a969
  - 851491c7-811a-41ec-a665-8715a2ec61a9
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
  - ad521bf5-2669-452d-81b0-0b55218e7ac4
  - 16741830-9ac1-40cf-8b25-ea9ba2f30305
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - 3d925d9b-d247-486b-9eee-f541cb605f5b
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 356d30dd-aa1b-4538-a106-2d9d0b09c317
  - 5ed3871c-f423-4dc3-9755-7ee2d3a676bc
  - 7d0c81d5-f6d6-4305-ba84-89f822c63ab7
  - 82359df9-6fb1-4321-8adf-db6f37f28079
  - 7dd86089-bbef-48ea-a9aa-eb199078fec5
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
---
