---
id: '8465'
blueprint: products
art_nr: '8465'
title: 'Afvalkorf 60 liter RVS, MQE60'
slug: afvalkorf-60-liter-rvs-mqe60
price: 14219
cost_price: 8532
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/8465.JPG
unit_price: 14219
vip_price: 12797
---
