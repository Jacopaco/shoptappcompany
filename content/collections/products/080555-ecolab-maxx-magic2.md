---
id: '080555'
blueprint: products
title: 'Ecolab MAXX Magic2 - 2 x 5 L'
price: 13064
description: '080555 Ecolab MAXX Magic2  geconc. allesreiniger  superbevochtigend Doos 2x5L'
img:
  - product_images/ecolab-maxx-magic2-9084500-(1).jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687524246
art_nr: '080555'
slug: ecolab-maxx-magic2-2-x-5-l
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 7667
unit_price: 8433
vip_price: 10887
prod_img: prod_img/080555.jpg
---
