---
id: '13159'
blueprint: product
art_nr: '13159'
title: 'Foamzeepdispenser 1500 ml RVS, DJF0032CS'
slug: foamzeepdispenser-1500-ml-rvs-djf0032cs
cost_price: 7122
unit_price: 11870
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 11870
vip_price: 10683
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Ook al hebben we het over een van de kleinste ruimtes in het bedrijf, het is prettig als de sanitaire ruimte aan alle comforteisen voldoet. Een keus voor de Mediclinics Foamzeepdispenser zal zeker in de smaak vallen bij uw gasten.

  Foamzeep staat bekend om zijn zachtheid. Het reinigt de handen tot diep in de poriën en laat een heerlijke geur achter op de handen van uw gasten. Een prettig gevoel tijdens de drukke werkdag. Daarbij is het design van deze foamzeepdispenser op z’n minst opvallend te noemen. De strakke rvs behuizing past in elk interieur waar stijlvol design de hoofdrol speelt. Gebruik is eenvoudig: door het indrukken van de pushbutton aan de voorzijde, doseert de dispenser exact de juiste hoeveelheid foamzeep voor een hygiënische wasbeurt.
prod_img: prod_img/djf0032cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719475834
---
