---
id: '572183660'
published: false
blueprint: products
art_nr: '572183660'
title: 'Blauw Microvezel droogdoek 61 cm x 46 cm'
slug: blauw-microvezel-droogdoek-61-cm-x-46-cm
cost_price: 520
unit_price: 925
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
price: 1185
vip_price: 988
prod_img: prod_img/572183660.JPG
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
