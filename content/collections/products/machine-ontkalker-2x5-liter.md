---
id: prca10
blueprint: products
art_nr: PRCA10
title: 'Machine ontkalker 2x5 liter'
slug: machine-ontkalker-2x5-liter
price: 4294
cost_price: 2520
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/prca10.JPG
unit_price: 2520
vip_price: 3578
---
