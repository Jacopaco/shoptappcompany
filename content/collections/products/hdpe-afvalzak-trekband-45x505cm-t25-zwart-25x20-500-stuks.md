---
id: '561455055'
published: false
blueprint: product
art_nr: '561455055'
title: 'HDPE afvalzak trekband 45x50+5cm T25 zwart (25x20) 500 stuks'
slug: hdpe-afvalzak-trekband-45x505cm-t25-zwart-25x20-500-stuks
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 2106
unit_price: 0
price: 3866
vip_price: 3222
locations:
  - 11006000-90bb-4728-b1e4-eb18e446374d
  - 28a7c695-6e63-45b3-b248-504e489c1bac
  - 89b7b333-1069-4801-8fa9-951e9839806a
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 7fd6f47e-0007-4685-8688-998d5ba15bec
  - 9febd33c-508e-4088-80ef-1681be040e54
  - e7be31ce-3466-4fd8-8ab7-67b612d0b5aa
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - c0b6b93f-b7ef-406c-b70d-55b11437c3b1
  - 4e8302b0-f18d-40a8-865e-7040d27f3085
  - 425b24e6-8d16-4aec-a49f-5353d0902b91
  - 84dec7cd-ea0a-46e7-a7a9-b254693d1da8
  - 175073cd-07d9-40df-9e81-927b690b393b
  - 4cb525d2-7ca0-4a71-918a-e46b92e51921
  - 1f727611-7eac-4fc0-978b-0e9b171f437a
prod_img: prod_img/561455055.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1727269212
---
