---
id: '5584'
blueprint: products
art_nr: '5584'
title: 'Toiletroldispenser 2rols kunststof/metaal wit, PQDuoPlus'
slug: toiletroldispenser-2rols-kunststofmetaal-wit-pqduoplus
price: 5900
cost_price: 3995
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  Toiletroldispenser voor 2 rollen voor wandmontage.
  De toiletkokers zitten om 2 kunststof assen, die zorgen voor automatische toiletrolwisseling.
  Assen blijven met lege kokers in de dispenser zitten.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster, op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Modulair systeem.
unit_price: 5900
vip_price: 5310
prod_img: prod_img/5584.JPG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1686220284
client_prices:
  - 465917bf-fcad-40f2-8dae-0de9505b610e
  - 465917bf-fcad-40f2-8dae-0de9505b610e
---
