---
id: '5650'
blueprint: products
art_nr: '5650'
title: 'Afvalbak 43 liter half open wit, PQA43'
slug: afvalbak-43-liter-half-open-wit-pqa43
price: 4720
cost_price: 2974
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met semi-open deksel.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
  Inhoud: 43 liter.
prod_img: prod_img/5650.JPG
unit_price: 4720
vip_price: 4248
---
