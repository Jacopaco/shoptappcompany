---
id: '12390'
blueprint: product
art_nr: '12390'
title: 'Handendroger automatisch RVS, M09ACS'
slug: handendroger-automatisch-rvs-m09acs
cost_price: 27775
unit_price: 50500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 50500
vip_price: 45450
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Op zoek naar strak design met een stoere rvs look? Dan past deze handendroger helemaal binnen uw sanitaire wensen. De verrassend strakke designlook maakt van deze handendroger een regelrechte blikvanger.

  De ene gast droogt zijn handen graag snel, terwijl de ander er liever de tijd voor neemt. Door de verstelbare motor controleert u de luchtsnelheid en de geluidssterkte. De buitengewoon snelle droogtijd van 10 - 12 seconden maakt deze handendroger zeer snel en efficiënt in gebruik.
prod_img: prod_img/afbeelding-m09acs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1717498971
---
