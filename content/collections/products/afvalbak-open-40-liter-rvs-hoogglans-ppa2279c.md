---
id: '11041'
blueprint: product
art_nr: '11041'
title: 'Afvalbak open 40 liter RVS hoogglans, PPA2279C'
slug: afvalbak-open-40-liter-rvs-hoogglans-ppa2279c
cost_price: 10440
unit_price: 17400
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 17400
vip_price: 15660
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak hoogglans 25 liter open, PPA2279C
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/open-bins-ppa2279c-1719846109.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719846115
---
