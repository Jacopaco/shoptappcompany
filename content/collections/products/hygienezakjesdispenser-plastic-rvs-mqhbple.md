---
id: '8280'
blueprint: products
art_nr: '8280'
title: 'Hygiënezakjesdispenser (plastic) RVS, MQHBPLE'
slug: hygienezakjesdispenser-plastic-rvs-mqhbple
price: 4825
cost_price: 2895
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 045bfd6e-ed53-4fa2-b8dc-bebd8c48229b
prod_img: prod_img/8280.JPG
unit_price: 4825
vip_price: 4342
---
