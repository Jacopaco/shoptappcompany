---
id: '98990'
blueprint: products
title: 'ALL-CARE handzeep 1x500 ml eurofles'
price: 241
img_url: 'https://all-care.eu/content/files/Images/TAPP_Company_BV/98990_Handzeep_12x_500_ml_1.jpg'
art_nr: 98990
slug: handzeep-eco-500ml-eurofles-vierkant
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: de901aec-80dd-40c6-be90-f2c5d8a24a1e
cost_price: 155
unit_price: 0
vip_price: 201
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1740392152
client_prices:
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
  - bebc5c46-0553-4d62-b6ff-de33648493c4
  - 4e00303b-45d0-49d8-9aad-8a8ef0e22948
  - 12c4ab0f-5529-4781-abd4-19c5cef834ec
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 1253e6c8-3d48-437f-b074-d8c75a7790fc
locations:
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 0ed46b40-5b9b-4df6-a92d-485c88408fe4
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
description: |-
  Deze handzeep 500 ML is een fris geparfumeerde, milde handreiniger met een mooie parelmoer uitstraling.
  Het product heeft een neutrale pH waarde, vergelijkbaar met de huid.

  Artikelnummer 98990
  Materiaal Handzeep
  Kleur Parelwit
  Toepassing alle dispenser die geschikt zijn voor 500 ml euroflessen
prod_img: prod_img/98990_002.jpg
product_type: physical
fixed_price: 215
---
