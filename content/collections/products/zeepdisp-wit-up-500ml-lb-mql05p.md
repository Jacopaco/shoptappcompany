---
id: '8025'
blueprint: products
art_nr: '8025'
title: 'Zeepdisp. wit UP 500ml LB, MQL05P'
slug: zeepdisp-wit-up-500ml-lb-mql05p
price: 10400
cost_price: 6240
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig af gekit kan worden.
  Wordt standaard met navulfles en geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8025.JPG
unit_price: 10400
vip_price: 9360
---
