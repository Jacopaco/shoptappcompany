---
id: '8060'
blueprint: products
art_nr: '8060'
title: 'Zeepdisp. RVS UP 1000ml LB, MQL10E'
slug: zeepdisp-rvs-up-1000ml-lb-mql10e
price: 13969
cost_price: 8382
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Zeep-& Desinfectiemiddeldispenser met lange bedieningsbeugel.
  Met RVS uitwisselbare doseerpomp.
  Met zwarte montageplaat die volledig afgekit kan worden.
  Wordt standaard met navulfles geleverd.
  Autoklaveerbaar.
prod_img: prod_img/8060.JPG
unit_price: 13969
vip_price: 12572
---
