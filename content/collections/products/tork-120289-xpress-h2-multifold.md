---
id: '120289'
published: false
blueprint: products
art_nr: '120289'
title: 'Tork Xpress® Zachte Multifold Handdoek H2'
slug: tork-120289-xpress-h2-multifold
price: 7948
cost_price: 5095
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
prod_img: prod_img/120289.png
unit_price: 6114
vip_price: 6623
description: |-
  Bied uw gasten goede handdroging en optimaal comfort met de zachte Advanced Tork Xpress® Zachte Multifold Handdoeken. Ze zijn zacht voor de huid en zorgen voor een luxueuze ervaring. Deze handdoeken zijn geschikt voor de Tork Xpress® Multifold Handdoek Dispenser, voor sanitaire ruimtes met een gemiddeld bezoekersaantal. Past ook in kleine ruimtes en biedt uw gasten comfort en hygiëne. Tissue is gemaakt van 100% gerecyclede vezels.

  Advanced-kwaliteit voor kostenbesparing en goede handdroging
  Aantrekkelijk Tork bladontwerp: maakt een geweldige indruk
  Vel-voor-vel-dosering voor verminderd verbruik en verhoogde hygiëne
  Embossing voelt fijn aan en maximaliseert het absorptieniveau
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729152344
---
