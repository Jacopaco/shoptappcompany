---
id: 21417719-afp-c
blueprint: products
art_nr: 21417719-AFP-C
title: 'Handdoekdispenser RVS afp-c + RVS uitneemplaatje, HSU 31 E ST'
slug: handdoekdispenser-rvs-afp-c-rvs-uitneemplaatje-hsu-31-e-st
cost_price: 8431
unit_price: 17680
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 17680
vip_price: 15912
prod_img: prod_img/21417719-afp-c.png
---
