---
id: '8300'
blueprint: products
art_nr: '8300'
title: 'Garagezeepdisp. alu 1000ml, MQHDV10A'
slug: garagezeepdisp-alu-1000ml-mqhdv10a
price: 13965
cost_price: 8379
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Garagezeepdispenser met korte bedieningsbeugel en RVS doseerpomp.
  Inclusief afsluitplaat met zichtvenster.
  Navulfles wordt standaard meegeleverd.
  Met zwarte afsluitplaat die volledig af gekit kan worden.
prod_img: prod_img/8300.JPG
unit_price: 13965
vip_price: 12568
---
