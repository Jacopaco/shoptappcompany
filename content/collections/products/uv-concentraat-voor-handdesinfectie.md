---
id: '54548'
blueprint: products
art_nr: '54548'
title: 'UV Concentraat voor handdesinfectie'
slug: uv-concentraat-voor-handdesinfectie
price: 1310
cost_price: 840
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: df16d57a-a11a-4f2b-a28c-23e0a555f918
unit_price: 840
vip_price: 1092
prod_img: prod_img/54548.jpg
---
