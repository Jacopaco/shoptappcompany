---
id: 21415815-afp-c
blueprint: products
art_nr: '21415815 AFP-C'
title: 'Spraydispenser 600 ml RVS afp-c, NSU 5 E/D ST'
slug: spraydispenser-600-ml-rvs-afp-c-nsu-5-ed-st
price: 16660
cost_price: 8996
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Spraydispenser voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415815-afp-c.JPG
unit_price: 16660
vip_price: 14994
---
