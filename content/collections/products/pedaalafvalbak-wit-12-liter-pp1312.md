---
id: '11085'
blueprint: product
art_nr: '11085'
title: 'Pedaalafvalbak wit 12 liter, PP1312'
slug: pedaalafvalbak-wit-12-liter-pp1312
cost_price: 3984
unit_price: 6640
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6640
vip_price: 5976
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/mediclinics-pp1312.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719845127
---
