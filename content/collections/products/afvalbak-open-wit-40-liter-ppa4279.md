---
id: '11030'
blueprint: product
art_nr: '11030'
title: 'Afvalbak open wit 40 liter, PPA4279'
slug: afvalbak-open-wit-40-liter-ppa4279
cost_price: 10140
unit_price: 16900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 16900
vip_price: 15210
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak wit 40liter open, PPA4279
  Afvalbak open.
  Met kunststof binnenring.
prod_img: prod_img/afbeelding-ppa4279.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1718806747
---
