---
id: '12126'
blueprint: product
art_nr: '12126'
title: 'Handendroger automatisch kunststof wit, M88APlus'
slug: handendroger-automatisch-kunststof-wit-m88aplus
cost_price: 10725
unit_price: 19500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 19500
vip_price: 17550
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Handendroger kunststof wit automatisch, M88APlus (Junior Plus)
  Automatische warme lucht handendroger voor wandmontage.
  1640 Watt.
  Bruto luchtopbrengst ca. 60l/sec.
  Luchtsnelheid: 60 km/u.
  Luchttemperatuur 52˚C.
  60dB.
  Droogtijd: 38 seconden.
prod_img: prod_img/mediclinics-m88aplus.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478906
---
