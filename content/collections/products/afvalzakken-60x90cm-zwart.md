---
id: '561609040'
blueprint: product
art_nr: '561609040'
title: 'Afvalzakken 60x90cm zwart'
slug: afvalzakken-60x90cm-zwart
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 1760
unit_price: 2640
price: 3231
vip_price: 2692
prod_img: prod_img/1012283-1733305953.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733305976
---
