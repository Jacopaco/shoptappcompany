---
id: 2201463-afp-c
blueprint: products
art_nr: '2201463 AFP-C.'
title: 'Toilet tissue dispenser (bulkpack) RVS afp-c, BUU 1 E ST'
slug: toilet-tissue-dispenser-bulkpack-rvs-afp-c-buu-1-e-st
price: 12660
cost_price: 6912
brands: f77a48e2-462d-4719-8961-2637e36823d5
description: |-
  Bulkpack dispenser
  Voor wandmontage.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Ook in wit verkrijgbaar!
prod_img: prod_img/2201463-afp-c..JPG
unit_price: 12660
vip_price: 11394
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
