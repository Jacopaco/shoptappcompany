---
id: '2254'
published: false
blueprint: products
art_nr: '2254'
title: 'Afvalzakken T10 50x55 |  25 liter zwart vervangen'
slug: afvalzakken-t10-50x55-25-liter-zwart-vervangen
price: 5342
cost_price: 2910
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
prod_img: prod_img/2254.JPG
unit_price: 0
vip_price: 4452
client_prices:
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - be14461b-4601-4a24-a6dc-17853268cc98
---
