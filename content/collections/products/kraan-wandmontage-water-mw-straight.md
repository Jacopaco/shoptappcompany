---
id: '25400004'
blueprint: products
art_nr: '25400004'
title: 'Kraan wandmontage water MW STRAIGHT'
slug: kraan-wandmontage-water-mw-straight
price: 54900
cost_price: 27477
brands: d17d6f0c-907f-412e-ad5d-ef68c1d2ad3c
categories: bc9df93b-d180-4c05-8fd8-719b6f9e1b0b
prod_img: prod_img/25400004.JPG
unit_price: 54900
vip_price: 54900
---
