---
id: '8230'
blueprint: products
art_nr: '8230'
title: 'Afvalbak 15 liter RVS, MQWB15E'
slug: afvalbak-15-liter-rvs-mqwb15e
price: 21200
cost_price: 12720
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak met klepdeksel.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandhouder.
  Met 4puntsbevestiging.
prod_img: prod_img/8230.JPG
unit_price: 21200
vip_price: 19080
client_prices:
  - f0a40d3e-b1c2-4568-994c-c4aecb027482
  - f0a40d3e-b1c2-4568-994c-c4aecb027482
locations:
  - 11d7f953-43aa-43d8-a76d-a6c9c45f0621
---
