---
id: cart00641
blueprint: products
art_nr: CART00641
title: 'ABENA handschoenen blauw Nitril maat XL 100 stuks'
slug: abena-handschoenen-blauw-nitril-maat-xl-100-stuks
price: 349
cost_price: 222
brands: 16f2ece5-3815-4743-bc02-35b2e1a43598
prod_img: prod_img/cart00641.JPG
unit_price: 349
vip_price: 349
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732788774
client_prices:
  - 20a3c4dd-dfc7-4b88-a8de-adf592de239c
  - 08b3a7c5-f348-4839-99b5-bb51bea2dace
  - ab594c46-8493-4a0c-b4f7-a8e838482d7a
  - 20a3c4dd-dfc7-4b88-a8de-adf592de239c
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 3af04335-2263-411c-a5cb-7ff339ae2a46
  - 87ab27e8-a05b-4dc5-9e3d-446fa8e3f4c9
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 8aa63c62-4698-4f95-97dc-65feb2c1bba3
  - 21fd8138-93dd-4a6e-992b-066f60627391
  - 0b583266-c93f-467d-89b8-83c65b7fb392
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - be14461b-4601-4a24-a6dc-17853268cc98
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9febd33c-508e-4088-80ef-1681be040e54
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 1f727611-7eac-4fc0-978b-0e9b171f437a
  - ff938854-4adb-42f4-aae1-2b3f0d52b517
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 0b83c198-6db3-49db-843a-ae2c3dd953cc
  - 84dec7cd-ea0a-46e7-a7a9-b254693d1da8
  - 11006000-90bb-4728-b1e4-eb18e446374d
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
  - 7dd86089-bbef-48ea-a9aa-eb199078fec5
  - 3d925d9b-d247-486b-9eee-f541cb605f5b
  - c83569ed-a907-4878-9fe2-ebcddaf260e8
  - 98c1d01f-7a34-4542-9bc7-cd1baa5fc119
  - 35ffadc8-cd7a-458b-b8f3-01732fd6cb74
  - ef78b35c-42e4-458c-a65b-1f2446135a30
categories: d806b731-8bf2-430f-adc1-1fce1d95b197
product_type: physical
---
