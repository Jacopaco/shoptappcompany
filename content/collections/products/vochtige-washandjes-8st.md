---
id: cart00915
blueprint: products
art_nr: CART00915
title: 'Vochtige washandjes 8st'
slug: vochtige-washandjes-8st
price: 140
cost_price: 90
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/cart00915.jpg
unit_price: 117
vip_price: 117
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
  - f5a4240b-d0d2-4c39-9cba-50eb7411acc9
  - 383b0cc2-fa6d-44cc-bd87-ed8808c34d0e
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
  - 9febd33c-508e-4088-80ef-1681be040e54
  - 0d65ba09-ad21-48c3-b963-f2bd0d31321b
  - 7148a8c7-c64d-4f8e-9f0f-71a331e04159
  - b4939c11-128b-47f8-a193-2067e03a205e
  - 1f727611-7eac-4fc0-978b-0e9b171f437a
  - ff938854-4adb-42f4-aae1-2b3f0d52b517
  - 6c6798bc-4091-4300-85c9-01df9bed5053
  - 0b83c198-6db3-49db-843a-ae2c3dd953cc
  - 84dec7cd-ea0a-46e7-a7a9-b254693d1da8
  - 11006000-90bb-4728-b1e4-eb18e446374d
  - 9bfe9ece-130b-4c3e-9900-ac455852d8f3
---
