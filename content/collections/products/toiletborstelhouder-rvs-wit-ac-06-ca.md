---
id: '13205'
blueprint: product
art_nr: '13205'
title: 'Toiletborstelhouder RVS wit, AC-06-CA'
slug: toiletborstelhouder-rvs-wit-ac-06-ca
cost_price: 3300
unit_price: 5500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 5500
vip_price: 4950
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Wat de MediQo-line zo opvallend maakt, is het strakke ontwerp. Dat is duidelijk zichtbaar aan deze toiletborstelhouder uit de serie. De strak vormgegeven buitenkant houdt op subtiele wijze de toiletborstel uit het oog van de bezoekers.

  Een extra voordeel van deze houder is de speciale vandaalbestendige 2-puntsbevestiging. Door de solide wandbevestiging kan de houder niet van de muur worden getrokken of geschopt. Het wit gepoedercoate rvs materiaal biedt bovendien voldoende bescherming om de toiletborstel te beveiligen tegen ongewenste vernielingen.
prod_img: prod_img/afbeelding-toiletborstelhouder-ac06ca-1719322382.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719322389
---
