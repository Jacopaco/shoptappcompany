---
id: '12370'
blueprint: product
art_nr: '12370'
title: 'Handendroger automatisch RVS, M06ACS'
slug: handendroger-automatisch-rvs-m06acs
cost_price: 28050
unit_price: 51000
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 51000
vip_price: 45900
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Al gebruikt u uw sanitaire ruimte voornamelijk voor praktische doeleinden, het oog wil ook wat. Met een ontspannen en sfeervol toilet, maakt u een onvergetelijke indruk op uw gasten.

  Deze rvs handendroger van Mediclinics is het prototype van eigentijds design. De moderne, strakke lijnen en de subtiel schuin weglopende zijde, maken van deze handendroger meer dan een eenvoudig accessoire. De handendroger kenmerkt zich door een optimale droogtijd en een aangename luchttemperatuur. Een aangename verrassing voor uw bezoekers.
prod_img: prod_img/afbeelding-m17acs-1719477901.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719477905
---
