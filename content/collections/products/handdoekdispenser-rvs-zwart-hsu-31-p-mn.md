---
id: '21422256'
blueprint: products
art_nr: '21422256'
title: 'Handdoekdispenser RVS zwart, HSU 31 P MN'
slug: handdoekdispenser-rvs-zwart-hsu-31-p-mn
price: 17680
cost_price: 10608
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
prod_img: prod_img/21422256.JPG
unit_price: 17680
vip_price: 15912
---
