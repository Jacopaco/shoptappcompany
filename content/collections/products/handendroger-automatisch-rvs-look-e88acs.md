---
id: '12230'
blueprint: product
art_nr: '12230'
title: 'Handendroger automatisch RVS look, E88ACS'
slug: handendroger-automatisch-rvs-look-e88acs
cost_price: 36575
unit_price: 66500
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 66500
vip_price: 59850
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Als het om hygiëne gaat, zullen uw gasten hoge eisen stellen. Een goed werkende zeepdispenser en handendroger zijn daarom cruciaal in elke sanitaire ruimte.

  Met de Mediclinics handendroger koopt u niet alleen kwaliteit, maar ook een smaakvol design. De strak afgewerkte stalen behuizing – rvs look –, sluit naadloos aan op elke interieurstijl. Tel daar de ideale luchttemperatuur, de automatische activatie en de droogsnelheid bij op, en uw keus is snel gemaakt.
prod_img: prod_img/mediclinics-e88acs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478579
---
