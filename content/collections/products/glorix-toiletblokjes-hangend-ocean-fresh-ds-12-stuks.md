---
id: '7513513'
blueprint: products
art_nr: '7513513'
title: 'Glorix toiletblokjes hangend ocean fresh ds. 12 stuks'
slug: glorix-toiletblokjes-hangend-ocean-fresh-ds-12-stuks
price: 2028
cost_price: 1300
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
prod_img: prod_img/7513513.jpg
unit_price: 1300
vip_price: 1690
---
