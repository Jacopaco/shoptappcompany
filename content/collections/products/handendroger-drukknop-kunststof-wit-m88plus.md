---
id: '12125'
blueprint: product
art_nr: '12125'
title: 'Handendroger drukknop kunststof wit, M88Plus'
slug: handendroger-drukknop-kunststof-wit-m88plus
cost_price: 10890
unit_price: 19800
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 19800
vip_price: 17820
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Handendroger kunststof wit drukknop, M88Plus (Junior Plus)
  Handendroger – drukknop bediening.
  1640 Watt.
  Met een druk op de knop wordt een 45 seconden timer geactiveerd.
  Bruto luchtopbrengst ca. 60l/sec.
  Luchtsnelheid: 60 km/u.
  Luchttemperatuur 51˚C.
  60dB.
  Droogtijd: 38 seconden.
prod_img: prod_img/mediclinics-m88plus.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478951
---
