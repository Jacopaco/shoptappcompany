---
id: '21417499'
blueprint: products
art_nr: '21417499'
title: 'Foamzeepdispenser 600 ml inbouw, NSU 5 F'
slug: foamzeepdispenser-600-ml-inbouw-nsu-5-f
cost_price: 6039
unit_price: 12660
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
price: 12660
vip_price: 11394
prod_img: prod_img/21417499.png
---
