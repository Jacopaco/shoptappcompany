---
id: 6540-1881
blueprint: products
art_nr: '6540  1881'
title: 'Onderzoeksbankrollen cellulose 2 lgs 39 cm x 150 m 6st'
slug: onderzoeksbankrollen-cellulose-2-lgs-39-cm-x-150-m-6st
price: 12405
cost_price: 6081
brands: d2e2fc4b-bc98-488c-90a2-f0adb231736a
categories: 8218d088-f732-41b2-a36e-13cfec06fed9
prod_img: prod_img/6540-1881.jpg
unit_price: 8920
vip_price: 10337
client_prices:
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
  - 2fec2271-8935-4b02-b3b1-dbddeb746b69
  - b0c021bd-503f-4f45-84de-dcc0dfc662ac
locations:
  - 87ab27e8-a05b-4dc5-9e3d-446fa8e3f4c9
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 3d925d9b-d247-486b-9eee-f541cb605f5b
  - 35ffadc8-cd7a-458b-b8f3-01732fd6cb74
  - ef78b35c-42e4-458c-a65b-1f2446135a30
---
