---
id: 5541-br
blueprint: products
art_nr: 5541-BR
title: 'Handdoekdispenser mini kunststof wit, PQMiniH'
slug: handdoekdispenser-mini-kunststof-wit-pqminih
cost_price: 1513
unit_price: 2565
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 2565
vip_price: 2308
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1715077623
---
