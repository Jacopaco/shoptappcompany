---
id: '23400500'
blueprint: products
art_nr: '23400500'
title: 'Poetsroldispenser midi RVS, C 01 E'
slug: poetsroldispenser-midi-rvs-c-01-e
cost_price: 4711
unit_price: 9900
brands: 521c8220-6d99-40b7-a175-bb68f20e6cf3
categories: d1ca567f-2127-432a-aec4-29fef09a974d
price: 9900
vip_price: 8910
prod_img: prod_img/23400500.png
---
