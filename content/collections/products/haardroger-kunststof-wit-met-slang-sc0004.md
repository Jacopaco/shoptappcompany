---
id: '12020'
blueprint: product
art_nr: '12020'
title: 'Haardroger kunststof wit met slang, SC0004'
slug: haardroger-kunststof-wit-met-slang-sc0004
cost_price: 10986
unit_price: 19975
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 19975
vip_price: 17977
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Haardroger automatisch wit + slang, SC0004
  Wandhaardroger 850 Watt.
  Droger wordt automatisch geactiveerd na uitname van de slang van de wandhouder.
  Met ingebouwde veiligheid switch-off van 15 minuten.
  Bruto luchtopbrengst ca. 11l/sec.
  Luchtsnelheid: 79 km/u.
  Luchttemperatuur 60˚C.
  69dB.
prod_img: prod_img/mediclinics-sc0004.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719479256
---
