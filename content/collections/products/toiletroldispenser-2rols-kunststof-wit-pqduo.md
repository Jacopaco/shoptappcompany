---
id: '5591'
blueprint: products
art_nr: '5591'
title: 'Toiletroldispenser 2rols kunststof wit, PQDuo'
slug: toiletroldispenser-2rols-kunststof-wit-pqduo
price: 2670
cost_price: 1682
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 4ace2b29-6ed9-4888-aa6c-74e6d8f5441a
description: |-
  2rolshouder voor standaard rollen.
  Voor wandmontage.
  Met slot en witte PQ sleutel.
  Standaard met grijs venster.
  op aanvraag leverbaar in verschillende kleuren vensters.
  Met witte plaque, geschikt voor bedrukking van uw logo.
  Universeel navulbaar.
  Niet geschikt voor gebruik in openbare ruimten!
prod_img: prod_img/5591.JPG
unit_price: 2670
vip_price: 2403
---
