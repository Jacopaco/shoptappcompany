---
id: '2174'
published: false
blueprint: products
art_nr: '2174'
title: 'Industrierol cellulose 1laags'
slug: industrierol-cellulose-1laags
price: 6957
cost_price: 4460
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
description: '27 cm x 1000 meter | 2 rollen in folie'
unit_price: 0
vip_price: 5798
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
