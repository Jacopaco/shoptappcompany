---
id: '188'
published: false
blueprint: products
title: 'Onderzoeksbankrollen 185x59cm (4rl)'
price: 8746
description: '59 cm | 4 x 150 meter | Ø 14,7 cm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/813-600x600.jpg'
art_nr: 188
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712058676
---
