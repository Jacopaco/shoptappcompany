---
id: '21421189'
blueprint: products
art_nr: '21421189'
title: 'Handdoekdispenser RVS zwart, HSU 15 P MN'
slug: handdoekdispenser-rvs-zwart-hsu-15-p-mn
cost_price: 7942
unit_price: 16660
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
price: 16660
vip_price: 14994
prod_img: prod_img/21421189.png
---
