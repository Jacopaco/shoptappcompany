---
id: '12280'
blueprint: product
art_nr: '12280'
title: 'Handendroger drukknop RVS, E05CS'
slug: handendroger-drukknop-rvs-e05cs
cost_price: 29645
unit_price: 53900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 53900
vip_price: 48510
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Interieurarchitecten zijn er enthousiast over; de robuuste en eigentijdse producten uit de Mediclinics-lijn. Dat is waarom onze klanten dan ook kiezen voor de warme lucht handendroger van Mediclinics. Met deze luxueuze hoogglans variant treft u een regelrechte eyecatcher.

  Deze rvs handendroger kan simpelweg niet aan uw aandacht ontsnappen. De mat geslepen kap met de subtiel ingezette drukknop laat zien dat een praktisch ontwerp ook mooi kan zijn. Met een druk op de knop activeert u het beluchtingssysteem, dat precies voldoende tijd biedt om uw handen goed droog te krijgen.
prod_img: prod_img/mediclinics-e05cs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478440
---
