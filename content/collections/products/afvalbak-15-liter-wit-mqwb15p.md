---
id: '8225'
blueprint: products
art_nr: '8225'
title: 'Afvalbak 15 liter wit, MQWB15P'
slug: afvalbak-15-liter-wit-mqwb15p
price: 17650
cost_price: 10590
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalbak met klepdeksel.
  Met binnenring en RVS wandplaat.
  De bak is afneembaar van de wandplaat.
  Met 4puntsbevestiging.
prod_img: prod_img/8225.JPG
unit_price: 17650
vip_price: 15885
---
