---
id: '54046'
blueprint: products
art_nr: '54046'
title: 'BATTERIJ MICROB. PROCEL  LR14'
slug: batterij-microb-procel-lr14
price: 2.73
cost_price: 1.75
brands: 3375fa99-d12c-4998-8110-4d347cec808b
categories: c47f6e34-69cd-45ce-9722-72b7abd2abfc
prod_img: prod_img/54046.
unit_price: 0
vip_price: 2.275
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685626033
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
