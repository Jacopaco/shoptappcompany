---
id: '151057'
blueprint: products
title: 'Comfortwaste type A afvalzak incontinentie 50x60cm'
price: 7105
description: 'Afvalzakken incontinentie 50x60cm wit'
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1732786591
img:
  - product_images/afvalzak-hdpe-90x110cm-blauw-t30-1rol-a-25st.-110p9-15028101.jpg
art_nr: '150790945'
slug: afvalzak-blauw-80x110cm-10x20-stuks-t60
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
cost_price: 3870
unit_price: 5224
vip_price: 5921
prod_img: prod_img/afvalzakken-incontinentie-50x60cm-wit.jpg
locations:
  - 4cef8fa7-db4a-4e92-acb7-b845094905f8
  - 330f5d7f-d5f6-4649-9a39-dead1a8265fe
  - 6463e748-7dae-432d-ad50-a774b95850ef
product_type: physical
---
