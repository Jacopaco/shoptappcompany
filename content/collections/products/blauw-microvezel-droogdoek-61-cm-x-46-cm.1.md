---
id: '572318660'
blueprint: products
art_nr: '572318660'
title: 'Blauw Microvezel droogdoek 61 cm x 46 cm'
slug: blauw-microvezel-droogdoek-61-cm-x-46-cm
cost_price: 520
unit_price: 925
brands: d15c8756-3015-4296-8d6f-4fb6caacc3d4
categories: c764401b-cfda-4ea9-8e67-0db8b881128e
price: 1185
vip_price: 988
prod_img: prod_img/572318660.JPG
locations:
  - c4144e72-b8d8-42c4-8c61-e78060a6797c
---
