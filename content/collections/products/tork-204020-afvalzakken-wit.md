---
id: '839204020'
blueprint: product
art_nr: '839204020'
title: 'TORK 204020 afvalzakken wit'
slug: tork-204020-afvalzakken-wit
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
cost_price: 8194
unit_price: 11706
price: 15044
vip_price: 12536
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
locations:
  - 99803d8c-c3ec-45e0-ae23-9d95c7be0152
prod_img: prod_img/204020-1733306018.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1733306065
---
