---
id: 21418109-afp-c
blueprint: products
art_nr: '21418109 AFP-C'
title: 'Handdoekroldispenser semi-automatisch RVS afp-c, HAU 1 E ST'
slug: handdoekroldispenser-semi-automatisch-rvs-afp-c-hau-1-e-st
price: 33410
cost_price: 18041
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekroldispenser voor wandmontage.
  Met automatische papiertoevoer.
  Een nieuw stuk papier wordt gedoseerd,
  zodra de onderste is afgescheurd.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  Werkt op 4 D-batterijen, deze worden NIET meegeleverd.
  Aanbevolen rolafmetingen:
  max. diameter: 190 mmm
  max. breedte: 210 mm
  min. binnen diameter: 38 mm
  Ook in wit verkrijgbaar!
prod_img: prod_img/21418109-afp-c.JPG
unit_price: 33410
vip_price: 30069
---
