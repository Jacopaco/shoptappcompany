---
id: '2251'
published: false
blueprint: products
art_nr: '2251'
title: 'Afvalzakken T23 60 x 80 | 50 liter grijs vervallen'
slug: afvalzakken-t23-60-x-80-50-liter-grijs-vervallen
price: 3514
cost_price: 1914
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
prod_img: prod_img/2251.jpg
unit_price: 0
vip_price: 2928
locations:
  - a0be2336-697d-408e-ae76-a075fb4e1f62
---
