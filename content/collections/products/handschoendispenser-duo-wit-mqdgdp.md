---
id: '8487'
blueprint: products
art_nr: '8487'
title: 'Handschoendispenser duo wit, MQDGDP'
slug: handschoendispenser-duo-wit-mqdgdp
price: 12695
cost_price: 7617
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Duo handschoendispenser voor wandmontage.
  Geschikt voor verschillende maten handschoendoosjes.
  Een verende RVS klembeugel houdt de doosjes op de plaats.
  Kan zowel horizontaal als verticaal gemonteerd worden.
  Met 4puntsbevestiging.
prod_img: prod_img/8487.JPG
unit_price: 12695
vip_price: 11425
---
