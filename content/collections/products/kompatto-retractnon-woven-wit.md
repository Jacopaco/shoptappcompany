---
id: '5655'
blueprint: products
art_nr: '5655'
title: 'KOMPATTO RETRACT.NON WOVEN WIT'
slug: kompatto-retractnon-woven-wit
price: 21900
cost_price: 16600
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekroldispenser kusntstof wit voor non woven rollen
  en textiel rollen. RETRACTABLE
  H 570 x B 380 x D 270 mm
prod_img: prod_img/5655.
unit_price: 21900
vip_price: 19710
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685626207
---
