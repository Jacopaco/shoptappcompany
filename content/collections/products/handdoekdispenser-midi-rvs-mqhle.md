---
id: '8185'
blueprint: products
art_nr: '8185'
title: 'Handdoekdispenser midi RVS, MQHLE'
slug: handdoekdispenser-midi-rvs-mqhle
price: 14855
cost_price: 8913
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Handdoekdispenser groot.
  Met zichtvenster voor inhoudscontrole.
  Afsluitbaar, slot zit aan de voorzijde.
  Cover gaat aan de voorzijde open.
  Met 4punts bevestiging.
  Interfold handdoeken passen na de aanschaf van een speciale adapter.
prod_img: prod_img/8185.JPG
unit_price: 14855
vip_price: 13369
---
