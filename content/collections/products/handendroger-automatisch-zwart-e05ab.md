---
id: '12295'
blueprint: product
art_nr: '12295'
title: 'Handendroger automatisch zwart, E05AB'
slug: handendroger-automatisch-zwart-e05ab
cost_price: 22495
unit_price: 40900
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 40900
vip_price: 36810
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Een bescheiden uiterlijk, met de eigenschappen van een krachtpatser. Dat typeert de Mediclinics - handendroger zwart automatisch.

  Deze subtiele variant biedt, ondanks zijn kleinere omvang, dezelfde kracht en comfort als zijn grote broer. Het zwart gepoedercoate frame onderstreept de robuustheid van deze subtiele handendroger. Net als bij de andere handendrogers in de Mediclinics-lijn, staat ook bij dit product gebruiksgemak voorop. Geen lastige handleidingen of moeilijke gebruiksaanwijzing; deze handendroger werkt gewoon op het moment dat het moet. Zodra de toiletbezoeker zijn handen onder de tuit plaatst, slaat de handendroger aan. De uitstekende temperatuur in combinatie met de nauwkeurig afgestelde luchtsnelheid zorgt ervoor dat uw handen binnen een halve minuut weer helemaal droog zijn.

  Extra hygiëne (aanraken van de handendroger is niet nodig)
  Blaast warme lucht
  Robuuste handendroger met subtiel design
prod_img: prod_img/e05ab.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478317
---
