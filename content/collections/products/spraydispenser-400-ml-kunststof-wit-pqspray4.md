---
id: '5508'
blueprint: products
art_nr: '5508'
title: 'Spraydispenser 400 ml kunststof wit, PQSpray4'
slug: spraydispenser-400-ml-kunststof-wit-pqspray4
price: 3180
cost_price: 2003
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Spraydispenser voor wandmontage.
  Met push cover voor het doseren van de vulgoederen, duwrichting naar de muur.
  Standaard met grijs venster.
  Op aanvraag leverbaar in verschillende kleuren vensters.
  Met slot en witte PQ sleutel.
  Met navulfles en spraypomp.
  Op aanvraag ook te gebruiken met pouches/cartridges.
  Modulair systeem.
  Geschikt voor bedrukking met uw logo.
prod_img: prod_img/5508.JPG
unit_price: 3180
vip_price: 2862
---
