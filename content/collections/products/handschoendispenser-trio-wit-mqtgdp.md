---
id: '8499'
blueprint: products
art_nr: '8499'
title: 'Handschoendispenser trio wit, MQTGDP'
slug: handschoendispenser-trio-wit-mqtgdp
price: 13460
cost_price: 8076
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 24b76aa0-4fe7-444b-b760-3c9c6c3de80d
description: |-
  Trio handschoendispenser voor wandmontage.
  Geschikt voor verschillende maten handschoendoosjes.
  Een verende RVS klembeugel houdt de doosjes op de plaats.
  Kan zowel horizontaal als verticaal gemonteerd worden.
  Met 4puntsbevestiging.
prod_img: prod_img/8499.JPG
unit_price: 13460
vip_price: 12114
---
