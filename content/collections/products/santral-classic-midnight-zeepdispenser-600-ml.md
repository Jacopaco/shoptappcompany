---
id: nsu-5-ps-mn
published: false
blueprint: products
art_nr: 'NSU 5 P/S MN'
title: 'SanTRAL® Classic Midnight zeepdispenser 600 ml'
slug: santral-classic-midnight-zeepdispenser-600-ml
price: 16660
cost_price: 9996
brands: f77a48e2-462d-4719-8961-2637e36823d5
unit_price: 16660
vip_price: 14994
prod_img: prod_img/nsu-5-ps-mn.JPG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685613665
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
