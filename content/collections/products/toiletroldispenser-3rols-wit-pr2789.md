---
id: '13740'
blueprint: product
art_nr: '13740'
title: 'Toiletroldispenser 3rols wit, PR2789'
slug: toiletroldispenser-3rols-wit-pr2789
cost_price: 4752
unit_price: 7920
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 7920
vip_price: 7128
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  U geniet extra van de dag na een ontspannen toiletbezoek. Het is daarom prettig als dit bezoek niet verstoord wordt door onverwachte teleurstellingen, zoals een lege toiletrolhouder. Met deze goed gevulde 3rolshouder, voorkomt u onnodige frustratie.

  Bij het design van de 3rolshouder van Mediclinics hebben de ontwerpers extra aandacht besteed aan de vormgeving. Het strakke, ronde uiterlijk past qua stijl in elk toilet. Bovendien is de inhoud van de houder eenvoudig controleerbaar via de kijkgleuf. Zo houdt u altijd inzicht in het verbruik en de huidige voorraad.

  Bent u op zoek naar een topmodel, zowel in functionaliteit als design? Kies dan voor de witte 3rolshouder van Mediclinics.
prod_img: prod_img/afbeeding-pr2789-1719320282.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719320292
---
