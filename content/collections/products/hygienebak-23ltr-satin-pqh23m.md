---
id: '5678'
blueprint: products
art_nr: '5678'
title: 'Hygiënebak 23ltr satin, PQH23M'
slug: hygienebak-23ltr-satin-pqh23m
price: 7890
cost_price: 4971
brands: 2e25d72a-1103-4222-958f-1f5cc5e77513
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Hygiënebak vrijstaand.
  Ook geschikt voor wandmontage d.m.v. bijgeleverde ophangbevestiging.
  Met gesloten inworpklep.
  In de deksel is er ruimte voor een geurblokje.
  De deksel is eenvoudig van de onderbak af te halen voor optimale zakverwisseling.
prod_img: prod_img/5678.JPG
unit_price: 7890
vip_price: 7101
---
