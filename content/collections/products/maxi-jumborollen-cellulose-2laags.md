---
id: '2125'
blueprint: products
title: 'Maxi jumborollen cellulose 2laags'
price: 2418
description: 'Verpakking van 6 x 380 meter in folie | ø 255 mm'
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2125_1_2.jpg'
art_nr: 2125
slug: maxi-jumborollen-cellulose-2laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: e3cc57dc-0279-46e4-a6aa-f71780232048
cost_price: 1550
unit_price: 0
vip_price: 2015
prod_img: prod_img/2125_1_2.jpg
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1688653446
client_prices:
  - 19806848-2c70-4b45-953e-d0ab15a5024b
  - 1300049a-83d7-47aa-86ff-49b7207633e6
  - 19806848-2c70-4b45-953e-d0ab15a5024b
locations:
  - 3bc12b37-5aff-4aed-8cd9-66f8ed6eae3b
  - a618e884-3fb2-4394-9a3f-effd139ef796
---
