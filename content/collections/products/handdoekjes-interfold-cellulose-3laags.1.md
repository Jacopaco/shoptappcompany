---
id: '2145'
blueprint: products
title: 'STARLIGHT 2145-FS Interfold handdoekjes 42x22 cm - 3 laags - 2000 stuks'
price: 4444
description: |-
  Interfold handdoeken 2000 vel - 3 laags

  Code 2145-FSC
  Materiaal cellulose
  Kleur wit
  Afmetingen vel 42 x 22 cm
  Lagen 3 laags
  Verpakking 20 x 100 vel in doos
  FSC gecertificeerd
img_url: 'https://www.all-care.eu/content/files/Images/TAPP_Company_BV/2145-3_1.jpg'
art_nr: 2145
slug: handdoekjes-interfold-cellulose-3laags
brands: 83e1c8b8-f158-42f2-b532-a990b0d5040c
categories: d3864e3a-69fc-41ee-9af8-fa776fc617b2
cost_price: 2849
unit_price: 0
vip_price: 3703
prod_img: prod_img/2145-3_1.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1729154106
client_prices:
  - f29dee5f-1953-487d-b766-b1d3baafb2f0
  - 6a9cdf89-b735-45d5-b0cd-ec4800e00349
product_type: physical
---
