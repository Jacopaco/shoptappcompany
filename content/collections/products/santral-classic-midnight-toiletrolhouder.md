---
id: tru-1-p-mn
published: false
blueprint: products
art_nr: 'TRU 1 P MN'
title: 'SanTRAL® Classic Midnight toiletrolhouder'
slug: santral-classic-midnight-toiletrolhouder
price: 6480
cost_price: 3888
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/tru-1-p-mn.JPG
unit_price: 6480
vip_price: 5832
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
