---
id: '12920'
blueprint: product
art_nr: '12920'
title: 'Handdoekdispenser wit, DT2106'
slug: handdoekdispenser-wit-dt2106
cost_price: 3960
unit_price: 6170
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 6170
vip_price: 5553
categories: 1b689325-711f-439f-9fff-b59122f29d67
description: |-
  Of het nu gaat om een gast of een medewerker, iedere bezoeker van uw sanitaire ruimte wil zijn bezoek eindigen met droge handen. U kunt dan ook niet zonder een flinke voorraad gevouwen handdoekjes.

  De handdoekdispenser van Mediclinics houdt rekening met de droogbehoefte van iedere gast. Van grootverbruiker tot het meer spaarzame type; de handdoekjes laten zich eenvoudig doseren via de open onderzijde. De kijkgleuf geeft uw facilitaire dienst gelegenheid om de doekjes ruim op tijd aan te vullen. Zo staan uw gasten nooit met lege handen.
prod_img: prod_img/afbeelding-dt2106.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1717498100
---
