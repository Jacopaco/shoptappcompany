---
id: '17321'
blueprint: product
art_nr: '17321'
title: 'Wandhaardroger wit met spiraalsnoer en stopcontact, Premium Smart 1200 Socket'
slug: wandhaardroger-wit-met-spiraalsnoer-en-stopcontact-premium-smart-1200-socket
cost_price: 3725
unit_price: 10343
brands: bf3c786c-6fca-445b-86b3-d67b12fd8984
price: 10343
vip_price: 9308
categories: f832415b-6165-4830-aa09-66452dc3e2ec
description: |-
  Valera Premium Smart Socket ABS kunststof wandhaardroger 1200 Watt. Wandhouder met aan/uit veiligheidsschakelaar en een standaard Europees stopcontact 16A. Voor de aansluiting op een wandcontactdoos. Voorzien van een schuifschakelaar met 2 standen voor de luchtopbrengst, 3 standen voor de temperatuur.

  - Artikelnummer 17321
  - Model Premium Smart 1200 Socket
  - Materiaal Kunststof
  - Kleur Wit
  - Hoogte 500 mm
  - Breedte 245 mm
  - Diepte 150 mm
  - Gewicht 1,1 kg
  - Garantie 1 jaar na aankoopdatum
  - Toepassing Wandmontage
  - Eigenschappen 2 standen luchtopbrengst, 2 standen temperatuur, koude luchtstand
  - Eigenschappen Wandhouder met aan/uit veiligheidsschakelaar en standaard EU stopcontact 16A
  - Wattage 1200 Watt
  - Toepassing Privé hotel slaapkamers/badkamers, pensions, woningen en scheepscabines.
prod_img: prod_img/17321_001.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1714730275
---
