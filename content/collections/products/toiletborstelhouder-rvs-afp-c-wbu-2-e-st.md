---
id: 21336600-afp-c
blueprint: products
art_nr: '21336600 AFP-C.'
title: 'Toiletborstelhouder RVS afp-c, WBU 2 E ST'
slug: toiletborstelhouder-rvs-afp-c-wbu-2-e-st
price: 13690
cost_price: 7393
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 0b4e0712-32b2-4e29-a253-032860b81f34
description: |-
  Toiletborstelhouder voor wandmontage.
  Met uitneembare kunststof opvangschaal. 
  Met kunststof zwarte vervangbare toiletborstel.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21336600-afp-c..JPG
unit_price: 13690
vip_price: 12321
---
