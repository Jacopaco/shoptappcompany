---
id: '13218'
blueprint: product
art_nr: '13218'
title: 'Toiletroldispenser 2rols RVS, PRO784CS'
slug: toiletroldispenser-2rols-rvs-pro784cs
cost_price: 5280
unit_price: 8800
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 8800
vip_price: 7920
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
description: |-
  U besteedt misschien veel aandacht aan de aankleding van uw sanitaire ruimte. U gunt uw bezoekers immers optimaal gebruiksgemak en comfort. Daarbij is een mooie en praktische toiletroldispener onmisbaar. Met deze Mediclinics 2rolshouder bent u altijd verzekerd van dat extra rolletje in uw toiletruimte.

  De subtiele vormgeving van deze 2rolshouder is uniek in zijn soort. Door de strakke rvs buitenkant is deze dispenser een stijlvolle aanvulling in elke toiletruimte. De subtiele pijl aan de onderkant van de dispenser geeft duidelijk aan hoe uw bezoeker de dispenser moet gebruiken.
  Is een van de rollen op? Dan vervangt u de rollen eenvoudig via de klep aan de bovenkant van de dispenser.

  Een handige 2rolshouder komt van pas in elke toiletruimte.
prod_img: prod_img/afbeelding-pro784cs-1719321951.png
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719321956
---
