---
id: 814s1422876
published: false
blueprint: products
art_nr: 814S1422876
title: 'SanTRAL® Classic Midnight toiletroldispenser 4-rols'
slug: santral-classic-midnight-toiletroldispenser-4-rols
price: 323.2
cost_price: 0.0
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1cbc45df-96d8-4022-8d2e-2ee96c7728be
prod_img: prod_img/814s1422876.JPG
unit_price: 323.2
vip_price: 258.56
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687853961
---
