---
id: '3815'
published: false
blueprint: products
art_nr: '3815'
title: 'Basic 3815,BASIC RVS open afvalbak 25 liter'
slug: afvalbak-25-liter-open-rvs-clb25l-cs
cost_price: 7068
unit_price: 11780
brands: 8bbcdfde-2ae5-4424-b85b-e29137fffb02
price: 14164
vip_price: 11803
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1728992405
---
