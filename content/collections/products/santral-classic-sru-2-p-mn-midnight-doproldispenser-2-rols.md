---
id: sru-2-p-mn
blueprint: products
art_nr: 'SRU 2 P MN'
title: 'SanTRAL Classic SRU 2 P MN Midnight doproldispenser 2-rols'
slug: santral-classic-sru-2-p-mn-midnight-doproldispenser-2-rols
price: 220.03
cost_price: 103
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: 1cbc45df-96d8-4022-8d2e-2ee96c7728be
unit_price: 220.03
vip_price: 176.024
prod_img: prod_img/sru-2-p-mn.png
---
