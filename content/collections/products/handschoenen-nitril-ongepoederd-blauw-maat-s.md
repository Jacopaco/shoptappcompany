---
id: 6990e935-fd66-468f-8c3a-1df9cf44dfd2
published: false
blueprint: products
title: 'Handschoenen Nitril ongepoederd blauw maat S'
price: 515
description: 'Handschoenen Nitril ongepoederd blauw maat S'
img:
  - product_images/nitril-handschoenen-doos-100-stuks-maat-l.jpg
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1712048246
product_type: physical
---
