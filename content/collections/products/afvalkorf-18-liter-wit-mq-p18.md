---
id: '3840'
blueprint: products
art_nr: '3840'
title: 'Afvalkorf 18 liter wit, MQ-P18'
slug: afvalkorf-18-liter-wit-mq-p18
price: 3400
cost_price: 2039
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
description: |-
  Afvalkorf vierkant, vrijstaand of voor wandmontage.
  Met 4 geïntegreerde ophangpunten.
prod_img: prod_img/3840.JPG
unit_price: 3400
vip_price: 3060
---
