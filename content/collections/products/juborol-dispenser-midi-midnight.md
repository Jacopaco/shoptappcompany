---
id: rhu-31-p-mn
published: false
blueprint: products
art_nr: 'RHU 31 P MN'
title: 'Juborol dispenser MIDI Midnight'
slug: juborol-dispenser-midi-midnight
price: 20950
cost_price: 12570
brands: f77a48e2-462d-4719-8961-2637e36823d5
prod_img: prod_img/rhu-31-p-mn.JPG
unit_price: 20950
vip_price: 18855
categories: b12283c5-e4bf-4b2c-84e5-70d9d323e5e2
---
