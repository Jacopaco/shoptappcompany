---
id: '12290'
blueprint: product
art_nr: '12290'
title: 'Handendroger automatisch RVS, E05ACS'
slug: handendroger-automatisch-rvs-e05acs
cost_price: 27170
unit_price: 49400
brands: 921c43bf-e3f3-481f-af32-c2d6a8c3c5bc
price: 49400
vip_price: 44460
categories: 75c53ffa-5374-4a08-9512-49da8c66c7d5
description: |-
  Als u van stoer houdt, kiest u voor mat rvs. Het zorgvuldig geslepen materiaal geeft elk ontwerp direct een robuuste uitstraling. Dat geldt zeker ook voor deze Mediclinics warme lucht handendroger.

  Met deze handendroger biedt u uw gasten niet alleen een in het oog springend accessoire, maar ook optimaal gebruikscomfort. Wie de handen uit de mouwen steekt, wordt direct getrakteerd op een flinke droogbeurt. De handendroger heeft slechts een halve minuut nodig om handen te drogen. Door de draaibare tuit is het mogelijk de luchtstroom naar wens te verplaatsen.
prod_img: prod_img/mediclinics-e05acs.jpg
product_type: physical
updated_by: 5931c3fa-b71e-46e8-bfb4-62709aecfefb
updated_at: 1719478362
---
