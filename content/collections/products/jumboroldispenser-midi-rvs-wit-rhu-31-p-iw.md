---
id: '2316600'
blueprint: products
art_nr: '2316600'
title: 'Jumboroldispenser midi RVS wit, RHU 31 P IW'
slug: jumboroldispenser-midi-rvs-wit-rhu-31-p-iw
cost_price: 9798
unit_price: 20950
brands: f77a48e2-462d-4719-8961-2637e36823d5
price: 20950
vip_price: 18855
prod_img: prod_img/2316600.png
categories: 862d88e0-7d91-42cb-ac35-0f30c5bf8f9b
---
