---
id: '080554'
blueprint: products
art_nr: '080554'
title: 'Ecolab MAXX Magic2 - 1 L'
slug: ecolab-maxx-magic2-1-l
price: 1378
cost_price: 809
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
unit_price: 890
vip_price: 1148
prod_img: prod_img/8055.JPG
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1685109497
locations:
  - 6463e748-7dae-432d-ad50-a774b95850ef
---
