---
id: '8290'
blueprint: products
art_nr: '8290'
title: 'Jashaak RVS, MQUHLE'
slug: jashaak-rvs-mquhle
price: 4570
cost_price: 2742
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 313c7a1e-a9b5-4bfc-95f5-37dfc77c7421
description: |-
  Universele jashaak voor wandmontage.
  Compleet massief.
  Met inkeping voor perfecte ophanging van de spullen.
prod_img: prod_img/8290.JPG
unit_price: 4570
vip_price: 4113
---
