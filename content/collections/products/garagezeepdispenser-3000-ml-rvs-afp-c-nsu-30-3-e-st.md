---
id: 21411078-afp-c
blueprint: products
art_nr: '21411078 AFP-C'
title: 'Garagezeepdispenser 3000 ml RVS afp-c, NSU 30-3 E ST'
slug: garagezeepdispenser-3000-ml-rvs-afp-c-nsu-30-3-e-st
price: 18420
cost_price: 9947
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Garagezeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21411078-afp-c.JPG
unit_price: 18420
vip_price: 16578
---
