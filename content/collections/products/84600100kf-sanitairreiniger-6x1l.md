---
id: 84600100kf
blueprint: products
title: 'RAINBOW sanitairreiniger 6x1 liter'
price: 2826
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515474
art_nr: 84600100KF
slug: rainbow-sanitairreiniger-6x1-liter
brands: 29c0a9ab-460c-4733-bf3d-8e3d0b9249a0
categories: b9b66de0-1355-4d17-926a-9b6da52394e7
cost_price: 1659
unit_price: 1110
vip_price: 2355
prod_img: prod_img/84600100kf.JPG
---
