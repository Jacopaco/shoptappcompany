---
id: '2255'
published: false
blueprint: products
title: 'Afvalzakken T10 45 x 50 | 16 liter transparant vervangen'
price: 3121
updated_by: b94aecbf-d569-4e0e-9483-8c8be947ec81
updated_at: 1687515397
img:
  - product_images/2255-1679560103.jpg
art_nr: '2255'
slug: afvalzakken-t10-45-x-50-16-liter-transparant-vervangen
brands: 97d84bd2-d93a-45eb-9333-9fb3766174f5
categories: 39255071-4c80-40f4-a799-3d1871ffc5f2
cost_price: 1700
unit_price: 0
vip_price: 2601
prod_img: prod_img/2255.jpg
client_prices:
  - 6d0190b5-4139-49e0-899e-76ea795c5036
  - 342ca54a-98c1-451c-8ea8-f377ef5519e9
  - d28ddf0a-4681-415d-a8e8-3199d8a714ac
  - 6d0190b5-4139-49e0-899e-76ea795c5036
locations:
  - d778662b-6031-4386-99d9-0f75ed4071a4
  - 6463e748-7dae-432d-ad50-a774b95850ef
  - 4a1a1187-4d87-46b7-8fb0-5d565f9a47a2
  - 1f7effa5-1ac3-44d8-8fb4-b4b06575d101
  - 9e892bb6-3012-4729-9233-8cf35bd7901d
  - 2ff26ae1-499b-4b86-bdef-7f9af142e76f
  - be14461b-4601-4a24-a6dc-17853268cc98
---
