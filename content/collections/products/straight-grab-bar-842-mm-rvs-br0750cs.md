---
id: '12603'
blueprint: products
art_nr: '12603'
title: 'Straight grab bar 842 mm RVS, BR0750CS'
slug: straight-grab-bar-842-mm-rvs-br0750cs
price: 7300
cost_price: 4380
brands: 5ea208fb-2568-420d-8693-dc5998b19f97
categories: 50cb6faa-f66f-4c50-ba88-32c6be15b08e
description: |-
  Grab bar recht 760 mm.
  De stang wordt d.m.v. de 2 ronde uiteinden van Ø 81 mm op de wand gemonteerd.
unit_price: 7300
vip_price: 6570
prod_img: prod_img/12603.JPG
---
