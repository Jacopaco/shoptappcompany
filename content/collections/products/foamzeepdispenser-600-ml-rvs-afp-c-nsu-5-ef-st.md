---
id: 21415811-afp-c
blueprint: products
art_nr: '21415811 AFP-C'
title: 'Foamzeepdispenser 600 ml RVS afp-c, NSU 5 E/F ST'
slug: foamzeepdispenser-600-ml-rvs-afp-c-nsu-5-ef-st
price: 17900
cost_price: 9666
brands: f77a48e2-462d-4719-8961-2637e36823d5
categories: eea0a373-d337-4f3d-bd71-a849ffdbdf73
description: |-
  Foamzeepdispenser voor wandmontage.
  Met navulbaar reservoir.
  Met kunststof slot en sleutel.
  Met venster voor inhoudscontrole.
  RVS duwbeugel voor het doseren van de vloeistof.
  Ook in wit verkrijgbaar!
prod_img: prod_img/21415811-afp-c.JPG
unit_price: 17900
vip_price: 16110
---
