---
id: '331413896'
blueprint: products
art_nr: '331413896'
title: 'Afvalbak open 50 liter RVS afp-c, OBU 50 E ST'
slug: afvalbak-open-50-liter-rvs-afp-c-obu-50-e-st
price: 28820
cost_price: 17292
brands: 44e81669-92ff-4a5e-bfc2-cd76aa4e9365
categories: 127304be-50f4-4dce-9210-2bbc1e1a4176
prod_img: prod_img/331413896.JPG
unit_price: 28820
vip_price: 25938
---
