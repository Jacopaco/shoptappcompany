<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Statamic\Facades\Site;
use App\Http\Controllers\HtmxController;
use App\Http\Controllers\ValidateField;
use App\Http\Controllers\ProductsImport;

use App\Http\Controllers\ReplaceOrder;
use App\Http\Controllers\CompleteCheckout;

use App\Livewire\SwitchLocation;
use App\Http\Controllers\InviteUser;

use Illuminate\Http\Request;

use App\Http\Middleware\ProcessCart;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



   Route::Post('/htmx/{controller_name}/{function_name}/{params?}', function ($controller_name, $function_name, $params = null){

      $app = app();
      $controller = $app->make('\App\Http\Controllers\\'.$controller_name);

      $parameters = explode(',',$params);

      return $controller->callAction($function_name, $parameters);
      
   });



Site::all()->each(function (Statamic\Sites\Site $site) {

    Route::prefix(Str::after(config('app.url'), $site->url()))->group(function () {

         //validate field
         Route::post('/validate-field/', ValidateField::class);

         //cp-utilities
         Route::post('/products-import', [ProductsImport::class, 'run']);

         Route::get('/partials/{partialpath}', HtmxController::class)->where('partialpath' , '.*');

         //app-routes
         Route::statamic('/login', 'users.login', [
            'title' => 'Tapp Company Shop | login',
         ]);

         Route::statamic('/forgot-password', 'users.forgot', [
            'title' => 'Tapp Company Shop | wachtwoord vergeten',
         ]);

         Route::statamic('/reset-password', 'users.reset', [
            'title' => 'Tapp Company Shop | wachtwoord vernieuwen',
         ]);

         Route::statamic('/account/orders', 'users.orders', [
            'title' => 'Bestellingen'
         ])->middleware('auth');

         Route::statamic('/account/orders/{order}', 'orders.show', [
            'title' => 'Bestellingen'
         ])->middleware('auth');

         Route::get('/reorder/{order}' , ReplaceOrder::class)->middleware('auth');

         Route::statamic('/assortiment', 'products.index', [
            'title' => 'Producten',
            'search_index' => 'products'
         ])->middleware('auth');

         Route::statamic('/producten', 'products.filter', [
            'categorypath' => 'all',
            'filterstring' => 'all',
         ])->middleware('auth');

         Route::statamic('/producten/{categorypath}/filter/{filterstring?}', 'products.filter', [
            'search_index' => 'products',
         ])->where('categorypath', '.*')->where('filterstring', '.*')->middleware('auth');

         Route::post('/search/results/',  function () {
            return (new \Statamic\View\View)
            ->template('products.filter')
            ->with(['filterstring' => 'all']);
        })->middleware('auth');

         Route::statamic('/account', 'users.show', [
            'title' => 'Mijn account'
         ])->middleware('auth');

         Route::statamic('/beheer/accounts', 'manage.users', [
            'title' => 'Beheer accounts'
         ])->middleware('auth');

         Route::statamic('/account/lijst', 'users.list', [
            'title' => 'Bestellingen'
         ])->middleware('auth');

         //Route::redirect('/cp/collections/locations', '/cp/collections/companies');
         Route::statamic('/winkelwagen', 'cart', ['title' => 'Je winkelwagen'])->middleware('auth');

         Route::middleware([ProcessCart::class])->group(function () {  

         Route::redirect('/bestellen', '/bestellen/gegevens')->middleware('auth');
         Route::statamic('/bestellen/gegevens', 'checkout.information', ['title' => 'Bestellen'])->middleware('auth');
         //Route::statamic('/checkout/shipping', 'checkout.shipping', ['title' => 'Checkout']);

         });

         Route::get('/bestellen/betaling', CompleteCheckout::class)->middleware('auth');

         Route::statamic('/bestellen/afgerond', 'checkout.complete', ['title' => 'Bestelling geplaatst'])->middleware('auth');

         //Route::statamic('/checkout/offsite/mollie', 'checkout.offsite.mollie');
         //Route::statamic('/checkout/offsite/paypal', 'checkout.offsite.paypal');

      });
});