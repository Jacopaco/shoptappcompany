<?php
namespace App\SessionCart;

use App\Helpers\CalcLineItemPrice;

class SetLineItem
{
    public function __invoke($productId, $quantity)
    {

        if (session()->missing('session-cart.products')){

            if( session()->has('simple-commerce-cart') ){

                session()->pull('simple-commerce-cart');

            }

            session()->put('session-cart.products', [] );
            
        }

        if( $quantity >= 1 ){

            $productTotal = $this->productTotal($productId, $quantity);

            session()->put('session-cart.products.'.$productId, [
                'quantity' => $quantity,
                'total' => $productTotal  
            ]);

        }

        $cartTotal = $this->cartTotal();

        session()->put('session-cart.total', $cartTotal);

    }


    private function productTotal($productId, $quantity){

        $lineItemPrice = (new CalcLineItemPrice)($productId);

        $price = $lineItemPrice;

        $total = ( $price / 100  ) * $quantity;

        return $total;

    }

    private function cartTotal(){

        $products = session()->get('session-cart.products');

        $total = array_sum(array_column($products, 'total'));

        return number_format($total, 2, ',', '.');

    }

}