<?php

namespace App\Http\Requests;

use App\Rules\ValidVat;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Statamic\Facades\Collection;

class CheckoutInformationRequest extends FormRequest
{

    protected $redirect = '/partials/checkout/_information_form';
    
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {

        $singleField = Request::get('field') ?? null;

        if( !Auth::check() ){

            $publicFields = config('simple-commerce.field_whitelist.orders');
        
            $orders = Collection::find('orders');
            $fields = $orders->entryBlueprint()->fields()->all()->toArray();
        
            $fields = array_intersect_key($fields, array_flip($publicFields));

            $validationArray = [];

            $validationArray['name'] = [ 'required' ]; 
            $validationArray['email'] = [ 'required' , 'email:' ];

            $validationArray['vat_id'] = [ 'required' , new ValidVat ]; 

            $validationArray += array_column($fields, 'validate', 'handle');

            if( isset( $singleField) ){

                if( key_exists($singleField, $validationArray) ){

                    $validateOnly = [ $singleField => $validationArray[$singleField] ];

                    return $validateOnly; 

                }else{

                    return [];

                }

            }else{

                return $validationArray;
                
            }

        }else{

            return [];

        }

    }

}
