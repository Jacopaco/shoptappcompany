<?php
 
namespace App\Http\Resources;
 
use Statamic\Http\Resources\API\UserResource;

use Statamic\Facades\User;
 
class CpUserResource extends UserResource
{
    public function toArray($request)
    {

        $user = User::find($this->resource->id);

        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'location' => $user->location->title,
            'status' => $user->last_login ? 'Actief' : 'Uitgenodigd',
            'roles' => $user->roles,
            'edit_url' => $user->edit_url,
        ];

    }
}