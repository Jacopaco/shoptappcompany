<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Auth;
use Statamic\Facades\User;

use DuncanMcClean\SimpleCommerce\Facades\Order;
use Exception;
use Statamic\Facades\Entry;

class ProcessCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        $sessionCart = session()->get('session-cart');

        if( !isset($sessionCart) || sizeof($sessionCart['products']) < 1 ){

            return redirect('/winkelwagen');

        }

        $order = $this->getOrder();

        $lineItems = [];

        foreach( $sessionCart['products'] as $productId => $data  ){

            $lineItems[] = [
                "product" => $productId,
                "quantity" => intval($data['quantity']),
                "total" => $data['total']
            ];

        }

        $order->set('items', $lineItems);
        $order->saveQuietly();
  
        //Find the order via SimpleCommerce facade to use recalculation method
        $order = Order::find($order->id);
        $order->recalculate();

        if (Auth::check() && !isset($order->customer) ) {

            $user = User::current();

            if( isset($user)){

                $order->customer($user->id);
                $order->save();

            }
            
        }

        $order->save();

        return $next($request);

    }

    private function getOrder(){

        //Check if there is a cart, if yes, get it, if not, make it
        if( session()->has('simple-commerce-cart') ){

            $orderId = session()->get('simple-commerce-cart');

            try{

               $order =  Entry::findOrFail($orderId);

            }
            catch( Exception $e ) {

                $order = $this->makeOrder();

            }

        }else{

            $order = $this->makeOrder();
      
        }

        return $order;


    }

    private function makeOrder(){

            //Make the order with Simple Commerce and add it to the session
            $order = Order::make();
            $order->save();
            $orderId = $order->id;
            session()->put('simple-commerce-cart', $orderId);

            //Find the order as Statamic Entry, this has better performance setting the line items.
            $order = Entry::find($orderId);

            return $order;

    }


}