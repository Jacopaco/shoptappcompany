<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Statamic\Support\Str;

use Statamic\Facades\Entry;

use Illuminate\Support\Carbon;

use DuncanMcClean\SimpleCommerce\Fieldtypes\MoneyFieldtype;
use App\Helpers\CalcPrice;

use Rap2hpoutre\FastExcel\FastExcel;


class ProductsImport extends Controller
{

    public $new = [];
    public $unpublished = [];
    public $published = [];
    public $checkPrices = [];

    public $updatedCount = 0;
    public $descriptionsAddedCount = 0;

    public $brands;

    public function __construct()
    {

        $this->middleware('auth');

        //Haal slugs van merken op
        $this->brands = Entry::query()
        ->where('collection', 'brands')
        ->get();
                
        
    }

    public function run(Request $request)
    {

        Cache::flush();

        $import = Entry::make()->collection('imports');

        if( $request->hasFile('product-feed') )
        {

            $file = $request->file('product-feed');

            try{
    
                $products = (new FastExcel)->import($file, function ($col) {

                    if( isset($col['Webshop merk']) ){
                        $brandSlug = Str::slug($col['Webshop merk'], '-');
                    }

                    if( $brandSlug && $this->brands->contains('slug', $brandSlug ) ){
                        
                        $publish = (bool) ( ($col['Webshop status'] === 'Actief' ) ? true : false  ); 

                        $data = array();
                        
                        $data['art_nr'] = $col['Nr.'];
                        $data['title'] = $col['Omschrijving'];
                        $data['slug'] = Str::slug($data['title'], '-');

                        $id = Str::slug($data['art_nr']);

                        //Get brand
                        $brand = Entry::query()
                        ->where('collection' , 'brands')
                        ->where('slug', $brandSlug)
                        ->first();

                        //Define brand
                        $data['brands'] = $brand->id;

                        $money = new MoneyFieldtype;

                        $costPrice = intVal($col['Kostprijs'] * 100 );
                        $unitPrice = intVal($col['Eenheidsprijs'] * 100);

                        $data['cost_price'] = $costPrice;
                        $data['unit_price'] = $unitPrice;

                        $calcPrices = CalcPrice::calc($data['brands'], $data['cost_price'], $data['unit_price']);

                        $data['price'] = $calcPrices['price'];
                        $data['vip_price'] = $calcPrices['vip_price'];

                        if( key_exists('subcatagorie', $col) ){

                            $categorySlug = Str::slug($col['subcatagorie'], '-');

                            $category =  Entry::query()
                            ->where('collection' , 'categories')
                            ->where('slug', $categorySlug)
                            ->first();
    
                            if( !isset($category) ){
                                
                                $category = Entry::make()
                                    ->collection('categories')
                                    ->slug($categorySlug)
                                    ->data([ 'title' => $col['subcatagorie'] ]);
    
                                $category->save();
                                
                            }
    
                            //Define categories
                            $data['categories'] = $category->id;

                        }

                        $match = Entry::find($id);

                        if( isset( $match ) ){ 
        
                            $match->set('title', $data['title']);
                            $match->set('slug', $data['slug']);
                            $match->set('brands', $data['brands']);
                           
                            $match->set('cost_price', $data['cost_price']);
                            $match->set('unit_price', $data['unit_price']);
                            $match->set('price', $data['price']);
                            $match->set('vip_price', $data['vip_price']);

                            if( key_exists('categories', $data) ){
                                $match->set('categories', $data['categories']);
                            }

                            if($publish == false){
                                
                                if($match->published == true){
                                    
                                    $this->unpublished[] =  $match->id;

                                }

                                $match->published(false);
                            
                            }else{

                                if($match->published == false){
                                    
                                    $this->new[] =  $match->id;

                                }

                            }
            
                            $match->saveQuietly();

                            $this->updatedCount++;
                            
                        }elseif($publish == true){
            
                            $product = Entry::make()->collection('products')->id($id)->slug($data['slug'])->data($data)->published(false);
                            
                            $product->saveQuietly();

                            $this->new[] = $product->id;
                            
                        }

                    }
        
                });
    
    
            }catch(Exception $e){
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        } 
        
        if( $request->hasFile('description-feed') )
        {
            $file = $request->file('description-feed');

            try{ 

                $descriptions = [];

                $rows = (new FastExcel)->import($file);

                foreach( $rows as $col){

                    if( $col['Taal'] === 'NLD' ){

                        if( !key_exists($col['Nr.'] , $descriptions) ){

                            $match = Entry::find(Str::slug($col['Nr.']));

                        }

                        if(key_exists($col['Nr.'] , $descriptions) ||  isset( $match ) ){ 

                            $descriptions[$col['Nr.']][] = $col['Tekst'];

                        }

                    }
                }

                //Zet voor elk artikel in de desc array de regels in het juiste veld.
                foreach( $descriptions as  $art => $description ){

                    $match = Entry::find(Str::slug($art));

                    $match->set('description', implode("\n", $description) );

                    $match->saveQuietly();

                    $this->descriptionsAdded++;
 
                }
    
            }catch(Exception $e){
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }

        $import->data([
            'date' => Carbon::parse('now'),
            'new' => $this->new,
            'unpublished' => $this->unpublished,
        ]);

        if($import->save()){
            return view('products-import-results', [
                'import' => $import->id,
                'new_count' => sizeof($this->new)
    
            ]);
        }
        else{
            echo 'Er is iets mis gegeaan bij het opslaan van de import.';
        }
        
    }
}