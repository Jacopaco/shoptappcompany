<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\GetLocationProducts;

class CloseMarketingbanner extends Controller
{
    public function closeBanner(){

        $request = Request::capture();

        session(['closed_marketingbanner' => true]);

        return response('', 200)->header('Hx-Trigger' , 'marketingbanner-closed');

    }

}