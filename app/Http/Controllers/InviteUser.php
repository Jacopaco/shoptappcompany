<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Statamic\Facades\User;
use Statamic\Notifications\ActivateAccount;

use App\Rules\UniqueUser;

class InviteUser extends Controller
{
    /**
     * Handle the incoming request.
     */

    public function rules() {
        return [

            'name' => ['required'],
    
            'email' => ['required' , 'email:rfc,dns' , new UniqueUser]
    
        ];
    } 


    public function submit()
    {

        $request = Request::capture();

        if($request->field){
      
            $this->validate($request, $this->rules());
            
            return back();

        }else{

            $this->validate($request, $this->rules());

            $user = User::make()->email($request->email)->data( [ 'name' => $request->name , 'location' => $request->location ]);
            $user->save();
    
            ActivateAccount::subject('Activeer je account bij Tapp Company!');
            ActivateAccount::body('Je account voor shoptappcompany.nl staat voor je klaar om geactiveerd te worden. Doe dit binnen 72 uur via onderstaande link.');
            $user->generateTokenAndSendActivateAccountNotification();

            return $this->view($request);

        }

    }

    protected function view(){

        $request = Request::capture();
    
        if( $request->hasHeader('Hx-Request') ){
    
            $content =  (new \Statamic\View\View)
            ->template('htmx._invite_user');
    
            return response($content)->header('Hx-Trigger' , 'submitted');
    
        }else{
    
            return back();
    
        }
    
    }

}