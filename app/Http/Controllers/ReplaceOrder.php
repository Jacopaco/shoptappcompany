<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DuncanMcClean\SimpleCommerce\Facades\Order;

use App\SessionCart\SetLineItem;

use Statamic\Facades\User;

class ReplaceOrder extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {

        session()->pull('session-cart');

        //Fill a card and redirect to checkout

        $templateOrder = Order::find($request->order);

        $lineItems = $templateOrder->lineItems()->toArray();

        foreach($lineItems as $lineItem){

            $setLineItem = (new SetLineItem)($lineItem->product->id, $lineItem->quantity);

        }
        
        return redirect('/bestellen');
         
    }
    
}