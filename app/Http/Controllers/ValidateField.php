<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutInformationRequest;

class ValidateField extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(CheckoutInformationRequest $request)
    {

        return redirect('/partials/checkout/_information_form')->withInput() ;
        
    }

}