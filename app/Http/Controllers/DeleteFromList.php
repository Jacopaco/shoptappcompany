<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Statamic\Facades\User;
use Statamic\Facades\Entry;

use App\Helpers\GetLocationProducts;

class DeleteFromList extends Controller
{
    public function quicklist($product){

        $user= User::current();

        $quicklistProducts = $user->quicklist->pluck('id')->toArray();    

        if(($key = array_search($product, $quicklistProducts)) !== false) {
            unset($quicklistProducts[$key]);
        } 

        $user->quicklist = array_values($quicklistProducts);
        
        if($user->save()){
            return $this->render();
        }

    }

    public function location($product, $locationId){

        $location = Entry::find($locationId);

        $locationProducts = GetLocationProducts::get($locationId);

        if(($key = array_search($product, $locationProducts)) !== false) {

            unset($locationProducts[$key]);

            $location->set('location_products', array_values($locationProducts));
            $location->set('template_location_products', null);

            if($location->save()){

                return $this->render();
    
            }

        }

    }

    public function render(){

        $request = Request::capture();
    
        if( $request->hasHeader('Hx-Request') ){
    
            $content =  (new \Statamic\View\View)
            ->template('htmx._edit_list')->with($request->toArray());
    
            return response($content)->header('Hx-Trigger' , 'deleted');
    
        }else{
    
            return back();
    
        }
    
    }

}