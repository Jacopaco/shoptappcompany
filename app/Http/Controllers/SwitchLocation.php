<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Statamic\Facades\User;
use Statamic\Facades\Entry;

class SwitchLocation extends Controller
{

    public function to($location){

        $user = User::current();
        $user->set('location', $location);
        $user->save();

        $content = (new \Statamic\View\View)
        ->template('htmx._switch_location');

        $trigger = 'location-switched';

        return response($content)->header('Hx-Trigger' , $trigger);

    }

}
