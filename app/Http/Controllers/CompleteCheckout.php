<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DuncanMcClean\SimpleCommerce\Facades\Order;

use DuncanMcClean\SimpleCommerce\Orders\OrderStatus;
use DuncanMcClean\SimpleCommerce\Orders\PaymentStatus;


class CompleteCheckout extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {

        $orderId = session()->get('simple-commerce-cart');

        $order = Order::find($orderId);

        $order->updateOrderStatus(OrderStatus::Placed);        
        $order->updatePaymentStatus(PaymentStatus::Paid);        

        if( $order->save() ){
              
            session()->pull('session-cart');

            return redirect('/bestellen/afgerond');
        
        }

    }
    
}