<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Statamic\Facades\User;
use Statamic\Facades\Entry;

use App\Helpers\GetLocationProducts;

class ManageList extends Controller
{

    public function set(){

        $request = Request::capture();

        $product = $request->product_id;
        $toQuicklist = $request->toQuicklist ?? false;
        $locations = $request->locations ?? [];

        $user = User::current();

        $quickList = $user->quicklist->pluck('id')->toArray();
        $userLocations = $user->location->company->locations;
      
        if( $toQuicklist && !in_array($product, $quickList)){
          
            $quickList[] = $product;

        }else{
            if( !$toQuicklist && ($key = array_search($product, $quickList)) !== false) {
                unset($quickList[$key]);
            } 
        }
     
        $quickList = array_values($quickList);
        
        $user->set('quicklist', $quickList);
        $user->save();
  

        foreach( $userLocations as $location ){

            $location = Entry::find($location->id);

            $locationProducts = GetLocationProducts::get($location->id);

            if(in_array($location->id, $locations)){

                if(!in_array($product, $locationProducts)){

                    $locationProducts[] = $product;

                    $location->set('location_products', array_values($locationProducts));
                    $location->set('template_location_products', null);

                    $location->save();

                }

            }else{
                
                if(($key = array_search($product, $locationProducts)) !== false) {

                    unset($locationProducts[$key]);

                    $location->set('location_products', array_values($locationProducts));
                    $location->set('template_location_products', null);

                    $location->save();

                } 

            }

        }

        return $this->render();
        
    } 

    public function render(){
        $request = Request::capture();

        $content = (new \Statamic\View\View)
        ->template('htmx._manage_list')->with($request->toArray());

        return response($content)->header('Hx-Trigger' , 'submitted');
    
    }

}