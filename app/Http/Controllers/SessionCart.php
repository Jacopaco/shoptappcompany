<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\CalcLineItemPrice;
use App\SessionCart\SetLineItem;

use DuncanMcClean\SimpleCommerce\Fieldtypes\MoneyFieldtype;

class SessionCart extends Controller
{
    /**
     * Handle the incoming request.
     */

    public $deleted;
    
    public function set(){
        
        $request = Request::capture();
        $productId = $request->product_id;
        $quantity = intVal($request->quantity); 
        
        if($quantity == 0 && session()->has('session-cart.products.'.$productId) ){

            session()->pull('session-cart.products.'.$productId);

            $this->deleted = true;

        }

        $setLineItem = (new SetLineItem)($productId, $quantity);

       return $this->render();
         
    }

    private function render(){

        $request = Request::capture();
    
        $content = (new \Statamic\View\View)
        ->template('partials.products._product_amount')->with($request->toArray());

        $trigger = ($this->deleted ? 'cart-updated, product-deleted' : 'cart-updated');

        return response($content)->header('Hx-Trigger' , $trigger);
        
    }

    
}