<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Statamic\Facades\User;

class EditUser extends Controller
{
    public function delete($id)
    {

        $user = User::find($id);
        $user->delete();

        return $this->render();
        
    }

    public function update(){

        $request = Request::capture();

        $this->validate($request, [ 'name' => ['required'] ]);

        $user = User::find($request->user);
        
        $user->set('name' , $request->name);
        $user->set('location' , $request->location);

        $user->save();

        return $this->render();
        
    }

    public function render(){

        $request = Request::capture();
    
        if( $request->hasHeader('Hx-Request') ){
    
            $content =  (new \Statamic\View\View)
            ->template('htmx._edit_user');
    
            return response($content)->header('Hx-Trigger' , 'submitted');
    
        }else{
    
            return back();
    
        }
    
    }

}