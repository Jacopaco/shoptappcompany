<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HtmxController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {

        $partialpath = $request->partialpath;
        $params = $request->all();

        $params['htmx'] = true;

        return (new \Statamic\View\View)
        ->template('partials/'.$partialpath)
        ->with($params)
        ;

    }
}