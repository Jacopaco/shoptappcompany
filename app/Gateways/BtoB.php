<?php

namespace App\Gateways;

use DuncanMcClean\SimpleCommerce\Contracts\Gateway;
use DuncanMcClean\SimpleCommerce\Contracts\Order;
use DuncanMcClean\SimpleCommerce\Gateways\BaseGateway;
use Illuminate\Http\Request;

class BtoB extends BaseGateway implements Gateway
{
    public function name(): string
    {
        return __('B2B');
    }

    public function prepare(Request $request, Order $order): array
    {
        return [];
    }

    public function checkout(Request $request, Order $order): array
    {
        $this->markOrderAsPaid($order);

        return [];
    }

    public function checkoutRules(): array
    {
        return [];
    }

    public function checkoutMessages(): array
    {
        return [];
    }

    public function refund(Order $order): array
    {
        return [];
    }

    public function webhook(Request $request)
    {
        //
    }

    public function fieldtypeDisplay($value): array
    {
        return [
            'text' => 'Op rekening',
            'url' => null,
        ];
    }
}
