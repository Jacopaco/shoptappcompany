<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

use Statamic\Facades\User;

class UniqueUser implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
     
            $match = User::findByEmail($value);
            if( isset($match) ){
                $fail('Er is al een account met dit emailadres');
            }
        
        
    }
}
