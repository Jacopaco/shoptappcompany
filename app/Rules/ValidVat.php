<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidVat implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $vat_number = $value;
        $vat_number = str_replace(array(' ', '.', '-', ',', ', '), '', trim($vat_number));

        $contents = @file_get_contents('https://controleerbtwnummer.eu/api/validate/'.$vat_number.'.json');

        if($contents === false) {

            throw new Exception('service unavailable');

        } 
        else {

            $res = json_decode($contents);

            if(!$res->valid) {

                $fail('Dit lijkt geen geldig BTW nummer.');

            }

        }

    }
}
