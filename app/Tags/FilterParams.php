<?php

namespace App\Tags;

use Statamic\Tags\Tags;

class FilterParams extends Tags
{
    /**
     * The {{ filter_params }} tag.
     *
     * @return string|array
     */
    public function index()
    {

      return $this->queryParams();

    }

    /**
     * The {{ filter_params:example }} tag.
     *
     * @return string|array
     */
    public function get()
    {

        $filterParams = $this->queryParams();

        if($filterParams != null){
            
            if( isset($this->params) && $this->params->get('filter') ){
                $getParam = $this->params->get('filter');
            }
         
            if( isset($getParam) ){
     
                $key =  array_search($getParam , array_column($filterParams , 'key'));
               
                if($key !== false ){
                
                    return $filterParams[$key]['values'];
                   
                }
                
            }

        }

    }

    //Below function is duplicated in tags/filterParams
    private function queryParams(){

        $filterString = $this->params->get('filterstring');

        if( $filterString != 'all' ){

            $filterParams = [];

            $queryParams = explode('/', $filterString);
            
            if( sizeof($queryParams) > 0 ){
    
                foreach( $queryParams as $filter ){
    
                    $filter = explode(':', $filter);
                    $filter = [ $filter[0] => $filter[1] ];
            
                    foreach( $filter as $key => $values ){
        
                        $filterParams[] =  [
                            'key' => $key,
                            'values' => explode(',', $values)
                        ];
        
                    }
        
                }
        
            }
                
            return $filterParams;

        }

    }

}
