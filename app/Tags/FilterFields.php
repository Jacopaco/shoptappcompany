<?php

namespace App\Tags;

use Statamic\Tags\Tags;

use Statamic\Facades\Entry;

use Illuminate\Support\Str;

class FilterFields extends Tags
{
    /**
     * The {{ filter_fields }} tag.
     *
     * @return string|array
     */


    public $properties = [];
    public $filterFields = [];

    public function index()
    {

        $categoryId = $this->params->get('category_id');

        $category = Entry::find($categoryId);

        $this->filterFields[] = [
            'handle' => 'brands',
            'display' => 'Merk'
        ];


        if( isset($category) ){

            if($category->properties){

                $this->addProps($category->slug,$category->get('properties'));

            }
           
            $subcategories = Entry::query()->where('parent' , $category->id)->whereNotNull('properties')->get();

            if( count( $subcategories) > 0 ){
        
                foreach( $subcategories as $subcategory){

                    if($subcategory->properties){

                        $this->addProps($subcategory->slug,$subcategory->get('properties'));

                    }
         
                }

            }

            foreach( $this->properties as $category => $properties ){

                foreach( $properties as $property ){

                    $this->filterFields[] = [
                        'handle' => $category.'_'.$property['property'],
                        'display' => $property['property']
                    ];

                }

            }

            return $this->filterFields;

        }

    }

    public function getHandle(){

        $filterFields = $this->index();
        
        $label = array_search($this->params);

    }

    private function addProps($category,$properties){

        $this->properties[$category] = $properties;

    }

}