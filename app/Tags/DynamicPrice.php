<?php

namespace App\Tags;

use Statamic\Tags\Tags;
use Statamic\Facades\User;

class DynamicPrice extends Tags
{
    /**
     * The {{ dynamic_price }} tag.
     *
     * @return string|array
     */
    public function index()
    {

        $productId = $this->params->get('product_id');

        $user = User::current();

        if(isset($user)){

            $clientPrices = $user->location->company->client_prices;

            if( count($clientPrices) > 0 ){

                foreach ($clientPrices as $clientPrice) {

                    if( isset($clientPrice->product->id) && $clientPrice->product->id === $productId ){

                        return $clientPrice->client_price;

                    }

                }

            }

        }

    }

    /**
     * The {{ dynamic_price:example }} tag.
     *
     * @return string|array
     */
    public function example()
    {
        
    }
}
