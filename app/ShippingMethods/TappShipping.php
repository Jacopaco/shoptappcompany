<?php

namespace App\ShippingMethods;

use DuncanMcClean\SimpleCommerce\Contracts\Order;
use DuncanMcClean\SimpleCommerce\Contracts\ShippingMethod;
use DuncanMcClean\SimpleCommerce\Orders\Address;
use DuncanMcClean\SimpleCommerce\Shipping\BaseShippingMethod;

class TappShipping extends BaseShippingMethod implements ShippingMethod
{
    public function name(): string
    {
        return __('tapp_shipping');
    }

    public function description(): string
    {
        return __('Description of your shipping method');
    }

    public function calculateCost(Order $order): int
    {

        if( $order->itemsTotal < 25000 ){

            return 750;

        }else{

            return 0;

        }
        
    }

    public function checkAvailability(Order $order, Address $address): bool
    {
        return true;
    }
}
