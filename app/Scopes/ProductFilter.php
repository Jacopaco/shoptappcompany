<?php

namespace App\Scopes;

use Hamcrest\Arrays\IsArray;
use Statamic\Query\Scopes\Scope;
use Statamic\Facades\Entry;
use App\Tags\FilterParams;
use GuzzleHttp\Psr7\Query;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class ProductFilter extends Scope
{
    /**
     * Apply the scope.
     *
     * @param \Statamic\Query\Builder $query
     * @param array $values
     * @return void
     */

    public function apply($query, $values)
    {
        $request = Request::capture();

        if($request->q){
            
            $q = $request->q;

            $hits = Http::withHeaders([
                'X-Algolia-Application-Id' => 'QTISB8HE7U',
                'X-Algolia-API-Key' => '661e383d83ac3940bb35fd3bf9ad41bd'
            ])->withBody(
                '{ "params": "query='.$q.'&hitsPerPage=1000" }'
            )->post('QTISB8HE7U-dsn.algolia.net/1/indexes/products-v2/query')->body();

            $hits = json_decode($hits,  true);

            if( $hits ){

                $hits = array_column($hits['hits'], 'art_nr');
                $query->whereIn('art_nr' , $hits);

            }

        }

        $categoryId = $values['category_id'] ?? null;

        $exclude = $values['exclude'] ?? false;
           
        $filterParams = $this->queryParams($values['filterstring']);

        $category = Entry::find($categoryId);

        if( isset($category) ){

            $categories = [ $category->id ];

            $subcategories = Entry::query()->where('parent' , $category->id)->get()->pluck('id')->toArray();

            if( count($subcategories) > 0 ){
            
                $categories = array_merge($categories, $subcategories);

            }

            $query->whereIn('categories',  $categories );

        }else{

            $allCategories = Entry::whereCollection('categories')->pluck('id')->toArray();

            $query->whereIn('categories',  $allCategories );

        }

        if( isset( $filterParams) ){

            foreach( $filterParams as $filter ){

                if( $filter['key'] != $exclude && $filter['values'] != null ){

                    $query->whereIn($filter['key'] ,  $filter['values']);

                }

            }

        }

    }

    //Below function is duplicated in scopes/ProductFilter
    private function queryParams($filterString){

        if( $filterString != 'all' ){

            $filterParams = [];

            $queryParams = explode('/', $filterString);
            
            if( sizeof($queryParams) > 0 ){
    
                foreach( $queryParams as $filter ){
    
                    $filter = explode(':', $filter);
                    $filter = [ $filter[0] => $filter[1] ];
            
                    foreach( $filter as $key => $values ){
        
                        $filterParams[] =  [
                            'key' => $key,
                            'values' => explode(',', $values)
                        ];
        
                    }
        
                }
        
            }
                
            return $filterParams;

        }

    }

}