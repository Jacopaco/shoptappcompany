<?php

namespace App\Scopes;

use Statamic\Query\Scopes\Filter;

class InactiveProductCheck extends Filter
{
    /**
     * Pin the filter.
     *
     * @var bool
     */
    public $pinned = true;

    /**
     * Define the filter's title.
     *
     * @return string
     */
    public static function title()
    {
        return 'Inactieve producten check';
    }

    /**
     * Define the filter's field items.
     *
     * @return array
     */
    public function fieldItems()
    {
        return [
            'lists' => [
                'type' => 'checkboxes',
                'options' => [
                    'users' => 'Snellijsten gebruikers',
                    'locations' => 'Bestellijsten vestigingen',
                    'client_prices' => 'Prijsafspraken',
                ]
            ]
        ];
    }

    /**
     * Apply the filter.
     *
     * @param \Statamic\Query\Builder $query
     * @param array $values
     * @return void
     */

    public function apply($query, $values)
    {

        foreach($values['lists'] as $list){
            $query->orWhereNotNull($list);
        }

        $query->whereIn('status', ['draft','unpublished']);
    }

    /**
     * Define the applied filter's badge text.
     *
     * @param array $values
     * @return string
     */
    public function badge($values)
    {
        
        return 'Inactieve producten check';

    }

    /**
     * Determine when the filter is shown.
     *
     * @param string $key
     * @return bool
     */
    public function visibleTo($key)
    {
        return key_exists('collection', $this->context) && $this->context['collection'] === 'products';
    }
}
