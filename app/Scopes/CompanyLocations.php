<?php

namespace App\Scopes;

use Statamic\Query\Scopes\Scope;

class CompanyLocations extends Scope
{
    /**
     * Apply the scope.
     *
     * @param \Statamic\Query\Builder $query
     * @param array $values
     * @return void
     */
    public function apply($query, $values)
    {

        $ref = request()->header('referer');
        $segments = explode('/' , $ref);
        $company = last($segments);

        $query->where('company', $company);

    }
}
