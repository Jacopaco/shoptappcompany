<?php

namespace App\Scopes;

use Statamic\Query\Scopes\Scope;

use Statamic\Facades\User;
use Statamic\Facades\Entry;

class OrderList extends Scope
{
    /**
     * Apply the scope.
     *
     * @param \Statamic\Query\Builder $query
     * @param array $values
     * @return void
     */
    public function apply($query, $values)
    {

        $user = User::current();

        if( in_array('manager',  $user->roles) ){

            $locations = $user->location->company->locations->pluck('id')->toArray();

            $userList = User::query()
            ->whereIn('location', $locations)
            ->get('id')->pluck('id')->toArray();

            $query->whereIn('customer', $userList);

        }else{

            $query->where('customer', $user->id);

        }

    }

} 