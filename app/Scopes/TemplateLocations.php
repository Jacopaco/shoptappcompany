<?php

namespace App\Scopes;

use Statamic\Query\Scopes\Scope;

use Statamic\Facades\Entry;

class TemplateLocations extends Scope
{
    /**
     * Apply the scope.
     *
     * @param \Statamic\Query\Builder $query
     * @param array $values
     * @return void
     */
    public function apply($query, $values)
    {

        $ref = request()->header('referer');
        $segments = explode('/' , $ref);

        if( in_array('companies', $segments) ){

            $companyId = last($segments);

        }
        elseif( in_array('locations', $segments) ){

            $locationId = last($segments);
            $location = Entry::find($locationId);
            $companyId = $location->company->id;

        }

        if( isset($companyId) ){
            $query->where('company' , $companyId);
        }

        $query->whereNull('template_location_products');

    }
}