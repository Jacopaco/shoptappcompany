<?php

namespace App\Providers;

use App\Helpers\CalcLineItemPrice;
use Illuminate\Support\ServiceProvider;
use Statamic\Statamic;

use App\Http\Resources\CpUserResource;
use Statamic\Http\Resources\API\Resource;
use Statamic\Http\Resources\API\UserResource;

use DuncanMcClean\SimpleCommerce\SimpleCommerce;
use DuncanMcClean\SimpleCommerce\Contracts\Order;
use DuncanMcClean\SimpleCommerce\Contracts\Product;
use DuncanMcClean\SimpleCommerce\Fieldtypes\MoneyFieldtype;

use Stillat\Relationships\Support\Facades\Relate;

use Statamic\Facades\Utility;

use App\Helpers\CalcPrice;
 
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

        Resource::map([
            UserResource::class => CpUserResource::class,
        ]);

        Relate::oneToMany(
            'locations.company',
            'companies.locations'
        );

        Relate::manyToMany(            
            'products.users',
            'user:quicklist',
        );
        
        Relate::ManyToMany(
            'locations.location_products',
            'products.locations'
        );

                
        Relate::ManyToMany(
            'companies.client_prices.product',
            'products.client_prices'
        );

         Statamic::vite('app', [
             'resources/js/cp.js',
             'resources/js/htmx.js',
             'resources/css/cp.css',
         ]);

        Utility::extend(function () {

            Utility::register('products-import')
            ->title('Product Import')
            ->description('Upload en verwerk excel product-feed')
            ->icon('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" height="48" width="48" stroke-width="1"><g><path d="M3.5,13.5h-2a1,1,0,0,1-1-1v-8h13v8a1,1,0,0,1-1,1h-2" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path><polyline points="4.5 10 7 7.5 9.5 10" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></polyline><line x1="7" y1="7.5" x2="7" y2="13.5" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></line><path d="M11.29,1A1,1,0,0,0,10.45.5H3.55A1,1,0,0,0,2.71,1L.5,4.5h13Z" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path><line x1="7" y1="0.5" x2="7" y2="4.5" fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></line></g></svg>')
            ->view('products-import');

        });
        

        SimpleCommerce::productPriceHook(function (Order $order, Product $product) {

            $money = new MoneyFieldtype;

            $lineItemPrice = (new CalcLineItemPrice)($product->id);

            return $lineItemPrice;

        });

    }

}