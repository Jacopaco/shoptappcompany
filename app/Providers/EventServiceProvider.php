<?php

namespace App\Providers;

use App\Listeners\SetClientPriceRelations;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Listeners\SetProperties;
use App\Listeners\SetUserLocation;
use App\Listeners\SetOrderCustomer;
use App\Listeners\SetPrices;

use Statamic\Events\EntryBlueprintFound;
use Statamic\Events\UserBlueprintFound;

use DuncanMcClean\SimpleCommerce\Events\OrderSaved;

use Statamic\Events\EntrySaved;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        EntryBlueprintFound::class => [
            SetProperties::class,
        ],
        EntrySaved::class => [
            SetPrices::class,
            SetClientPriceRelations::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
