<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DuncanMcClean\SimpleCommerce\Fieldtypes\MoneyFieldtype;
use Statamic\Facades\Entry;

class FixCLientPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix-client-prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $money = new MoneyFieldtype;

       $companies = Entry::whereCollection('companies');

       foreach( $companies as $company ){

            $clientPrices = $company->client_prices;

            $fixed = [];

            foreach( $clientPrices as $clientPrice ){

                
                $id = $clientPrice->raw('product');

                if(isset($id)){

                    $price = $clientPrice->raw('client_price');

                    $fixedPrice = $money->process($price);

                    $fixed[] = [ 'product' => $id , $fixedPrice ];

                    $this->info($price.'--'.$fixedPrice);


                }

            }

        }

    }

}