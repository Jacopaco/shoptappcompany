<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Helpers\AddCLientPriceRelation;

use Statamic\Facades\Entry;

class FillClientPriceRelations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill-client-price-relations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
       $companies = Entry::whereCollection('companies');

       foreach( $companies as $company ){

            $clientPrices = $company->client_prices;

            foreach( $clientPrices as $clientPrice ){

                
                $id = $clientPrice->raw('product');

                if(isset($id)){

                    $relation = (new AddCLientPriceRelation)($company->id, $id);              

                }else{

                    $this->info('no id >> '. $company->id);

                }

            }

        }

    }

}