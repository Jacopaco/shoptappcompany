<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Statamic\Facades\Entry;

class ConvertLocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        
        $locations = Entry::query()
            ->where('collection', 'locations')
            ->whereNotNull('company')
            ->get();

        foreach( $locations as $location ){

            if( isset( $location->company->title ) ){
                if( $location->company_adress ){

                    $address = $location->company->billing_adress ?? null;
                    $city = $location->company->billing_city ?? null;
                    $postal_code = $location->company->zip ?? null;
    
                }else{
    
                    $address = $location->adress ?? null;
                    $city = $location->city ?? null;
                    $postal_code = $location->zip ?? null;
    
                }
    
                $addressArray = explode(' ', $address);
    
                foreach( $addressArray as $key => $partial ){
                    if (preg_match('~[0-9]+~', $partial)) {
                        $number = $partial;
                        unset($addressArray[$key]);
                    }else{
                        $number = null;
                    } 
                }
    
                $street = implode(' ', $addressArray);
            
                $location->set('shipping_name', $location->title);
                $location->set('shipping_street', $street);
                $location->set('shipping_number', $number);
                $location->set('shipping_city', $city);
                $location->set('shipping_postal_code', $postal_code);
    
                $this->info( 
                    print_r(
                        [
                            'location' => $location->title,
                            'company' => $location->company->title,
                            'street' => $location->shipping_street,
                            'number' => $location->shipping_number,
                            'city' => $location->shipping_city,
                            'postal_code' => $location->shipping_postal_code,
                        ]
                    )
                );

                $location->saveQuietly();

            }
        }
    }
}
