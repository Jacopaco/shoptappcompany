<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Statamic\Facades\Entry;
use Statamic\Facades\User;
use Illuminate\Support\Str;

//Convert all product id's to art_nr and update links.

class ConvertProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $count = 0;

        $products = Entry::whereCollection('products')->whereIn('status', ['published', 'draft']);
        $idLookup = [];

        $companies = Entry::whereCollection('companies');
        $locations = Entry::whereCollection('locations');
        $orders = Entry::whereCollection('orders');
        $users = User::all();

        
        foreach( $products as $product ){

            if( isset( $product->art_nr ) ){

                $oldId = $product->id;
                $newId =  Str::slug($product->art_nr);
                
                $product->set('id', $newId);
    
                $product->saveQuietly();
    
                $idLookup[$oldId] = $newId;
    
                $count++;

            }

        }

        foreach( $companies as $company ){

            $clientPrices = $company->get('client_prices');

            if( $clientPrices  !== null ){

              // the flat array values need to be replaced by the values of corresponding keys in other array.

              foreach( $clientPrices as $key => $clientPrice){

                if( isset($clientPrice['product']) && key_exists($clientPrice['product'] , $idLookup) ){

                    $clientPrices[$key]['product'] = $idLookup[$clientPrice['product']];

                }

              }

              $company->set('client_prices' , $clientPrices );
              $company->saveQuietly();
            

            }

        }

        $this->info('Related companies updated');

        foreach( $locations as $location ){

            $locationProducts = $location->get('location_products');

            if( $locationProducts  !== null ){

              // the flat array values need to be replaced by the values of corresponding keys in other array.

              foreach( $locationProducts as $key => $locationProduct ){

                if( key_exists($locationProduct , $idLookup) ){
                    $locationProducts[$key] = $idLookup[$locationProduct];
                }

              }

              $location->set('location_products' , array_flatten($locationProducts) );
              $location->saveQuietly();   

            }

        }

        $this->info('Related locations updated');

        foreach( $orders as $order ){

          $this->info('updating order: '.$order->id);

            $lineItems = $order->get('items');

            if( $lineItems !== null ){

              foreach( $lineItems as $key => $lineItem){

                if( key_exists($lineItem['product'] , $idLookup) ){

                    $lineItems[$key]['product'] = $idLookup[$lineItem['product']];

                }

              }

              $order->set('items' , $lineItems );
              $order->saveQuietly();
  
              $this->info('done');

            }

        }

        $this->info('Related orders updated');

        
        foreach( $users as $user ){

            $userProducts = $user->get('quicklist');

            if( $userProducts  !== null ){

              foreach( $userProducts as $key => $userProduct){

                if( key_exists($userProduct , $idLookup) ){

                    $userProducts[$key] = $idLookup[$userProduct];

                }

                $user->set('quicklist' , array_flatten($userProducts));
                $user->saveQuietly();

              }

            }

        }

        $this->info('Related users updated');

        return Command::SUCCESS;

    }
}