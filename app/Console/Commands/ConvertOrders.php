<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;

use Illuminate\Support\Carbon;

use Statamic\Facades\Entry;
use Statamic\Facades\User;

use DuncanMcClean\SimpleCommerce\Facades\Order;
use DuncanMcClean\SimpleCommerce\Fieldtypes\MoneyFieldtype;
use DuncanMcClean\SimpleCommerce\Orders\OrderStatus;
use DuncanMcClean\SimpleCommerce\Orders\PaymentStatus;


class ConvertOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        $money = new MoneyFieldtype;
        
        $oldOrders = Entry::query()
            ->where('collection', 'old_orders')
            ->orderBy('order_date','asc')
            ->get();

        foreach( $oldOrders as $oldOrder ){

            $order = Order::make();

            $oldOrderProducts = $oldOrder->get('products');

            $itemsTotal = 0;
            $taxTotal = 0;

            foreach( $oldOrderProducts as $product ){
            
                $findProduct = Entry::find($product['product']);

                if( isset( $findProduct )){
                    $productId = $findProduct->id;
                    
                }else{

                    $findProduct = Entry::query()
                                    ->where('collection','products')
                                    ->where('art_nr', $product['product_number'])
                                    ->orWhere('title', $product['product_title'])
                                    ->whereIn('status' , ['published','draft'])
                                    ->first();
                    
                    $productId = $findProduct->id ?? null;
                }

                if($productId != null){

                    $total = $product['amount'] * $product['price'];

                    $itemsTotal += $total;
    
                    $taxTotal += $total * 0.21;

                    $total = $money->process(number_format($total, 2 ));

                    $order->withoutRecalculating(function () use (&$order , $product, $productId, $total) {
    
                        $add = $order->addLineItem([
                            'product' => $productId,
                            'quantity' => $product['amount'],
                            'total' => $total
                        ]);
    
                    });

                }else{
                    $this->info('Product not found: '.$oldOrder['company']['title']);
                }

        }

        $grandTotal = $itemsTotal + $taxTotal;

        $itemsTotal = $money->preProcess(number_format($itemsTotal, 2));

        $taxTotal = $money->preProcess(number_format($taxTotal, 2));
        $grandTotal = $money->preProcess(number_format($grandTotal, 2));
       
        $order->itemsTotal($itemsTotal);
        $order->taxTotal($taxTotal);
        $order->grandTotal($grandTotal);

        $user = User::query()->where('name', $oldOrder->user)->first();

        if( isset($user) ){

            $order->customer($user->id);

            $order->set('org_id', $user->location->company->kvk ?? null);
        
            $order->set('use_shipping_for_billing' , true);

            $order->set('shipping_name', $user->location->shipping_name);
            $order->set('shipping_street', $user->location->shipping_street);
            $order->set('shipping_city', $user->location->shipping_city);
            $order->set('shipping_postal_code', $user->location->shipping_postal_code);

        }else{
            $this->info('User not found: '.$oldOrder->user);
        }

        $order->updateOrderStatus(OrderStatus::Placed);

        $order->set('status_log', [
            [
                'status' => 'placed',
                'timestamp' => Carbon::parse($oldOrder['order_date'])->timestamp,
                'data' => [] 
            ]

        ]);

        $order->updatePaymentStatus(PaymentStatus::Paid);

        $order->save();

        $this->info($oldOrder['order_date']);

    }

}

}