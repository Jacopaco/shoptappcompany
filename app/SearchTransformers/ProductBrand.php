<?php
namespace App\SearchTransformers;

use Statamic\Facades\Entry;
 
class ProductBrand
{
    public function handle($value, $field, $searchable)
    {
        if( isset($value) ){

            $brand = Entry::find($value);

            if(isset($brand)){

                return $brand['title'];

            }
        
        }
    }
}