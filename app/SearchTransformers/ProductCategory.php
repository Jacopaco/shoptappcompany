<?php
namespace App\SearchTransformers;

use Statamic\Facades\Entry;
 
class ProductCategory
{
    public function handle($value, $field, $searchable)
    {
        if( isset($value) ){

            $category = Entry::find($value);

            if(isset($category)){

                return $category['title'];

            }
        
        }
    }
}