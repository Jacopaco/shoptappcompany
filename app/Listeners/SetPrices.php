<?php

namespace App\Listeners;
use Statamic\Events\EntrySaved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Statamic\Facades\Entry;

use App\Helpers\CalcPrice;

class SetPrices
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Statamic\Events\EntrySaved  $event
     * @return void
     */
    public function handle(EntrySaved $event)
    {

        if ($event->entry->collection->handle === 'products') {

            $product = $event->entry;       
            $this->setPrices($product);

        }

        if ($event->entry->collection->handle === 'brands') {

            $brandId = $event->entry->id;

            $products = Entry::query()
                        ->where('collection', 'products')
                        ->where('brands', $brandId)
                        ->get();

            foreach($products as $product){

                $this->setPrices($product);

            }

        }
       
    }

    private function setPrices($product){

        $calcPrices = CalcPrice::calc($product->brands->id, $product->cost_price, $product->unit_price);

        $product->set('price', $calcPrices['price']);
        $product->set('vip_price', $calcPrices['vip_price']);

        $product->saveQuietly();

    }

}