<?php

namespace App\Listeners;
use Statamic\Events\EntrySaved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Statamic\Facades\Entry;

use App\Helpers\CalcPrice;

use Illuminate\Support\Facades\Artisan;

use App\Helpers\AddCLientPriceRelation;

class SetClientPriceRelations
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Statamic\Events\EntrySaved  $event
     * @return void
     */
    public function handle(EntrySaved $event)
    {

        if ($event->entry->collection->handle === 'companies') {

            $company = $event->entry;

            $clientPrices =  $company->client_prices;

            if( sizeof($clientPrices) > 0 ){

                foreach( $clientPrices as $clientPrice ){

                    $id = $clientPrice->raw('product');

                    if(isset($id)){
    
                        $relation = (new AddCLientPriceRelation)($company->id, $id);              
    
                    }

                }


            }

        }

    }

}