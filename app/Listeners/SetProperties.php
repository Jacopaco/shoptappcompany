<?php

namespace App\Listeners;
use Statamic\Events\EntryBlueprintFound;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Statamic\Support\Str;

class SetProperties
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\Statamic\Events\EntryBlueprintFound  $event
     * @return void
     */
    public function handle(EntryBlueprintFound $event)
    {



        if ( Str::contains(request()->url(), '/users' ) || Str::contains(request()->url(), '/blueprints/' ) || !Str::contains(request()->url(), '/cp' )) {
            return;
        }

        $handle = $event->blueprint->namespace();
    
        if ($handle === 'collections.products' && isset($event->entry) && isset($event->entry->categories) ) {

            $bp = $event->blueprint;

            $contents = $bp->contents();

            $category = $event->entry->categories;

            $categoryHandle = $category->slug;
    
            $props = $category->properties;

            $filterFields = [];

            foreach( $props as $prop ){
                
                $options = [];
                
                foreach( $prop->options as $option ){

                    $options[Str::slug($option)] = $option;

                } 

                $filterFields[] = [
                    "handle" => $categoryHandle.'_'.Str::slug($prop->property),
                    "field" => ["display" => $prop->property, "type" => "select", 'default' => '-',
                            'options' => $options
                        ]
                    ];

            }

            $contents['tabs']['props'] = [
                'display' => 'Eigenschappen',
                'sections' => [
                    [
                        "fields" =>  $filterFields
                        
                    ]
                ]
            ];
    
            $bp->setContents($contents);

        }
       
    }

}