<?php

namespace App\Helpers;

use Statamic\Facades\Entry;


class AddCLientPriceRelation
{
    /**
     * @return bool
     */

    public function __invoke($companyId, $productId){

        $product = Entry::query()   
                        ->where('collection', 'products')
                        ->whereIn('status', ['draft', 'unpublished' ,'published'])
                        ->where('id', $productId)
                        ->first();
    
        if( $product ){

            $productClientPrices = $product->client_prices->pluck('id')->toArray();

            $key = array_search($companyId, $productClientPrices, true );

            if( $key == false ){

                $productClientPrices[] = $companyId;
                $product->set('client_prices', $productClientPrices);

               return($product->saveQuietly());

            }

        }
        else{

            return false;

        }

    }

}