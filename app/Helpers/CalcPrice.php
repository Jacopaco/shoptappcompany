<?php

namespace App\Helpers;

use Statamic\Facades\Entry;

use DuncanMcClean\SimpleCommerce\Fieldtypes\MoneyFieldtype;

class CalcPrice
{
    /**
     * @return string|array
     */

    public static function calc($brand, $cost_price, $unit_price){

        $money = new MoneyFieldtype;

        $price = 0;
        $vip_price = 0;

        $basePrices = [
            'cost_price' => $cost_price,
            'unit_price' => $unit_price
        ];
        
        // Get price adaptions for product as defined on brand entry
        $brand = Entry::find($brand);

        if ($brand && $brand->alt_price )
        {

            $basePrice = $basePrices[strval($brand->alt_price_source)];

            if( isset($basePrice) && $basePrice > 0 )
            {

                if( $brand->alt_price_type == 'percentage')
                {

                    $vip_price = ( $basePrice * ($brand->alt_price_percentage / 100) );

                }
                elseif( $brand->alt_price_type == 'solid')
                {

                    $vip_price = ( $basePrice + $brand->alt_price_solid );

                }

                if($brand->alt_price_source == 'cost_price'){

                    $price = ( $vip_price * 1.2 );

                }else{

                    $price = $unit_price;

                }

        }

        }
        else
        {

            $price = $unit_price ?? 0;
            $vip_price = $unit_price ?? 0;

        }

        return [

            'price' => intVal($price),
            'vip_price' => intVal($vip_price),
            
        ];
        
    }

}