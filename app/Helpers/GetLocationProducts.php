<?php

namespace App\Helpers;

use Statamic\Facades\Entry;

class GetLocationProducts
{
    /**
     * @return string|array
     */

    public static function get($locationId){


        $location = Entry::find($locationId);

        if( isset($location->template_location_products) ){

            $location = $location->template_location_products;

        }

        return  $location->location_products->pluck('id')->toArray();    

    }

}