<?php

namespace App\Helpers;

use DuncanMcClean\SimpleCommerce\Facades\Product;

use Illuminate\Support\Facades\Auth;
use Statamic\Facades\User;

class CalcLineItemPrice
{
    /**
     * @return string|array
     */

    public function __invoke($productId){

        $product = Product::find($productId);

        if (Auth::check()) {
     
            $user = User::current();
       
            $clientPrices = $user->location->company->client_prices;
       
            if( count($clientPrices) > 0 ){
            
                foreach ($clientPrices as $clientPrice) {

       
                    if( isset($clientPrice->product->id) && $clientPrice->product->id === $product->id){
       
                        $client_price =  $clientPrice->raw('client_price');

                        return $client_price;
       
                    }
       
                }
       
                return $product->data['fixed_price'] ?? $product->data['vip_price'] ?? $product->price();
       
            }
            else{
             
                return $product->data['fixed_price'] ?? $product->data['vip_price'] ?? $product->price();
       
            }   
            
       
        }
        else{
       
            return $product->price();
       
        }

    }       

}