import algoliasearch from 'algoliasearch/lite';

import { autocomplete, getAlgoliaResults } from '@algolia/autocomplete-js';

import { createRedirectUrlPlugin } from '@algolia/autocomplete-plugin-redirect-url';

import '@algolia/autocomplete-theme-classic';

import Alpine from 'alpinejs';
import morph from '@alpinejs/morph';
 
window.Alpine = Alpine;
Alpine.plugin(morph);
Alpine.start();

window.htmx = htmx;

const searchClient = algoliasearch(
  'QTISB8HE7U',
  '661e383d83ac3940bb35fd3bf9ad41bd'
);

const redirectUrlPlugin = createRedirectUrlPlugin();

document.addEventListener("DOMContentLoaded", function() {

const search = autocomplete({
  container: '#autocomplete',
  placeholder: 'Zoek in producten',
  plugins: [redirectUrlPlugin],
  openOnFocus: true,
  getSources({ query, setContext }) {
    return [
      {
        sourceId: 'products',
        getItems() {
          return getAlgoliaResults({
            searchClient,
            queries: [
              {
                indexName: 'products-v2',
                query,
                params: {
                  hitsPerPage: 1200,
                  attributesToSnippet: ['description:12'],
                  ruleContexts: ['enable-redirect-url'], // triggers rules configured only with this context
                  snippetEllipsisText: '…',
                  filters: 'published:true',
                },
              },
            ],
            //transformResponse({ hits, results }) {
              //setContext({ results: results[0] });
              //return hits;
            //},
          });
        },
        getItemUrl({ item }) {
          return item.url;
        },
        templates: {
          item({ item, components, html }) {
            return html`<a class="block group py-2" href="${item.url}">
              <div class="flex flex-row items-center">
                <div class="relative w-12 h-12 p-0.5 mr-2 grid place-items-center">
                  <img
                    src="https://tappcompany.nautilus.nl-ams1.upcloudobjects.com/${item.prod_img}"
                    alt="${item.name}"
                    class="max-w-full max-h-full"
                  />
                </div>
                <div class="max-w-full ">
                  <div class="text-sm font-medium ">
                    ${components.Highlight({
                      hit: item,
                      attribute: 'title',
                    })}
                  </div>
                  <div class="text-xs ">
                    ${components.Snippet({
                      hit: item,
                      attribute: 'description',
                    })}
                  </div>
                </div>
              </div>
            </a>`;
          },
        },
      },
    ];
  },
  onSubmit({ state }) {

      const q = state.query;
    
      window.location.href = '/producten/all/filter/all?q=' + q ;
    
  },
});

document.getElementById('toggleSearch').addEventListener("click", function() {
  search.isOpen ? search.setIsOpen(false) : search.setIsOpen(true);
});

});