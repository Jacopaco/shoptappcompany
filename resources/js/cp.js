/**
 * When extending the control panel, be sure to uncomment the necessary code for your build process:
 * https://statamic.dev/extending/control-panel
 */

import Fieldtype from './components/fieldtypes/ManageUsers.vue';

import 'htmx.org';

Statamic.booting(() => {
    Statamic.$components.register('manage_users-fieldtype', Fieldtype);
});

if(window.location.href.indexOf("/collections/companies/entries/") > -1 ) {
        /**
     * Utility function to add CSS in multiple passes.
     * @param {string} styleString
     */
    function addStyle(styleString) {
        const style = document.createElement('style');
        style.textContent = styleString;
        document.head.append(style);
    }

    addStyle(`
    .stack-container {
        position:absolute !important;
        inset:64px 128px !important;
    }
    .stack-overlay{
        background:none !important;
    }
    .stack-container:has(.data-table){
        inset:128px 256px !important;
        background:white;
    }

    .stack-container .stack-content{
        padding-top:48px;
    }

    .stack-container  .publish-sidebar,
    .stack-container  .breadcrumb
    {
        display:none !important;
    }

    .stack-container  .btn-primary
    {
    color: transparent !important;
    }

    .stack-container .breadcrumb + div
    {
        background:white !important;
        padding:12px 24px 12px 24px;
        z-index: 50;
        position:absolute !important;
        top:0;
        left:0;
        right:0;
    }

    .stack-container  .btn-primary:after
    {
    position:absolute;
    color: white !important;
    content: "Toepassen" ;

    }
    `);


    Statamic.$hooks.on('entry.saved', (resolve,reject,payload) => {

        resolve();

        if(payload.collection === 'locations'){
           
            setTimeout(() => {
                document.querySelectorAll('.btn-close')[0].click();
            }, "5");

            setTimeout(() => {
                document.querySelectorAll('.btn-primary')[0].click();
            }, "10");
            
        }
        
    });

}