<div>
Import geslaagd. er zijn {{ $new_count }} nieuwe producten toegevoegd.
<br>
Bekijk via onderstaande link de import en controleer de wijzigingen.
<br>
</div>

<a class="inline-block text-blue-400 mt-4 p-1 border-2 border-blue-400 rounded-md"  href="/cp/collections/imports/entries/{{ $import }}">Bekijk en controleer import </a>