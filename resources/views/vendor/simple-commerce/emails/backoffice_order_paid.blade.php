{{ $taxIncludedInPrices = config('simple-commerce.tax_engine_config.included_in_prices'); }}

@component('mail::message')
# {{ __('Nieuwe bestelling')  }} 

**{{ __('Ordernummer') }}:** #{{ $order->orderNumber() }}

@component('mail::table')
| Artikel       | Aant.        | Prijs | Totaal |
| :--------- | :------------- | :----- | :----- |
@foreach ($order->lineItems() as $lineItem)
| {{ $lineItem->product()->resource()->art_nr }} - [{!! str_replace('|', '-', $lineItem->product()->get('title')) !!}]({{ optional($lineItem->product()->resource())->absoluteUrl() }}) | {{ $lineItem->quantity() }} |{{ number_format(($lineItem->total() / $lineItem->quantity()) / 100, 2, ',', '.') }} | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($taxIncludedInPrices ? $lineItem->totalIncludingTax() : $lineItem->total(), $site) }} |
@endforeach
| | | Subtotaal: | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($taxIncludedInPrices ? $order->itemsTotalWithTax() : $order->itemsTotal(), $site) }}
@if($order->coupon())
| | | {{ __('Coupon') }}: | -{{ \DuncanMcClean\SimpleCommerce\Currency::parse($order->couponTotal(), $site) }}
@endif
| | | Verzending: | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($taxIncludedInPrices ? $order->shippingTotalWithTax() : $order->shippingTotal(), $site) }}
@if(!$taxIncludedInPrices)
| | | BTW(21%): | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($order->taxTotal(), $site) }}
@endif
| | | **Totaal:** | **{{ \DuncanMcClean\SimpleCommerce\Currency::parse($order->grandTotal(), $site) }}**
| | | |
@endcomponent

## {{ __('Gegevens') }}

@if($order->customer())
* **{{ __('Naam') }}:** {{ $order->customer()->name() }}
* **{{ __('Email') }}:** {{ $order->customer()->email() }}
@endif

@if($order->billingAddress())
* **{{ __('Factuuradres') }}:** {{ $order->billingAddress()->__toString() }}
@endif

@if($order->shippingAddress())
* **{{ __('Verzendadres') }}:** {{ $order->shippingAddress()->__toString() }}
@endif

{{ config('app.name') }}
@endcomponent