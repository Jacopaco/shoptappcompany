{{ $taxIncludedInPrices = config('simple-commerce.tax_engine_config.included_in_prices') }}

@component('mail::message')
# {{ __('Je bestelling bij Tapp Company') }}

{{ __('We hebben je bestelling (**#:orderNumber**) ontvangen. Wij gaan er mee aan de slag. Hieronder zie je een overzicht van jouw bestelling:', [
    'orderNumber' => $order->orderNumber(),
]) }}

@component('mail::table')
| {{ __('Product') }}       | {{ __('Aant.') }}         | {{ __('Totaal') }} |
| :--------- | :------------- | :----- |
@foreach ($order->lineItems() as $lineItem)
| [{!! str_replace('|', '-', $lineItem->product()->get('title')) !!}]({{ optional($lineItem->product()->resource())->absoluteUrl() }}) | {{ $lineItem->quantity() }} | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($taxIncludedInPrices ? $lineItem->totalIncludingTax() : $lineItem->total(), $site) }} |
@endforeach
| | {{ __('Subtotaal') }}: | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($taxIncludedInPrices ? $order->itemsTotalWithTax() : $order->itemsTotal(), $site) }}
@if($order->coupon())
| | {{ __('Coupon') }}: | -{{ \DuncanMcClean\SimpleCommerce\Currency::parse($order->couponTotal(), $site) }}
@endif
| | {{ __('Verzending') }}: | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($taxIncludedInPrices ? $order->shippingTotalWithTax() : $order->shippingTotal(), $site) }}
@if(!$taxIncludedInPrices)
| | {{ __('BTW (21%)') }}: | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($order->taxTotal(), $site) }}
@endif
| | **{{ __('Totaal') }}:** | {{ \DuncanMcClean\SimpleCommerce\Currency::parse($order->grandTotal(), $site) }}
| | |
@endcomponent

## {{ __('Jouw gegevens') }}

@if($order->customer())
* **{{ __('Naam') }}:** {{ $order->customer()->name() }}
* **{{ __('Email') }}:** {{ $order->customer()->email() }}
@endif

@if($order->shippingAddress())
* **{{ __('Verzendadres') }}:** {{ $order->shippingAddress()->__toString() }}
@endif

@if($order->billingAddress())
* **{{ __('Factuuradres') }}:** {{ $order->billingAddress()->__toString() }}
@endif


<br>

{{ __('Neem bij vragen contact met ons op!') }}

{{ __('Met vriendelijke groet') }},<br>
{{ config('app.name') }}
@endcomponent