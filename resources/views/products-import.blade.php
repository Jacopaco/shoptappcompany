@extends('statamic::layout')
@section('title', __('Product feed uploaden'))
 
@section('content')

    <div class="flex items-center justify-between">
        <h1>{{ __('Product feed uploaden') }}</h1>
    </div>
    <script src="/vendor/app/js/htmx.js" ></script>
    <div class="mt-3 card">
        <form hx-encoding='multipart/form-data'  hx-post="/products-import"  hx-indicator="#indicator">
            @csrf <!-- {{ csrf_field() }} -->
            <label for="product-feed">Producten</label><input id="product-feed" type="file" name="product-feed" />
            <label for="description-feed">Omschrijvingen</label><input id="description-feed"  type="file" name="description-feed" />
            <div class="flex flex-row items-center p-2" >
                <button type="submit" class="btn-primary w-48" id="submit" >
                    Verwerken
                </button>
                <div class="htmx-indicator" id="indicator" style="margin-left:12px">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 40 40" stroke="#737f8c"><g fill="none" fill-rule="evenodd"><g transform="translate(2 2)" stroke-width="4"><circle stroke-opacity=".5" cx="18" cy="18" r="18"></circle><path d="M36 18c0-9.94-8.06-18-18-18"><animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"></animateTransform></path></g></g></svg>
                </div>
            </div>
        </form>
    </div>
@stop